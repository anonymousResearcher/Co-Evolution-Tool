/**
 */
package org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Business Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.BusinessOperation#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage#getBusinessOperation()
 * @model
 * @generated
 */
public interface BusinessOperation extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage#getBusinessOperation_Name()
	 * @model dataType="org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.BusinessOperation#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // BusinessOperation
