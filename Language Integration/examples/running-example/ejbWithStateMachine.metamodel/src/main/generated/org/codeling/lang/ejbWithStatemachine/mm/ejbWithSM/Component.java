/**
 */
package org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.Component#getName <em>Name</em>}</li>
 *   <li>{@link org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.Component#getOperations <em>Operations</em>}</li>
 *   <li>{@link org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.Component#getReferences <em>References</em>}</li>
 *   <li>{@link org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.Component#getStatemachine <em>Statemachine</em>}</li>
 * </ul>
 *
 * @see org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage#getComponent()
 * @model
 * @generated
 */
public interface Component extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage#getComponent_Name()
	 * @model dataType="org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.Component#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Operations</b></em>' containment reference list.
	 * The list contents are of type {@link org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.BusinessOperation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations</em>' containment reference list.
	 * @see org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage#getComponent_Operations()
	 * @model containment="true"
	 * @generated
	 */
	EList<BusinessOperation> getOperations();

	/**
	 * Returns the value of the '<em><b>References</b></em>' reference list.
	 * The list contents are of type {@link org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.Component}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>References</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>References</em>' reference list.
	 * @see org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage#getComponent_References()
	 * @model
	 * @generated
	 */
	EList<Component> getReferences();

	/**
	 * Returns the value of the '<em><b>Statemachine</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Statemachine</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Statemachine</em>' containment reference.
	 * @see #setStatemachine(StateMachine)
	 * @see org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage#getComponent_Statemachine()
	 * @model containment="true"
	 * @generated
	 */
	StateMachine getStatemachine();

	/**
	 * Sets the value of the '{@link org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.Component#getStatemachine <em>Statemachine</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Statemachine</em>' containment reference.
	 * @see #getStatemachine()
	 * @generated
	 */
	void setStatemachine(StateMachine value);

} // Component
