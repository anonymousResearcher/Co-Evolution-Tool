package org.codeling.lang.ejbWithStatemachine.ial.mm.componenttype_feature;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Must be used with the @EJB annotation to declare an injected bean a child
 * component.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Child {

}
