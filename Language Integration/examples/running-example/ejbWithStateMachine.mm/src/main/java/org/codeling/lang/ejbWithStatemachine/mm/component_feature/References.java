package org.codeling.lang.ejbWithStatemachine.mm.component_feature;

@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
@java.lang.annotation.Target(java.lang.annotation.ElementType.FIELD)
public @interface References {
	
	Class<? extends org.codeling.lang.ejbWithStatemachine.mm.Component>[] value();
}
