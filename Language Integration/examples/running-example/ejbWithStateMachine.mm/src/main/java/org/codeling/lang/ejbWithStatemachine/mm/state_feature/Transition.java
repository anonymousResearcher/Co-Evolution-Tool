package org.codeling.lang.ejbWithStatemachine.mm.state_feature;

@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
@java.lang.annotation.Target(java.lang.annotation.ElementType.METHOD)
public @interface Transition {

	Class<? extends org.codeling.lang.ejbWithStatemachine.mm.State> target();
	
}
