package org.codeling.lang.ejbWithStatemachine.mm;

@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
@java.lang.annotation.Target(java.lang.annotation.ElementType.TYPE)
public @interface StateMachine {
	
}
