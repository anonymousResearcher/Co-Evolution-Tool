package org.codeling.example.ejbsm;

import org.codeling.lang.ejbWithStatemachine.mm.State;
import org.codeling.lang.ejbWithStatemachine.mm.state_feature.Transition;
import org.codeling.mechanism.runtime.common.Logging;
import org.codeling.mechanism.runtime.common.Logging.LogLevel;

public class WithinSale implements State {

	@Transition(target = WithinSale.class)
	public void scanCode() {
		Logging.log(LogLevel.MODEL, "Executing transition 'scanCode' from state 'WithinSale'.");
	}

	@Transition(target = AwaitingPayment.class)
	public void finishSale() {
		Logging.log(LogLevel.MODEL, "Executing transition 'finishSale' from state 'WithinSale'.");
	}
}
