package org.codeling.example.ejbsm;

import org.codeling.lang.ejbWithStatemachine.mm.State;
import org.codeling.lang.ejbWithStatemachine.mm.state_feature.Transition;
import org.codeling.mechanism.runtime.common.Logging;
import org.codeling.mechanism.runtime.common.Logging.LogLevel;

public class AwaitingPayment implements State {

	@Transition(target = Ready.class)
	public void receivedPayment() {
		Logging.log(LogLevel.MODEL, "Executing transition 'receivedPayment' from state 'AwaitingPayment'.");
	}
}
