package org.codeling.example.ejbsm.runtime;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.Arrays;

import org.codeling.example.ejbsm.BarcodeScanner;
import org.codeling.example.ejbsm.CashDesk;
import org.codeling.example.ejbsm.CashDeskStateMachine;
import org.codeling.lang.ejbWithStatemachine.runtime.StateMachineRuntime;
import org.codeling.mechanism.runtime.common.IntegratedModelException;
import org.junit.Test;

public class EJBWithStateMachineTests {
	@Test
	public void testSuccess() throws NoSuchFieldException, SecurityException, IllegalArgumentException,
			IllegalAccessException, IntegratedModelException {
		// Initialize what the EJB runtime would initialize
		CashDesk cd = new CashDesk();
		BarcodeScanner bcs = new BarcodeScanner();
		Field f = cd.getClass().getDeclaredField("barcodeScanner");
		f.setAccessible(true);
		f.set(cd, bcs);
		f.setAccessible(false);

		// Call the @PostConstruct method
		cd.init();

		Field smrField = cd.getClass().getDeclaredField("smr");
		smrField.setAccessible(true);

		@SuppressWarnings("unchecked")
		StateMachineRuntime<CashDeskStateMachine> smr = (StateMachineRuntime<CashDeskStateMachine>) smrField.get(cd);
		assertTrue(smr.getPossibleTransitions().contains("scanCode"));

		cd.addItemToCart();
		assertTrue(smr.getPossibleTransitions().containsAll(Arrays.asList("scanCode", "finishSale")));

		cd.addItemToCart();
		assertTrue(smr.getPossibleTransitions().containsAll(Arrays.asList("scanCode", "finishSale")));

		cd.addItemToCart();
		assertTrue(smr.getPossibleTransitions().containsAll(Arrays.asList("scanCode", "finishSale")));

		cd.checkout();
		assertTrue(smr.getPossibleTransitions().contains("scanCode"));

		// Try another sale
		cd.addItemToCart();
		assertTrue(smr.getPossibleTransitions().containsAll(Arrays.asList("scanCode", "finishSale")));

		cd.checkout();
		assertTrue(smr.getPossibleTransitions().contains("scanCode"));
	}

	@Test(expected = IllegalStateException.class)
	public void testDoubleCheckout() throws NoSuchFieldException, SecurityException, IllegalArgumentException,
			IllegalAccessException, IntegratedModelException {
		// Initialize what the EJB runtime would initialize
		CashDesk cd = new CashDesk();
		BarcodeScanner bcs = new BarcodeScanner();
		Field f = cd.getClass().getDeclaredField("barcodeScanner");
		f.setAccessible(true);
		f.set(cd, bcs);
		f.setAccessible(false);

		// Call the @PostConstruct method
		cd.init();
		cd.checkout();

		// Should not work. Exception expected here.
		cd.checkout();
	}
}
