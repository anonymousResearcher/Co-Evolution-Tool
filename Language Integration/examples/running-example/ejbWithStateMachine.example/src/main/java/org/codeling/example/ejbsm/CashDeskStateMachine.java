package org.codeling.example.ejbsm;

import org.codeling.lang.ejbWithStatemachine.mm.State;
import org.codeling.lang.ejbWithStatemachine.mm.StateMachine;
import org.codeling.lang.ejbWithStatemachine.mm.statemachine_feature.Initial;
import org.codeling.lang.ejbWithStatemachine.mm.statemachine_feature.States;

@StateMachine
public class CashDeskStateMachine {

	@Initial(Ready.class)
	State initial;

	@States({ Ready.class, WithinSale.class })
	State[] states;

}
