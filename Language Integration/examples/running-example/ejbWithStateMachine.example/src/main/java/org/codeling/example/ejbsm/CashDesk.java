package org.codeling.example.ejbsm;

import java.util.LinkedList;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateful;

import org.codeling.lang.ejbWithStatemachine.mm.component_feature.Operations;
import org.codeling.lang.ejbWithStatemachine.mm.component_feature.Statemachine;
import org.codeling.lang.ejbWithStatemachine.runtime.StateMachineRuntime;
import org.codeling.mechanism.runtime.common.IntegratedModelException;

@Stateful
public class CashDesk {

	final LinkedList<String> items = new LinkedList<>();
	StateMachineRuntime<CashDeskStateMachine> smr;

	@Statemachine
	CashDeskStateMachine cashDeskStateMachine;

	@EJB
	BarcodeScanner barcodeScanner;

	@PostConstruct
	public void init() throws IntegratedModelException {
		smr = new StateMachineRuntime<>(CashDeskStateMachine.class);
		smr.initialize();
		smr.initializeContainments();
		smr.initializeCrossReferences();

		cashDeskStateMachine = (CashDeskStateMachine) smr.getInstance();
	}

	@Operations
	public void addItemToCart() throws IntegratedModelException {
		items.add(barcodeScanner.scanItem());
		smr.executeTransition("scanCode");
	}

	@Operations
	public void checkout() throws IntegratedModelException {
		// Execute a real sale
		items.clear();
		smr.executeTransition("finishSale");
	}

}
