package org.codeling.lang.ejbWithStatemachine.runtime.statemachine_feature;

import org.codeling.lang.ejbWithStatemachine.mm.statemachine_feature.Initial;
import org.codeling.lang.ejbWithStatemachine.runtime.StateRuntime;
import org.codeling.mechanism.runtime.AnnotatedMemberReferenceRuntime;
import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;

public class InitialRuntime extends AnnotatedMemberReferenceRuntime {

	public InitialRuntime(TypeMechanismRuntime owningRuntime) {
		super(owningRuntime, Initial.class, StateRuntime.class, false);
	}
	
}
