package org.codeling.lang.ejbWithStatemachine.runtime;

import org.codeling.lang.ejbWithStatemachine.mm.Component;
import org.codeling.mechanism.runtime.MarkerInterfaceRuntime;
import org.codeling.mechanism.runtime.common.IntegratedModelException;

public class ComponentRuntime<T extends Component> extends MarkerInterfaceRuntime<Component, T> {
	
	// Reference Runtimes
	org.codeling.lang.ejbWithStatemachine.runtime.component_feature.OperationsRuntime operationsRuntime;
	org.codeling.lang.ejbWithStatemachine.runtime.component_feature.ReferencesRuntime referencesRuntime;
	org.codeling.lang.ejbWithStatemachine.runtime.component_feature.StatemachineRuntime statemachineRuntime;
	
	public ComponentRuntime(Class<T> implementingClass) throws IntegratedModelException {
		super(implementingClass, Component.class);
	}
	
	@Override
	public void initializeContainments() throws IntegratedModelException {
		super.initializeContainments();
		
		// Reference Runtimes
		operationsRuntime = new org.codeling.lang.ejbWithStatemachine.runtime.component_feature.OperationsRuntime(this);
		operationsRuntime.initialize();
		operationsRuntime.initializeContainments();
		operationsRuntime.initializeCrossReferences();
		
		statemachineRuntime = new org.codeling.lang.ejbWithStatemachine.runtime.component_feature.StatemachineRuntime(this);
		statemachineRuntime.initialize();
		
	}
	
	@Override
	public void initializeCrossReferences() throws IntegratedModelException {
		super.initializeCrossReferences();
						
		// Reference Runtimes
		referencesRuntime = new org.codeling.lang.ejbWithStatemachine.runtime.component_feature.ReferencesRuntime(this);
		referencesRuntime.initialize();
		
	}
	
	public org.codeling.lang.ejbWithStatemachine.runtime.component_feature.OperationsRuntime getOperationsRuntime() {
		return operationsRuntime;
	}
	
	public org.codeling.lang.ejbWithStatemachine.runtime.component_feature.ReferencesRuntime getReferencesRuntime() {
		return referencesRuntime;
	}
	
	public org.codeling.lang.ejbWithStatemachine.runtime.component_feature.StatemachineRuntime getStatemachineRuntime() {
		return statemachineRuntime;
	}
	
}