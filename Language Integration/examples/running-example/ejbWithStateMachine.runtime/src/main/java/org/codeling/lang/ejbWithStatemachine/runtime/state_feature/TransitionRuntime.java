package org.codeling.lang.ejbWithStatemachine.runtime.state_feature;

import org.codeling.lang.ejbWithStatemachine.mm.state_feature.Transition;
import org.codeling.mechanism.runtime.ContainmentOperationRuntime;
import org.codeling.mechanism.runtime.common.IntegratedModelException;
import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;

public class TransitionRuntime extends ContainmentOperationRuntime {
	
	// Reference Runtimes
	org.codeling.lang.ejbWithStatemachine.runtime.transition_feature.TargetRuntime targetRuntime;
	
	public TransitionRuntime(TypeMechanismRuntime owningTypeRuntime) throws IntegratedModelException {
		super(owningTypeRuntime, Transition.class);
	}
	
	@Override
	public void initialize() throws IntegratedModelException {
		super.initialize();
	}
	
	@Override
	public void initializeContainments() throws IntegratedModelException {
		super.initializeContainments();
		
	}
	
	@Override
	public void initializeCrossReferences() throws IntegratedModelException {
		super.initializeCrossReferences();
		
		// Reference Runtimes
		targetRuntime = new org.codeling.lang.ejbWithStatemachine.runtime.transition_feature.TargetRuntime(this);
		targetRuntime.initialize();
		
	}
	
	public org.codeling.lang.ejbWithStatemachine.runtime.transition_feature.TargetRuntime getTargetRuntime() {
		return targetRuntime;
	}
	

}
