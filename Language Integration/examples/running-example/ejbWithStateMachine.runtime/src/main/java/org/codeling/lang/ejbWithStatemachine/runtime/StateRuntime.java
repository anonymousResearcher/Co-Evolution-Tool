package org.codeling.lang.ejbWithStatemachine.runtime;

import org.codeling.lang.ejbWithStatemachine.mm.State;
import org.codeling.mechanism.runtime.MarkerInterfaceRuntime;
import org.codeling.mechanism.runtime.common.IntegratedModelException;

public class StateRuntime<T extends State> extends MarkerInterfaceRuntime<State, T> {
	
	// Reference Runtimes
	org.codeling.lang.ejbWithStatemachine.runtime.state_feature.TransitionRuntime transitionRuntime;
	
	public StateRuntime(Class<T> implementingClass) throws IntegratedModelException {
		super(implementingClass, State.class);
	}
	
	@Override
	public void initializeContainments() throws IntegratedModelException {
		super.initializeContainments();
		
		// Reference Runtimes
		transitionRuntime = new org.codeling.lang.ejbWithStatemachine.runtime.state_feature.TransitionRuntime(this);
		transitionRuntime.initialize();
		transitionRuntime.initializeContainments();
		transitionRuntime.initializeCrossReferences();
		
	}
	
	@Override
	public void initializeCrossReferences() throws IntegratedModelException {
		super.initializeCrossReferences();
						
	}
	
	public org.codeling.lang.ejbWithStatemachine.runtime.state_feature.TransitionRuntime getTransitionRuntime() {
		return transitionRuntime;
	}
	
}