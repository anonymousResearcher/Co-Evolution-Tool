package org.codeling.lang.ejbWithStatemachine.runtime.transition_feature;

import org.codeling.lang.ejbWithStatemachine.runtime.StateRuntime;
import org.codeling.mechanism.runtime.ContainmentOperationReferenceRuntime;
import org.codeling.mechanism.runtime.ContainmentOperationRuntime;

public class TargetRuntime extends ContainmentOperationReferenceRuntime {

	public TargetRuntime(ContainmentOperationRuntime owningRuntime) {
		super(owningRuntime, "target", StateRuntime.class, false);
	}
	
}
