package org.codeling.lang.ejbWithStatemachine.runtime.component_feature;

import org.codeling.lang.ejbWithStatemachine.mm.component_feature.Operations;
import org.codeling.mechanism.runtime.ContainmentOperationRuntime;
import org.codeling.mechanism.runtime.common.IntegratedModelException;
import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;

public class OperationsRuntime extends ContainmentOperationRuntime {
	
	
	public OperationsRuntime(TypeMechanismRuntime owningTypeRuntime) throws IntegratedModelException {
		super(owningTypeRuntime, Operations.class);
	}
	
	@Override
	public void initialize() throws IntegratedModelException {
		super.initialize();
	}
	
	@Override
	public void initializeContainments() throws IntegratedModelException {
		super.initializeContainments();
		
	}
	
	@Override
	public void initializeCrossReferences() throws IntegratedModelException {
		super.initializeCrossReferences();
		
	}
	

}
