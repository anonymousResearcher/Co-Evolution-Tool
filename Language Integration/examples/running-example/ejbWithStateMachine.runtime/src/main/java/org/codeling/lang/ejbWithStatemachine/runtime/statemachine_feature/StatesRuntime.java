package org.codeling.lang.ejbWithStatemachine.runtime.statemachine_feature;

import org.codeling.lang.ejbWithStatemachine.mm.statemachine_feature.States;
import org.codeling.lang.ejbWithStatemachine.runtime.StateRuntime;
import org.codeling.mechanism.runtime.AnnotatedMemberReferenceRuntime;
import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;

public class StatesRuntime extends AnnotatedMemberReferenceRuntime {

	public StatesRuntime(TypeMechanismRuntime owningRuntime) {
		super(owningRuntime, States.class, StateRuntime.class, true);
	}
	
}
