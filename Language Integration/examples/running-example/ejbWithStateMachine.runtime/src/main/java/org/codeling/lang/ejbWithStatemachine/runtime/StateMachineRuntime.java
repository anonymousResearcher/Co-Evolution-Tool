package org.codeling.lang.ejbWithStatemachine.runtime;

import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

import org.codeling.lang.ejbWithStatemachine.mm.State;
import org.codeling.lang.ejbWithStatemachine.mm.StateMachine;
import org.codeling.lang.ejbWithStatemachine.runtime.state_feature.TransitionRuntime;
import org.codeling.mechanism.runtime.TypeAnnotationRuntime;
import org.codeling.mechanism.runtime.common.IntegratedModelException;
import org.codeling.mechanism.runtime.common.Logging;
import org.codeling.mechanism.runtime.common.Logging.LogLevel;

public class StateMachineRuntime<T> extends TypeAnnotationRuntime<T> {

	StateRuntime<?> currentStateRuntime;

	// Reference Runtimes
	org.codeling.lang.ejbWithStatemachine.runtime.statemachine_feature.InitialRuntime initialRuntime;
	org.codeling.lang.ejbWithStatemachine.runtime.statemachine_feature.StatesRuntime statesRuntime;

	public StateMachineRuntime(Class<T> implementingClass) throws IntegratedModelException {
		super(implementingClass, StateMachine.class);
	}

	@Override
	public void initializeContainments() throws IntegratedModelException {
		super.initializeContainments();

		// Reference Runtimes
		statesRuntime = new org.codeling.lang.ejbWithStatemachine.runtime.statemachine_feature.StatesRuntime(this);
		statesRuntime.initialize();

	}

	@Override
	public void initializeCrossReferences() throws IntegratedModelException {
		super.initializeCrossReferences();

		// Reference Runtimes
		initialRuntime = new org.codeling.lang.ejbWithStatemachine.runtime.statemachine_feature.InitialRuntime(this);
		initialRuntime.initialize();

		// Set current state (not generated)
		currentStateRuntime = (StateRuntime<?>) initialRuntime.getTargetRuntime(initialRuntime.getTargets()[0]);
		// or: Runtimes.getInstance().get(initialRuntime.getTargets()[0])
	}

	public org.codeling.lang.ejbWithStatemachine.runtime.statemachine_feature.InitialRuntime getInitialRuntime() {
		return initialRuntime;
	}

	public org.codeling.lang.ejbWithStatemachine.runtime.statemachine_feature.StatesRuntime getStatesRuntime() {
		return statesRuntime;
	}

	public StateRuntime<?> getCurrentStateRuntime() {
		return currentStateRuntime;
	}

	public void executeTransition(String transitionName) throws IntegratedModelException {
		if (!isExecutable(transitionName))
			throw new IllegalStateException(MessageFormat.format("The transition {0} is not allowed in the state {1}.",
					transitionName, currentStateRuntime.getInstance().getClass().getSimpleName()));

		Logging.log(LogLevel.MODEL_DEBUG, MessageFormat.format("Executing transition {0}.", transitionName));
		TransitionRuntime transitionRuntime = currentStateRuntime.getTransitionRuntime();
		transitionRuntime.invoke(transitionName);
		StateRuntime<?> targetStateRuntime = (StateRuntime<?>) transitionRuntime.getTargetRuntime()
				.getTargetRuntime(transitionName)[0];
		State targetState = (State) targetStateRuntime.getInstance();

		Logging.log(LogLevel.MODEL_DEBUG, MessageFormat.format("Executed transition {0}. New State is {1}.",
				transitionName, targetState.getClass().getSimpleName()));
		currentStateRuntime = targetStateRuntime;
	}

	public boolean isExecutable(String transitionName) throws IntegratedModelException {
		return getPossibleTransitions().contains(transitionName);
	}

	public List<String> getPossibleTransitions() throws IntegratedModelException {
		TransitionRuntime transitionRuntime = currentStateRuntime.getTransitionRuntime();
		List<Method> containmentOperations = transitionRuntime.getContainmentOperations();
		return containmentOperations.stream().map(Method::getName).collect(Collectors.toList());
	}

}
