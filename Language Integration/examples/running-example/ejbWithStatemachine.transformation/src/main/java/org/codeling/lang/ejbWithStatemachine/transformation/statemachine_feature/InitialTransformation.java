package org.codeling.lang.ejbWithStatemachine.transformation.statemachine_feature;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.State;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.StateMachine;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.references.AnnotatedMemberReferenceTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;

public class InitialTransformation extends AnnotatedMemberReferenceTransformation<StateMachine, State> {

	public InitialTransformation(ClassMechanismTransformation<StateMachine, IType> parentTransformation) {
		super(parentTransformation, ejbWithSMPackage.eINSTANCE.getStateMachine_Initial());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return null;
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
	}
}
