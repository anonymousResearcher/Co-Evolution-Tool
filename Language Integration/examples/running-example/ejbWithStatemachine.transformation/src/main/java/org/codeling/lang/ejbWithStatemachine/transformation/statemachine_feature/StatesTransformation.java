package org.codeling.lang.ejbWithStatemachine.transformation.statemachine_feature;

import java.util.List;
import java.util.Set;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.State;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.StateMachine;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage;
import org.codeling.lang.ejbWithStatemachine.transformation.StateTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.references.AnnotatedMemberReferenceTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;

public class StatesTransformation extends AnnotatedMemberReferenceTransformation<StateMachine, State> {

	public StatesTransformation(ClassMechanismTransformation<StateMachine, IType> parentTransformation) {
		super(parentTransformation, ejbWithSMPackage.eINSTANCE.getStateMachine_States());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return new StateTransformation(this);
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		Set<IType> targetTypes = resolveTargetTypes();
		for (IType targetType : targetTypes) {
			StateTransformation t = new StateTransformation(this);
			t.setCodeElement(targetType);
			result.add(t);
		}
	}
}
