package org.codeling.lang.ejbWithStatemachine.transformation;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.BusinessOperation;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage;
import org.codeling.lang.ejbWithStatemachine.transformation.component_feature.OperationsTransformation;
import org.codeling.mechanisms.transformations.classes.ContainmentOperationTargetTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;

public class BusinessOperationTransformation extends ContainmentOperationTargetTransformation<BusinessOperation> {

	public BusinessOperationTransformation(OperationsTransformation parentTransformation) {
		super(parentTransformation, ejbWithSMPackage.eINSTANCE.getBusinessOperation());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
	}

}
