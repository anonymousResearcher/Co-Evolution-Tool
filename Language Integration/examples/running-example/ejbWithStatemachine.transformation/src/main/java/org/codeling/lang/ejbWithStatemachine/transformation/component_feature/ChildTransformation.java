package org.codeling.lang.ejbWithStatemachine.transformation.component_feature;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.List;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.JDTUtils;
import org.codeling.lang.base.java.ProfilesUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.base.java.transformation.references.IALHolder;
import org.codeling.lang.base.java.transformation.references.IALTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.ReferenceMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.codeling.utils.Models;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.modelversioning.emfprofile.Stereotype;
import org.modelversioning.emfprofileapplication.StereotypeApplication;

import de.mkonersmann.il.core.ComponentType;
import de.mkonersmann.il.profiles.Profiles;

public class ChildTransformation extends ReferenceMechanismTransformation<StereotypeApplication, ComponentType, IType>
		implements IALTransformation<StereotypeApplication, IType> {

	private IALHolder holder = new IALHolder();

	public ChildTransformation(ClassMechanismTransformation<StereotypeApplication, IType> parentTransformation,
			EReference eReference) {
		super(parentTransformation, eReference);
	}

	private void addChildAnnotation(IField field) {
		try {
			JDTUtils.addImportIfNecessary(codeElement,
					"org.codeling.lang.ejbWithStatemachine.ial.mm.componenttype_feature.Child", null);
			ASTUtils.addAnnotation(field, "Child", null);
		} catch (JavaModelException e) {
			e.printStackTrace();
		}
	}

	public boolean hasExpectedAnnotation(IField field) {
		boolean childAnnotationExists = (field.getAnnotation("Child").exists() && (field.getCompilationUnit()
				.getImport("org.codeling.lang.ejbWithStatemachine.ial.mm.componenttype_feature.Child").exists()
				|| field.getCompilationUnit()
						.getImport("org.codeling.lang.ejbWithStatemachine.ial.mm.componenttype_feature.*").exists()))
				|| field.getAnnotation("org.codeling.lang.ejbWithStatemachine.ial.mm.componenttype_feature.Child")
						.exists();
		boolean injectAnnotationExists = (field.getAnnotation("EJB").exists()
				&& (field.getCompilationUnit().getImport("javax.ejb.EJB").exists()
						|| field.getCompilationUnit().getImport("javax.ejb.*").exists()))
				|| field.getAnnotation("javax.ejb.EJB").exists();

		return childAnnotationExists && injectAnnotationExists;
	}

	@Override
	public IALHolder getIALHolder() {
		return holder;
	}

	@Override
	public StereotypeApplication resolveTranslatedIALElement(EObject foundationalElement) {
		EList<StereotypeApplication> appliedStereotypes = ProfilesUtils.getAppliedStereotypes(foundationalElement);
		Stereotype scopedComponent = ProfilesUtils.getStereotype("HierarchicalComponentTypeScoped",
				Profiles.COMPONENTS_HIERARCHY_SCOPED.load());
		for (StereotypeApplication app : appliedStereotypes) {
			if (app.getStereotype() == scopedComponent) {
				return app;
			}
		}
		return null;
	}

	@Override
	public boolean codeFragmentExists() {
		return true; // The reference always exists. It might be empty.
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return null;
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		updateCodeFragments(); // Everything is handled in update
	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		List<EObject> targets = Models.getTargetsAsList(modelElement, eReference);
		for (EObject target : targets) {
			String targetName = getNameAttributeValue(target);
			targetName = Character.toLowerCase(targetName.charAt(0)) + targetName.substring(1);
			IField field = codeElement.getField(targetName);
			if (field == null || !field.exists())
				continue; // Field must have been created before by the ReferencesTransformation.
			if (!hasExpectedAnnotation(field)) {
				addChildAnnotation(field);
			}
		}
	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		updateCodeFragments(); // Everything is handled in update
	}

	@Override
	public StereotypeApplication transformToModel() throws CodelingException {
		resolveCodeElement();
		IField[] fields;
		try {
			fields = codeElement.getFields();
		} catch (JavaModelException e) {
			throw new CodelingException(MessageFormat.format("Could not find fields of code element  [{0}]",
					codeElement.getHandleIdentifier()), e);
		}

		for (IField field : fields) {
			if (hasExpectedAnnotation(field)) {
				IType targetType = null;
				try {
					targetType = ASTUtils.getFieldType(field);
				} catch (JavaModelException e1) {
					addError(MessageFormat.format("Could not resolve type of field [{0}]", field.getHandleIdentifier()),
							e1);
					continue;
				}
				String id = idRegistry.getIDFromCodeElement(targetType);
				if (id == null)
					continue; // This field is not part of any model notation

				try {
					StereotypeApplication application = ProfilesUtils.findOrApplyStereotype(
							"HierarchicalComponentTypeScoped", Profiles.COMPONENTS_HIERARCHY_SCOPED.load(),
							holder.getFoundationalIALElement(), holder.getFoundationalIALElement().eResource());

					EObject target = idRegistry.resolveTranslationModelElement(id,
							EcoreUtil.getRootContainer(holder.getFoundationalIALElement()));
					@SuppressWarnings("unchecked")
					List<EObject> targets = (List<EObject>) application.eGet(eReference);
					if (targets == null) {
						targets = new LinkedList<EObject>();
						targets.add(target);
						application.eSet(eReference, targets);
					} else {
						targets.add(target);
					}
				} catch (IOException e) {
					addError("Could not find or apply stereotype \"HierarchicalComponentTypeScoped\" to a target.");
					continue;
				}
			}
		}

		// This should also add the scoped hierarchy stereotype to the architecture and
		// ensure that all root component types are referenced as system instance

		// Always return null. Cannot return multiple StereotypeApplications
		return null;
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub

	}

}
