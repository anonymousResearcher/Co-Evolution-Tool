package org.codeling.lang.ejbWithStatemachine.transformation;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.StateMachine;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage;
import org.codeling.lang.ejbWithStatemachine.transformation.statemachine_feature.InitialTransformation;
import org.codeling.lang.ejbWithStatemachine.transformation.statemachine_feature.StatesTransformation;
import org.codeling.mechanisms.transformations.classes.TypeAnnotationTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;

public class StateMachineTransformation extends TypeAnnotationTransformation<StateMachine> {

	public StateMachineTransformation(
			AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation) {
		super(parentTransformation, ejbWithSMPackage.eINSTANCE.getStateMachine());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new InitialTransformation(this));
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new StatesTransformation(this));
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new StatesTransformation(this));
	}
}
