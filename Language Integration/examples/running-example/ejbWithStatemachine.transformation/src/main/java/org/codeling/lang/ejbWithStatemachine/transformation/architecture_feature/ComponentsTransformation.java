package org.codeling.lang.ejbWithStatemachine.transformation.architecture_feature;

import java.util.List;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.Architecture;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.Component;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage;
import org.codeling.lang.ejbWithStatemachine.transformation.ComponentTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.references.NinjaSingletonContainmentTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;

public class ComponentsTransformation extends NinjaSingletonContainmentTransformation<Architecture, Component> {

	public ComponentsTransformation(ClassMechanismTransformation<Architecture, IJavaElement> parentTransformation) {
		super(parentTransformation, ejbWithSMPackage.eINSTANCE.getArchitecture_Components());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return new ComponentTransformation(this);
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		try {
			for (IType type : ASTUtils.getTypes(codeRoot)) {
				ComponentTransformation t = new ComponentTransformation(this);
				if (type.isClass() && t.hasExpectedAnnotation(type)) {
					t.setCodeElement(type);
					result.add(t);
				}
			}
		} catch (Exception e) {
			addError("Error while getting types in project.", e);
		}
	}
}
