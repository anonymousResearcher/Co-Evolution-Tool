package org.codeling.lang.ejbWithStatemachine.transformation;

import java.util.Arrays;
import java.util.List;

import org.codeling.lang.base.java.IALTransformationTuple;
import org.codeling.lang.base.java.JavaBasedImplementationLanguageDefinition;
import org.codeling.lang.base.java.ProfilesUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.base.modeltrans.henshintgg.HenshinTGGBasedLanguageDefinitionHelper;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage;
import org.codeling.lang.ejbWithStatemachine.transformation.component_feature.ChildTransformation;
import org.codeling.mechanisms.MechanismsMapping;
import org.codeling.mechanisms.classes.ContainmentOperationMechanism;
import org.codeling.mechanisms.classes.MarkerInterfaceMechanism;
import org.codeling.mechanisms.classes.TypeAnnotationMechanism;
import org.codeling.mechanisms.containment.NinjaSingletonContainmentReferenceMechanism;
import org.codeling.mechanisms.references.AnnotatedMemberReferenceMechanism;
import org.codeling.transformationmanager.TransformationResult;
import org.codeling.utils.CodelingException;
import org.codeling.utils.IDRegistry;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.osgi.framework.FrameworkUtil;

import de.mkonersmann.il.profiles.Profiles;

public class EjbWithStateMachineLanguage extends JavaBasedImplementationLanguageDefinition {

	final URI henshinTGGFileURI = URI.createPlatformPluginURI(
			"/" + FrameworkUtil.getBundle(getClass()).getSymbolicName() + "/AIL2IAL.henshin", true);

	public EjbWithStateMachineLanguage() {
		ejbWithSMPackage.eINSTANCE.getName(); // Initialize the Ecore package

		// Create the mechanism mapping
		MechanismsMapping m = MechanismsMapping.getInstance();
		ejbWithSMPackage i = ejbWithSMPackage.eINSTANCE;
		m.put(i.getStateMachine(), TypeAnnotationMechanism.class);
		m.put(i.getState(), MarkerInterfaceMechanism.class);
		m.put(i.getStateMachine_States(), AnnotatedMemberReferenceMechanism.class);
		m.put(i.getStateMachine_Initial(), AnnotatedMemberReferenceMechanism.class);
		m.put(i.getState_Transition(), ContainmentOperationMechanism.class);
		m.put(i.getTransition_Target(), NinjaSingletonContainmentReferenceMechanism.class);

		// Add all EClasses to the cross references. The order is irrelevant
		for (final EClassifier c : ejbWithSMPackage.eINSTANCE.getEClassifiers())
			if (c instanceof EClass)
				crossReferenceOrder.add((EClass) c);

		// Create the map of model elements to reference transformations
		ialTransformations.put(i.getComponent(),
				new IALTransformationTuple(
						ProfilesUtils.getEReference(Profiles.COMPONENTS_HIERARCHY_SCOPED.load(),
								"HierarchicalComponentTypeScoped", "childTypes"),
						Arrays.asList(ChildTransformation.class)));
	}

	@Override
	public AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> createRootTransformationInstance() {
		return new ArchitectureTransformation();
	}

	@Override
	public TransformationResult transformIMToTM(List<EObject> imRoots, IDRegistry idRegistry) throws CodelingException {
		return new HenshinTGGBasedLanguageDefinitionHelper().transformIMToTM(this, henshinTGGFileURI, imRoots,
				idRegistry, monitor);
	}

	@Override
	public TransformationResult transformTMToIM(List<EObject> tmRoots, IDRegistry idRegistry) throws CodelingException {
		return new HenshinTGGBasedLanguageDefinitionHelper().transformTMToIM(this, henshinTGGFileURI, tmRoots,
				idRegistry, monitor);
	}

	@Override
	public List<String> getSelectedProfiles() {
		return new HenshinTGGBasedLanguageDefinitionHelper().getSelectedProfiles(henshinTGGFileURI);
	}

}
