package org.codeling.lang.ejbWithStatemachine.transformation.transition_feature;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.State;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.Transition;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.classes.ContainmentOperationTargetTransformation;
import org.codeling.mechanisms.transformations.references.ContainmentOperationReferenceAnnotationParameterTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;

public class TargetTransformation
		extends ContainmentOperationReferenceAnnotationParameterTransformation<Transition, State> {

	public TargetTransformation(ContainmentOperationTargetTransformation<Transition> parentTransformation) {
		super(parentTransformation, ejbWithSMPackage.eINSTANCE.getTransition_Target());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return null;
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}
	
}
