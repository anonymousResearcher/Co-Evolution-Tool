package org.codeling.lang.ejbWithStatemachine.transformation;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.Architecture;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage;
import org.codeling.lang.ejbWithStatemachine.transformation.architecture_feature.ComponentsTransformation;
import org.codeling.mechanisms.transformations.classes.NinjaSingletonTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;

public class ArchitectureTransformation extends NinjaSingletonTransformation<Architecture> {

	public ArchitectureTransformation() {
		super(null, ejbWithSMPackage.eINSTANCE.getArchitecture());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new ComponentsTransformation(this));
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new ComponentsTransformation(this));
	}

}
