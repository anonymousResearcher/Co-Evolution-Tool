package org.codeling.examples.ccm.mm.interface_feature;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Operations {

	String timeResourceDemand();

	Class<?>[] rolesAllowed();

	Class<?> executionStrategy();
}
