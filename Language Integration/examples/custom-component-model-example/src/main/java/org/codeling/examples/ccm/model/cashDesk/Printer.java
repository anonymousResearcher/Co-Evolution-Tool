package org.codeling.examples.ccm.model.cashDesk;

import org.codeling.examples.ccm.mm.Attribute;
import org.codeling.examples.ccm.mm.ComponentType;

@ComponentType(version = "1.0")
public class Printer implements IPrinter {

	@Attribute
	public static final boolean parallel = false;

	@Override
	public void print(String content) {
		// Simulate a printing
		System.out.println("##### Printing -  BEGIN   ====");
		System.out.println(content);
		System.out.println("##### Printing - FINISHED ====");
	}

}
