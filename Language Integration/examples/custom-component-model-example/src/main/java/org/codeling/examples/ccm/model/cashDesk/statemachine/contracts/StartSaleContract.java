package org.codeling.examples.ccm.model.cashDesk.statemachine.contracts;

import org.codeling.examples.ccm.mm.Contract;
import org.codeling.examples.ccm.model.cashDesk.CashDeskVariables;

public class StartSaleContract implements Contract {

	CashDeskVariables pre;

	CashDeskVariables post;

	public boolean validatePreCondition() {
		return pre.getScannedItems() != null;
	}

	public boolean validatePostCondition() {
		return post.getScannedItems() != null;
	}

}
