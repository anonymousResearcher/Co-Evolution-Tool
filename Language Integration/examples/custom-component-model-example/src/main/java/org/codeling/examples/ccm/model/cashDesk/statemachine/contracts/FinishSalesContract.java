package org.codeling.examples.ccm.model.cashDesk.statemachine.contracts;

import org.codeling.examples.ccm.mm.Contract;
import org.codeling.examples.ccm.model.cashDesk.CashDeskVariables;

public class FinishSalesContract implements Contract {

	CashDeskVariables pre;

	CashDeskVariables post;

	@Override
	public boolean validatePreCondition() {
		return pre.getScannedItems() != null;
	}

	@Override
	public boolean validatePostCondition() {
		return post.getScannedItems().size() == 0;
	}

}
