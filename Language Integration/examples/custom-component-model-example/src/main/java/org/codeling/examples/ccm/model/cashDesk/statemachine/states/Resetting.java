package org.codeling.examples.ccm.model.cashDesk.statemachine.states;

import org.codeling.examples.ccm.mm.State;
import org.codeling.examples.ccm.mm.state_feature.Transitions;
import org.codeling.examples.ccm.model.cashDesk.CashDeskVariables;
import org.codeling.examples.ccm.model.cashDesk.statemachine.contracts.ResettingContract;

public class Resetting implements State {

	@Transitions(target = Ready.class, contracts = { ResettingContract.class })
	public void scanItem(CashDeskVariables variables) {
		variables.clearScannedItems();
	}
}
