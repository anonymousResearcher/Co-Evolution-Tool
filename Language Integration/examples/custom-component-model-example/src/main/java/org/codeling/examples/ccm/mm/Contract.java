package org.codeling.examples.ccm.mm;

public interface Contract {

	boolean validatePreCondition();

	boolean validatePostCondition();

}
