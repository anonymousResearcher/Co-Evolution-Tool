package org.codeling.examples.ccm.model.cashDesk;

import java.util.Random;

import org.codeling.examples.ccm.mm.Attribute;
import org.codeling.examples.ccm.mm.ComponentType;

@ComponentType(version = "1.0")
public class BarCodeScanner implements IBarCodeScanner {

	@Attribute
	public static final boolean parallel = false;

	private Random r = new Random();

	@Override
	public String scan() {
		return "" + r.nextInt();
	}

}
