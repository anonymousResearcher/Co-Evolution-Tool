package org.codeling.examples.ccm.model;

import java.util.Queue;

import org.codeling.examples.ccm.mm.Attribute;
import org.codeling.examples.ccm.mm.ComponentType;
import org.codeling.examples.ccm.mm.State;
import org.codeling.examples.ccm.mm.StateMachine;
import org.codeling.examples.ccm.mm.Variables;
import org.codeling.examples.ccm.mm.architecture_feature.Initial;
import org.codeling.examples.ccm.mm.architecture_feature.Required;
import org.codeling.examples.ccm.mm.architecture_feature.States;
import org.codeling.examples.ccm.model.cashDesk.CashDeskVariables;
import org.codeling.examples.ccm.model.cashDesk.IBarCodeScanner;
import org.codeling.examples.ccm.model.cashDesk.IPrinter;
import org.codeling.examples.ccm.model.cashDesk.statemachine.states.AwaitingPayment;
import org.codeling.examples.ccm.model.cashDesk.statemachine.states.Ready;
import org.codeling.examples.ccm.model.cashDesk.statemachine.states.Scanning;
import org.codeling.examples.ccm.runtime.StateMachineRuntime;

@ComponentType(version = "1.0")
@StateMachine
public class CashDesk implements ICashDesk {

	@Attribute
	public static final boolean parallel = false;

	@Initial(Ready.class)
	State initial;

	@Required
	IBarCodeScanner iBarcodeScanner;

	@Required
	IPrinter iPrinter;
	
	@Required
	IStoreServer iStoreServer;

	@States({ Ready.class, Scanning.class, AwaitingPayment.class })
	State[] states;

	@Variables
	CashDeskVariables variables;
	
	final StateMachineRuntime<CashDesk> smr;

	public CashDesk() {
		smr = new StateMachineRuntime<CashDesk>(this.getClass());
		smr.initialize();
		smr.initializeContainments();
		smr.initializeCrossReferences();
	}

	@Override
	public void scanItem() {
		// Execute the state machine transition
		smr.executeTransition("scanItem");
	}

	@Override
	public void openCashBox() {
		// Execute the state machine transition
		smr.executeTransition("openCashBox");
	}

	@Override
	public void finish() {
		// Execute the state machine transition
		smr.executeTransition("finishSale");
	}

	public Queue<String> getScannedItems() {
		return variables.getScannedItems();
	}

}
