package marker_interface.metamodel;

public interface Component {
	public void onStart();

	public void onStop();
}
