package marker_interface.model;

import java.text.MessageFormat;

import marker_interface.metamodel.Component;

/**
 * This class represents an instance of Component with the name BarCodeScanner
 * based on the Marker Interface mechanism.
 * 
 * The operation signatures of onStart and onStop are elements within the entry
 * point of the meta model notation of the Marker Interface. Their
 * implementation here is part of the entry point of the model notation.
 */
public class BarCodeScanner implements Component {

	public void onStart() {
		System.out.println(MessageFormat.format("[{0}] onStart.", this.getClass().getCanonicalName()));
	}

	public void onStop() {
		System.out.println(MessageFormat.format("[{0}] onStop.", this.getClass().getCanonicalName()));
	}

}
