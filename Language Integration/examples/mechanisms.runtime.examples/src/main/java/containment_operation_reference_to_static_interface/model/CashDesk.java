package containment_operation_reference_to_static_interface.model;

import containment_operation_reference_to_static_interface.metamodel.Component;
import containment_operation_reference_to_static_interface.metamodel.Required;

public class CashDesk implements Component {

	/**
	 * The runtime sets a static interface implementer as target. The runtime
	 * can use the target type. The entry point can use the target static
	 * interface.
	 * 
	 * This style is useful when the reference has it's own executional
	 * semantics, and the semantics of the reference does not follow
	 * standardized ways, and - the target class is not contained, or - the
	 * target class is referenced by other classes.
	 */
	@Required
	public void required(IBarCodeScanner iBarCodeScanner) {
		iBarCodeScanner.scanCode();
	}

}
