package containment_operation_reference_to_static_interface.model;

import containment_operation_reference_to_static_interface.metamodel.ComponentInterface;

@ComponentInterface
public interface IBarCodeScanner {
	public void scanCode();
}
