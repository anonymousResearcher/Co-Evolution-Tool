package constant_member_attribute.model;

import constant_member_attribute.metamodel.Attribute;
import constant_member_attribute.metamodel.Component;

/**
 * The Constant Member Attribute mechanism depends on the attribute's owner to
 * be represented by a type, e.g. by the Marker Interface or Type Annotation
 * mechanism. In this example, the Component is represented using the Marker
 * Interface mechansim.
 */
public class BarCodeScanner implements Component {

	/**
	 * The Constant Member Attribute mechanism represents the attribute version
	 * with the member attribute version.
	 */
	@Attribute
	public static final String version = "1.0";

}
