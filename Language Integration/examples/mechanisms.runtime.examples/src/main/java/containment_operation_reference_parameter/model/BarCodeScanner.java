package containment_operation_reference_parameter.model;

import containment_operation_reference_parameter.metamodel.Component;
import containment_operation_reference_parameter.metamodel.Jobs;

/**
 * This type could represent a class using the Annotation Type or Marker
 * Interface mechanism.
 */
public class BarCodeScanner implements Component {

	/**
	 * The runtime sets a target. The runtime can use the target type. The entry
	 * point can use the target static interface.
	 * 
	 * This style is useful when the reference has it's own executional
	 * semantics, besides the semantics of the target class, and the reference's
	 * semantics is not standardized, and
	 * - the target class is not contained, or
	 * - the target class is referenced by other classes.
	 */
	@Jobs
	public void jobs(ScanCode scanCode) {
		scanCode.moveItemInFrontOfScanner();
		System.out.println("Should not beep.");
		if (scanCode.validate()) {
			scanCode.execute();
		}

		System.out.println("Oops, there was an item in front of the scanner. Let's undo the scan. Undoing beeps twice.");
		scanCode.undo();

		scanCode.emptyScannerArea();
		System.out.println("Ok, this time it should not beep.");
		if (scanCode.validate()) {
			scanCode.execute();
		}

		System.out.println("Works.");
	}

}
