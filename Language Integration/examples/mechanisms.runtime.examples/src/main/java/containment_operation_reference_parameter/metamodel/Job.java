package containment_operation_reference_parameter.metamodel;

public interface Job {
	/*
	 * The following operations are part of the marker interface's entry point.
	 * They are part of the job's execution semantics.
	 */
	void undo();

	void execute();

	boolean validate();

}
