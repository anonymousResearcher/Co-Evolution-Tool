package type_annotation.model;

import java.text.MessageFormat;

import type_annotation.metamodel.Component;

/**
 * This class represents an instance of Component with the name BarCodeScanner
 * based on the Type Annotation mechanism.
 * 
 * The operation signatures of onStart and onStop and their implementation are part
 * of the entry point of the model notation.
 */
@Component
public class BarCodeScanner {
	public void onStart() {
		System.out.println(MessageFormat.format("[{0}] onStart.", this.getClass().getCanonicalName()));
	}

	public void onStop() {
		System.out.println(MessageFormat.format("[{0}] onStop.", this.getClass().getCanonicalName()));
	}

}
