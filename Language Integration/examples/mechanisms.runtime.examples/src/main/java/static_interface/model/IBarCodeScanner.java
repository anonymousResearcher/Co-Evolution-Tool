package static_interface.model;

import static_interface.metamodel.ComponentInterface;

/**
 * This interface represents an instance of ComponentInterface with the name
 * IBarCodeScanner, based on the Static Interface mechanism.
 * 
 * It implies that the meta model element has no execution semantics. A class
 * represented by the static interface mechanism can have attributes represented
 * via the attribute annotation mechanism, and containment references to types
 * represented with the containment operation mechanism.
 */
@ComponentInterface 
public interface IBarCodeScanner {

}
