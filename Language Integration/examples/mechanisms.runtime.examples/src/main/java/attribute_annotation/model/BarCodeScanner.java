package attribute_annotation.model;

import attribute_annotation.metamodel.Component;
import attribute_annotation.metamodel.Version;

/**
 * The Constant Member Attribute mechanism depends on the attribute's owner to
 * be represented by a type, e.g. by the Marker Interface or Type Annotation
 * mechanism. In this example, the Component is represented using the Marker
 * Interface mechanism.
 * 
 * The Attribute Annotation mechanism represents the attribute version with the
 * annotation Version.
 */
@Version("1.0")
public class BarCodeScanner implements Component {

}
