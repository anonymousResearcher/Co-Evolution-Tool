package containment_operation.type.runtime;

import org.codeling.mechanism.runtime.ContainmentOperationRuntime;
import org.codeling.mechanism.runtime.MarkerInterfaceRuntime;
import org.codeling.mechanism.runtime.common.IntegratedModelException;
import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;

import containment_operation.type.metamodel.Component;
import containment_operation.type.metamodel.Operations;

public class BarCodeScannerRuntimeWithOperationRuntime<T extends Component>
		extends MarkerInterfaceRuntime<Component, T> {

	OperationRuntime operationsRuntime;

	public BarCodeScannerRuntimeWithOperationRuntime(Class<T> implementingClass) throws IntegratedModelException {
		super(implementingClass, Component.class);
	}

	@Override
	public void initialize() throws IntegratedModelException {
		super.initialize();
	}

	@Override
	public void initializeContainments() throws IntegratedModelException {
		super.initializeContainments();

		// Create runtimes for attributes and references
		operationsRuntime = new OperationRuntime(this);
		operationsRuntime.initialize();
	}

	public OperationRuntime getOperationsRuntime() {
		return operationsRuntime;
	}
}

class OperationRuntime extends ContainmentOperationRuntime {

	public OperationRuntime(TypeMechanismRuntime owningRuntime) {
		super(owningRuntime, Operations.class);
	}

}