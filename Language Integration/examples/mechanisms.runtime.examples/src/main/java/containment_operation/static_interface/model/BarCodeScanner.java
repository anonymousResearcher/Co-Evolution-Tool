package containment_operation.static_interface.model;

import java.text.MessageFormat;

import containment_operation.static_interface.metamodel.Component;

/**
 * This class represents an instance of Component with the name BarCodeScanner
 * based on the Marker Interface mechanism.
 * 
 * The focus here lies on the implementation of the interface IBarCodeScanner,
 * which is representation of a class via the Static Interface mechanism.
 * 
 * When the static interface representation IBarCodeScanner declares operation
 * signatures within its entry point or represents contained objects using the
 * specific containment operation mechanism, these methods have to be
 * implemented here.
 */
public class BarCodeScanner implements Component, IBarCodeScanner {

	boolean scannedCode = false;
	
	/**
	 * This is an implementation of the component operation declared as
	 * reference in the {@link IBarCodeScanner} class.
	 */
	public void scanCode() {
		System.out
				.println(MessageFormat.format("The component [{0}] is executing the semantics of the class [scanCode].",
						this.getClass().getSimpleName()));
		scannedCode = true; 
	}

}
