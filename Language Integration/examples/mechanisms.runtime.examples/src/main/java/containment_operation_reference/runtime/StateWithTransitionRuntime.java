package containment_operation_reference.runtime;

import org.codeling.mechanism.runtime.ContainmentOperationReferenceRuntime;
import org.codeling.mechanism.runtime.ContainmentOperationRuntime;
import org.codeling.mechanism.runtime.MarkerInterfaceRuntime;
import org.codeling.mechanism.runtime.common.IntegratedModelException;
import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;

import containment_operation_reference.metamodel.State;
import containment_operation_reference.metamodel.Transition;

public class StateWithTransitionRuntime<T extends State> extends MarkerInterfaceRuntime<State, T> {

	TransitionWithTargetRuntime transitionRuntime;

	public StateWithTransitionRuntime(Class<T> implementingClass) throws IntegratedModelException {
		super(implementingClass, State.class);
	}

	@Override
	public void initialize() throws IntegratedModelException {
		super.initialize();
	}

	@Override
	public void initializeContainments() throws IntegratedModelException {
		super.initializeContainments();

		// Create runtimes for attributes and references
		transitionRuntime = new TransitionWithTargetRuntime(this);
		transitionRuntime.initialize();
		transitionRuntime.initializeContainments();
	}

	@Override
	public void initializeCrossReferences() throws IntegratedModelException {
		super.initializeCrossReferences();

		transitionRuntime.initializeCrossReferences();
	}

	public TransitionWithTargetRuntime getTransitionRuntime() {
		return transitionRuntime;
	}
}

class TransitionWithTargetRuntime extends ContainmentOperationRuntime {

	TargetRuntime targetRuntime;

	public TransitionWithTargetRuntime(TypeMechanismRuntime owningRuntime) {
		super(owningRuntime, Transition.class);
	}

	@Override
	public void initialize() throws IntegratedModelException {
		super.initialize();
	}

	@Override
	public void initializeCrossReferences() throws IntegratedModelException {
		super.initializeCrossReferences();

		// Create runtimes for attributes and references
		targetRuntime = new TargetRuntime(this);
		targetRuntime.initialize();
	}

	public TargetRuntime getTargetRuntime() {
		return targetRuntime;
	}

}

class TargetRuntime extends ContainmentOperationReferenceRuntime {

	public TargetRuntime(ContainmentOperationRuntime owningRuntime) {
		super(owningRuntime, "target", StateWithTransitionRuntime.class, false);
	}

}