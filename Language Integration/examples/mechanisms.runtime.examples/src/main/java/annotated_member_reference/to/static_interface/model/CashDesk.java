package annotated_member_reference.to.static_interface.model;

import java.text.MessageFormat;

import annotated_member_reference.to.static_interface.metamodel.Component;
import annotated_member_reference.to.static_interface.metamodel.Required;

/**
 * This type represents an instance of Component with the name CashDesk, based
 * on the TypeAnnotation mechanism.
 */
@Component
public class CashDesk {

	/**
	 * These Reference Annotation mechanism is used here to represent a
	 * reference to a class that is translated with the Static Interface
	 * mechanism. The type of the target is the interface.
	 * 
	 * The runtime will inject an implementation of the interface, when one
	 * exists. The runtime controls, which instance is injected. The exact
	 * details of the injection is subject to the runtime.
	 */
	@Required
	IBarCodeScanner barCodeScanner;

	public void onStart() {
		System.out.println(MessageFormat.format(
				"The component [{0}] has a reference [barCodeScanner] to a [{1}]. The injected static interface implementer is named [{2}].",
				this.getClass().getSimpleName(),
				IBarCodeScanner.class.getAnnotations()[0].annotationType().getSimpleName(),
				barCodeScanner.getClass().getSimpleName()));

		barCodeScanner.doBeep();
	}

	public void onStop() {
	}

}
