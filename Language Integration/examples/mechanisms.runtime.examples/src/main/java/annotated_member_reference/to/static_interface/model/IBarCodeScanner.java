package annotated_member_reference.to.static_interface.model;

import annotated_member_reference.to.static_interface.metamodel.ComponentInterface;

/**
 * This interface represents an instance of ComponentInterface with the name
 * IBarCodeScanner, based on the Static Interface mechanism.
 */
@ComponentInterface
public interface IBarCodeScanner {
	/**
	 * This is an operation as part of the entry point of the Static Interface
	 * mechanism.
	 */
	public void doBeep();
}
