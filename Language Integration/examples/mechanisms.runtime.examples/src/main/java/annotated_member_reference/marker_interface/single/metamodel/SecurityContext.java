package annotated_member_reference.marker_interface.single.metamodel;

public interface SecurityContext {

	String[] getAllowedRoles();

}
