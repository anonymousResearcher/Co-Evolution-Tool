package annotated_member_reference.marker_interface.multi.metamodel;

public interface SecurityContext {

	String[] getAllowedRoles();

}
