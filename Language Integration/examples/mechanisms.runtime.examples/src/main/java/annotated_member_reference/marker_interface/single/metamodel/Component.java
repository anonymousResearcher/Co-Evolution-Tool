package annotated_member_reference.marker_interface.single.metamodel;

public interface Component {
	public void onStart();

	public void onStop();
}