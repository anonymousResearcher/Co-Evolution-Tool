package annotated_member_reference.marker_interface.single.metamodel;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Context {

	Class<? extends SecurityContext> value();

}
