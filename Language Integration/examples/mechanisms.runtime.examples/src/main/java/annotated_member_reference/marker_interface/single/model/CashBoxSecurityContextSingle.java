package annotated_member_reference.marker_interface.single.model;

import annotated_member_reference.marker_interface.single.metamodel.SecurityContext;

public class CashBoxSecurityContextSingle implements SecurityContext {

	public String[] getAllowedRoles() {
		return new String[] { "Admin", "Teacher", "Moderator" };
	}
}
