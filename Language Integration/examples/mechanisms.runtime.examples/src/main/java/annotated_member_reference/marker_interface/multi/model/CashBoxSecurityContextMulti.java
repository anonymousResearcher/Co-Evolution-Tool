package annotated_member_reference.marker_interface.multi.model;

import annotated_member_reference.marker_interface.multi.metamodel.SecurityContext;

public class CashBoxSecurityContextMulti implements SecurityContext {

	public String[] getAllowedRoles() {
		return new String[] { "Admin", "Teacher", "Moderator" };
	}
}
