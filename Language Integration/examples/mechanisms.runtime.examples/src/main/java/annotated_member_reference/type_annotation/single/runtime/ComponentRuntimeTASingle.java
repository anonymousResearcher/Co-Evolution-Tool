package annotated_member_reference.type_annotation.single.runtime;

import org.codeling.mechanism.runtime.AnnotatedMemberReferenceRuntime;
import org.codeling.mechanism.runtime.MarkerInterfaceRuntime;
import org.codeling.mechanism.runtime.TypeAnnotationRuntime;
import org.codeling.mechanism.runtime.common.IntegratedModelException;
import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;

import annotated_member_reference.type_annotation.single.metamodel.Component;
import annotated_member_reference.type_annotation.single.metamodel.Context;
import annotated_member_reference.type_annotation.single.metamodel.SecurityContext;

public class ComponentRuntimeTASingle<T extends Component> extends MarkerInterfaceRuntime<Component, T> {

	ContextRuntime resourcesRuntime;

	public ComponentRuntimeTASingle(Class<T> implementingClass) throws IntegratedModelException {
		super(implementingClass, Component.class);
	}

	@Override
	public void initialize() throws IntegratedModelException {
		super.initialize();
	}

	@Override
	public void initializeContainments() throws IntegratedModelException {
		resourcesRuntime = new ContextRuntime(this);
		resourcesRuntime.initialize();
	}
}

class ContextRuntime extends AnnotatedMemberReferenceRuntime {

	public ContextRuntime(TypeMechanismRuntime owningRuntime) {
		super(owningRuntime, Context.class, SecurityContextRuntime.class, true);
	}

}

class SecurityContextRuntime<T> extends TypeAnnotationRuntime<T> {
	public SecurityContextRuntime(Class<T> implementingClass) {
		super(implementingClass, SecurityContext.class);
	}
}