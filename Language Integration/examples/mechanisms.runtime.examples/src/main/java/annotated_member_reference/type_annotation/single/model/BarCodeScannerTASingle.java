package annotated_member_reference.type_annotation.single.model;

import java.text.MessageFormat;

import annotated_member_reference.type_annotation.single.metamodel.Component;
import annotated_member_reference.type_annotation.single.metamodel.Context;

public class BarCodeScannerTASingle implements Component {

	/**
	 * The Reference Annotation mechanism is used here to represent a reference
	 * to a class that is translated with the Marker Interface mechanism. The
	 * type of the target is the marker interface. The name of the annotation is
	 * equivalent to the name of the reference.
	 * 
	 * The runtime will inject an instance of the class given in the annotation
	 * parameter into the member reference. The runtime controls, which instance
	 * is injected. The exact details of the injection is subject to the
	 * runtime.
	 */
	@Context
	CashBoxSecurityContextTASingle context;

	public void onStart() {
		System.out.println(MessageFormat.format(
				"The component [{0}] has a reference [context] to the [SecurityContext] named [{1}].",
				this.getClass().getSimpleName(), context.getClass().getSimpleName()));

		for (String r : context.getAllowedRoles())
			System.out.println(MessageFormat.format("Allowed Roles in Security Context: [{0}]", r));
	}

	public void onStop() {
	}

}
