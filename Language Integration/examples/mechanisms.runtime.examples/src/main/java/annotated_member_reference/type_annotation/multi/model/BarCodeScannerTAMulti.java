package annotated_member_reference.type_annotation.multi.model;

import java.text.MessageFormat;

import annotated_member_reference.type_annotation.multi.metamodel.Component;
import annotated_member_reference.type_annotation.multi.metamodel.Resources;

public class BarCodeScannerTAMulti implements Component{

	/**
	 * The Reference Annotation mechanism is used here to represent a reference
	 * to a class that is translated with the Marker Interface mechanism. The
	 * type of the target is the marker interface. The name of the annotation is
	 * equivalent to the name of the reference.
	 * 
	 * The runtime will inject instance of the classes given in the annotation
	 * parameter into the member reference. The runtime controls, which
	 * instances are injected. The exact details of the injection is subject to
	 * the runtime.
	 */
	@Resources
	BarCodeScannerResourceContainerTAMulti barCodeScannerResourceContainerTAMulti;

	@Resources
	SharedResourceContainerTAMulti sharedResourceContainerTAMulti;

	public void onStart() {
		System.out.println(MessageFormat.format(
				"The component [{0}] has a reference [resources] to the [ResourceContainer] named [{1}], with the following content:",
				this.getClass().getSimpleName(), barCodeScannerResourceContainerTAMulti.getClass().getName()));

		for (String r : barCodeScannerResourceContainerTAMulti.getContent())
			System.out.println(MessageFormat.format("Resource in resource container: [{0}]", r));

		System.out.println(MessageFormat.format(
				"The component [{0}] has a reference [resources] to the [ResourceContainer] named [{1}], with the following content:",
				this.getClass().getSimpleName(), sharedResourceContainerTAMulti.getClass().getName()));

		for (String r : sharedResourceContainerTAMulti.getContent())
			System.out.println(MessageFormat.format("Resource in resource container: [{0}]", r));
	}

	public void onStop() {
	}

}
