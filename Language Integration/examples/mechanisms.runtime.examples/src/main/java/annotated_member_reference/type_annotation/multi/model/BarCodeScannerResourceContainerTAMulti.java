package annotated_member_reference.type_annotation.multi.model;

@annotated_member_reference.type_annotation.multi.metamodel.ResourceContainer
public class BarCodeScannerResourceContainerTAMulti {

	public String[] getContent() {
		return new String[] { "Red Shirt", "Black Shirt", "White Shirt" };
	}
}
