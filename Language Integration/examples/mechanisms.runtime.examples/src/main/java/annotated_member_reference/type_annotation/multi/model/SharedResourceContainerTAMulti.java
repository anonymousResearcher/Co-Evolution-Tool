package annotated_member_reference.type_annotation.multi.model;

import annotated_member_reference.type_annotation.multi.metamodel.ResourceContainer;

@ResourceContainer
public class SharedResourceContainerTAMulti {

	public String[] getContent() {
		return new String[] { "Blue Birds", "Brown Snakes", "Yellow Reptiles" };
	}
}
