package containment_operation.type.runtime;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;

import org.junit.Test;

import containment_operation.type.model.BarCodeScanner;
import containment_operation.type.runtime.BarCodeScannerRuntimeWithOperationRuntime;

public class ContainmentOperationTest {
	@Test
	public void containmentOperation() throws Exception {
		// Initialize the source Marker Interface and the target Marker
		// Interface class
		final BarCodeScannerRuntimeWithOperationRuntime<BarCodeScanner> runtime = new BarCodeScannerRuntimeWithOperationRuntime<>(
				BarCodeScanner.class);
		runtime.initialize();
		runtime.initializeContainments();

		runtime.getOperationsRuntime().invoke("scanCode");
		final Field f = runtime.getInstance().getClass().getDeclaredField("scannedCode");
		f.setAccessible(true);
		final boolean scanned = (boolean) f.get(runtime.getInstance());
		assertTrue(scanned);
	}
}
