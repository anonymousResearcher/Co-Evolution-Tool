package containment_operation.static_interface.runtime;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;

import org.junit.Test;

import containment_operation.static_interface.model.BarCodeScanner;

public class ContainmentOperationTest {
	@Test
	public void static_interface() throws Exception {
		// Initialize the source Marker Interface and the target Marker
		// Interface class
		final BarCodeScannerRuntimeWithInterfaceRuntime<BarCodeScanner> runtime = new BarCodeScannerRuntimeWithInterfaceRuntime<>(
				BarCodeScanner.class);
		runtime.initialize();
		runtime.initializeContainments();

		runtime.getOperationsRuntime().invoke("scanCode");
		final Field f = runtime.getInstance().getClass().getDeclaredField("scannedCode");
		f.setAccessible(true);
		final boolean scanned = (boolean) f.get(runtime.getInstance());
		assertTrue(scanned);
	}
}
