package type_annotation.runtime;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import type_annotation.model.BarCodeScanner;

public class TypeAnnotationTest {
	@Test
	public void typeAnnotation() throws Exception {
		// Create a runtime instance of the object. It can be used to access the
		// entry points.
		final BarCodeScannerRuntime<BarCodeScanner> barCodeScannerRuntime = new BarCodeScannerRuntime<BarCodeScanner>(BarCodeScanner.class);
		barCodeScannerRuntime.initialize();
		assertNotNull(barCodeScannerRuntime.getInstance());

		// Execute the execution semantics of the class. In the example, it has
		// the execution semantics to be started and stopped.
		barCodeScannerRuntime.startComponent();
		barCodeScannerRuntime.stopComponent();

		// Should not throw an exception until now
	}
}
