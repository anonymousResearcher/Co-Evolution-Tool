package containment_operation_reference.runtime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;
import org.junit.Test;

import containment_operation_reference.model.Finished;
import containment_operation_reference.model.Initialized;

public class ContainmentOperationReferenceTest {
	@Test
	public void containmentOperationReference() throws Exception {
		final StateWithTransitionRuntime targetRuntime = new StateWithTransitionRuntime(Finished.class);
		targetRuntime.initialize();

		final StateWithTransitionRuntime sourceRuntime = new StateWithTransitionRuntime(Initialized.class);
		sourceRuntime.initialize();
		sourceRuntime.initializeContainments();
		sourceRuntime.initializeCrossReferences();

		TypeMechanismRuntime[] rt = sourceRuntime.getTransitionRuntime().getTargetRuntime().getTargetRuntime("finish");
		assertNotNull(rt);
		assertEquals(1, rt.length);
		assertSame(targetRuntime, rt[0]);
	}
}
