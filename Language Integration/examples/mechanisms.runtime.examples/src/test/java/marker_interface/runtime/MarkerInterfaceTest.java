package marker_interface.runtime;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import marker_interface.model.BarCodeScanner;

public class MarkerInterfaceTest {
	@Test
	public void markerInterface() throws Exception {
		// Create a runtime instance of the object. It can be used to access the
		// entry points.
		final ComponentRuntime componentRuntime = new ComponentRuntime(BarCodeScanner.class);
		componentRuntime.initialize();
		assertNotNull(componentRuntime.getInstance());

		// Execute the execution semantics of the class. In the example, it has
		// the execution semantics to be started and stopped.
		componentRuntime.startComponent();
		componentRuntime.stopComponent();

		// Should not throw an exception until now
	}
}
