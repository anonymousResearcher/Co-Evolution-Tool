package static_interface_implementation.runtime;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import static_interface_implementation.model.BarCodeScanner;

public class StaticInterfaceImplementationTest {
	@Test
	public void staticInterfaceImplementation() throws Exception {
		// Create a runtime instance of the object. It can be used to access the
		// entry points.
		ProvidedRuntime<BarCodeScanner> runtime = new ProvidedRuntime<BarCodeScanner>(BarCodeScanner.class);

		runtime.initialize();
		assertNotNull(runtime.getInstance());

		assertArrayEquals(new String[] { "doBeep" }, runtime.getProvidedOperationNames());
		assertEquals("beep", runtime.getInstance().doBeep());
	}
}
