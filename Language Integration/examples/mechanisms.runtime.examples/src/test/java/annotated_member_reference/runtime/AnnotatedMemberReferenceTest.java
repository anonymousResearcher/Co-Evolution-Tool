package annotated_member_reference.runtime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Array;
import java.lang.reflect.Field;

import org.codeling.mechanism.runtime.common.Runtimes;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import annotated_member_reference.marker_interface.multi.model.BarCodeScannerMulti;
import annotated_member_reference.marker_interface.multi.model.CashBoxSecurityContextMulti;
import annotated_member_reference.marker_interface.multi.model.EnterpriseSecurityContext;
import annotated_member_reference.marker_interface.multi.runtime.ComponentRuntimeMulti;
import annotated_member_reference.marker_interface.single.model.BarCodeScannerSingle;
import annotated_member_reference.marker_interface.single.model.CashBoxSecurityContextSingle;
import annotated_member_reference.marker_interface.single.runtime.ComponentRuntimeSingle;
import annotated_member_reference.type_annotation.multi.metamodel.runtime.ComponentRuntimeTAMulti;
import annotated_member_reference.type_annotation.multi.model.BarCodeScannerResourceContainerTAMulti;
import annotated_member_reference.type_annotation.multi.model.BarCodeScannerTAMulti;
import annotated_member_reference.type_annotation.multi.model.SharedResourceContainerTAMulti;
import annotated_member_reference.type_annotation.single.model.BarCodeScannerTASingle;
import annotated_member_reference.type_annotation.single.model.CashBoxSecurityContextTASingle;
import annotated_member_reference.type_annotation.single.runtime.ComponentRuntimeTASingle;

public class AnnotatedMemberReferenceTest {
	@Before
	@After
	public void init() {
		Runtimes.getInstance().clear();
	}

	@Test
	public void marker_interface_multi() throws Exception {
		final ComponentRuntimeMulti<BarCodeScannerMulti> runtime = new ComponentRuntimeMulti<>(
				BarCodeScannerMulti.class);
		runtime.initialize();
		runtime.initializeContainments();

		final Field f = BarCodeScannerMulti.class.getDeclaredField("securityContext");
		f.setAccessible(true);
		final Object target = f.get(runtime.getInstance());
		assertNotNull(target);
		assertEquals(2, Array.getLength(target));
		assertTrue(Array.get(target, 0) instanceof CashBoxSecurityContextMulti);
		assertTrue(Array.get(target, 1) instanceof EnterpriseSecurityContext);
	}

	@Test
	public void marker_interface_single() throws Exception {
		final ComponentRuntimeSingle<BarCodeScannerSingle> runtime = new ComponentRuntimeSingle<>(
				BarCodeScannerSingle.class);
		runtime.initialize();
		runtime.initializeContainments();

		final Field f = BarCodeScannerSingle.class.getDeclaredField("securityContext");
		f.setAccessible(true);
		final Object target = f.get(runtime.getInstance());
		assertNotNull(target);
		assertTrue(target instanceof CashBoxSecurityContextSingle);
	}

	@Test
	public void type_annotation_single() throws Exception {
		final ComponentRuntimeTASingle<BarCodeScannerTASingle> runtime = new ComponentRuntimeTASingle<>(
				BarCodeScannerTASingle.class);
		runtime.initialize();
		runtime.initializeContainments();

		final Field f = BarCodeScannerTASingle.class.getDeclaredField("context");
		f.setAccessible(true);
		final Object target = f.get(runtime.getInstance());

		assertNotNull(target);
		assertTrue(target instanceof CashBoxSecurityContextTASingle);
	}

	@Test
	public void type_annotation_multi() throws Exception {
		final ComponentRuntimeTAMulti<BarCodeScannerTAMulti> runtime = new ComponentRuntimeTAMulti<>(
				BarCodeScannerTAMulti.class);
		runtime.initialize();
		runtime.initializeContainments();

		final Field fBCS = BarCodeScannerTAMulti.class.getDeclaredField("barCodeScannerResourceContainerTAMulti");
		fBCS.setAccessible(true);
		final Object bcsTarget = fBCS.get(runtime.getInstance());

		final Field fSRC = BarCodeScannerTAMulti.class.getDeclaredField("sharedResourceContainerTAMulti");
		fSRC.setAccessible(true);
		final Object srcTarget = fSRC.get(runtime.getInstance());

		assertNotNull(bcsTarget);
		assertTrue(bcsTarget instanceof BarCodeScannerResourceContainerTAMulti);
		assertNotNull(srcTarget);
		assertTrue(srcTarget instanceof SharedResourceContainerTAMulti);
	}

}
