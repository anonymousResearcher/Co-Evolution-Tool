/**
 */
package org.codeling.lang.jee7.mm.JEE7;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Session Bean</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.SessionBean#isIsStateless <em>Is Stateless</em>}</li>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.SessionBean#isIsStateful <em>Is Stateful</em>}</li>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.SessionBean#isIsSingleton <em>Is Singleton</em>}</li>
 * </ul>
 *
 * @see org.codeling.lang.jee7.mm.JEE7.JEE7Package#getSessionBean()
 * @model
 * @generated
 */
public interface SessionBean extends Bean {
	/**
	 * Returns the value of the '<em><b>Is Stateless</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Stateless</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Stateless</em>' attribute.
	 * @see #setIsStateless(boolean)
	 * @see org.codeling.lang.jee7.mm.JEE7.JEE7Package#getSessionBean_IsStateless()
	 * @model default="false"
	 * @generated
	 */
	boolean isIsStateless();

	/**
	 * Sets the value of the '{@link org.codeling.lang.jee7.mm.JEE7.SessionBean#isIsStateless <em>Is Stateless</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Stateless</em>' attribute.
	 * @see #isIsStateless()
	 * @generated
	 */
	void setIsStateless(boolean value);

	/**
	 * Returns the value of the '<em><b>Is Stateful</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Stateful</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Stateful</em>' attribute.
	 * @see #setIsStateful(boolean)
	 * @see org.codeling.lang.jee7.mm.JEE7.JEE7Package#getSessionBean_IsStateful()
	 * @model
	 * @generated
	 */
	boolean isIsStateful();

	/**
	 * Sets the value of the '{@link org.codeling.lang.jee7.mm.JEE7.SessionBean#isIsStateful <em>Is Stateful</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Stateful</em>' attribute.
	 * @see #isIsStateful()
	 * @generated
	 */
	void setIsStateful(boolean value);

	/**
	 * Returns the value of the '<em><b>Is Singleton</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Singleton</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Singleton</em>' attribute.
	 * @see #setIsSingleton(boolean)
	 * @see org.codeling.lang.jee7.mm.JEE7.JEE7Package#getSessionBean_IsSingleton()
	 * @model
	 * @generated
	 */
	boolean isIsSingleton();

	/**
	 * Sets the value of the '{@link org.codeling.lang.jee7.mm.JEE7.SessionBean#isIsSingleton <em>Is Singleton</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Singleton</em>' attribute.
	 * @see #isIsSingleton()
	 * @generated
	 */
	void setIsSingleton(boolean value);

} // SessionBean
