/**
 */
package org.codeling.lang.jee7.mm.JEE7;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Entity Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.EntityAttribute#getName <em>Name</em>}</li>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.EntityAttribute#isIsMany <em>Is Many</em>}</li>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.EntityAttribute#getPrimitiveType <em>Primitive Type</em>}</li>
 * </ul>
 *
 * @see org.codeling.lang.jee7.mm.JEE7.JEE7Package#getEntityAttribute()
 * @model
 * @generated
 */
public interface EntityAttribute extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.codeling.lang.jee7.mm.JEE7.JEE7Package#getEntityAttribute_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.codeling.lang.jee7.mm.JEE7.EntityAttribute#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Is Many</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Many</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Many</em>' attribute.
	 * @see #setIsMany(boolean)
	 * @see org.codeling.lang.jee7.mm.JEE7.JEE7Package#getEntityAttribute_IsMany()
	 * @model
	 * @generated
	 */
	boolean isIsMany();

	/**
	 * Sets the value of the '{@link org.codeling.lang.jee7.mm.JEE7.EntityAttribute#isIsMany <em>Is Many</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Many</em>' attribute.
	 * @see #isIsMany()
	 * @generated
	 */
	void setIsMany(boolean value);

	/**
	 * Returns the value of the '<em><b>Primitive Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Primitive Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Primitive Type</em>' attribute.
	 * @see #setPrimitiveType(String)
	 * @see org.codeling.lang.jee7.mm.JEE7.JEE7Package#getEntityAttribute_PrimitiveType()
	 * @model required="true"
	 * @generated
	 */
	String getPrimitiveType();

	/**
	 * Sets the value of the '{@link org.codeling.lang.jee7.mm.JEE7.EntityAttribute#getPrimitiveType <em>Primitive Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Primitive Type</em>' attribute.
	 * @see #getPrimitiveType()
	 * @generated
	 */
	void setPrimitiveType(String value);

} // EntityAttribute
