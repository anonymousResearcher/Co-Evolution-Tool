/**
 */
package org.codeling.lang.jee7.mm.JEE7;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.Operation#getName <em>Name</em>}</li>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.Operation#getParameters <em>Parameters</em>}</li>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.Operation#getReturnType <em>Return Type</em>}</li>
 * </ul>
 *
 * @see org.codeling.lang.jee7.mm.JEE7.JEE7Package#getOperation()
 * @model
 * @generated
 */
public interface Operation extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.codeling.lang.jee7.mm.JEE7.JEE7Package#getOperation_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.codeling.lang.jee7.mm.JEE7.Operation#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link org.codeling.lang.jee7.mm.JEE7.OperationParameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference list.
	 * @see org.codeling.lang.jee7.mm.JEE7.JEE7Package#getOperation_Parameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<OperationParameter> getParameters();

	/**
	 * Returns the value of the '<em><b>Return Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Return Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Return Type</em>' containment reference.
	 * @see #setReturnType(OperationParameter)
	 * @see org.codeling.lang.jee7.mm.JEE7.JEE7Package#getOperation_ReturnType()
	 * @model containment="true"
	 * @generated
	 */
	OperationParameter getReturnType();

	/**
	 * Sets the value of the '{@link org.codeling.lang.jee7.mm.JEE7.Operation#getReturnType <em>Return Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Return Type</em>' containment reference.
	 * @see #getReturnType()
	 * @generated
	 */
	void setReturnType(OperationParameter value);

} // Operation
