# JEE 7 Integration for Codeling
These plugins build a language binding for JEE 7 Codeling.
This is the basis for the case study JACK 3. It includes a meta model and model integration concept transformations for
* EJB beans,
* CDI beans,
* Java ServerFaces beans,
* their operations,
* their interrelationships,
* entity beans,
* their attributes,
* their references,
* deployed archives, and
* namespaces