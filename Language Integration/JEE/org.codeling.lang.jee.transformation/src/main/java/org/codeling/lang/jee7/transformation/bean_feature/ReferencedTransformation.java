package org.codeling.lang.jee7.transformation.bean_feature;

import java.util.List;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.Bean;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.references.AnnotatedMemberReferenceTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;

public class ReferencedTransformation<OWNINGBEANTYPE extends Bean, TARGETBEANTYPE extends Bean>
		extends AnnotatedMemberReferenceTransformation<OWNINGBEANTYPE, TARGETBEANTYPE> {

	public ReferencedTransformation(ClassMechanismTransformation<OWNINGBEANTYPE, IType> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getBean_Referenced());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return null;
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean hasExpectedAnnotation(IField field) {
		return ASTUtils.hasAnnotation(field, "javax.ejb.EJB", "javax.inject.Inject");
	}

	@Override
	protected String getNewAnnotationName() {
		return "javax.inject.Inject";
	}

}
