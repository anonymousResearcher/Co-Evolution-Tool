package org.codeling.lang.jee7.transformation;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.lang.jee7.mm.JEE7.OperationParameter;
import org.codeling.lang.jee7.transformation.operationparameter_feature.EntityTypeTransformation;
import org.codeling.lang.jee7.transformation.operationparameter_feature.IsManyTransformation;
import org.codeling.lang.jee7.transformation.operationparameter_feature.PrimitiveTypeTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.ILocalVariable;

public class OperationParameterTransformation extends ClassMechanismTransformation<OperationParameter, ILocalVariable> {

	public OperationParameterTransformation(
			AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getOperationParameter());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new IsManyTransformation(this));
		result.add(new PrimitiveTypeTransformation(this));
		result.add(new EntityTypeTransformation(this));
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new IsManyTransformation(this));
		result.add(new PrimitiveTypeTransformation(this));
		result.add(new EntityTypeTransformation(this));
	}

	public boolean hasExpectedAnnotation(IField field) {
		return true;
	}

	protected String getNewAnnotationName() {
		return null;
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public OperationParameter transformToModel() throws CodelingException {
		String name = codeElement.getElementName();
		modelElement = (OperationParameter) eClass.getEPackage().getEFactoryInstance().create(eClass);
		setNameAttributeValue(modelElement, name);
		return modelElement;
	}

}
