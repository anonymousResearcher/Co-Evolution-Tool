package org.codeling.lang.jee7.transformation.entityattribute_feature;

import java.text.MessageFormat;
import java.util.List;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.EntityAttribute;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.mechanisms.transformations.AttributeMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.Signature;

public class IsManyTransformation extends AttributeMechanismTransformation<EntityAttribute, IField> {

	public IsManyTransformation(AbstractModelCodeTransformation<? extends EObject, IField> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getEntityAttribute_IsMany());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public EntityAttribute transformToModel() throws CodelingException {
		String signature;
		try {
			signature = codeElement.getTypeSignature();
		} catch (JavaModelException e) {
			throw new CodelingException(MessageFormat.format("Could not get type signature.", e));
		}
		boolean isArray = Signature.getTypeSignatureKind(signature) == Signature.ARRAY_TYPE_SIGNATURE;
		if (isArray) { // Shortcut to avoid unnecessary newSupertypeHierarchy below.
			modelElement.eSet(eAttribute, true);
			return modelElement;
		}

		boolean isCollection = false;
		try {
			// If it is a primitive or void it is not a collection
			if (Signature.getTypeSignatureKind(signature) != Signature.BASE_TYPE_SIGNATURE) {
				IType fieldType = ASTUtils.getType(Signature.toString(Signature.getTypeErasure(signature)),
						((IType) codeElement.getParent()));
				for (IType t : fieldType.newSupertypeHierarchy(null).getAllSupertypes(fieldType)) {
					if (t.getFullyQualifiedName().equals("java.util.Collection")) {
						isCollection = true;
						break;
					}
				}
			}
		} catch (JavaModelException | IllegalArgumentException e) {
			throw new CodelingException("Could not get a type hierarchy.", e);
		}
		modelElement.eSet(eAttribute, isCollection);
		return modelElement;
	}
}
