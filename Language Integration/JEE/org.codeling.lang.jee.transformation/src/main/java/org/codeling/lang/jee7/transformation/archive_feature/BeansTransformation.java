package org.codeling.lang.jee7.transformation.archive_feature;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.Archive;
import org.codeling.lang.jee7.mm.JEE7.Bean;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.references.ProjectReferenceTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;

public class BeansTransformation extends ProjectReferenceTransformation<Archive, Bean> {

	public BeansTransformation(ClassMechanismTransformation<Archive, IJavaProject> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getArchive_Beans());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return null;
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
	}
}