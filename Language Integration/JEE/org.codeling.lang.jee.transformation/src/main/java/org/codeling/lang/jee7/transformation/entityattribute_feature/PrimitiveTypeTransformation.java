package org.codeling.lang.jee7.transformation.entityattribute_feature;

import java.text.MessageFormat;
import java.util.List;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.EntityAttribute;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.mechanisms.transformations.AttributeMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.JavaModelException;

public class PrimitiveTypeTransformation extends AttributeMechanismTransformation<EntityAttribute, IField> {

	public PrimitiveTypeTransformation(
			AbstractModelCodeTransformation<? extends EObject, IField> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getEntityAttribute_PrimitiveType());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public EntityAttribute transformToModel() throws CodelingException {
		String signature;
		try {
			signature = codeElement.getTypeSignature();
			signature = ASTUtils.resolveTypeSignature(signature, codeElement.getDeclaringType());
		} catch (JavaModelException e) {
			throw new CodelingException(MessageFormat.format("Could not get the root type signature.", e));
		}

		modelElement.eSet(eAttribute, signature);
		return modelElement;
	}
}
