package org.codeling.lang.jee7.transformation;

import java.util.List;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.EntityAttribute;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.lang.jee7.transformation.entityattribute_feature.IsManyTransformation;
import org.codeling.lang.jee7.transformation.entityattribute_feature.PrimitiveTypeTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

public class EntityAttributeTransformation extends ClassMechanismTransformation<EntityAttribute, IField> {

	public EntityAttributeTransformation(
			AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getEntityAttribute());
	}

	@Override
	public IField resolveCodeElement() throws CodelingException {
		try {
			for (IField f : ((IType) parentTransformation.getCodeElement()).getFields())
				if (f.getElementName().equals(modelElement.getName())) {
					codeElement = f;
					return f;
				}
			return codeElement;
		} catch (JavaModelException e) {
			throw new CodelingException("Could not resolve EntityAttribute", e);
		}
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new IsManyTransformation(this));
		result.add(new PrimitiveTypeTransformation(this));
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new IsManyTransformation(this));
		result.add(new PrimitiveTypeTransformation(this));
	}

	public boolean hasExpectedAnnotation(IField field) {
		return ASTUtils.hasAnnotation(field, "javax.persistence.Entity", "javax.persistence.Id");
	}

	protected String getNewAnnotationName() {
		return "javax.persistence.Entity";
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public EntityAttribute transformToModel() throws CodelingException {
		String name = codeElement.getElementName();
		modelElement = (EntityAttribute) eClass.getEPackage().getEFactoryInstance().create(eClass);
		setNameAttributeValue(modelElement, name);
		return modelElement;
	}

}
