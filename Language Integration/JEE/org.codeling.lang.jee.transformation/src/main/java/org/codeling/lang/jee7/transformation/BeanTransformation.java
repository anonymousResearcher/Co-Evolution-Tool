package org.codeling.lang.jee7.transformation;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.Bean;
import org.codeling.lang.jee7.transformation.bean_feature.OperationsTransformation;
import org.codeling.lang.jee7.transformation.bean_feature.ReferencedTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;

/**
 * This is a helper class for adding child transformations of all concrete
 * subclasses ob the {@link EClass} {@link Bean}.
 */
public class BeanTransformation<BEANTYPE extends Bean> {

	public void doCreateCrossReferencesTransformations(
			ClassMechanismTransformation<BEANTYPE, IType> actualTransformation,
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new ReferencedTransformation<BEANTYPE, Bean>(actualTransformation));
	}

	void doCreateChildTransformationsToCode(ClassMechanismTransformation<BEANTYPE, IType> actualTransformation,
			List<AbstractModelCodeTransformation<?, ?>> result) {
		result.add(new OperationsTransformation<BEANTYPE>(actualTransformation));

	}

	void doCreateChildTransformationsToModel(ClassMechanismTransformation<BEANTYPE, IType> actualTransformation,
			List<AbstractModelCodeTransformation<?, ?>> result) {
		result.add(new OperationsTransformation<BEANTYPE>(actualTransformation));
	}

}
