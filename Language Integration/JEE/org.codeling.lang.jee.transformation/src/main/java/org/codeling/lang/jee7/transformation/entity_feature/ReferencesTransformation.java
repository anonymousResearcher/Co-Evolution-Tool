package org.codeling.lang.jee7.transformation.entity_feature;

import java.util.List;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.Entity;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.references.AnnotatedMemberReferenceTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;

public class ReferencesTransformation extends AnnotatedMemberReferenceTransformation<Entity, Entity> {

	public ReferencesTransformation(ClassMechanismTransformation<Entity, IType> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getEntity_References());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean hasExpectedAnnotation(IField field) {
		return ASTUtils.hasAnnotation(field, "javax.persistence.ManyToOne", "javax.persistence.ManyToMany",
				"javax.persistence.OneToMany", "javax.persistence.OneToOne");
	}

	@Override
	protected String getNewAnnotationName() {
		return eReference.isMany() ? "javax.persistence.OneToMany" : "javax.persistence.OneToOne";
	}
}
