package org.codeling.lang.jee7.transformation.bean_feature;

import java.text.MessageFormat;
import java.util.List;

import org.codeling.lang.base.java.ProfilesUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.base.java.transformation.references.IALHolder;
import org.codeling.lang.base.java.transformation.references.IALTransformation;
import org.codeling.lang.jee7.mm.JEE7.Operation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.ReferenceMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.codeling.utils.Models;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.modelversioning.emfprofile.Stereotype;
import org.modelversioning.emfprofileapplication.StereotypeApplication;

import de.mkonersmann.il.core.ComponentType;
import de.mkonersmann.il.core.Interface;
import de.mkonersmann.il.core.Provision;
import de.mkonersmann.il.profiles.Profiles;

public class ChildOperationTransformation
		extends ReferenceMechanismTransformation<StereotypeApplication, Operation, IType>
		implements IALTransformation<StereotypeApplication, IType> {

	private IALHolder holder = new IALHolder();

	public ChildOperationTransformation(ChildTypeTransformation childTypeTransformation) {
		super(null,
				ProfilesUtils.getEReference(
						ProfilesUtils.getStereotype("OperationInterface", Profiles.INTERFACES_TYPE_OPERATIONS.load()),
						"operations"));
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return null;
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub

	}

	@Override
	public IALHolder getIALHolder() {
		return holder;
	}

	@Override
	public StereotypeApplication resolveTranslatedIALElement(EObject foundationalElement) {
		EList<StereotypeApplication> appliedStereotypes = ProfilesUtils.getAppliedStereotypes(foundationalElement);
		Stereotype scopedComponent = ProfilesUtils.getStereotype("HierarchicalComponentTypeScoped",
				Profiles.COMPONENTS_HIERARCHY_SCOPED.load());
		for (StereotypeApplication app : appliedStereotypes) {
			if (app.getStereotype() == scopedComponent) {
				return app;
			}
		}
		return null;
	}

	@Override
	public boolean codeFragmentExists() {
		return true;
	}

	protected String getNewAnnotationName() {
		// No annotation
		return null;
	}

	@Override
	public void setCodeElement(IType codeElement) {
		// TODO Auto-generated method stub

	}

	@Override
	public IType resolveCodeElement() throws CodelingException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IType getCodeElement() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		updateCodeFragments();
	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		// For every child component, create delegation operations for provided
		// operations of child components.
		List<EObject> children = Models.getTargetsAsList(modelElement, ProfilesUtils.getEReference(
				Profiles.COMPONENTS_HIERARCHY_SCOPED.load(), "HierarchicalComponentTypeScoped", "childTypes"));
		createProvidedOperationRecursively(children, null);
	}

	private void createProvidedOperationRecursively(List<EObject> children, ComponentType topmostProvidingComponent) {
		for (EObject child : children) {
			ComponentType ct = (ComponentType) child;
			for (StereotypeApplication app : ProfilesUtils.getAppliedStereotypes(child)) {
				if (!app.getStereotype().getName().equals("HierarchicalComponentTypeScoped"))
					continue;
				for (Provision prov : ((ComponentType) child).getProvidedInterfaces()) {
					Interface iface = prov.getInterface();
					for (StereotypeApplication operationInterface : ProfilesUtils.getAppliedStereotypes(iface)) {
						if (!operationInterface.getStereotype().getName().equals("OperationInterface"))
							continue;

						List<EObject> operations = Models.getTargetsAsList(operationInterface,
								ProfilesUtils.getEReference(operationInterface.getStereotype(), "operations"));
						createDelegatingOperation(operations,
								topmostProvidingComponent != null ? topmostProvidingComponent : ct);
					}
				}
				List<EObject> sub_children = (List<EObject>) app
						.eGet(ProfilesUtils.getEReference(app.getStereotype(), "childTypes"));
				createProvidedOperationRecursively(sub_children,
						topmostProvidingComponent != null ? topmostProvidingComponent : ct);
			}
		}
	}

	private void createDelegatingOperation(List<EObject> targets, ComponentType componentType) {
		IType type = (IType) holder.getIALCodeElement();
		for (EObject target : targets) {
			if (type.getMethod(getNameAttributeValue(target), new String[0]).exists()) {
				// Method already exists. Do nothing
			} else {
				// Method does not exist. Create a new one.
				String targetName = getNameAttributeValue(target);

				// For presentation purposes. This is currently only able to handle the name of
				// Annotated Member References (Single) references.
				String content = "public void " + targetName.substring(0, 1).toLowerCase() + targetName.substring(1)
						+ "(){" + componentType.getName().substring(0, 1).toLowerCase()
						+ componentType.getName().substring(1) + "." + targetName.substring(0, 1).toLowerCase()
						+ targetName.substring(1) + "();" + "}";

				try {
					type.createMethod(content, null, true, null);
				} catch (JavaModelException e) {
					addWarning(MessageFormat.format("Could not add method [{0}] to [{1}].", target,
							codeElement.getElementName()), e);
				}
			}
		}
	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		updateCodeFragments();
	}

	@Override
	public StereotypeApplication transformToModel() throws CodelingException {
		// TODO Auto-generated method stub
		return null;
	}
}
