package org.codeling.lang.jee7.transformation.operation_feature;

import java.text.MessageFormat;
import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.lang.jee7.mm.JEE7.Operation;
import org.codeling.lang.jee7.mm.JEE7.OperationParameter;
import org.codeling.lang.jee7.transformation.OperationParameterTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.ReferenceMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.ILocalVariable;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.internal.core.JavaElement;
import org.eclipse.jdt.internal.core.LocalVariable;

public class ReturnTypeTransformation extends ReferenceMechanismTransformation<Operation, OperationParameter, IMethod> {

	OperationParameterTransformation returnTypeTransformation = null;

	public ReturnTypeTransformation(ClassMechanismTransformation<Operation, IMethod> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getOperation_ReturnType());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return new OperationParameterTransformation(this);
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		try {
			ILocalVariable r = new LocalVariable((JavaElement) codeElement, null, 0, 0, 0, 0,
					codeElement.getReturnType(), null, 0, true);
			returnTypeTransformation = new OperationParameterTransformation(this);
			returnTypeTransformation.setCodeElement(r);
			result.add(returnTypeTransformation);
		} catch (JavaModelException e) {
			addError(MessageFormat.format("Could not get parameters of method [{0}].[{1}]",
					codeElement.getDeclaringType().getElementName(), codeElement.getElementName()));
		}
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public Operation transformToModel() throws CodelingException {
		final OperationParameter target = returnTypeTransformation.getModelElement();
		modelElement.eSet(eReference, target);
		return modelElement;
	}
}
