package org.codeling.lang.jee7.transformation.facesbean_feature;

import java.util.List;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.FacesBean;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.mechanisms.transformations.AttributeMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;

public class IsSessionScopedTransformation extends AttributeMechanismTransformation<FacesBean, IType> {

	public IsSessionScopedTransformation(
			AbstractModelCodeTransformation<? extends EObject, IType> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getFacesBean_IsSessionScoped());
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public FacesBean transformToModel() throws CodelingException {
		boolean isSessionScoped = ASTUtils.hasAnnotation((IType) parentCodeElement,
				"javax.faces.view.SessionScoped");
		modelElement.eSet(JEE7Package.eINSTANCE.getFacesBean_IsSessionScoped(), isSessionScoped);
		return modelElement;
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}
}
