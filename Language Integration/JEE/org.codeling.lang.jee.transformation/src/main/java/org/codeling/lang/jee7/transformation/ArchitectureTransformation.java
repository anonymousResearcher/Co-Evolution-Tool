package org.codeling.lang.jee7.transformation;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.Architecture;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.lang.jee7.transformation.architecture_feature.ArchivesTransformation;
import org.codeling.lang.jee7.transformation.architecture_feature.BeansTransformation;
import org.codeling.lang.jee7.transformation.architecture_feature.EntitiesTransformation;
import org.codeling.lang.jee7.transformation.architecture_feature.NamespacesTransformation;
import org.codeling.mechanisms.transformations.classes.NinjaSingletonTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;

public class ArchitectureTransformation extends NinjaSingletonTransformation<Architecture> {

	public ArchitectureTransformation() {
		super(null, JEE7Package.eINSTANCE.getArchitecture());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// Child transformations for cross references
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {

		// Child transformations for containment references
		result.add(new BeansTransformation(this));
		result.add(new EntitiesTransformation(this));
		result.add(new ArchivesTransformation(this));
		result.add(new NamespacesTransformation(this));
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {

		// Child transformations for containment references
		result.add(new BeansTransformation(this));
		result.add(new EntitiesTransformation(this));
		result.add(new ArchivesTransformation(this));
		result.add(new NamespacesTransformation(this));
	}

}