package org.codeling.lang.jee7.transformation;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.Archive;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.lang.jee7.transformation.archive_feature.BeansTransformation;
import org.codeling.lang.jee7.transformation.archive_feature.EntitiesTransformation;
import org.codeling.mechanisms.transformations.classes.ProjectTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;

public class ArchiveTransformation extends ProjectTransformation<Archive> {

	public ArchiveTransformation(
			AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getArchive());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new BeansTransformation(this));
		result.add(new EntitiesTransformation(this));
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {

	}

}