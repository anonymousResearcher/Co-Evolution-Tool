package org.codeling.lang.jee7.transformation;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.lang.jee7.mm.JEE7.Operation;
import org.codeling.lang.jee7.transformation.operation_feature.ReturnTypeTransformation;
import org.codeling.mechanisms.transformations.classes.ContainmentOperationTargetTransformation;
import org.codeling.mechanisms.transformations.references.ContainmentOperationTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;

public class OperationTransformation extends ContainmentOperationTargetTransformation<Operation> {

	public OperationTransformation(
			ContainmentOperationTransformation<? extends EObject, Operation> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getOperation());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// Removed. See below
//		 result.add(new ParametersTransformation(this));
		 result.add(new ReturnTypeTransformation(this));
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// Removed because the resulting model is too big for HenshinTGG
//		 result.add(new ParametersTransformation(this));
		 result.add(new ReturnTypeTransformation(this));
	}

}
