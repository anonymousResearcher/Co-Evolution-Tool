package org.codeling.lang.jee7.transformation;

import java.util.List;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.lang.jee7.mm.JEE7.SessionBean;
import org.codeling.lang.jee7.transformation.sessionbean_feature.IsSingletonTransformation;
import org.codeling.lang.jee7.transformation.sessionbean_feature.IsStatefulTransformation;
import org.codeling.lang.jee7.transformation.sessionbean_feature.IsStatelessTransformation;
import org.codeling.mechanisms.transformations.classes.TypeAnnotationTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;

public class SessionBeanTransformation extends TypeAnnotationTransformation<SessionBean> {

	public SessionBeanTransformation(
			AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getSessionBean());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		new BeanTransformation<SessionBean>().doCreateCrossReferencesTransformations(this, result);
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		new BeanTransformation<SessionBean>().doCreateChildTransformationsToCode(this, result);

		// Child transformations for attributes
		result.add(new IsStatelessTransformation(this));
		result.add(new IsStatefulTransformation(this));
		result.add(new IsSingletonTransformation(this));

	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		new BeanTransformation<SessionBean>().doCreateChildTransformationsToModel(this, result);

		
		// Child transformations for attributes
		IsStatelessTransformation isStateless = new IsStatelessTransformation(this);
		isStateless.setModelElement(modelElement);
		isStateless.setPriorModelElement(priorModelElement);
		result.add(isStateless);

		IsStatefulTransformation isStateful = new IsStatefulTransformation(this);
		isStateful.setModelElement(modelElement);
		isStateful.setPriorModelElement(priorModelElement);
		result.add(isStateful);

		IsSingletonTransformation isSingleton = new IsSingletonTransformation(this);
		isSingleton.setModelElement(modelElement);
		isSingleton.setPriorModelElement(priorModelElement);
		result.add(isSingleton);

	}

	@Override
	public boolean hasExpectedAnnotation(IType type) {
		return ASTUtils.hasAnnotation(type, "javax.ejb.Stateful", "javax.ejb.Stateless", "javax.ejb.Singleton");
	}

	@Override
	protected String getNewAnnotationName() {
		if (modelElement.isIsStateful())
			return "javax.ejb.Statefule";
		else if (modelElement.isIsSingleton())
			return "javax.ejb.Singleton";
		else
			return "javax.ejb.Stateless";
	}
}
