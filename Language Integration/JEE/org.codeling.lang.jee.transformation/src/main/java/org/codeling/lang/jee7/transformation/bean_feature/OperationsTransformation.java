package org.codeling.lang.jee7.transformation.bean_feature;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.Bean;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.lang.jee7.mm.JEE7.Operation;
import org.codeling.lang.jee7.transformation.OperationTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.references.ContainmentOperationTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;

public class OperationsTransformation<BEANTYPE extends Bean>
		extends ContainmentOperationTransformation<BEANTYPE, Operation> {

	public OperationsTransformation(ClassMechanismTransformation<BEANTYPE, IType> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getBean_Operations());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return new OperationTransformation(this);
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		resolveCodeElement();
		for (IMethod m : methods) {
			OperationTransformation t = new OperationTransformation(this);
			t.setCodeElement(m);
			result.add(t);
		}
	}

	@Override
	public boolean hasExpectedAnnotation(IMethod method) {
		// The AIL uses no annotation
		return true;
	}

	@Override
	protected String getNewAnnotationName() {
		// The AIL uses no annotation
		return null;
	}
}
