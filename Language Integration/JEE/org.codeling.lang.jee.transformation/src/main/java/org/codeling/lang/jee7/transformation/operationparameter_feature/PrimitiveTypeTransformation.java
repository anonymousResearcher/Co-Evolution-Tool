package org.codeling.lang.jee7.transformation.operationparameter_feature;

import java.text.MessageFormat;
import java.util.List;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.lang.jee7.mm.JEE7.OperationParameter;
import org.codeling.mechanisms.transformations.AttributeMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.ILocalVariable;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

public class PrimitiveTypeTransformation extends AttributeMechanismTransformation<OperationParameter, ILocalVariable> {

	public PrimitiveTypeTransformation(
			AbstractModelCodeTransformation<? extends EObject, ILocalVariable> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getOperationParameter_PrimitiveType());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public OperationParameter transformToModel() throws CodelingException {
		String signature;
		try {
			signature = codeElement.getTypeSignature();
			signature = ASTUtils.resolveTypeSignature(signature, codeElement.getDeclaringMember().getDeclaringType());
			IType parameterType = ASTUtils.getType(signature, codeElement.getDeclaringMember().getDeclaringType());
			List<AbstractModelCodeTransformation<?, ?>> t = findTranslatedElements.getTransformationsFor(parameterType);
			if (!t.isEmpty())
				return modelElement; // The target is already translated as entity. It should be handled by
										// EntityTypeTransformation
		} catch (JavaModelException e) {
			throw new CodelingException(MessageFormat.format("Could not get the root type signature.", e));
		}

		modelElement.eSet(eAttribute, signature);
		return modelElement;
	}
}
