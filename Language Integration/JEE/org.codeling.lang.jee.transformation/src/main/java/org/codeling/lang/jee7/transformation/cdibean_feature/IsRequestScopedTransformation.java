package org.codeling.lang.jee7.transformation.cdibean_feature;

import java.util.List;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.CDIBean;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.mechanisms.transformations.AttributeMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;

public class IsRequestScopedTransformation extends AttributeMechanismTransformation<CDIBean, IType> {

	public IsRequestScopedTransformation(
			AbstractModelCodeTransformation<? extends EObject, IType> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getCDIBean_IsRequestScoped());
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public CDIBean transformToModel() throws CodelingException {
		boolean isRequestScoped = ASTUtils.hasAnnotation((IType) parentCodeElement,
				"javax.enterprise.context.RequestScoped");
		modelElement.eSet(JEE7Package.eINSTANCE.getCDIBean_IsRequestScoped(), isRequestScoped);
		return modelElement;
	}


	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

}
