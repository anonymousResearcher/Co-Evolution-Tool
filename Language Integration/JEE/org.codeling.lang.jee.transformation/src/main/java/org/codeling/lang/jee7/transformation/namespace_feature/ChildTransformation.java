package org.codeling.lang.jee7.transformation.namespace_feature;

import java.util.List;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.lang.jee7.mm.JEE7.Namespace;
import org.codeling.lang.jee7.transformation.NamespaceTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.references.NamespaceHierarchyChildTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IPackageFragment;

public class ChildTransformation extends NamespaceHierarchyChildTransformation<Namespace> {

	public ChildTransformation(
			ClassMechanismTransformation<Namespace, IPackageFragment> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getNamespace_Child());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub

	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return new NamespaceTransformation(this);
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		try {
			String thisName = codeElement.getElementName();
			String[] thisNameSegments = thisName.split("\\.");
			for (IPackageFragment pf : ASTUtils.getAllDistinctChildPackageFragmentsOfAll(codeRoot)) {
				String childName = pf.getElementName();
				String[] childNameSegments = childName.split("\\.");
				if (childNameSegments.length == thisNameSegments.length + 1 && childName.startsWith(thisName)) {
					// Only direct children
					NamespaceTransformation t = new NamespaceTransformation(this);
					t.setCodeElement(pf);
					result.add(t);
				}
			}
		} catch (Exception e) {
			addError("Error while getting packageFragments", e);
		}
	}

}
