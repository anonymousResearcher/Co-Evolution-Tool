package org.codeling.lang.jee7.transformation.operationparameter_feature;

import java.text.MessageFormat;
import java.util.List;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.base.java.transformation.IModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.Entity;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.lang.jee7.mm.JEE7.OperationParameter;
import org.codeling.lang.jee7.transformation.EntityTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.ReferenceMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.ILocalVariable;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

public class EntityTypeTransformation
		extends ReferenceMechanismTransformation<OperationParameter, Entity, ILocalVariable> {

	public EntityTypeTransformation(
			ClassMechanismTransformation<OperationParameter, ILocalVariable> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getOperationParameter_EntityType());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}
	
	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return null;
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public OperationParameter transformToModel() throws CodelingException {
		String signature;
		try {
			signature = codeElement.getTypeSignature();
			signature = ASTUtils.resolveTypeSignature(signature, codeElement.getDeclaringMember().getDeclaringType());
			IType parameterType = ASTUtils.getType(signature, codeElement.getDeclaringMember().getDeclaringType());
			List<AbstractModelCodeTransformation<?, ?>> transformations = findTranslatedElements
					.getTransformationsFor(parameterType);

			Entity target = null;
			if (!transformations.isEmpty()) {
				for (IModelCodeTransformation<?, ?> t : transformations) {
					if (t instanceof EntityTransformation) {
						target = ((EntityTransformation) t).getModelElement();
						break;
					}
				}
			}
			modelElement.eSet(eReference, target);
			return modelElement;
		} catch (JavaModelException e) {
			throw new CodelingException(MessageFormat.format("Could not get the root type signature.", e));
		}
	}

}
