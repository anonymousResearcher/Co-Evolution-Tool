package org.codeling.lang.jee7.transformation.architecture_feature;

import java.util.List;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.Architecture;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.lang.jee7.mm.JEE7.Namespace;
import org.codeling.lang.jee7.transformation.NamespaceTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.references.NinjaSingletonContainmentTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IPackageFragment;

public class NamespacesTransformation extends NinjaSingletonContainmentTransformation<Architecture, Namespace> {

	public NamespacesTransformation(ClassMechanismTransformation<Architecture, IJavaElement> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getArchitecture_Namespaces());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub

	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return new NamespaceTransformation(this);
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		try {
			for (IPackageFragment pf : ASTUtils.getDistinctDirectChildPackageFragmentsOfAll(codeRoot)) {
				if (pf.isDefaultPackage())
					continue; // Ignore the default package.
				NamespaceTransformation t = new NamespaceTransformation(this);
				t.setCodeElement(pf);
				result.add(t);
			}
		} catch (Exception e) {
			String projectName = "(no project name available)";
			if (codeRoot != null)
				projectName = codeElement.getElementName();
			addError("Error while getting packageFragments in project " + projectName, e);
		}
	}

}
