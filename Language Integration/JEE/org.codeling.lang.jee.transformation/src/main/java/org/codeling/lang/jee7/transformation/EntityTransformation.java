package org.codeling.lang.jee7.transformation;

import java.util.List;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.Entity;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.lang.jee7.transformation.entity_feature.AttributesTransformation;
import org.codeling.lang.jee7.transformation.entity_feature.OperationsTransformation;
import org.codeling.lang.jee7.transformation.entity_feature.ReferencesTransformation;
import org.codeling.mechanisms.transformations.classes.TypeAnnotationTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;

public class EntityTransformation extends TypeAnnotationTransformation<Entity> {

	public EntityTransformation(
			AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getEntity());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new ReferencesTransformation(this));
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new AttributesTransformation(this));
		result.add(new OperationsTransformation(this));
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new AttributesTransformation(this));
		result.add(new OperationsTransformation(this));
	}

	final String annotationName = "javax.persistence.Entity";

	@Override
	public boolean hasExpectedAnnotation(IType type) {
		return ASTUtils.hasAnnotation(type, annotationName);
	}

	@Override
	protected String getNewAnnotationName() {
		return annotationName;
	}
}
