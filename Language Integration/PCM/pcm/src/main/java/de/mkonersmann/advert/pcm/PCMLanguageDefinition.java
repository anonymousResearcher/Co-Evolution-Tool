package de.mkonersmann.advert.pcm;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.codeling.lang.base.modeltrans.henshintgg.HenshinTGGBasedLanguageDefinition;
import org.codeling.lang.base.modeltrans.henshintgg.HenshinTGGTransformation;
import org.codeling.lang.base.modeltrans.henshintgg.RecoverPriorModelElements;
import org.codeling.lang.base.modeltrans.henshintgg.TGGDirection;
import org.codeling.transformationmanager.TransformationResult;
import org.codeling.utils.CodelingConfiguration;
import org.codeling.utils.CodelingException;
import org.codeling.utils.IDRegistry;
import org.codeling.utils.Models;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.gmf.runtime.diagram.core.services.ViewService;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.osgi.framework.FrameworkUtil;
import org.palladiosimulator.pcm.core.composition.AssemblyConnector;
import org.palladiosimulator.pcm.core.composition.AssemblyContext;
import org.palladiosimulator.pcm.core.composition.Connector;
import org.palladiosimulator.pcm.repository.OperationProvidedRole;
import org.palladiosimulator.pcm.repository.OperationRequiredRole;
import org.palladiosimulator.pcm.repository.ProvidedRole;
import org.palladiosimulator.pcm.repository.Repository;
import org.palladiosimulator.pcm.repository.RepositoryComponent;
import org.palladiosimulator.pcm.repository.RequiredRole;
import org.palladiosimulator.pcm.system.System;

import de.uka.ipd.sdq.identifier.Identifier;
import de.uka.ipd.sdq.pcm.gmf.composite.part.PalladioComponentModelComposedStructureDiagramEditorPlugin;
import de.uka.ipd.sdq.pcm.gmf.repository.part.PalladioComponentModelRepositoryDiagramEditorPlugin;

/**
 * @author Anonymous Researcher <anonymous@example.org>
 *
 */
public class PCMLanguageDefinition extends HenshinTGGBasedLanguageDefinition {

	static final URI henshinTGGFileURI = URI.createPlatformPluginURI(
			"/" + FrameworkUtil.getBundle(PCMLanguageDefinition.class).getSymbolicName() + "/pcm2il.henshin", true);

	public PCMLanguageDefinition() {
		super(henshinTGGFileURI);
	}

	// The PCM henshin TGG transformations were defined with the PCM as source and
	// the IAL as target language. The HenshinTGGBasedLanguageDefinitionHelper
	// assumes the other direction. Therefore the helper cannot be used here.
	public TransformationResult transformToSM(List<EObject> ilRoots, IDRegistry idRegistry, IProgressMonitor monitor) {
		final HenshinTGGTransformation task = new HenshinTGGTransformation(this, henshinTGGFileURI,
				"Transforming IL to ADL", TGGDirection.BACKWARD, idRegistry, ilRoots);
		TransformationResult result = task.execute(monitor);
		task.propagateIDsFromRegistryToModel();
		return result;
	}

	@Override
	public TransformationResult transformToTM(List<EObject> adlModelRoots, List<EObject> preChangeILRoots,
			IDRegistry idRegistry, IProgressMonitor monitor) {
		// Execute =Forward Propagation=
		monitor.subTask(String.format("Executing TGG Task =FWPPG="));
		final HenshinTGGTransformation bwppgTask = new HenshinTGGTransformation(this,
				henshinTGGFileURI, "FWPPG Rules ADL to IL", TGGDirection.FORWARD_PROPAGATION, idRegistry, adlModelRoots);
		final TransformationResult result = bwppgTask.execute(monitor);

		// Delete the id registry entries of deleted elements
		List<String> updatedIDs = bwppgTask.propagateIDsFromModelToRegistry();
		Set<String> deletedIDs = idRegistry.getAllIds();
		deletedIDs.removeAll(updatedIDs);
		idRegistry.deleteEntries(deletedIDs);

		// Recover elements lost during the translation
		// TODO file name coupled with ADL2ILHandler. Decouple somehow
		Models.store(result.getModelRoots(), CodelingConfiguration.DEBUG_MODELDIR_PATH + "6_adl2il.xmi");
		new RecoverPriorModelElements().recoverAll(preChangeILRoots, result.getModelRoots(),
				this, result.getIdRegistry(), idRegistry);

		return result;
	}

	@Override
	public void transformationCompleteHook(List<EObject> modelRoots, IProgressMonitor monitor) {
		try {
			createSeparateRepositoryAndSystem(modelRoots);
		} catch (final CodelingException | IOException e) {
			addError("Error while seperating PCM repository and system", e);
		}
	}

	@Override
	public void transformationToCodePreStartHook(String modelFilepath, IProgressMonitor monitor) {
		try {
			aggregateRepositoryAndSystem(modelFilepath);
		} catch (IOException e) {
			addError("Error while aggregating PCM repository and system.", e);
		}
	}

	private void aggregateRepositoryAndSystem(String modelFilepath) throws IOException {
		final ResourceSet rSet = new ResourceSetImpl();
		final Resource aggregatedResource = rSet.getResource(URI.createPlatformResourceURI(modelFilepath, true), true);
		aggregatedResource.getContents().clear();

		final Resource repositoryResource = rSet
				.getResource(URI.createPlatformResourceURI(modelFilepath + ".repository", true), true);
		aggregatedResource.getContents().addAll(repositoryResource.getContents());

		final Resource systemResource = rSet.getResource(URI.createPlatformResourceURI(modelFilepath + ".system", true),
				true);
		aggregatedResource.getContents().addAll(systemResource.getContents());
		aggregatedResource.save(Collections.EMPTY_MAP);

		fixReferencesFromSystemToRepository(find(System.class, aggregatedResource.getContents()),
				find(Repository.class, aggregatedResource.getContents()));
		aggregatedResource.save(Collections.EMPTY_MAP);
	}

	private void fixReferencesFromSystemToRepository(System system, Repository repository) {
		for (final AssemblyContext context : system.getAssemblyContexts__ComposedStructure()) {
			final RepositoryComponent referencedComponent = context.getEncapsulatedComponent__AssemblyContext();
			if (referencedComponent != null) {
				final RepositoryComponent localComponent = find(repository, referencedComponent);
				context.setEncapsulatedComponent__AssemblyContext(localComponent);
			}
		}

		for (final Connector connector : system.getConnectors__ComposedStructure()) {
			final AssemblyConnector assemblyConnector = (AssemblyConnector) connector;
			final ProvidedRole providedRole = assemblyConnector.getProvidedRole_AssemblyConnector();
			final RequiredRole requiredRole = assemblyConnector.getRequiredRole_AssemblyConnector();

			if (providedRole != null) {
				final ProvidedRole localProvidedRole = find(repository, providedRole);
				assemblyConnector.setProvidedRole_AssemblyConnector((OperationProvidedRole) localProvidedRole);
			}

			if (requiredRole != null) {
				final RequiredRole localRequiredRole = find(repository, requiredRole);
				assemblyConnector.setRequiredRole_AssemblyConnector((OperationRequiredRole) localRequiredRole);
			}
		}

	}

	private ProvidedRole find(Repository repository, ProvidedRole referencedProvidedRole) {
		final String originalId = getProxiedID(referencedProvidedRole);
		for (final RepositoryComponent component : repository.getComponents__Repository())
			for (final ProvidedRole providedRole : component.getProvidedRoles_InterfaceProvidingEntity())
				if (providedRole.getId().equals(originalId))
					return providedRole;

		return null;
	}

	private RequiredRole find(Repository repository, RequiredRole referencedRequiredRole) {
		final String originalId = getProxiedID(referencedRequiredRole);
		for (final RepositoryComponent component : repository.getComponents__Repository())
			for (final RequiredRole requiredRole : component.getRequiredRoles_InterfaceRequiringEntity())
				if (requiredRole.getId().equals(originalId))
					return requiredRole;

		return null;
	}

	private RepositoryComponent find(Repository repository, RepositoryComponent referencedComponent) {
		final String originalId = getProxiedID(referencedComponent);
		for (final RepositoryComponent component : repository.getComponents__Repository()) {
			if (component.getId().equals(originalId))
				return component;
		}
		return null;
	}

	private String getProxiedID(EObject eobject) {
		return ((MinimalEObjectImpl) eobject).eProxyURI().fragment();
	}

	private void createSeparateRepositoryAndSystem(List<EObject> modelRoots) throws CodelingException, IOException {
		if (modelRoots.isEmpty())
			throw new CodelingException(
					"Cannot create a PCM Repository or System, because the ADL model roots are empty.");

		final Repository repository = find(Repository.class, modelRoots);
		if (repository == null)
			throw new CodelingException("Expected a PCM Repository, but none exists in "
					+ modelRoots.get(0).eResource().getURI().toString());

		moveToNewResource(repository, repository.eResource().getURI().appendFileExtension("repository"));
		createDiagram(repository);

		final System system = find(System.class, modelRoots);
		if (system == null)
			throw new CodelingException(
					"Expected a PCM System, but none exists in " + modelRoots.get(0).eResource().getURI().toString());
		moveToNewResource(system, system.eResource().getURI().appendFileExtension("system"));
		createDiagram(system);
	}

	@SuppressWarnings("unchecked")
	private <T extends EObject> T find(Class<T> classToFind, List<EObject> modelRoots) {
		for (final EObject root : modelRoots)
			if (classToFind.isAssignableFrom(root.getClass()))
				return (T) root;
		return null;
	}

	private void moveToNewResource(EObject eObject, URI targetURI) {
		final Resource resource = eObject.eResource().getResourceSet().createResource(targetURI);
		resource.getContents().add(eObject);
		try {
			resource.save(Collections.EMPTY_MAP);
		} catch (final IOException e) {
			addError("Could not move an eobject to a new resource", e);
		}
	}

	private void createDiagram(System system) throws IOException {
		final Resource modelResource = system.eResource();
		final Resource diagramResource = modelResource.getResourceSet()
				.createResource(modelResource.getURI().appendFileExtension("system_diagram"));

		final Diagram diagram = ViewService.createDiagram(system, "CompositeModel",
				PalladioComponentModelComposedStructureDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);
		diagramResource.getContents().add(diagram);
		diagramResource.save(Collections.EMPTY_MAP);
	}

	private void createDiagram(Repository repository) throws IOException {
		final Resource modelResource = repository.eResource();
		final Resource diagramResource = modelResource.getResourceSet()
				.createResource(modelResource.getURI().appendFileExtension("repository_diagram"));

		final Diagram diagram = ViewService.createDiagram(repository, "PCM Repository Model",
				PalladioComponentModelRepositoryDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);
		diagramResource.getContents().add(diagram);
		diagramResource.save(Collections.EMPTY_MAP);
	}

	public String getID(EObject eobject) {
		if (eobject instanceof Identifier)
			return ((Identifier) eobject).getId();

		// TODO: Replace with something more helpful
		throw new IllegalArgumentException(
				String.format("Tried to get an id from an EObject %s, but the EObject is not of the type Identifier.",
						eobject.eClass().getName()));
	}

	public void setID(EObject eobject, String id) {
		if (!(eobject instanceof Identifier))
			// TODO: Replace with something more helpful
			throw new IllegalArgumentException(
					String.format("Tried to set id %s to EObject %s, but the EObject is not of the type Identifier.",
							id, eobject.eClass().getName()));

		((Identifier) eobject).setId(id);
	}
}
