package org.codeling.mechanism.runtime.common;

import java.lang.reflect.InvocationTargetException;

import org.codeling.mechanism.runtime.common.Logging.LogLevel;

public abstract class TypeMechanismRuntime<T> extends IntegrationMechanismRuntime {

	protected T instance;

	public void initializeContainments() throws IntegratedModelException {
		// May be overridden
		Logging.log(LogLevel.INFRASTRUCTURE, "Inizializing Containments of " + this.getClass().getSimpleName());
	}

	public void initializeCrossReferences() throws IntegratedModelException {
		// May be overridden
		Logging.log(LogLevel.INFRASTRUCTURE, "Inizializing Cross References of " + this.getClass().getSimpleName());
	}

	protected void invokeMethod(String methodName, Object callee) throws IntegratedModelException {
		try {
			callee.getClass().getMethod(methodName).invoke(callee);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new IntegratedModelException("Could not find method [" + methodName + "] on the object " + callee);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new IntegratedModelException("Could not invoke method [" + methodName + "] on the object " + callee);
		}
	}

	public T getInstance() {
		return instance;
	}

}
