package org.codeling.mechanism.runtime;

import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;

/**
 * The Package Hierarchy mechanism employs no structure that can be
 * instantiated, and no entry point for individual execution semantics. As
 * packages cannot be type checked or instantiated, it suffices to use strings
 * for their representation. When classes, interfaces, or annotations are
 * considered, their owning packages can be used via
 * element.getPackage().getName().
 */
public class PackageHierarchyRuntime extends TypeMechanismRuntime<Object> {

}
