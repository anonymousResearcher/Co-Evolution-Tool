package org.codeling.mechanism.runtime.common;

import java.util.LinkedHashMap;

/**
 * This class maps translated objects to their mechanism. This can only contain
 * runtimes for elements that are translated to types.
 */
public class Runtimes extends LinkedHashMap<Object, IntegrationMechanismRuntime> {

	private static final long serialVersionUID = 9206022584679103071L;
	private static Runtimes INSTANCE;

	private Runtimes() {
	}

	public static Runtimes getInstance() {
		if (INSTANCE == null)
			INSTANCE = new Runtimes();
		return INSTANCE;
	}

	public IntegrationMechanismRuntime get(Class<?> type) {
		for (Object o : keySet()) {
			if (type.isAssignableFrom(o.getClass())) {
				return get(o);
			}
		}
		return null;
	}
	
	@Override
	public IntegrationMechanismRuntime put(Object key, IntegrationMechanismRuntime value) {
		return super.put(key, value);
	}
}
