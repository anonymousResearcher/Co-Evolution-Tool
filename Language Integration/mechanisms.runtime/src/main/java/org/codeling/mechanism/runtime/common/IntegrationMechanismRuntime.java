package org.codeling.mechanism.runtime.common;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.text.MessageFormat;

import org.codeling.mechanism.runtime.common.Logging.LogLevel;

public abstract class IntegrationMechanismRuntime {

	public void initialize() throws IntegratedModelException {
		// May be overridden
		Logging.log(LogLevel.INFRASTRUCTURE, "Inizializing "+this.getClass().getSimpleName());
	}
	
	// Below are utility methods
	protected static <T, I> boolean isInterfaceOf(Class<I> markerInterfaceClass, Class<T> implementingClass)
			throws IntegratedModelException {
		if (!markerInterfaceClass.isInterface()) {
			if (implementingClass.isInterface())
				throw new IntegratedModelException(MessageFormat.format(
						"[Warning] interfaceOf(type, interface) called with swapped parameters: ([{0}], [{1}]). I am now fixing it, but please correct it.",
						markerInterfaceClass.getCanonicalName(), implementingClass.getCanonicalName()));
			else
				throw new IntegratedModelException(MessageFormat.format(
						"Cannot determine if [{0}] is implementing [{1}], because [{1}] is not an interface..",
						implementingClass.getCanonicalName(), markerInterfaceClass.getCanonicalName()));
		}
		final Class<?>[] interfaces = implementingClass.getInterfaces();
		for (final Class<?> i : interfaces)
			if (i == markerInterfaceClass)
				return true;
		return false;
	}

	protected boolean isSubinterfaceOf(Class<?> subtype, Class<?> supertype) {
		if (!(subtype.isInterface() && supertype.isInterface()))
			return false;

		final Class<?>[] superInterfaces = subtype.getInterfaces();
		for (final Class<?> i : superInterfaces)
			if (i == supertype)
				return true;
		return false;
	}

	protected boolean hasAnnotation(Class<?> typeOrInterface, Class<? extends Annotation> annotation) {
		return typeOrInterface.isAnnotationPresent(annotation);
	}

	protected static void set(Object object, Field field, Object value) throws IntegratedModelException {
		field.setAccessible(true);
		try {
			field.set(object, field.getType().cast(value));
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new IntegratedModelException(MessageFormat
					.format("Could not set the value of the field [{0}] to the value [{1}].", field, value), e);
		}
		field.setAccessible(false);
	}

}
