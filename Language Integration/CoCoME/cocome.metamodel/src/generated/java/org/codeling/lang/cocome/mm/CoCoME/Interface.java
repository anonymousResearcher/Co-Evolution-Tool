/**
 */
package org.codeling.lang.cocome.mm.CoCoME;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.codeling.lang.cocome.mm.CoCoME.Interface#getOperations <em>Operations</em>}</li>
 * </ul>
 *
 * @see org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage#getInterface()
 * @model
 * @generated
 */
public interface Interface extends Name {
	/**
	 * Returns the value of the '<em><b>Operations</b></em>' containment reference list.
	 * The list contents are of type {@link org.codeling.lang.cocome.mm.CoCoME.Operation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations</em>' containment reference list.
	 * @see org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage#getInterface_Operations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Operation> getOperations();

} // Interface
