/**
 */
package org.codeling.lang.cocome.mm.CoCoME.impl;

import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.mm.CoCoME.Operation;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OperationImpl extends NameImpl implements Operation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoCoMEPackage.Literals.OPERATION;
	}

} //OperationImpl
