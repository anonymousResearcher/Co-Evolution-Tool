/**
 */
package org.codeling.lang.cocome.mm.CoCoME;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.codeling.lang.cocome.mm.CoCoME.CoCoMEFactory
 * @model kind="package"
 * @generated
 */
public interface CoCoMEPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "CoCoME";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://CoCoME/";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "CoCoME";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CoCoMEPackage eINSTANCE = org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.NameImpl <em>Name</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.NameImpl
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getName_()
	 * @generated
	 */
	int NAME = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME__NAME = 0;

	/**
	 * The number of structural features of the '<em>Name</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Name</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.ComponentImpl <em>Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.ComponentImpl
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getComponent()
	 * @generated
	 */
	int COMPONENT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__NAME = NAME__NAME;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__CHILDREN = NAME_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Provides</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__PROVIDES = NAME_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Requires</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__REQUIRES = NAME_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Events</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__EVENTS = NAME_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Handled</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__HANDLED = NAME_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Dispatched</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__DISPATCHED = NAME_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_FEATURE_COUNT = NAME_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_OPERATION_COUNT = NAME_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.StateImpl <em>State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.StateImpl
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getState()
	 * @generated
	 */
	int STATE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__NAME = NAME__NAME;

	/**
	 * The number of structural features of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_FEATURE_COUNT = NAME_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_OPERATION_COUNT = NAME_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.ServerImpl <em>Server</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.ServerImpl
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getServer()
	 * @generated
	 */
	int SERVER = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER__NAME = COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER__CHILDREN = COMPONENT__CHILDREN;

	/**
	 * The feature id for the '<em><b>Provides</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER__PROVIDES = COMPONENT__PROVIDES;

	/**
	 * The feature id for the '<em><b>Requires</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER__REQUIRES = COMPONENT__REQUIRES;

	/**
	 * The feature id for the '<em><b>Events</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER__EVENTS = COMPONENT__EVENTS;

	/**
	 * The feature id for the '<em><b>Handled</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER__HANDLED = COMPONENT__HANDLED;

	/**
	 * The feature id for the '<em><b>Dispatched</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER__DISPATCHED = COMPONENT__DISPATCHED;

	/**
	 * The feature id for the '<em><b>Transfer Objects</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER__TRANSFER_OBJECTS = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Server</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Server</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.TransferObjectImpl <em>Transfer Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.TransferObjectImpl
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getTransferObject()
	 * @generated
	 */
	int TRANSFER_OBJECT = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFER_OBJECT__NAME = NAME__NAME;

	/**
	 * The number of structural features of the '<em>Transfer Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFER_OBJECT_FEATURE_COUNT = NAME_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Transfer Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFER_OBJECT_OPERATION_COUNT = NAME_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.ArchitectureImpl <em>Architecture</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.ArchitectureImpl
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getArchitecture()
	 * @generated
	 */
	int ARCHITECTURE = 5;

	/**
	 * The feature id for the '<em><b>Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCHITECTURE__COMPONENTS = 0;

	/**
	 * The number of structural features of the '<em>Architecture</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCHITECTURE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Architecture</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCHITECTURE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.InterfaceImpl <em>Interface</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.InterfaceImpl
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getInterface()
	 * @generated
	 */
	int INTERFACE = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__NAME = NAME__NAME;

	/**
	 * The feature id for the '<em><b>Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__OPERATIONS = NAME_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_FEATURE_COUNT = NAME_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_OPERATION_COUNT = NAME_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.OperationImpl <em>Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.OperationImpl
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getOperation()
	 * @generated
	 */
	int OPERATION = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__NAME = NAME__NAME;

	/**
	 * The number of structural features of the '<em>Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_FEATURE_COUNT = NAME_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_OPERATION_COUNT = NAME_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.ModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.ModelImpl
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getModel()
	 * @generated
	 */
	int MODEL = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__NAME = COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__CHILDREN = COMPONENT__CHILDREN;

	/**
	 * The feature id for the '<em><b>Provides</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__PROVIDES = COMPONENT__PROVIDES;

	/**
	 * The feature id for the '<em><b>Requires</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__REQUIRES = COMPONENT__REQUIRES;

	/**
	 * The feature id for the '<em><b>Events</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__EVENTS = COMPONENT__EVENTS;

	/**
	 * The feature id for the '<em><b>Handled</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__HANDLED = COMPONENT__HANDLED;

	/**
	 * The feature id for the '<em><b>Dispatched</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__DISPATCHED = COMPONENT__DISPATCHED;

	/**
	 * The feature id for the '<em><b>States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__STATES = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Initial State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__INITIAL_STATE = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__TRANSITIONS = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.ConsoleImpl <em>Console</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.ConsoleImpl
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getConsole()
	 * @generated
	 */
	int CONSOLE = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSOLE__NAME = COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSOLE__CHILDREN = COMPONENT__CHILDREN;

	/**
	 * The feature id for the '<em><b>Provides</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSOLE__PROVIDES = COMPONENT__PROVIDES;

	/**
	 * The feature id for the '<em><b>Requires</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSOLE__REQUIRES = COMPONENT__REQUIRES;

	/**
	 * The feature id for the '<em><b>Events</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSOLE__EVENTS = COMPONENT__EVENTS;

	/**
	 * The feature id for the '<em><b>Handled</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSOLE__HANDLED = COMPONENT__HANDLED;

	/**
	 * The feature id for the '<em><b>Dispatched</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSOLE__DISPATCHED = COMPONENT__DISPATCHED;

	/**
	 * The number of structural features of the '<em>Console</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSOLE_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Console</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSOLE_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.TransitionImpl <em>Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.TransitionImpl
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getTransition()
	 * @generated
	 */
	int TRANSITION = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__NAME = NAME__NAME;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TARGET = NAME_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_FEATURE_COUNT = NAME_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_OPERATION_COUNT = NAME_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.EventImpl <em>Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.EventImpl
	 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getEvent()
	 * @generated
	 */
	int EVENT = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__NAME = NAME__NAME;

	/**
	 * The number of structural features of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FEATURE_COUNT = NAME_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OPERATION_COUNT = NAME_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.codeling.lang.cocome.mm.CoCoME.Component <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Component
	 * @generated
	 */
	EClass getComponent();

	/**
	 * Returns the meta object for the containment reference list '{@link org.codeling.lang.cocome.mm.CoCoME.Component#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Children</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Component#getChildren()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_Children();

	/**
	 * Returns the meta object for the containment reference list '{@link org.codeling.lang.cocome.mm.CoCoME.Component#getProvides <em>Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Provides</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Component#getProvides()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_Provides();

	/**
	 * Returns the meta object for the reference list '{@link org.codeling.lang.cocome.mm.CoCoME.Component#getRequires <em>Requires</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Requires</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Component#getRequires()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_Requires();

	/**
	 * Returns the meta object for the containment reference list '{@link org.codeling.lang.cocome.mm.CoCoME.Component#getEvents <em>Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Events</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Component#getEvents()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_Events();

	/**
	 * Returns the meta object for the reference list '{@link org.codeling.lang.cocome.mm.CoCoME.Component#getHandled <em>Handled</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Handled</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Component#getHandled()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_Handled();

	/**
	 * Returns the meta object for the reference list '{@link org.codeling.lang.cocome.mm.CoCoME.Component#getDispatched <em>Dispatched</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Dispatched</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Component#getDispatched()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_Dispatched();

	/**
	 * Returns the meta object for class '{@link org.codeling.lang.cocome.mm.CoCoME.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.State
	 * @generated
	 */
	EClass getState();

	/**
	 * Returns the meta object for class '{@link org.codeling.lang.cocome.mm.CoCoME.Name <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Name</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Name
	 * @generated
	 */
	EClass getName_();

	/**
	 * Returns the meta object for the attribute '{@link org.codeling.lang.cocome.mm.CoCoME.Name#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Name#getName()
	 * @see #getName_()
	 * @generated
	 */
	EAttribute getName_Name();

	/**
	 * Returns the meta object for class '{@link org.codeling.lang.cocome.mm.CoCoME.Server <em>Server</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Server</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Server
	 * @generated
	 */
	EClass getServer();

	/**
	 * Returns the meta object for the containment reference list '{@link org.codeling.lang.cocome.mm.CoCoME.Server#getTransferObjects <em>Transfer Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Transfer Objects</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Server#getTransferObjects()
	 * @see #getServer()
	 * @generated
	 */
	EReference getServer_TransferObjects();

	/**
	 * Returns the meta object for class '{@link org.codeling.lang.cocome.mm.CoCoME.TransferObject <em>Transfer Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transfer Object</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.TransferObject
	 * @generated
	 */
	EClass getTransferObject();

	/**
	 * Returns the meta object for class '{@link org.codeling.lang.cocome.mm.CoCoME.Architecture <em>Architecture</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Architecture</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Architecture
	 * @generated
	 */
	EClass getArchitecture();

	/**
	 * Returns the meta object for the containment reference list '{@link org.codeling.lang.cocome.mm.CoCoME.Architecture#getComponents <em>Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Components</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Architecture#getComponents()
	 * @see #getArchitecture()
	 * @generated
	 */
	EReference getArchitecture_Components();

	/**
	 * Returns the meta object for class '{@link org.codeling.lang.cocome.mm.CoCoME.Interface <em>Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interface</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Interface
	 * @generated
	 */
	EClass getInterface();

	/**
	 * Returns the meta object for the containment reference list '{@link org.codeling.lang.cocome.mm.CoCoME.Interface#getOperations <em>Operations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operations</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Interface#getOperations()
	 * @see #getInterface()
	 * @generated
	 */
	EReference getInterface_Operations();

	/**
	 * Returns the meta object for class '{@link org.codeling.lang.cocome.mm.CoCoME.Operation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Operation
	 * @generated
	 */
	EClass getOperation();

	/**
	 * Returns the meta object for class '{@link org.codeling.lang.cocome.mm.CoCoME.Model <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Model
	 * @generated
	 */
	EClass getModel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.codeling.lang.cocome.mm.CoCoME.Model#getStates <em>States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>States</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Model#getStates()
	 * @see #getModel()
	 * @generated
	 */
	EReference getModel_States();

	/**
	 * Returns the meta object for the reference '{@link org.codeling.lang.cocome.mm.CoCoME.Model#getInitialState <em>Initial State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Initial State</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Model#getInitialState()
	 * @see #getModel()
	 * @generated
	 */
	EReference getModel_InitialState();

	/**
	 * Returns the meta object for the containment reference list '{@link org.codeling.lang.cocome.mm.CoCoME.Model#getTransitions <em>Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Transitions</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Model#getTransitions()
	 * @see #getModel()
	 * @generated
	 */
	EReference getModel_Transitions();

	/**
	 * Returns the meta object for class '{@link org.codeling.lang.cocome.mm.CoCoME.Console <em>Console</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Console</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Console
	 * @generated
	 */
	EClass getConsole();

	/**
	 * Returns the meta object for class '{@link org.codeling.lang.cocome.mm.CoCoME.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Transition
	 * @generated
	 */
	EClass getTransition();

	/**
	 * Returns the meta object for the reference '{@link org.codeling.lang.cocome.mm.CoCoME.Transition#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Transition#getTarget()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Target();

	/**
	 * Returns the meta object for class '{@link org.codeling.lang.cocome.mm.CoCoME.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event</em>'.
	 * @see org.codeling.lang.cocome.mm.CoCoME.Event
	 * @generated
	 */
	EClass getEvent();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CoCoMEFactory getCoCoMEFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.ComponentImpl <em>Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.ComponentImpl
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getComponent()
		 * @generated
		 */
		EClass COMPONENT = eINSTANCE.getComponent();

		/**
		 * The meta object literal for the '<em><b>Children</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__CHILDREN = eINSTANCE.getComponent_Children();

		/**
		 * The meta object literal for the '<em><b>Provides</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__PROVIDES = eINSTANCE.getComponent_Provides();

		/**
		 * The meta object literal for the '<em><b>Requires</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__REQUIRES = eINSTANCE.getComponent_Requires();

		/**
		 * The meta object literal for the '<em><b>Events</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__EVENTS = eINSTANCE.getComponent_Events();

		/**
		 * The meta object literal for the '<em><b>Handled</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__HANDLED = eINSTANCE.getComponent_Handled();

		/**
		 * The meta object literal for the '<em><b>Dispatched</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__DISPATCHED = eINSTANCE.getComponent_Dispatched();

		/**
		 * The meta object literal for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.StateImpl <em>State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.StateImpl
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getState()
		 * @generated
		 */
		EClass STATE = eINSTANCE.getState();

		/**
		 * The meta object literal for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.NameImpl <em>Name</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.NameImpl
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getName_()
		 * @generated
		 */
		EClass NAME = eINSTANCE.getName_();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAME__NAME = eINSTANCE.getName_Name();

		/**
		 * The meta object literal for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.ServerImpl <em>Server</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.ServerImpl
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getServer()
		 * @generated
		 */
		EClass SERVER = eINSTANCE.getServer();

		/**
		 * The meta object literal for the '<em><b>Transfer Objects</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVER__TRANSFER_OBJECTS = eINSTANCE.getServer_TransferObjects();

		/**
		 * The meta object literal for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.TransferObjectImpl <em>Transfer Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.TransferObjectImpl
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getTransferObject()
		 * @generated
		 */
		EClass TRANSFER_OBJECT = eINSTANCE.getTransferObject();

		/**
		 * The meta object literal for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.ArchitectureImpl <em>Architecture</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.ArchitectureImpl
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getArchitecture()
		 * @generated
		 */
		EClass ARCHITECTURE = eINSTANCE.getArchitecture();

		/**
		 * The meta object literal for the '<em><b>Components</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARCHITECTURE__COMPONENTS = eINSTANCE.getArchitecture_Components();

		/**
		 * The meta object literal for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.InterfaceImpl <em>Interface</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.InterfaceImpl
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getInterface()
		 * @generated
		 */
		EClass INTERFACE = eINSTANCE.getInterface();

		/**
		 * The meta object literal for the '<em><b>Operations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE__OPERATIONS = eINSTANCE.getInterface_Operations();

		/**
		 * The meta object literal for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.OperationImpl <em>Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.OperationImpl
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getOperation()
		 * @generated
		 */
		EClass OPERATION = eINSTANCE.getOperation();

		/**
		 * The meta object literal for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.ModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.ModelImpl
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getModel()
		 * @generated
		 */
		EClass MODEL = eINSTANCE.getModel();

		/**
		 * The meta object literal for the '<em><b>States</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL__STATES = eINSTANCE.getModel_States();

		/**
		 * The meta object literal for the '<em><b>Initial State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL__INITIAL_STATE = eINSTANCE.getModel_InitialState();

		/**
		 * The meta object literal for the '<em><b>Transitions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL__TRANSITIONS = eINSTANCE.getModel_Transitions();

		/**
		 * The meta object literal for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.ConsoleImpl <em>Console</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.ConsoleImpl
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getConsole()
		 * @generated
		 */
		EClass CONSOLE = eINSTANCE.getConsole();

		/**
		 * The meta object literal for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.TransitionImpl <em>Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.TransitionImpl
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getTransition()
		 * @generated
		 */
		EClass TRANSITION = eINSTANCE.getTransition();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__TARGET = eINSTANCE.getTransition_Target();

		/**
		 * The meta object literal for the '{@link org.codeling.lang.cocome.mm.CoCoME.impl.EventImpl <em>Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.EventImpl
		 * @see org.codeling.lang.cocome.mm.CoCoME.impl.CoCoMEPackageImpl#getEvent()
		 * @generated
		 */
		EClass EVENT = eINSTANCE.getEvent();

	}

} //CoCoMEPackage
