/**
 */
package org.codeling.lang.cocome.mm.CoCoME;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transfer Object</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage#getTransferObject()
 * @model
 * @generated
 */
public interface TransferObject extends Name {
} // TransferObject
