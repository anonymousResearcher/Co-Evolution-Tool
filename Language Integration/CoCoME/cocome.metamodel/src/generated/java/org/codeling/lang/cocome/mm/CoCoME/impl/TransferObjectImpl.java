/**
 */
package org.codeling.lang.cocome.mm.CoCoME.impl;

import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.mm.CoCoME.TransferObject;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transfer Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TransferObjectImpl extends NameImpl implements TransferObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransferObjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoCoMEPackage.Literals.TRANSFER_OBJECT;
	}

} //TransferObjectImpl
