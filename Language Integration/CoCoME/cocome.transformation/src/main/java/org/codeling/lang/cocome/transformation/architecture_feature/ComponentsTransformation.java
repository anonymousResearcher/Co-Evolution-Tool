package org.codeling.lang.cocome.transformation.architecture_feature;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.cocome.mm.CoCoME.Architecture;
import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.mm.CoCoME.Component;
import org.codeling.lang.cocome.transformation.ComponentTransformation;
import org.codeling.lang.cocome.transformation.ConsoleTransformation;
import org.codeling.lang.cocome.transformation.ModelTransformation;
import org.codeling.lang.cocome.transformation.ServerTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.references.NinjaSingletonContainmentTransformation;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

public class ComponentsTransformation extends NinjaSingletonContainmentTransformation<Architecture, Component> {

	public ComponentsTransformation(ClassMechanismTransformation<Architecture, IJavaElement> parentTransformation) {
		super(parentTransformation, CoCoMEPackage.eINSTANCE.getArchitecture_Components());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub

	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		if (modelElement.eClass() == CoCoMEPackage.eINSTANCE.getServer())
			return new ServerTransformation(this);
		else if (modelElement.eClass() == CoCoMEPackage.eINSTANCE.getConsole())
			return new ConsoleTransformation(this);
		else
			return new ModelTransformation(this);
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		try {
			IPackageFragment mainPackageFragment = ASTUtils.getPackageFragment("org.cocome.tradingsystem", codeRoot);
			for (IPackageFragment f : findSubpackageSubcomponents(mainPackageFragment)) {
				ComponentTransformation componentT = new ComponentTransformation(this);
				if (componentT.isComponent(f)) {
					componentT.setCodeElement(f);
					result.add(componentT);
				}
			}

			for (IPackageFragment f : ASTUtils.getDirectChildPackageFragmentsOf(mainPackageFragment)) {
				for (IType type : ComponentsTransformation.findTypeSubcomponents(f)) {
					// Is a direct subpackage of this type's package
					AbstractModelCodeTransformation<?, IType> t = null;
					ServerTransformation serverT = new ServerTransformation(this);
					ConsoleTransformation consoleT = new ConsoleTransformation(this);
					ModelTransformation modelT = new ModelTransformation(this);

					if (serverT.isServer(type))
						t = serverT;
					else if (consoleT.isConsole(type))
						t = consoleT;
					else if (modelT.isModel(type))
						t = modelT;
					else
						continue;

					t.setCodeElement(type);
					result.add(t);
				}
			}

		} catch (Exception e) {
			String projectName = "(no project name available)";
			if (codeRoot != null)
				projectName = codeElement.getElementName();
			addError("Error while getting types in project " + projectName, e);
		}
	}

	// Overwriting transformToModel, because the implementation in the supertype
	// requires the targets to be marker interfaces or type annotations
	@Override
	public Architecture transformToModel() throws CodelingException {
		if (modelElement == null)
			throw new IllegalStateException(
					"modelElement must not be null. Please set the field value to the object that owns the reference.");

		try {
			IPackageFragment mainPackageFragment = ASTUtils.getPackageFragment("org.cocome.tradingsystem", codeRoot);
			List<String> targets = new LinkedList<>();
			targets.addAll(ComponentsTransformation.findSubpackageSubcomponents(mainPackageFragment).stream()
					.map(f -> f.getElementName()).collect(Collectors.toList()));
			targets.addAll(ComponentsTransformation.findTypeSubcomponents(mainPackageFragment).stream().map(t -> {
				String name = t.getElementName();
				if (name.endsWith("Model"))
					return name.substring(0, "Model".length());
				else if (name.endsWith("Server"))
					return name.substring(0, "Server".length());
				else if (name.endsWith("Console"))
					return name.substring(0, "Console".length());
				return name;
			}).collect(Collectors.toList()));

			List<Component> targetElements = getTargetObjects(modelElement, eReference, targets);
			if (eReference.isMany()) {
				((List<Component>) modelElement.eGet(eReference)).addAll(targetElements);
			} else {
				modelElement.eSet(eReference, targetElements.get(0));
			}
		} catch (JavaModelException e) {
			addError("Error while finding subpackge components.", e);
		}

		return modelElement;
	}

	/**
	 * Returns every package fragment that is a subcomponent itself
	 * 
	 * @param fragment
	 * @return
	 * @throws JavaModelException
	 */
	public static List<IPackageFragment> findSubpackageSubcomponents(IPackageFragment fragment)
			throws JavaModelException {
		List<IPackageFragment> result = new LinkedList<>();
		if (fragment.hasSubpackages()) {
			List<IPackageFragment> subPackages = ASTUtils.getDirectChildPackageFragmentsOf(fragment);
			for (IPackageFragment f : subPackages)
				// Is a direct subpackage of this type's package
				if (new ComponentTransformation(null).isComponent(f)) {
					result.add(f);
				}
		}
		return result;
	}

	public static List<IType> findTypeSubcomponents(IPackageFragment fragment) throws JavaModelException {
		List<IType> result = new LinkedList<>();
		for (IType type : ASTUtils.getTypes(fragment)) {
			if (!type.isClass())
				continue;

			if (new ServerTransformation(null).isServer(type) || new ConsoleTransformation(null).isConsole(type)
					|| new ModelTransformation(null).isModel(type))
				result.add(type);
		}
		return result;
	}
}
