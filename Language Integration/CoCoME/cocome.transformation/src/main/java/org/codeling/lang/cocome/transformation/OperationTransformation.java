package org.codeling.lang.cocome.transformation;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.mm.CoCoME.Operation;
import org.codeling.lang.cocome.transformation.interface_feature.OperationsTransformation;
import org.codeling.mechanisms.transformations.classes.ContainmentOperationTargetTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;

public class OperationTransformation extends ContainmentOperationTargetTransformation<Operation> {

	public OperationTransformation(OperationsTransformation parentTransformation) {
		super(parentTransformation, CoCoMEPackage.eINSTANCE.getOperation());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {

	}

}