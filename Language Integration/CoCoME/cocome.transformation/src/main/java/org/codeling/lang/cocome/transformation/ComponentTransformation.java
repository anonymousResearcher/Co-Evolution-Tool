package org.codeling.lang.cocome.transformation;

import java.util.List;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.cocome.mm.CoCoME.CoCoMEFactory;
import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.mm.CoCoME.Component;
import org.codeling.lang.cocome.transformation.component_feature.ChildrenTransformation;
import org.codeling.lang.cocome.transformation.component_feature.EventsTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

public class ComponentTransformation extends ClassMechanismTransformation<Component, IPackageFragment> {

	public ComponentTransformation(
			AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation) {
		super(parentTransformation, CoCoMEPackage.eINSTANCE.getComponent());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new ChildrenTransformation<Component, Component, IPackageFragment>(this));
		result.add(new EventsTransformation<Component, IPackageFragment>(this));
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new ChildrenTransformation<Component, Component, IPackageFragment>(this));
		result.add(new EventsTransformation<Component, IPackageFragment>(this));
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public Component transformToModel() throws CodelingException {
		modelElement = CoCoMEFactory.eINSTANCE.createComponent();
		modelElement.setName(codeElement.getElementName().substring(codeElement.getElementName().lastIndexOf(".") + 1));
		return modelElement;
	}

	public boolean isComponent(IPackageFragment fragment) throws JavaModelException {
		// Packages that contain components are components themselves, when they are
		// below "org.cocome.tradingsystem".
		if (!fragment.getElementName().startsWith("org.cocome.tradingsystem")) {
			log.warning("The fragment " + fragment.getElementName()
					+ " is NOT a component, because it is out of the root package scope.", null);
			return false;
		}

		boolean hasSubcomponentPackages = false;
		for (IPackageFragment child : ASTUtils.getDirectChildPackageFragmentsOf(fragment))
			if (isComponent(child) || hasComponentTypes(child)) {
				hasSubcomponentPackages = true;
				break;
			}

		boolean hasATypeThatRepresentsAComponent = hasComponentTypes(fragment);

		if (hasSubcomponentPackages && hasATypeThatRepresentsAComponent)
			return false;
		if (hasSubcomponentPackages && !hasATypeThatRepresentsAComponent)
			return true;
		if (!hasSubcomponentPackages && hasATypeThatRepresentsAComponent)
			return false;
		if (!hasSubcomponentPackages && !hasATypeThatRepresentsAComponent)
			return false;
		return false;
	}

	private boolean hasComponentTypes(IPackageFragment fragment) throws JavaModelException {
		for (ICompilationUnit u : fragment.getCompilationUnits()) {
			IType t = u.getType(u.getElementName().substring(0, u.getElementName().length() - ".java".length()));
			if (new ConsoleTransformation(null).isConsole(t) || new ServerTransformation(null).isServer(t)
					|| new ModelTransformation(null).isModel(t)) {
				return true;
			}
		}
		return false;
	}

}