package org.codeling.lang.cocome.transformation.component_feature;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.mm.CoCoME.Component;
import org.codeling.lang.cocome.mm.CoCoME.Event;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.ReferenceMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

public class DispatchedTransformation<COMPONENTTYPE extends Component, OWNERCLASS extends IJavaElement>
		extends ReferenceMechanismTransformation<COMPONENTTYPE, Event, OWNERCLASS> {

	public DispatchedTransformation(
			ClassMechanismTransformation<COMPONENTTYPE, OWNERCLASS> parentTransformation) {
		super(parentTransformation, CoCoMEPackage.eINSTANCE.getComponent_Dispatched());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return null;
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public COMPONENTTYPE transformToModel() throws CodelingException {
		try {
			LinkedList<String> dispatchedEvents = new LinkedList<String>();
			if (codeElement instanceof IType) { // Package Fragment Components do not dispatch events
				IType type = (IType) codeElement;
				String source = type.getSource();
				String matchToFind = "new (.*)Event\\(.*\\)";
				Pattern pattern = Pattern.compile(matchToFind);
				Matcher match = pattern.matcher(source);

				while (match.find()) {
					dispatchedEvents.add(match.group(1));
				}

				List<Event> targetElements = getTargetObjects(modelElement, eReference, dispatchedEvents);
				((List<Event>) modelElement.eGet(eReference)).addAll(targetElements);
			}
		} catch (JavaModelException e) {
			addError("Could not transform reference Architecture.events to model representation.", e);
		}
		return modelElement;
	}
}
