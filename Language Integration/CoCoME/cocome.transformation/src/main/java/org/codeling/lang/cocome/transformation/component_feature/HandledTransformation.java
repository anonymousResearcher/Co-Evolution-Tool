package org.codeling.lang.cocome.transformation.component_feature;

import java.util.LinkedList;
import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.mm.CoCoME.Component;
import org.codeling.lang.cocome.mm.CoCoME.Event;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.ReferenceMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.Signature;

public class HandledTransformation<COMPONENTTYPE extends Component,  OWNERCLASS extends IJavaElement>
		extends ReferenceMechanismTransformation<COMPONENTTYPE, Event, OWNERCLASS> {

	public HandledTransformation(ClassMechanismTransformation<COMPONENTTYPE, OWNERCLASS> parentTransformation) {
		super(parentTransformation, CoCoMEPackage.eINSTANCE.getComponent_Handled());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return null;
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public COMPONENTTYPE transformToModel() throws CodelingException {
		try {
			LinkedList<String> handledEvents = new LinkedList<String>();
			if (codeElement instanceof IType) { // Package Fragment Components do not dispatch events
				IType type = (IType) codeElement;
				String shortName = "";
				if (modelElement.getName().endsWith("Model"))
					shortName = modelElement.getName().substring(0, modelElement.getName().indexOf("Model"));
				else if (modelElement.getName().endsWith("Server"))
					shortName = modelElement.getName().substring(0, modelElement.getName().indexOf("Server"));
				else if (modelElement.getName().endsWith("Console"))
					shortName = modelElement.getName().substring(0, modelElement.getName().indexOf("Console"));
				else
					shortName = modelElement.getName();
				String consumerName = "I" + shortName + "EventConsumer";
				ICompilationUnit cu = type.getPackageFragment().getCompilationUnit(consumerName + ".java");
				if (!cu.exists())
					return modelElement;
				IType consumer = cu.getType(consumerName);
				if (!consumer.exists())
					return modelElement;

				for (IMethod m : consumer.getMethods()) {
					if (m.getElementName().equals("onEvent")) {
						String eventTypeName = Signature.toString(m.getParameterTypes()[0]);
						handledEvents.add(eventTypeName.substring(0, eventTypeName.length() - "Event".length()));
					}
				}

				List<Event> targetElements = getTargetObjects(modelElement, eReference, handledEvents);
				((List<Event>) modelElement.eGet(eReference)).addAll(targetElements);
			}
		} catch (JavaModelException e) {
			addError("Could not transform reference Architecture.events to model representation.", e);
		}
		return modelElement;
	}
}
