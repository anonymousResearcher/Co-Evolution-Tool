package org.codeling.lang.cocome.transformation.server_feature;

import java.util.LinkedList;
import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.mm.CoCoME.Server;
import org.codeling.lang.cocome.mm.CoCoME.TransferObject;
import org.codeling.lang.cocome.transformation.TransferObjectTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.ReferenceMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

public class TransferObjectsTransformation extends ReferenceMechanismTransformation<Server, TransferObject, IType> {

	LinkedList<String> targetNames = new LinkedList<>();

	public TransferObjectsTransformation(ClassMechanismTransformation<Server, IType> parentTransformation) {
		super(parentTransformation, CoCoMEPackage.eINSTANCE.getServer_TransferObjects());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return null;
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		ICompilationUnit[] cus = new ICompilationUnit[0];
		try {
			cus = codeElement.getPackageFragment().getCompilationUnits();
		} catch (JavaModelException e) {
			addError("Could not get CompilationUnits", e);
		}
		for (ICompilationUnit cu : cus) {
			if (cu.getElementName().endsWith("TO.java")) {
				try {
					IType toType = cu.getTypes()[0];
					targetNames.add(
							toType.getElementName().substring(0, toType.getElementName().length() - "TO".length()));
					TransferObjectTransformation t = new TransferObjectTransformation(this);
					t.setCodeElement(toType);
					result.add(t);
				} catch (JavaModelException e) {
					addError("Could not get main type from compilation unit " + cu.getElementName()
							+ ". Ignoring the unit.");
				}
			}
		}
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public Server transformToModel() throws CodelingException {
		List<TransferObject> targetElements = getTargetObjects(modelElement, eReference, targetNames);
		((List<TransferObject>) modelElement.eGet(eReference)).addAll(targetElements);
		return modelElement;
	}
}
