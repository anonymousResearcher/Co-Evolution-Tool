package org.codeling.lang.cocome.transformation;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.mm.CoCoME.Transition;
import org.codeling.lang.cocome.transformation.model_feature.TransitionsTransformation;
import org.codeling.lang.cocome.transformation.transition_feature.TargetTransformation;
import org.codeling.mechanisms.transformations.classes.ContainmentOperationTargetTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;

public class TransitionTransformation extends ContainmentOperationTargetTransformation<Transition> {

	public TransitionTransformation(TransitionsTransformation parentTransformation) {
		super(parentTransformation, CoCoMEPackage.eINSTANCE.getTransition());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// Child transformations for cross references
		result.add(new TargetTransformation(this));
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {

		// Child transformations for containment references
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {

		// Child transformations for containment references
	}
}