package org.codeling.lang.cocome.transformation.model_feature;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.mm.CoCoME.Model;
import org.codeling.lang.cocome.mm.CoCoME.Transition;
import org.codeling.lang.cocome.transformation.TransitionTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.references.ContainmentOperationTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

public class TransitionsTransformation extends ContainmentOperationTransformation<Model, Transition> {

	public TransitionsTransformation(ClassMechanismTransformation<Model, IType> parentTransformation) {
		super(parentTransformation, CoCoMEPackage.eINSTANCE.getModel_Transitions());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return null;
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		resolveCodeElement();
		for (IMethod m : methods) {
			TransitionTransformation t = new TransitionTransformation(this);
			t.setCodeElement(m);
			result.add(t);
		}
	}

	@Override
	public boolean hasExpectedAnnotation(IMethod method) {
		try {
			return method.getSource().contains("state = CashDeskState.");
		} catch (JavaModelException e) {
			addError("Could not get method source of method " + method.getParent().getElementName() + "."
					+ method.getElementName());
			return false;
		}
	}

	@Override
	protected String getNewAnnotationName() {
		return null;
	}
}
