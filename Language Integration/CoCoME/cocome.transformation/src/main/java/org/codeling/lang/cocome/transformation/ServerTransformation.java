package org.codeling.lang.cocome.transformation;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.cocome.mm.CoCoME.CoCoMEFactory;
import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.mm.CoCoME.Component;
import org.codeling.lang.cocome.mm.CoCoME.Server;
import org.codeling.lang.cocome.transformation.component_feature.ChildrenTransformation;
import org.codeling.lang.cocome.transformation.component_feature.DispatchedTransformation;
import org.codeling.lang.cocome.transformation.component_feature.EventsTransformation;
import org.codeling.lang.cocome.transformation.component_feature.HandledTransformation;
import org.codeling.lang.cocome.transformation.component_feature.ProvidesTransformation;
import org.codeling.lang.cocome.transformation.component_feature.RequiresTransformation;
import org.codeling.lang.cocome.transformation.server_feature.TransferObjectsTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;

public class ServerTransformation extends ClassMechanismTransformation<Server, IType> {

	public ServerTransformation(
			AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation) {
		super(parentTransformation, CoCoMEPackage.eINSTANCE.getServer());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// Child transformations for cross references
		result.add(new RequiresTransformation<Server>(this));
		result.add(new HandledTransformation<Server, IType>(this));
		result.add(new DispatchedTransformation<Server, IType>(this));

		// Child transformations for cross references
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new ChildrenTransformation<Server, Component, IType>(this));
		result.add(new ProvidesTransformation<Server>(this));
		result.add(new EventsTransformation<Server, IType>(this));
		
		// Child transformations for containment references
		result.add(new TransferObjectsTransformation(this));
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new ChildrenTransformation<Server, Component, IType>(this));
		result.add(new ProvidesTransformation<Server>(this));
		result.add(new EventsTransformation<Server, IType>(this));

		// Child transformations for containment references
		result.add(new TransferObjectsTransformation(this));
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public Server transformToModel() throws CodelingException {
		modelElement = CoCoMEFactory.eINSTANCE.createServer();
		modelElement.setName(getNameFromCodeElement(codeElement));
		return modelElement;
	}

	public boolean isServer(IType type) {
		return getNameFromCodeElement(type) != null;
	}

	private String getNameFromCodeElement(IType type) {
		String name = type.getElementName();
		if (name.endsWith("Server")) {
			return name;
		} else
			return null;
	}
}