package org.codeling.lang.cocome;

import java.util.Arrays;
import java.util.List;

import org.codeling.lang.base.java.JavaBasedImplementationLanguageDefinition;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.base.modeltrans.henshintgg.HenshinTGGBasedLanguageDefinitionHelper;
import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.transformation.ArchitectureTransformation;
import org.codeling.mechanisms.MechanismsMapping;
import org.codeling.mechanisms.classes.StaticInterfaceMechanism;
import org.codeling.mechanisms.references.StaticInterfaceImplementationMechanism;
import org.codeling.transformationmanager.TransformationResult;
import org.codeling.utils.CodelingException;
import org.codeling.utils.IDRegistry;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.osgi.framework.FrameworkUtil;

import de.mkonersmann.il.profiles.Profiles;

public class CoCoMELanguageDefinition extends JavaBasedImplementationLanguageDefinition {

	final URI henshinTGGFileURI = URI.createPlatformPluginURI(
			"/" + FrameworkUtil.getBundle(getClass()).getSymbolicName() + "/COCOME2IAL.henshin", true);

	public CoCoMELanguageDefinition() {
		CoCoMEPackage i = CoCoMEPackage.eINSTANCE;
		i.getName(); // Load the package

		// Create the mechanism mapping
		MechanismsMapping m = MechanismsMapping.getInstance();
		m.put(i.getInterface(), StaticInterfaceMechanism.class);
		m.put(i.getComponent_Provides(), StaticInterfaceImplementationMechanism.class);

		// Create the map of model elements to reference transformations
		// referenceTransformations.put(i.getCDIBean(),
		// Arrays.asList(ChildTransformation.class));

		// Create the list of cross references
		for (final EClassifier c : i.getEClassifiers())
			if (!crossReferenceOrder.contains(c) && c instanceof EClass)
				crossReferenceOrder.add((EClass) c);
	}

	@Override
	public AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> createRootTransformationInstance() {
		return new ArchitectureTransformation();
	}

	@Override
	public TransformationResult transformIMToTM(List<EObject> cmRoots, IDRegistry idregistry) throws CodelingException {
		return new HenshinTGGBasedLanguageDefinitionHelper().transformIMToTM(this, henshinTGGFileURI, cmRoots,
				idregistry, monitor);
	}

	@Override
	public TransformationResult transformTMToIM(List<EObject> ilRoots, IDRegistry idRegistry) throws CodelingException {
		return new HenshinTGGBasedLanguageDefinitionHelper().transformTMToIM(this, henshinTGGFileURI, ilRoots,
				idRegistry, monitor);
	}

	@Override
	public List<String> getSelectedProfiles() {
		return Arrays.asList(Profiles.COMPONENTS_HIERARCHY_SCOPED.getNsURI(),
				Profiles.INTERFACES_TYPE_OPERATIONS.getNsURI(), Profiles.INTERFACES_TYPE_EVENTS.getNsURI(),
				Profiles.DATATYPES_COMMON.getNsURI(), Profiles.BEHAVIOUR_STATEMACHINE.getNsURI(),
				Profiles.COMPONENTS_INSTANTIATION_FIXED.getNsURI(), Profiles.COMPONENTS_STATE_STATEFUL.getNsURI(),
				Profiles.COMPONENTS_STATE_STATELESS.getNsURI(), Profiles.INTERFACES_SCOPED.getNsURI(),
				Profiles.DEPLOYMENT.getNsURI(), Profiles.CONNECTORS.getNsURI(),
				Profiles.CONNECTORS_PROCEDURE_CALLS_SYNCHRONOUS_1TO1.getNsURI(),
				Profiles.CONNECTORS_EVENTS_1TON.getNsURI());
	}

}
