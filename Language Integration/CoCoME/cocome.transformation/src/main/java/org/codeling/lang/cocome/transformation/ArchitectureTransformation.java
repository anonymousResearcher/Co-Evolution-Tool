package org.codeling.lang.cocome.transformation;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.cocome.mm.CoCoME.Architecture;
import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.transformation.architecture_feature.ComponentsTransformation;
import org.codeling.mechanisms.transformations.classes.NinjaSingletonTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;

public class ArchitectureTransformation extends NinjaSingletonTransformation<Architecture> {

	public ArchitectureTransformation() {
		super(null, CoCoMEPackage.eINSTANCE.getArchitecture());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// Child transformations for cross references
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {

		// Child transformations for containment references
		result.add(new ComponentsTransformation(this));
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {

		// Child transformations for containment references
		result.add(new ComponentsTransformation(this));
	}
}