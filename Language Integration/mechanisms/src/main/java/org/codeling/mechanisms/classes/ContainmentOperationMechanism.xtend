package org.codeling.mechanisms.classes

import org.codeling.mechanisms.ClassMechanism
import org.eclipse.emf.ecore.ENamedElement
import org.eclipse.emf.ecore.EReference
import org.eclipse.jdt.core.IJavaElement
import org.eclipse.jdt.core.IPackageFragment
import org.eclipse.jdt.core.IType

class ContainmentOperationMechanism extends ClassMechanism {

	override getName() {
		"Containment Operation"
	}

	override createMetaModelLibrary(IPackageFragment packageFragment, ENamedElement element) {
		var String annotationName = element.name.toFirstUpper;

		val content = '''
			package «packageFragment.elementName»;
			
			@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
			@java.lang.annotation.Target(java.lang.annotation.ElementType.METHOD)
			public @interface «annotationName» {
				
			}
		''';

		return packageFragment.createCompilationUnit('''«annotationName».java''', content, true, monitor).getType(
			annotationName);
	}

	override createRuntime(IPackageFragment packageFragment, ENamedElement element) {
		var String typeName = element.name.toFirstUpper;
		val String owningElementName = (element as EReference).EContainingClass.name;

		val content = '''
			package «packageFragment.elementName»;
			
			import «languageName».mm.«owningElementName.toLowerCase»_feature.«typeName»;
			import org.codeling.mechanism.runtime.ContainmentOperationRuntime;
			import org.codeling.mechanism.runtime.common.IntegratedModelException;
			import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;
			
			public class «typeName»Runtime extends ContainmentOperationRuntime {
				
				«super.attributeRuntimeFields»
				«super.referenceRuntimeFields»
				
				public «typeName»Runtime(TypeMechanismRuntime owningTypeRuntime) throws IntegratedModelException {
					super(owningTypeRuntime, «typeName».class);
				}
				
				@Override
				public void initialize() throws IntegratedModelException {
					super.initialize();
				}
				
				@Override
				public void initializeContainments() throws IntegratedModelException {
					super.initializeContainments();
					
					«super.attributeRuntimeInitialization»
					«super.getReferenceRuntimeInitialization(true)»
				}
				
				@Override
				public void initializeCrossReferences() throws IntegratedModelException {
					super.initializeCrossReferences();
					
					«super.getReferenceRuntimeInitialization(false)»
				}
				
				«super.getReferenceRuntimeFieldGetters»
			
			}
		''';

		packageFragment.createCompilationUnit('''«typeName»Runtime.java''', content, true, monitor);
	}

	override createTransformation(IPackageFragment packageFragment, ENamedElement element) {
		var String typeName = element.name.toFirstUpper;
		val EReference eReference = element as EReference;
		val String eReferenceInstantiation = eReference.
			EFeatureInstantiation

		val content = '''
			package «packageFragment.elementName»;
			
			import java.util.List;
			
			import org.codeling.mechanisms.transformations.classes.ContainmentOperationTransformation;
			import org.eclipse.emf.ecore.EObject;
			import org.eclipse.jdt.core.IJavaElement;
			import org.eclipse.jdt.core.IType;
			
			import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
			import «eReference.packageName».«eReference.packageName»Package;
			import «eReference.packageName».«eReference.EContainingClass.name»;
			
			public class «typeName»Transformation
					extends ContainmentOperationTransformation<«eReference.EContainingClass.name», «eReference.EType.name»> {
			
				public «typeName»Transformation(AbstractModelCodeTransformation<? extends EObject, IType> parentTransformation) {
					super(parentTransformation, «eReferenceInstantiation»);
				}
			
				@Override
				public void doCreateCrossReferencesTransformations(
						List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
					// TODO Auto-generated method stub
				}
			
				@Override
				protected void doCreateChildTransformationsToCode(
						List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
				// TODO Auto-generated method stub
				}
			
				@Override
				protected void doCreateChildTransformationsToModel(
						List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
				// TODO Auto-generated method stub
				}
			}
		'''

		packageFragment.createCompilationUnit('''«typeName»Transformation.java''', content, true, monitor);
	}

	override canHandle(IJavaElement codeElement, ENamedElement metaModelElement) {
		return codeElement instanceof IType && (codeElement as IType).methods.stream.anyMatch(
			m |
				m.getAnnotation(metaModelElement.name.toFirstLower).exists
		);
	}

}
