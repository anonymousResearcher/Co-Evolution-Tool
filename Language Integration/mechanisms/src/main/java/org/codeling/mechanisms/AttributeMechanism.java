package org.codeling.mechanisms;

import org.eclipse.emf.ecore.EAttribute;

public abstract class AttributeMechanism extends Mechanism {
	protected EAttribute eAttribute;

	public void setEAttribute(EAttribute eAttribute) {
		this.eAttribute = eAttribute;
	}

}