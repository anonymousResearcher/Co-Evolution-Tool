package org.codeling.mechanisms

import java.text.MessageFormat
import java.util.Map
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.core.runtime.NullProgressMonitor
import org.eclipse.emf.common.util.TreeIterator
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.ENamedElement
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.emf.ecore.EcorePackage
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.jdt.core.IJavaElement
import org.eclipse.jdt.core.IPackageFragment

abstract class Mechanism {
	protected IProgressMonitor monitor = new NullProgressMonitor();
	protected String languageName = "";
	protected Map<ENamedElement, IJavaElement> modelToCodeMap = null;

	abstract def String getName();

	def IJavaElement createMetaModelLibrary(IPackageFragment packageFragment, ENamedElement element) {
	}

	def void createTransformation(IPackageFragment packageFragment, ENamedElement element) {
	}

	def void createRuntime(IPackageFragment packageFragment, ENamedElement element) {
	}

	def void setMonitor(IProgressMonitor monitor) {
		this.monitor = monitor;
	}

	def protected String getEClassInstantiation(EClass eClass) {
		return eClass.getPackageName + "Package.eINSTANCE.get" + eClass.getName() + "()";
	}

	def protected String getEFeatureInstantiation(EStructuralFeature eFeature) {
		val EClass container = eFeature.getEContainingClass();
		var String packageName = container.packageName;
		val String capitalizedFeatureName = eFeature.name.toFirstUpper;
		return '''«packageName»Package.eINSTANCE.get«container.name»_«capitalizedFeatureName»()''';
	}

	def protected String getPackageName(EClass eClass) {
		return eClass.getEPackage().getName();
	}

	def protected String getPackageName(EStructuralFeature eFeature) {
		val EClass eClass = eFeature.getEContainingClass();
		return getPackageName(eClass);
	}

	def void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	def void setModelToCodeMap(Map<ENamedElement, IJavaElement> modelToCodeMap) {
		this.modelToCodeMap = modelToCodeMap;
	}

	def EObject getTargetElement(EObject modelElement, EReference eReference, String targetElementName) {
		val EClass targetEClass = eReference.EReferenceType;
		val TreeIterator<EObject> ti = modelElement.eResource.allContents;
		for (; ti.hasNext();) {
			val EObject e = ti.next;
			if (e.eClass == targetEClass) {
				if (e.nameAttributeValue.equals(
					targetElementName)) {
					return e;
				}
			}
		}
		throw new IllegalStateException('''Could not set the eReference [«eReference.EContainingClass».«eReference.name»], because I did not find an EObject of the target class «targetEClass.name» with an EAttribute "name" that has the value "«targetElementName»".''');
	}

	def protected String getNameAttributeValue(EObject modelElement) {
		val EStructuralFeature nameFeature = modelElement.eClass().getEStructuralFeature("name");
		if (!(nameFeature instanceof EAttribute))
			throw new IllegalStateException(
				MessageFormat.format("The 'name' feature of the eclass {} is not an EAttribute, but must be.",
					modelElement.eClass().getName()));
		return modelElement.eGet(nameFeature) as String;
	}

	def protected void setNameAttributeValue(EObject modelElement, String value) {
		val EStructuralFeature nameFeature = modelElement.eClass().getEStructuralFeature("name");
		if (!(nameFeature instanceof EAttribute))
			throw new IllegalStateException(
				MessageFormat.format("The 'name' feature of the eclass {} is not an EAttribute, but must be.",
					modelElement.eClass().getName()));
		modelElement.eSet(nameFeature, value);
	}

	protected def EReference getEReferenceThatOwns(EClass owningClass) {
		val EObject rootPackage = EcoreUtil.getRootContainer(owningClass);
		val TreeIterator<EObject> ti = rootPackage.eResource.allContents;
		for (; ti.hasNext();) {
			val EObject e = ti.next;
			if (e.eClass == EcorePackage.eINSTANCE.EReference) {
				// found a reference
				val EReference reference = e as EReference;
				if (reference.getEReferenceType() == owningClass && reference.isContainment()) {
					// I found a reference that targets my owner as container
					return reference;
				}
			}
		}
		throw new IllegalArgumentException(
			"Did not find any reference that declares ownership to the EClass " + owningClass.name
		);
	}

	/**
	 * Returns whether the given Java element can be handled by this mechanism.
	 * E.g. the MarkerInterfaceMechanism validates whether the given element is
	 * a type which implements the required interface.
	 */
	public abstract def boolean canHandle(IJavaElement codeElement, ENamedElement metaModelElement);
}
