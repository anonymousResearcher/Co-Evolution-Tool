package org.codeling.mechanisms.classes

import org.codeling.mechanisms.ClassMechanism
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.ENamedElement
import org.eclipse.jdt.core.IJavaElement
import org.eclipse.jdt.core.IJavaProject
import org.eclipse.jdt.core.IPackageFragment

class ProjectMechanism extends ClassMechanism {

	override getName() {
		"Project"
	}

	override createMetaModelLibrary(IPackageFragment packageFragment, ENamedElement element) {
	}

	override createRuntime(IPackageFragment packageFragment, ENamedElement element) {
	}

	override createTransformation(IPackageFragment packageFragment, ENamedElement element) {
		val String typeName = element.name.toFirstUpper;
		val EClass eClass = element as EClass;
		val String eClassInstantiation = getEClassInstantiation(eClass);

		val content = '''
		package «packageFragment.elementName»;
		
		import java.util.List;
		
		import org.codeling.mechanisms.transformations.classes.MarkerInterfaceTransformation;
		import org.eclipse.emf.ecore.EObject;
		import org.eclipse.jdt.core.IJavaElement;
		
		import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
		import org.codeling.utils.CodelingException;
		«createImportsForTargetTransformations»
		import «eClass.packageName».«eClass.packageName»Package;
		import «eClass.packageName».«eClass.name»;
		
		
		«IF eClass.requiresFeaturePackageImport»
			import «packageFragment.elementName».«typeName.toLowerCase»_feature.*;
		«ENDIF»
		
		public class «typeName»Transformation extends ProjectTransformation<Archive> {
		
			public «typeName»Transformation(AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation) {
				super(parentTransformation, «eClassInstantiation»);
			}
		
			«createCrossReferencesTrasformations»
			
			«createChildTransformationsToCode»
			
			«createChildTransformationsToModel»
		}''';

		packageFragment.createCompilationUnit('''«typeName»Transformation.java''', content, true, monitor);
	}
	
	override canHandle(IJavaElement codeElement, ENamedElement metaModelElement) {
		return codeElement instanceof IJavaProject;
	}
}
