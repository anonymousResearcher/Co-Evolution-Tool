package org.codeling.mechanisms.classes

import org.codeling.mechanisms.ClassMechanism
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.ENamedElement
import org.eclipse.jdt.core.IJavaElement
import org.eclipse.jdt.core.IPackageFragment
import org.eclipse.jdt.core.IType

class TypeAnnotationMechanism extends ClassMechanism {

	override getName() {
		"Type Annotation"
	}

	override createMetaModelLibrary(IPackageFragment packageFragment, ENamedElement element) {
		var String annotationName = element.name.toFirstUpper;

		val content = '''
			package «packageFragment.elementName»;
			
			@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
			@java.lang.annotation.Target(java.lang.annotation.ElementType.TYPE)
			public @interface «annotationName» {
				
			}
		''';

		return packageFragment.createCompilationUnit('''«annotationName».java''', content, true, monitor).getType(
			annotationName);
	}

	override createRuntime(IPackageFragment packageFragment, ENamedElement element) {
		var String typeName = element.name.toFirstUpper;
		val String parentPackageName = packageFragment.elementName.substring(0,
			packageFragment.elementName.lastIndexOf("."));

		val content = '''
			package «packageFragment.elementName»;
						
			import java.lang.reflect.Method;
			import java.util.List;
			import java.util.stream.Collectors;
			
			import «parentPackageName».mm.«typeName»;
			import org.codeling.mechanism.runtime.TypeAnnotationRuntime;
			import org.codeling.mechanism.runtime.common.IntegratedModelException;
			
			public class «typeName»Runtime<T> extends TypeAnnotationRuntime<T> {
			
				«super.attributeRuntimeFields»
				«super.referenceRuntimeFields»
			
				public «typeName»Runtime(Class<T> implementingClass) throws IntegratedModelException {
					super(implementingClass, «typeName».class);
				}
			
				@Override
				public void initializeContainments() throws IntegratedModelException {
					super.initializeContainments();
			
					«super.attributeRuntimeInitialization»
					«super.containmentReferenceRuntimeInitialization»
				}
			
				@Override
				public void initializeCrossReferences() throws IntegratedModelException {
					super.initializeCrossReferences();
			
					«super.crossReferenceRuntimeInitialization»
				}
			
				«super.getReferenceRuntimeFieldGetters»
			
			}
		''';

		packageFragment.createCompilationUnit('''«typeName»Runtime.java''', content, true, monitor);
	}

	override createTransformation(IPackageFragment packageFragment, ENamedElement element) {
		val String typeName = element.name.toFirstUpper;
		val EClass eClass = element as EClass;
		val String eClassName = eClass.name;
		val String eClassInstantiation = getEClassInstantiation(eClass);

		val content = '''
			package «packageFragment.elementName»;
			
			import java.util.List;
			
			import org.codeling.mechanisms.transformations.classes.TypeAnnotationTransformation;
			import org.eclipse.emf.ecore.EObject;
			import org.eclipse.jdt.core.IJavaElement;
			
			import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
			import org.codeling.utils.CodelingException;
			import «eClass.packageName».«eClass.packageName»Package;
			import «eClass.packageName».«eClass.name»;
			import org.codeling.lang.«languageName».transformation.component_feature.VersionTransformation;
			«IF eClass.requiresFeaturePackageImport»
				import «packageFragment.elementName».«typeName.toLowerCase»_feature.*;
			«ENDIF»
					
			public class «typeName»Transformation
					extends TypeAnnotationTransformation<«eClassName»> {
			
				public «typeName»Transformation(AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation) {
					super(parentTransformation, «eClassInstantiation»);
				}
			
				«createCrossReferencesTrasformations»
				
				«createChildTransformationsToCode»
				
				«createChildTransformationsToModel»
			}
		''';

		packageFragment.createCompilationUnit('''«typeName»Transformation.java''', content, true, monitor);
	}
	
	override canHandle(IJavaElement codeElement, ENamedElement metaModelElement) {
		return codeElement instanceof IType && (codeElement as IType).getAnnotation(metaModelElement.name).exists;
	}

}
