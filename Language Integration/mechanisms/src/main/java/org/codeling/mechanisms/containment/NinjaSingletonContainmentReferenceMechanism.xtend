package org.codeling.mechanisms.containment

import org.codeling.mechanisms.ContainmentMechanism
import org.eclipse.emf.ecore.ENamedElement
import org.eclipse.emf.ecore.EReference
import org.eclipse.jdt.core.IJavaElement
import org.eclipse.jdt.core.IMethod
import org.eclipse.jdt.core.IPackageFragment

class NinjaSingletonContainmentReferenceMechanism extends ContainmentMechanism {

	override getName() {
		"Ninja Singleton Containment"
	}

	override createMetaModelLibrary(IPackageFragment packageFragment, ENamedElement element) {
	}

	override createRuntime(IPackageFragment packageFragment, ENamedElement element) {
	}

	override createTransformation(IPackageFragment packageFragment, ENamedElement element) {
		var String typeName = element.name.toFirstUpper;
		val EReference eReference = element as EReference;
		val String eReferenceInstantiation = eReference.EFeatureInstantiation

		val content = '''
			package «packageFragment.elementName»;
			
			import java.util.List;
			
			import org.eclipse.emf.ecore.EObject;
			import org.eclipse.jdt.core.IJavaElement;
			import org.eclipse.jdt.core.IType;
			
			import «eReference.packageName».«eReference.packageName»Package;
			import «eReference.packageName».«eReference.EContainingClass.name»;
			import «eReference.packageName».«eReference.EType.name»;
			import «languageName».transformation.«eReference.EType.name»Transformation;
			import org.codeling.mechanisms.transformations.references.NinjaSingletonContainmentTransformation;
			
			import org.codeling.lang.base.java.ASTUtils;
			import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
			import org.codeling.utils.Models;
			import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
			import org.codeling.lang.«languageName».transformation.«eReference.EType.name»Transformation;
			
			public class «typeName»Transformation extends NinjaSingletonContainmentTransformation<«eReference.EContainingClass.name», «eReference.EType.name»> {
			
				public «typeName»Transformation(AbstractModelCodeTransformation<? extends EObject, IJavaElement> parentTransformation) {
					super(parentTransformation, «eReferenceInstantiation»);
				}
			
				@Override
				public void doCreateCrossReferencesTransformations(
						List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
					// TODO Auto-generated method stub
			
				}
			
				@Override
				protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
						EObject targetModelElement) {
					return new «eReference.EType.name»Transformation(this);
				}
			
				@Override
				protected void doCreateChildTransformationsToModel(
						List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
					try {
						for (IType type : ASTUtils.getTypes(codeRoot)) {
							if (!type.isClass())
								continue;
							«eReference.EType.name»Transformation t = new «eReference.EType.name»Transformation(this);
							t.setCodeElement(type);
							result.add(t);
						}
					} catch (Exception e) {
						String projectName = "(no project name available)";
						if (codeRoot != null)
							projectName = codeElement.getElementName();
						addError("Error while getting types in project " + projectName, e);
					}
				}
			}
		'''

		packageFragment.createCompilationUnit('''«typeName»Transformation.java''', content, true, monitor);
	}

	override canHandle(IJavaElement codeElement, ENamedElement metaModelElement) {
		return codeElement instanceof IMethod &&
			(codeElement as IMethod).getAnnotation((metaModelElement.eContainer as ENamedElement).name.toFirstUpper).
				exists;
			}
		}
		