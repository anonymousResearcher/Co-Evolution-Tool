package org.codeling.mechanisms.classes

import org.codeling.mechanisms.ClassMechanism
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.ENamedElement
import org.eclipse.jdt.core.IJavaElement
import org.eclipse.jdt.core.IPackageFragment
import org.eclipse.jdt.core.IType

class MarkerInterfaceMechanism extends ClassMechanism {

	override getName() {
		"Marker Interface"
	}

	override createMetaModelLibrary(IPackageFragment packageFragment, ENamedElement element) {
		val String ifaceName = element.name.toFirstUpper;

		val content = '''
		package «packageFragment.elementName»;
		
		public interface «ifaceName» {
			
		}''';

		return packageFragment.createCompilationUnit('''«ifaceName».java''', content, true, monitor).getType(
			ifaceName
		);
	}

	override createRuntime(IPackageFragment packageFragment, ENamedElement element) {
		val String typeName = element.name.toFirstUpper;
		val String parentPackageName = packageFragment.elementName.substring(0,
			packageFragment.elementName.lastIndexOf("."));

		val content = '''
		package «packageFragment.elementName»;
		
		import «parentPackageName».mm.«typeName»;
		import org.codeling.mechanism.runtime.MarkerInterfaceRuntime;
		import org.codeling.mechanism.runtime.common.IntegratedModelException;
		
		public class «typeName»Runtime<T extends «typeName»> extends MarkerInterfaceRuntime<«typeName», T> {
			
			«super.attributeRuntimeFields»
			«super.referenceRuntimeFields»
			
			public «typeName»Runtime(Class<T> implementingClass) throws IntegratedModelException {
				super(implementingClass, «typeName».class);
			}
			
			@Override
			public void initializeContainments() throws IntegratedModelException {
				super.initializeContainments();
				
				«super.attributeRuntimeInitialization»
				«super.getReferenceRuntimeInitialization(true)»
			}
			
			@Override
			public void initializeCrossReferences() throws IntegratedModelException {
				super.initializeCrossReferences();
								
				«super.getReferenceRuntimeInitialization(false)»
			}
			
			«super.getReferenceRuntimeFieldGetters»
		}''';

		packageFragment.createCompilationUnit('''«typeName»Runtime.java''', content, true, monitor);
	}

	override createTransformation(IPackageFragment packageFragment, ENamedElement element) {
		val String typeName = element.name.toFirstUpper;
		val EClass eClass = element as EClass;
		val String eClassName = eClass.name;
		val String eClassInstantiation = getEClassInstantiation(eClass);

		val content = '''
		package «packageFragment.elementName»;
		
		import java.util.List;
		
		import org.codeling.mechanisms.transformations.classes.MarkerInterfaceTransformation;
		import org.eclipse.emf.ecore.EObject;
		import org.eclipse.jdt.core.IJavaElement;
		
		import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
		import org.codeling.utils.CodelingException;
		import «eClass.packageName».«eClass.packageName»Package;
		import «eClass.packageName».«eClass.name»;
		
		
		«IF eClass.requiresFeaturePackageImport»
			import «packageFragment.elementName».«typeName.toLowerCase»_feature.*;
		«ENDIF»
		
		public class «typeName»Transformation extends MarkerInterfaceTransformation<«eClassName»> {
		
			public «typeName»Transformation(AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation) {
				super(parentTransformation, «eClassInstantiation»);
			}
		
			«createCrossReferencesTrasformations»
			
			«createChildTransformationsToCode»
		
			«createChildTransformationsToModel»
		}''';

		packageFragment.createCompilationUnit('''«typeName»Transformation.java''', content, true, monitor);
	}

	override canHandle(IJavaElement codeElement, ENamedElement metaModelElement) {
		if (!(codeElement instanceof IType)) {
			return false;
		}
		val IType type = codeElement as IType;

		return type.superInterfaceNames.stream.anyMatch(
			s | s.equals(metaModelElement.name.toFirstUpper)
		);
	}
}
