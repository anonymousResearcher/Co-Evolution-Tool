package de.mkonersmann.henshin.tgg.api;

public enum TGGTripleComponent {
	SOURCE, CORRESPONDENCE, TARGET;
}
