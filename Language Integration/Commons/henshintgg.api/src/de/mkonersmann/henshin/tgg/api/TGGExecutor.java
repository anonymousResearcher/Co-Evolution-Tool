package de.mkonersmann.henshin.tgg.api;

import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.henshin.interpreter.EGraph;

public interface TGGExecutor {

	public EGraph importModel(EObject rootObject);

	public EGraph importModel(List<EObject> rootObjects);

	public boolean executeForwardTransformation();

	public boolean executeBackwardTransformation();

	public boolean executeForwardPropagation();

	public boolean executeBackwardPropagation();

	public boolean executeConsistencyCheck();

	public boolean executeIntegration();

	public void removeSourceGraph();

	public void removeTargetGraph();

	public void removeCorrespondenceGraph();

	public void removeSourceAndCorrespondenceGraphs();

	public void removeCorrespondenceAndTargetGraphs();

	/**
	 * Creates a new model instance from the triple graph. It creates new
	 * {@link EObject}s for each node in the graph.
	 *
	 * @return A list of root objects.
	 */
	public List<EObject> createModelInstanceFromTripleGraph();

	/**
	 * Creates a new model instance from the triple graph. It creates new
	 * {@link EObject}s for each node in the graph.
	 *
	 * @param componentFilter
	 *            Only nodes of this component are exported. If null, all nodes are
	 *            exported.
	 *
	 * @return A list of root objects.
	 */
	public List<EObject> createModelInstanceFromTripleGraph(TGGTripleComponent componentFilter);

	public Map<List<EObject>, List<EObject>> getInitial2ResultElements();

}
