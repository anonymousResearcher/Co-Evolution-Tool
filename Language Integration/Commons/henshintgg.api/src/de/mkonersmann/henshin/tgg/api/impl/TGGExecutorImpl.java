package de.mkonersmann.henshin.tgg.api.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.henshin.interpreter.EGraph;
import org.eclipse.emf.henshin.interpreter.UnitApplication;
import org.eclipse.emf.henshin.interpreter.impl.EGraphImpl;
import org.eclipse.emf.henshin.model.Annotation;

import de.mkonersmann.henshin.tgg.api.TGGExecutor;
import de.mkonersmann.henshin.tgg.api.TGGModel;
import de.mkonersmann.henshin.tgg.api.TGGTripleComponent;
import de.tub.tfs.henshin.tgg.interpreter.TggEngine;
import de.tub.tfs.henshin.tgg.interpreter.TggTransformationInfo;
import de.tub.tfs.henshin.tgg.interpreter.TripleComponent;
import de.tub.tfs.henshin.tgg.interpreter.impl.TggApplicationImpl;
import de.tub.tfs.henshin.tgg.interpreter.impl.TggEngineOperational;
import de.tub.tfs.henshin.tgg.interpreter.impl.TggTransformationInfoImpl;

public class TGGExecutorImpl implements TGGExecutor {

	private static final String name_BT_RULE_FOLDER = "BTRuleFolder";
	private static final String name_FT_RULE_FOLDER = "FTRuleFolder";
	private static final String name_CC_RULE_FOLDER = "CCRuleFolder";
	private static final String name_IT_RULE_FOLDER = "ITRuleFolder";

	EGraph graph;
	TGGModel model;
	UnitApplication application;
	TggTransformationInfo trafoInfo;
	Map<List<EObject>, List<EObject>> source2target = new HashMap<>();

	public TGGExecutorImpl(TGGModel model) {
		this.model = model;
	}

	private void prepareEngine() {
		trafoInfo = new TggTransformationInfoImpl();
		trafoInfo.fillTranslatedMaps(graph.getRoots(), false);
		final TggEngine engine = new TggEngineOperational(graph, trafoInfo);
		application = new TggApplicationImpl(engine, trafoInfo);
		application.setEGraph(graph);
	}

	@Override
	public boolean executeForwardTransformation() {
		prepareEngine();
		application.setUnit(model.getModule().getUnit(name_FT_RULE_FOLDER));

		final boolean success = executeTransformation();
		resolveSource2TargetMap(true);

		return success;
	}

	public boolean executeTransformation() {
		return application.execute(null);
	}

	@Override
	public boolean executeBackwardTransformation() {
		prepareEngine();
		application.setUnit(model.getModule().getUnit(name_BT_RULE_FOLDER));

		final boolean success = executeTransformation();
		resolveSource2TargetMap(false);

		return success;
	}

	@Override
	public boolean executeForwardPropagation() {
		prepareEngine();
		removeCorrespondenceAndTargetGraphs();
		return executeForwardTransformation();
	}

	@Override
	public boolean executeBackwardPropagation() {
		prepareEngine();
		removeSourceAndCorrespondenceGraphs();
		return executeBackwardTransformation();
	}

	@Override
	public boolean executeConsistencyCheck() {
		prepareEngine();
		application.setUnit(model.getModule().getUnit(name_CC_RULE_FOLDER));

		final boolean success = executeTransformation();
		resolveSource2TargetMap(true);

		return success;
	}

	@Override
	public boolean executeIntegration() {
		prepareEngine();
		application.setUnit(model.getModule().getUnit(name_IT_RULE_FOLDER));

		final boolean success = executeTransformation();
		resolveSource2TargetMap(true);

		return success;
	}

	@Override
	public void removeSourceGraph() {
		removeMultipleNodes(TripleComponent.SOURCE);
	}

	@Override
	public void removeTargetGraph() {
		removeMultipleNodes(TripleComponent.TARGET);

	}

	@Override
	public void removeCorrespondenceGraph() {
		removeMultipleNodes(TripleComponent.CORRESPONDENCE);
	}

	@Override
	public void removeSourceAndCorrespondenceGraphs() {
		removeCorrespondenceGraph();
		removeSourceGraph();
	}

	@Override
	public void removeCorrespondenceAndTargetGraphs() {
		removeCorrespondenceGraph();
		removeTargetGraph();
	}

	/**
	 * Removes all nodes of the given component type
	 *
	 * @param nodeType
	 */
	private void removeMultipleNodes(TripleComponent nodeType) {
		final Set<EObject> rootsToDelete = new HashSet<>();
		for (final EObject root : graph.getRoots()) {
			final TripleComponent component = getTripleComponent(root);
			if (component == null || component == nodeType)
				// Delete the tree when the component is asked to be deleted, or
				// it cannot be assigned to a triple component
				rootsToDelete.add(root);
		}

		for (final EObject root : rootsToDelete)
			graph.removeTree(root);
	}

	TripleComponent getTripleComponent(EObject eobject) {
		final String objectNamespace = eobject.eClass().getEPackage().getNsURI();
		final Annotation tggAnnotation = model.getModule().getAnnotations().get(0);
		for (final Annotation importAnnotation : tggAnnotation.getAnnotations()) {
			final String uri = importAnnotation.getKey();
			if (uri.equals(objectNamespace)) {
				final String objectComponent = importAnnotation.getValue();
				return TripleComponent.valueOf(objectComponent);
			}
		}

		return null;
	}

	@Override
	public List<EObject> createModelInstanceFromTripleGraph() {
		return createModelInstanceFromTripleGraph(null);
	}

	@Override
	public List<EObject> createModelInstanceFromTripleGraph(TGGTripleComponent componentFilter) {
		if (componentFilter != null)
			switch (componentFilter) {
			case SOURCE:
				removeCorrespondenceAndTargetGraphs();
				break;
			case TARGET:
				removeSourceAndCorrespondenceGraphs();
				break;
			case CORRESPONDENCE:
				removeSourceGraph();
				removeTargetGraph();
			}

		return graph.getRoots();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.mkonersmann.henshin.tgg.api.TGGExecutor#importGraph(org.eclipse.emf
	 * .ecore.EObject)
	 */
	@Override
	public EGraph importModel(List<EObject> rootObjects) {
		graph = new EGraphImpl(rootObjects);
		return graph;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.mkonersmann.henshin.tgg.api.TGGExecutor#importModel(org.eclipse.emf
	 * .ecore.EObject, java.lang.String)
	 */
	@Override
	public EGraph importModel(EObject rootObject) {
		final LinkedList<EObject> l = new LinkedList<EObject>();
		l.add(rootObject);
		return importModel(l);
	}

	@Override
	public Map<List<EObject>, List<EObject>> getInitial2ResultElements() {
		return source2target;
	}

	class AddTreeElementsToSet implements Consumer<EObject> {
		HashSet<EObject> set;

		public AddTreeElementsToSet(HashSet<EObject> set) {
			this.set = set;
		}

		@Override
		public void accept(EObject t) {
			set.add(t);
			t.eAllContents().forEachRemaining(new AddTreeElementsToSet(set));
		}
	}

	@SuppressWarnings("unchecked")
	private void resolveSource2TargetMap(boolean isForward) {
		source2target.clear();
		final HashMap<EObject, TripleComponent> tripleComponentNodeMap = trafoInfo.getTripleComponentNodeMap();

		final HashSet<EObject> sourceNodes = new HashSet<>();
		graph.getRoots().parallelStream().filter(o -> tripleComponentNodeMap.get(o) == TripleComponent.SOURCE)
				.forEach(new AddTreeElementsToSet(sourceNodes));

		final HashSet<EObject> correspondenceNodes = new HashSet<>();
		graph.getRoots().parallelStream().filter(o -> tripleComponentNodeMap.get(o) == TripleComponent.CORRESPONDENCE)
				.forEach(new AddTreeElementsToSet(correspondenceNodes));

		final HashSet<EObject> targetNodes = new HashSet<>();
		graph.getRoots().parallelStream().filter(o -> tripleComponentNodeMap.get(o) == TripleComponent.TARGET)
				.forEach(new AddTreeElementsToSet(targetNodes));

		for (final EObject correspondenceNode : correspondenceNodes) {
			final List<EObject> sources = new LinkedList<>();
			final List<EObject> targets = new LinkedList<>();

			for (final EReference ref : correspondenceNode.eClass().getEAllReferences()) {
				final Set<EObject> refTargets = new HashSet<EObject>();
				if (!ref.isMany())
					refTargets.add((EObject) correspondenceNode.eGet(ref));
				else
					refTargets.addAll((List<EObject>) correspondenceNode.eGet(ref));

				sources.addAll(refTargets.stream().filter(t -> sourceNodes.contains(t)).collect(Collectors.toSet()));
				targets.addAll(refTargets.stream().filter(t -> targetNodes.contains(t)).collect(Collectors.toSet()));
			}

			if (isForward)
				source2target.put(sources, targets);
			else
				source2target.put(targets, sources);
		}
	}
}
