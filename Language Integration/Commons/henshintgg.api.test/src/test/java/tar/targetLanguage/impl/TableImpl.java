/**
 */
package tar.targetLanguage.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import tar.targetLanguage.Column;
import tar.targetLanguage.FKey;
import tar.targetLanguage.Table;
import tar.targetLanguage.TargetLanguagePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Table</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tar.targetLanguage.impl.TableImpl#getName <em>Name</em>}</li>
 *   <li>{@link tar.targetLanguage.impl.TableImpl#getFkeys <em>Fkeys</em>}</li>
 *   <li>{@link tar.targetLanguage.impl.TableImpl#getPkey <em>Pkey</em>}</li>
 *   <li>{@link tar.targetLanguage.impl.TableImpl#getCols <em>Cols</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TableImpl extends MinimalEObjectImpl.Container implements Table {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFkeys() <em>Fkeys</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFkeys()
	 * @generated
	 * @ordered
	 */
	protected EList<FKey> fkeys;

	/**
	 * The cached value of the '{@link #getPkey() <em>Pkey</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPkey()
	 * @generated
	 * @ordered
	 */
	protected Column pkey;

	/**
	 * The cached value of the '{@link #getCols() <em>Cols</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCols()
	 * @generated
	 * @ordered
	 */
	protected EList<Column> cols;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TargetLanguagePackage.Literals.TABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TargetLanguagePackage.TABLE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FKey> getFkeys() {
		if (fkeys == null) {
			fkeys = new EObjectContainmentEList<FKey>(FKey.class, this, TargetLanguagePackage.TABLE__FKEYS);
		}
		return fkeys;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Column getPkey() {
		if (pkey != null && pkey.eIsProxy()) {
			InternalEObject oldPkey = (InternalEObject)pkey;
			pkey = (Column)eResolveProxy(oldPkey);
			if (pkey != oldPkey) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TargetLanguagePackage.TABLE__PKEY, oldPkey, pkey));
			}
		}
		return pkey;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Column basicGetPkey() {
		return pkey;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPkey(Column newPkey) {
		Column oldPkey = pkey;
		pkey = newPkey;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TargetLanguagePackage.TABLE__PKEY, oldPkey, pkey));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Column> getCols() {
		if (cols == null) {
			cols = new EObjectContainmentEList<Column>(Column.class, this, TargetLanguagePackage.TABLE__COLS);
		}
		return cols;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TargetLanguagePackage.TABLE__FKEYS:
				return ((InternalEList<?>)getFkeys()).basicRemove(otherEnd, msgs);
			case TargetLanguagePackage.TABLE__COLS:
				return ((InternalEList<?>)getCols()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TargetLanguagePackage.TABLE__NAME:
				return getName();
			case TargetLanguagePackage.TABLE__FKEYS:
				return getFkeys();
			case TargetLanguagePackage.TABLE__PKEY:
				if (resolve) return getPkey();
				return basicGetPkey();
			case TargetLanguagePackage.TABLE__COLS:
				return getCols();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TargetLanguagePackage.TABLE__NAME:
				setName((String)newValue);
				return;
			case TargetLanguagePackage.TABLE__FKEYS:
				getFkeys().clear();
				getFkeys().addAll((Collection<? extends FKey>)newValue);
				return;
			case TargetLanguagePackage.TABLE__PKEY:
				setPkey((Column)newValue);
				return;
			case TargetLanguagePackage.TABLE__COLS:
				getCols().clear();
				getCols().addAll((Collection<? extends Column>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TargetLanguagePackage.TABLE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case TargetLanguagePackage.TABLE__FKEYS:
				getFkeys().clear();
				return;
			case TargetLanguagePackage.TABLE__PKEY:
				setPkey((Column)null);
				return;
			case TargetLanguagePackage.TABLE__COLS:
				getCols().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TargetLanguagePackage.TABLE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case TargetLanguagePackage.TABLE__FKEYS:
				return fkeys != null && !fkeys.isEmpty();
			case TargetLanguagePackage.TABLE__PKEY:
				return pkey != null;
			case TargetLanguagePackage.TABLE__COLS:
				return cols != null && !cols.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //TableImpl
