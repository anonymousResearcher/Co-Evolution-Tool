/**
 */
package src.source;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Association</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link src.source.Association#getName <em>Name</em>}</li>
 *   <li>{@link src.source.Association#getSrc <em>Src</em>}</li>
 *   <li>{@link src.source.Association#getDest <em>Dest</em>}</li>
 *   <li>{@link src.source.Association#getLeftMultiplicity <em>Left Multiplicity</em>}</li>
 * </ul>
 *
 * @see src.source.SourcePackage#getAssociation()
 * @model
 * @generated
 */
public interface Association extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see src.source.SourcePackage#getAssociation_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link src.source.Association#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Src</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Src</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Src</em>' reference.
	 * @see #setSrc(src.source.Class)
	 * @see src.source.SourcePackage#getAssociation_Src()
	 * @model required="true"
	 * @generated
	 */
	src.source.Class getSrc();

	/**
	 * Sets the value of the '{@link src.source.Association#getSrc <em>Src</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Src</em>' reference.
	 * @see #getSrc()
	 * @generated
	 */
	void setSrc(src.source.Class value);

	/**
	 * Returns the value of the '<em><b>Dest</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dest</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dest</em>' reference.
	 * @see #setDest(src.source.Class)
	 * @see src.source.SourcePackage#getAssociation_Dest()
	 * @model required="true"
	 * @generated
	 */
	src.source.Class getDest();

	/**
	 * Sets the value of the '{@link src.source.Association#getDest <em>Dest</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dest</em>' reference.
	 * @see #getDest()
	 * @generated
	 */
	void setDest(src.source.Class value);

	/**
	 * Returns the value of the '<em><b>Left Multiplicity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left Multiplicity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Multiplicity</em>' attribute.
	 * @see #setLeftMultiplicity(int)
	 * @see src.source.SourcePackage#getAssociation_LeftMultiplicity()
	 * @model
	 * @generated
	 */
	int getLeftMultiplicity();

	/**
	 * Sets the value of the '{@link src.source.Association#getLeftMultiplicity <em>Left Multiplicity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left Multiplicity</em>' attribute.
	 * @see #getLeftMultiplicity()
	 * @generated
	 */
	void setLeftMultiplicity(int value);

} // Association
