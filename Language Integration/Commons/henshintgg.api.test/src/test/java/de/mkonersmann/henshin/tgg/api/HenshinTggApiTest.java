package de.mkonersmann.henshin.tgg.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;
import org.eclipse.emf.henshin.trace.Trace;
import org.junit.Test;
import org.osgi.framework.FrameworkUtil;

import de.tub.tfs.henshin.tgg.TggPackage;
import src.source.SourcePackage;
import tar.targetLanguage.TargetLanguagePackage;

public class HenshinTggApiTest {

	@Test
	public void testForwardTransformation() {
		final HenshinResourceSet resourceSet = new HenshinResourceSet();
		TggPackage.eINSTANCE.getName();

		// Load MMs
		SourcePackage.eINSTANCE.getName();
		TargetLanguagePackage.eINSTANCE.getName();

		final TGGModel model = TGGExecutorFactory.INSTANCE.createTGGModel(
				URI.createPlatformPluginURI(FrameworkUtil.getBundle(this.getClass()).getSymbolicName() + "/src/test/resources/test.henshin", true));
		final Resource initialGraph = resourceSet
				.getResource("src/test/resources/forwardTransformationTest/initialGraph.xmi");
		final TGGExecutor executor = model.createTGGExecutor(initialGraph.getContents());

		assertTrue(executor.executeForwardTransformation());
		final List<EObject> resultRoots = executor.createModelInstanceFromTripleGraph(TGGTripleComponent.TARGET);
		assertEquals(1, resultRoots.size());
		assertEquals("Database", resultRoots.get(0).eClass().getName());
		assertEquals(0, resultRoots.get(0).eContents().size());
	}

	@Test
	public void testBackwardTransformation() {
		final HenshinResourceSet resourceSet = new HenshinResourceSet();
		TggPackage.eINSTANCE.getName();

		// Load MMs
		SourcePackage.eINSTANCE.getName();
		TargetLanguagePackage.eINSTANCE.getName();

		final TGGModel model = TGGExecutorFactory.INSTANCE.createTGGModel(
				URI.createPlatformPluginURI(FrameworkUtil.getBundle(this.getClass()).getSymbolicName() + "/src/test/resources/test.henshin", true));
		final Resource initialGraph = resourceSet
				.getResource("src/test/resources/backwardTransformationTest/initialGraph.xmi");
		final TGGExecutor executor = model.createTGGExecutor(initialGraph.getContents());

		assertTrue(executor.executeBackwardTransformation());
		final List<EObject> resultRoots = executor.createModelInstanceFromTripleGraph(TGGTripleComponent.SOURCE);

		assertEquals(1, resultRoots.size());
		assertEquals("ClassDiagram", resultRoots.get(0).eClass().getName());
		assertEquals(1, resultRoots.get(0).eContents().size());
	}

	@Test
	public void testForwardPropagation() {
		final HenshinResourceSet resourceSet = new HenshinResourceSet();
		TggPackage.eINSTANCE.getName();

		// Load MMs
		SourcePackage.eINSTANCE.getName();
		TargetLanguagePackage.eINSTANCE.getName();

		final TGGModel model = TGGExecutorFactory.INSTANCE.createTGGModel(
				URI.createPlatformPluginURI(FrameworkUtil.getBundle(this.getClass()).getSymbolicName() + "/src/test/resources/test.henshin", true));
		final Resource initialGraph = resourceSet
				.getResource("src/test/resources/forwardPropagationTest/initialGraph.xmi");
		final TGGExecutor executor = model.createTGGExecutor(initialGraph.getContents());

		assertTrue(executor.executeForwardPropagation());
		final List<EObject> resultRoots = executor.createModelInstanceFromTripleGraph(TGGTripleComponent.TARGET);

		assertEquals(1, resultRoots.size());
		assertEquals("Database", resultRoots.get(0).eClass().getName());
		assertEquals(2, resultRoots.get(0).eContents().size());
	}

	@Test
	public void testBackwardPropagation() {
		final HenshinResourceSet resourceSet = new HenshinResourceSet();
		TggPackage.eINSTANCE.getName();

		// Load MMs
		SourcePackage.eINSTANCE.getName();
		TargetLanguagePackage.eINSTANCE.getName();

		final TGGModel model = TGGExecutorFactory.INSTANCE.createTGGModel(URI.createPlatformPluginURI(
				FrameworkUtil.getBundle(this.getClass()).getSymbolicName() + "/src/test/resources/test.henshin", true));
		final Resource initialGraph = resourceSet
				.getResource("src/test/resources/backwardPropagationTest/initialGraph.xmi");
		final TGGExecutor executor = model.createTGGExecutor(initialGraph.getContents());

		assertTrue(executor.executeBackwardPropagation());
		final List<EObject> resultRoots = executor.createModelInstanceFromTripleGraph(TGGTripleComponent.SOURCE);

		assertEquals(1, resultRoots.size());
		assertEquals("ClassDiagram", resultRoots.get(0).eClass().getName());
		assertEquals(2, resultRoots.get(0).eContents().size());
	}

	@Test
	public void testIntegrationTest() {
		final HenshinResourceSet resourceSet = new HenshinResourceSet();
		TggPackage.eINSTANCE.getName();

		// Load MMs
		SourcePackage.eINSTANCE.getName();
		TargetLanguagePackage.eINSTANCE.getName();

		final TGGModel model = TGGExecutorFactory.INSTANCE.createTGGModel(
				URI.createPlatformPluginURI(
						FrameworkUtil.getBundle(this.getClass()).getSymbolicName() + "/src/test/resources/test.henshin", true));
		final Resource initialGraph = resourceSet.getResource("src/test/resources/integrationTest/initialGraph.xmi");
		final TGGExecutor executor = model.createTGGExecutor(initialGraph.getContents());

		assertTrue(executor.executeIntegration());
		final List<EObject> resultRoots = executor.createModelInstanceFromTripleGraph();

		assertEquals(3, resultRoots.size());
		assertEquals("ClassDiagram", resultRoots.get(0).eClass().getName());
		assertEquals(1, resultRoots.get(0).eContents().size());

		assertEquals("Database", resultRoots.get(1).eClass().getName());
		assertEquals(2, resultRoots.get(1).eContents().size());

		assertEquals("Trace", resultRoots.get(2).eClass().getName());
		assertEquals(1, resultRoots.get(2).eContents().size());
		assertSame(resultRoots.get(0), ((Trace) resultRoots.get(2)).getSource().get(0));
		assertSame(resultRoots.get(1), ((Trace) resultRoots.get(2)).getTarget().get(0));

		assertEquals(1, ((Trace) resultRoots.get(2)).getSubTraces().size());
		assertSame(resultRoots.get(0).eContents().get(0),
				((Trace) resultRoots.get(2)).getSubTraces().get(0).getSource().get(0));
		assertSame(resultRoots.get(1).eContents().get(0),
				((Trace) resultRoots.get(2)).getSubTraces().get(0).getTarget().get(0));
	}
}
