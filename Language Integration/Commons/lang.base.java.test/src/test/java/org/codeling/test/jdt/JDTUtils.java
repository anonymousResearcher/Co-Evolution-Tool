package org.codeling.test.jdt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.launching.JavaRuntime;

public class JDTUtils {
	public static IJavaProject createSimpleProject(String name) throws ExecutionException, CoreException {
		IProject proj = getProject(name);
		if (proj != null) {
			proj.delete(true, null);
		}

		IJavaProject project = getJavaProject(name, "bin");

		// set the build path
		IClasspathEntry[] buildPath = {
				JavaCore.newSourceEntry(project.getProject().getFullPath().append("src").append("main").append("java")),
				JavaRuntime.getDefaultJREContainerEntry() };

		project.setRawClasspath(buildPath, project.getProject().getFullPath().append("bin"), null);

		// create folder by using resources package
		IFolder folder = project.getProject().getFolder("src");
		folder.create(true, true, null);
		folder = folder.getFolder("main");
		folder.create(true, true, null);
		folder = folder.getFolder("java");
		folder.create(true, true, null);

		return project;
	}

	public static IProject getProject(String name) {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IProject project = root.getProject(name);
		if (!project.exists()) {
			return null;
		}
		return project;
	}

	/**
	 * Creates a IJavaProject.
	 * 
	 * @param projectName
	 *            The name of the project
	 * @return Returns the Java project handle
	 * @throws CoreException
	 *             Project creation failed
	 */
	public static IJavaProject getJavaProject(String projectName) throws CoreException {
		return getJavaProject(projectName, "bin");
	}

	/**
	 * Creates a IJavaProject.
	 * 
	 * @param projectName
	 *            The name of the project
	 * @param binFolderName
	 *            Name of the output folder
	 * @return Returns the Java project handle
	 * @throws CoreException
	 *             Project creation failed
	 */
	public static IJavaProject getJavaProject(String projectName, String binFolderName) throws CoreException {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IProject project = root.getProject(projectName);
		if (!project.exists()) {
			project.create(null);
		} else {
			project.refreshLocal(IResource.DEPTH_INFINITE, null);
		}

		if (!project.isOpen()) {
			project.open(null);
		}

		IPath outputLocation;
		if (binFolderName != null && binFolderName.length() > 0) {
			IFolder binFolder = project.getFolder(binFolderName);
			if (!binFolder.exists()) {
				binFolder.create(false, true, null);
			}
			outputLocation = binFolder.getFullPath();
		} else {
			outputLocation = project.getFullPath();
		}

		if (!project.hasNature(JavaCore.NATURE_ID)) {
			addNatureToProject(project, JavaCore.NATURE_ID, null);
		}

		IJavaProject jproject = JavaCore.create(project);

		jproject.setOutputLocation(outputLocation, null);
		jproject.setRawClasspath(new IClasspathEntry[0], null);

		return jproject;
	}

	private static void addNatureToProject(IProject proj, String natureId, IProgressMonitor monitor)
			throws CoreException {
		IProjectDescription description = proj.getDescription();
		String[] prevNatures = description.getNatureIds();
		String[] newNatures = new String[prevNatures.length + 1];
		System.arraycopy(prevNatures, 0, newNatures, 0, prevNatures.length);
		newNatures[prevNatures.length] = natureId;
		description.setNatureIds(newNatures);
		proj.setDescription(description, monitor);
	}

	public IPackageFragment createRootPackage(IJavaProject javaProject, IPackageFragmentRoot packageFragmentRoot,
			String packageName) throws JavaModelException {
		return packageFragmentRoot.createPackageFragment(packageName, true, null);
	}

	public static void removeProject(String projectName) throws CoreException {
		IProject p = getProject(projectName);
		if (p != null) {
			p.delete(true, true, new NullProgressMonitor());
		}
	}

	public static String getTestJavaSource(ClassLoader classLoader, String resourcePath) throws IOException {
		URL url;
		BufferedReader in = null;
		String content = "";

		try {
			url = classLoader.getResource("src/test/resources/" + resourcePath);
			InputStream inputStream = url.openConnection().getInputStream();
			in = new BufferedReader(new InputStreamReader(inputStream));
			String inputLine;
			String nl = System.getProperty("line.separator");
			while ((inputLine = in.readLine()) != null) {
				content += inputLine + nl;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (in != null)
				in.close();
		}
		return content;
	}

}
