/**
 */
package il.metamodel.ilDummy;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see il.metamodel.ilDummy.DummyFactory
 * @model kind="package"
 * @generated
 */
public interface DummyPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ilDummy";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://example.org/ilDummy/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ilDummy";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DummyPackage eINSTANCE = il.metamodel.ilDummy.impl.DummyPackageImpl.init();

	/**
	 * The meta object id for the '{@link il.metamodel.ilDummy.impl.ILComponentImpl <em>IL Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see il.metamodel.ilDummy.impl.ILComponentImpl
	 * @see il.metamodel.ilDummy.impl.DummyPackageImpl#getILComponent()
	 * @generated
	 */
	int IL_COMPONENT = 0;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IL_COMPONENT__VERSION = 0;

	/**
	 * The number of structural features of the '<em>IL Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IL_COMPONENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>IL Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IL_COMPONENT_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link il.metamodel.ilDummy.ILComponent <em>IL Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IL Component</em>'.
	 * @see il.metamodel.ilDummy.ILComponent
	 * @generated
	 */
	EClass getILComponent();

	/**
	 * Returns the meta object for the attribute '{@link il.metamodel.ilDummy.ILComponent#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see il.metamodel.ilDummy.ILComponent#getVersion()
	 * @see #getILComponent()
	 * @generated
	 */
	EAttribute getILComponent_Version();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DummyFactory getDummyFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link il.metamodel.ilDummy.impl.ILComponentImpl <em>IL Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see il.metamodel.ilDummy.impl.ILComponentImpl
		 * @see il.metamodel.ilDummy.impl.DummyPackageImpl#getILComponent()
		 * @generated
		 */
		EClass IL_COMPONENT = eINSTANCE.getILComponent();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IL_COMPONENT__VERSION = eINSTANCE.getILComponent_Version();

	}

} //DummyPackage
