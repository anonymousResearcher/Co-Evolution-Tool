/**
 */
package jobDependencyTestMetaModel.impl;

import jobDependencyTestMetaModel.Bean;
import jobDependencyTestMetaModel.JobDependencyTestMetaModelPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bean</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BeanImpl extends MinimalEObjectImpl.Container implements Bean {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BeanImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JobDependencyTestMetaModelPackage.Literals.BEAN;
	}

} //BeanImpl
