/**
 */
package jobDependencyTestMetaModel.impl;

import jobDependencyTestMetaModel.JobDependencyTestMetaModelPackage;
import jobDependencyTestMetaModel.Provision;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Provision</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ProvisionImpl extends MinimalEObjectImpl.Container implements Provision {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProvisionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JobDependencyTestMetaModelPackage.Literals.PROVISION;
	}

} //ProvisionImpl
