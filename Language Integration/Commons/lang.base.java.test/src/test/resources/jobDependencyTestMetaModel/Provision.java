/**
 */
package jobDependencyTestMetaModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Provision</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see jobDependencyTestMetaModel.JobDependencyTestMetaModelPackage#getProvision()
 * @model
 * @generated
 */
public interface Provision extends EObject {
} // Provision
