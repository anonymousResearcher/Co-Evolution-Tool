/**
 */
package jobDependencyTestMetaModel;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see jobDependencyTestMetaModel.JobDependencyTestMetaModelFactory
 * @model kind="package"
 * @generated
 */
public interface JobDependencyTestMetaModelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "jobDependencyTestMetaModel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://example.org/jdtmm";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "jdtmm";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	JobDependencyTestMetaModelPackage eINSTANCE = jobDependencyTestMetaModel.impl.JobDependencyTestMetaModelPackageImpl.init();

	/**
	 * The meta object id for the '{@link jobDependencyTestMetaModel.impl.BeanImpl <em>Bean</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see jobDependencyTestMetaModel.impl.BeanImpl
	 * @see jobDependencyTestMetaModel.impl.JobDependencyTestMetaModelPackageImpl#getBean()
	 * @generated
	 */
	int BEAN = 0;

	/**
	 * The number of structural features of the '<em>Bean</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEAN_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Bean</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEAN_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link jobDependencyTestMetaModel.impl.InterfaceImpl <em>Interface</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see jobDependencyTestMetaModel.impl.InterfaceImpl
	 * @see jobDependencyTestMetaModel.impl.JobDependencyTestMetaModelPackageImpl#getInterface()
	 * @generated
	 */
	int INTERFACE = 1;

	/**
	 * The number of structural features of the '<em>Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link jobDependencyTestMetaModel.impl.ProvisionImpl <em>Provision</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see jobDependencyTestMetaModel.impl.ProvisionImpl
	 * @see jobDependencyTestMetaModel.impl.JobDependencyTestMetaModelPackageImpl#getProvision()
	 * @generated
	 */
	int PROVISION = 2;

	/**
	 * The number of structural features of the '<em>Provision</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVISION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Provision</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVISION_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link jobDependencyTestMetaModel.Bean <em>Bean</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bean</em>'.
	 * @see jobDependencyTestMetaModel.Bean
	 * @generated
	 */
	EClass getBean();

	/**
	 * Returns the meta object for class '{@link jobDependencyTestMetaModel.Interface <em>Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interface</em>'.
	 * @see jobDependencyTestMetaModel.Interface
	 * @generated
	 */
	EClass getInterface();

	/**
	 * Returns the meta object for class '{@link jobDependencyTestMetaModel.Provision <em>Provision</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provision</em>'.
	 * @see jobDependencyTestMetaModel.Provision
	 * @generated
	 */
	EClass getProvision();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	JobDependencyTestMetaModelFactory getJobDependencyTestMetaModelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link jobDependencyTestMetaModel.impl.BeanImpl <em>Bean</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see jobDependencyTestMetaModel.impl.BeanImpl
		 * @see jobDependencyTestMetaModel.impl.JobDependencyTestMetaModelPackageImpl#getBean()
		 * @generated
		 */
		EClass BEAN = eINSTANCE.getBean();

		/**
		 * The meta object literal for the '{@link jobDependencyTestMetaModel.impl.InterfaceImpl <em>Interface</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see jobDependencyTestMetaModel.impl.InterfaceImpl
		 * @see jobDependencyTestMetaModel.impl.JobDependencyTestMetaModelPackageImpl#getInterface()
		 * @generated
		 */
		EClass INTERFACE = eINSTANCE.getInterface();

		/**
		 * The meta object literal for the '{@link jobDependencyTestMetaModel.impl.ProvisionImpl <em>Provision</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see jobDependencyTestMetaModel.impl.ProvisionImpl
		 * @see jobDependencyTestMetaModel.impl.JobDependencyTestMetaModelPackageImpl#getProvision()
		 * @generated
		 */
		EClass PROVISION = eINSTANCE.getProvision();

	}

} //JobDependencyTestMetaModelPackage
