/**
 */
package jobDependencyTestMetaModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bean</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see jobDependencyTestMetaModel.JobDependencyTestMetaModelPackage#getBean()
 * @model
 * @generated
 */
public interface Bean extends EObject {
} // Bean
