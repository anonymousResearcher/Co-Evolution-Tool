package org.codeling.lang.base.java;

import java.util.concurrent.ExecutionException;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.launching.JavaRuntime;

public class JDTUtils {
	public static IJavaProject createSimpleProject(String name) throws ExecutionException, CoreException {
		final IProject proj = getProject(name);
		if (proj != null) {
			proj.delete(true, null);
		}

		final IJavaProject project = getJavaProject(name, "bin");

		// set the build path
		final IClasspathEntry[] buildPath = {
				JavaCore.newSourceEntry(project.getProject().getFullPath().append("src").append("main").append("java")),
				JavaRuntime.getDefaultJREContainerEntry() };

		project.setRawClasspath(buildPath, project.getProject().getFullPath().append("bin"), null);

		// create folder by using resources package
		IFolder folder = project.getProject().getFolder("src");
		folder.create(true, true, null);
		folder = folder.getFolder("main");
		folder.create(true, true, null);
		folder = folder.getFolder("java");
		folder.create(true, true, null);

		return project;
	}

	public static IProject getProject(String name) {
		final IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		final IProject project = root.getProject(name);
		if (!project.exists()) {
			return null;
		}
		return project;
	}

	/**
	 * Creates a IJavaProject.
	 *
	 * @param projectName
	 *            The name of the project
	 * @return Returns the Java project handle
	 * @throws CoreException
	 *             Project creation failed
	 */
	public static IJavaProject getJavaProject(String projectName) throws CoreException {
		return getJavaProject(projectName, "bin");
	}

	/**
	 * Creates a IJavaProject.
	 *
	 * @param projectName
	 *            The name of the project
	 * @param binFolderName
	 *            Name of the output folder
	 * @return Returns the Java project handle
	 * @throws CoreException
	 *             Project creation failed
	 */
	public static IJavaProject getJavaProject(String projectName, String binFolderName) throws CoreException {
		final IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		final IProject project = root.getProject(projectName);
		if (!project.exists()) {
			project.create(null);
		} else {
			project.refreshLocal(IResource.DEPTH_INFINITE, null);
		}

		if (!project.isOpen()) {
			project.open(null);
		}

		IPath outputLocation;
		if (binFolderName != null && binFolderName.length() > 0) {
			final IFolder binFolder = project.getFolder(binFolderName);
			if (!binFolder.exists()) {
				binFolder.create(false, true, null);
			}
			outputLocation = binFolder.getFullPath();
		} else {
			outputLocation = project.getFullPath();
		}

		if (!project.hasNature(JavaCore.NATURE_ID)) {
			addNatureToProject(project, JavaCore.NATURE_ID, null);
		}

		final IJavaProject jproject = JavaCore.create(project);

		jproject.setOutputLocation(outputLocation, null);
		jproject.setRawClasspath(new IClasspathEntry[0], null);

		return jproject;
	}

	private static void addNatureToProject(IProject proj, String natureId, IProgressMonitor monitor)
			throws CoreException {
		final IProjectDescription description = proj.getDescription();
		final String[] prevNatures = description.getNatureIds();
		final String[] newNatures = new String[prevNatures.length + 1];
		System.arraycopy(prevNatures, 0, newNatures, 0, prevNatures.length);
		newNatures[prevNatures.length] = natureId;
		description.setNatureIds(newNatures);
		proj.setDescription(description, monitor);
	}

	public IPackageFragment createRootPackage(IJavaProject javaProject, IPackageFragmentRoot packageFragmentRoot,
			String packageName) throws JavaModelException {
		return packageFragmentRoot.createPackageFragment(packageName, true, null);
	}

	public static void removeProject(String projectName) throws CoreException {
		final IProject p = getProject(projectName);
		if (p != null) {
			p.delete(true, true, new NullProgressMonitor());
		}
	}

	/**
	 * Adds the desired import to the given type, if it is neither already imported
	 * (either directly or with the .* notation), nor if it is in the same package
	 * as the given type.
	 */
	public static void addImportIfNecessary(IType type, String importString, IProgressMonitor monitor)
			throws JavaModelException {
		// If the import string is empty, do not change anything.
		if (importString == null || importString.isEmpty())
			return;
		String onDemandAlternative = importString.substring(0, importString.lastIndexOf(".") + 1) + "*";
		ICompilationUnit cu = type.getCompilationUnit();
		if (cu.getImport(importString).exists() || cu.getImport(onDemandAlternative).exists()
				|| type.getPackageFragment().getElementName().equals(importString))
			return;
		cu.createImport(importString, null, monitor);
	}

}
