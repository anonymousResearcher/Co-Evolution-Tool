package org.codeling.mechanisms.transformations.classes

import org.codeling.lang.base.java.ASTUtils
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation
import org.codeling.mechanisms.transformations.ClassMechanismTransformation
import org.codeling.utils.CodelingException
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.jdt.core.ICompilationUnit
import org.eclipse.jdt.core.IJavaElement
import org.eclipse.jdt.core.IPackageFragment
import org.eclipse.jdt.core.IType

abstract class TypeAnnotationTransformation<ELEMENTECLASS extends EObject> extends ClassMechanismTransformation<ELEMENTECLASS, IType> {

	new(AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation,
		EClass eClass) {
		super(parentTransformation, eClass)
	}

	override createCodeFragments() throws CodelingException {
		var IPackageFragment targetPackage = if (parentCodeElement !== null && parentCodeElement instanceof IType)
				(parentCodeElement as IType).packageFragment
			else
				createDefaultPackage();

		val String name = modelElement.nameAttributeValue;
		val String typeName = name.toFirstUpper;
		val String annotationName = getNewAnnotationName().substring(getNewAnnotationName().lastIndexOf(".") + 1);
		val String annotationPackage = getNewAnnotationName().substring(0, getNewAnnotationName().lastIndexOf("."));

		val String content = '''
		package «targetPackage.elementName»;
		
		import «annotationPackage».«annotationName»;
		
		@«annotationName»
		public class «typeName» {}''';
		val ICompilationUnit cu = targetPackage.createCompilationUnit(typeName + ".java", content, true, null);
		codeElement = cu.getType(typeName);
	}

	override updateCodeFragments() throws CodelingException {
		// Update of name attribute
		val String oldName = priorModelElement.nameAttributeValue;
		val String newName = modelElement.nameAttributeValue;

		if (!newName.equals(oldName)) {
			ASTUtils.renameType(codeElement, newName); // Execute renaming refactoring
			codeElement = codeElement.getPackageFragment().getCompilationUnit(newName + ".java").getType(newName);
		}
	}

	override deleteCodeFragments() {
		codeElement.compilationUnit.delete(true, null);
	}

	override transformToModel() throws CodelingException {
		// Create element
		modelElement = eClass.EPackage.EFactoryInstance.create(eClass);

		// Set name attribute value
		val String name = codeElement.elementName;
		setNameAttributeValue(modelElement, name);

		return modelElement;
	}

	public def boolean hasExpectedAnnotation(IType type) {
		return type.getAnnotation(
			eClass.name.toFirstUpper
		).exists;
	}

	protected def String getNewAnnotationName() {
		return '''org.codeling.lang.«getLanguageName».mm.«eClass.name.toFirstUpper»''';
	}

}
