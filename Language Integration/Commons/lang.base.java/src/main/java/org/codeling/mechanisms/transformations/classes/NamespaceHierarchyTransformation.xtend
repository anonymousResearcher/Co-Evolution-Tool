package org.codeling.mechanisms.transformations.classes

import org.codeling.lang.base.java.ASTUtils
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation
import java.util.List
import org.codeling.mechanisms.transformations.ClassMechanismTransformation
import org.codeling.utils.CodelingException
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.jdt.core.IJavaElement
import org.eclipse.jdt.core.IPackageFragment

abstract class NamespaceHierarchyTransformation<ELEMENTECLASS extends EObject> extends ClassMechanismTransformation<ELEMENTECLASS, IPackageFragment> {
	EReference rootNamespaceEReference;
	EReference childReference

	new(AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation,
		EClass namespaceEClass, EReference rootNamespaceEReference, EReference childReference) {
		super(parentTransformation, namespaceEClass)
		this.rootNamespaceEReference = rootNamespaceEReference;
		this.childReference = childReference;
	}

	override resolveCodeElement() throws CodelingException {
		val StringBuffer packageFQN = new StringBuffer();
		var EObject currentPackage = modelElement;
		do {
			if (currentPackage != modelElement)
				packageFQN.append(".");
			packageFQN.append(currentPackage.nameAttributeValue);
			currentPackage = modelElement.eContainer;
		} while (currentPackage.eClass == eClass);
		codeElement = ASTUtils.getPackageFragment(packageFQN.toString(), codeRoot);
	}

	override createCodeFragments() throws CodelingException {
	}

	override updateCodeFragments() throws CodelingException {
	}

	override deleteCodeFragments() {
	}

	override transformToModel() throws CodelingException {
		// Find the list of fragments
		val String[] fragments = codeElement.elementName.split("\\."); // a,b,c
		modelElement = eClass.EPackage.EFactoryInstance.create(eClass) as ELEMENTECLASS;
		modelElement.nameAttributeValue = fragments.last;
		return modelElement;
	}

	def ELEMENTECLASS addNewNamespaceBranch(ELEMENTECLASS parentNamespace, List<String> fragments) {
		var lastNamespace = parentNamespace;
		for (String fragment : fragments) {
			val ELEMENTECLASS namespace = eClass.EPackage.EFactoryInstance.create(eClass) as ELEMENTECLASS;
			namespace.nameAttributeValue = fragment;
			addToOrSetReference(lastNamespace, childReference, namespace);
			lastNamespace = namespace;
		}
		return lastNamespace;
	}

	def ELEMENTECLASS addNewNamespaceRoot(String[] fragments) {
		var ELEMENTECLASS rootNamespace = null;
		var EObject parentNamespace = null;
		for (String fragment : fragments) {
			val ELEMENTECLASS namespace = eClass.EPackage.EFactoryInstance.create(eClass) as ELEMENTECLASS;
			namespace.nameAttributeValue = fragment;
			if (parentNamespace === null) {
				rootNamespace = namespace;
				addToOrSetReference(parentTransformation.modelElement, rootNamespaceEReference, namespace);
			} else {
				addToOrSetReference(parentNamespace, childReference, namespace);
			}
			parentNamespace = namespace;
		}
		return rootNamespace
	}

	def void addToOrSetReference(EObject from, EReference reference, EObject to) {
		if (reference.isMany) {
			(from.eGet(reference) as List<EObject>).add(to);
		} else {
			from.eSet(reference, to);
		}
	}

}
