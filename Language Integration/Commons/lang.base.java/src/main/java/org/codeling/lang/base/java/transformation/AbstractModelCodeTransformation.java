package org.codeling.lang.base.java.transformation;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.RecursiveAction;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.ModelUtils;
import org.codeling.lang.base.java.transformation.references.IALTransformation;
import org.codeling.languageregistry.LanguageDefinition;
import org.codeling.utils.CodelingException;
import org.codeling.utils.CodelingLogger;
import org.codeling.utils.IDRegistry;
import org.codeling.utils.TraceLogTopic;
import org.codeling.utils.errorcontainers.IMayHaveIssues;
import org.codeling.utils.errorcontainers.Issue;
import org.codeling.utils.errorcontainers.Level;
import org.codeling.utils.errorcontainers.MayHaveIssues;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;

public abstract class AbstractModelCodeTransformation<ELEMENTECLASS extends EObject, JAVAELEMENTCLASS extends IJavaElement>
		extends RecursiveAction implements IMayHaveIssues, IModelCodeTransformation<ELEMENTECLASS, JAVAELEMENTCLASS> {

	protected CodelingLogger log = new CodelingLogger(getClass());
	public static final String FQN_SEPARATOR = ",";
	public static final String FQN_SEPARATOR_REGEXP = ",";

	/**
	 * Should the transformation execute towards the code or towards the model?
	 **/
	protected boolean toCode = false;

	/**
	 * Should the transformation translate only cross references? The transformation
	 * is to be called once with onlyCrossReferences set to false, then, afterwards
	 * with it being set to true.
	 */
	protected boolean onlyCrossReferences = false;

	/** The reference to the code root **/
	protected List<IJavaProject> codeRoot;

	/** The LHS model element **/
	protected ELEMENTECLASS modelElement;

	/** The reference to the modelElement's parent **/
	protected EObject parentModelElement;

	/** The RHS code element **/
	protected JAVAELEMENTCLASS codeElement;

	/** The reference to the modelElement's parent codelement **/
	protected IJavaElement parentCodeElement;

	/** The LHS model element of the former model, if applicable **/
	protected ELEMENTECLASS priorModelElement;

	/** The IDRegistry of the former model, if applicable **/
	protected IDRegistry priorIDRegistry;

	/** The reference to the dependency manager **/
	protected FindTranslatedElements findTranslatedElements;

	/**
	 * The list of transformations of containment reference transformations
	 */
	private List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> childTransformations;

	/**
	 * The list of transformations for cross references transformations
	 */
	private List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> crossReferencesTransformations;

	/**
	 * A registry of id mappings
	 */
	protected IDRegistry idRegistry;

	/**
	 * The parent transformation in a tree structure
	 */
	protected IModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation;

	/**
	 * The language definition, which declares this transformation
	 */
	private LanguageDefinition languageDefinition;

	protected AbstractModelCodeTransformation(
			AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation) {
		this.parentTransformation = parentTransformation;
		if (parentTransformation != null) {
			this.parentModelElement = parentTransformation.modelElement;
			this.parentCodeElement = parentTransformation.codeElement;
			this.idRegistry = parentTransformation.idRegistry;
			this.priorIDRegistry = parentTransformation.priorIDRegistry;
			this.findTranslatedElements = parentTransformation.findTranslatedElements;
			this.codeRoot = parentTransformation.codeRoot;
			this.languageDefinition = parentTransformation.getLanguageDefinition();

			if (this instanceof IALTransformation && parentTransformation instanceof IALTransformation) {
				IALTransformation<?, ?> thisIAL = ((IALTransformation<?, ?>) this);
				IALTransformation<?, ?> parentIAL = ((IALTransformation<?, ?>) parentTransformation);
				thisIAL.getIALHolder().setIALRoots(parentIAL.getIALHolder().getIALRoots());
			}
			parentTransformation.addChildIssue(this);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.concurrent.RecursiveAction#compute()
	 */
	@Override
	protected void compute() {
		try {
			LinkedList<String> validationMessages = new LinkedList<>();
			if (toCode) {
				if (validateToCode(validationMessages))
					computeTransformationToCode();
			} else {
				if (validateToModel(validationMessages))
					computeTransformationToModel();
				else
					validationMessages.forEach(m -> addWarning(m));
			}
		} catch (Throwable t) {
			addError("Error while executing model-code transformation " + this.getClass().getSimpleName(), t);
		}
	}

	private void computeTransformationToCode() {
		log.trace(TraceLogTopic.M2C_EXECUTED_TRANSFORMATIONS, String.format(
				"Executing %s in direction Model-to-Code for the eclass %s with the name %s",
				this.getClass().getSimpleName(), modelElement != null ? modelElement.eClass().getName() : "(none)",
				modelElement != null && modelElement.eClass().getEAttributes().stream()
						.filter(a -> a.getName().equals("name")).count() > 0 ? getNameAttributeValue(modelElement)
								: "(none)"));

		if (!onlyCrossReferences) {
			try {
				if (modelElement == null) { // The model element has been deleted
					resolveCodeElement();
					deleteCodeFragments();
				} else if (priorModelElement == null) { // The model element is new
					createCodeFragments();
				} else { // The model element might have changed
					resolveCodeElement();
					updateCodeFragments();
				}
			} catch (final Throwable e) {
				final String message = String.format(
						"Could not execute model-to-code transformation '%s' for element '%s'.",
						this.getClass().getName(), modelElement);
				addError(message, e);
			} finally {
				try {
					// Execute child transformations.
					// ReferenceMechanismTransformation already do this themselves, because they use
					// the results of the child transformations. Therefore we only execute
					// childTransformations that do not already exist.
					if (childTransformations == null || childTransformations.isEmpty()) {
						childTransformations = createChildTransformationsToCode();
						childTransformations.forEach(c -> c.setOnlyCrossReferences(onlyCrossReferences));
						invokeAll(childTransformations);
					}
				} catch (final Throwable e) {
					final String message = String.format(
							"Could not create model-to-code cross reference transformations of '%s' for the elemnet '%s'.",
							this.getClass().getName(), modelElement);
					issues.addError(message, e);
				} finally {
					// Registers all class, reference, and attribute transformations for an Eclass.
					EClass eClass = modelElement == null ? priorModelElement.eClass() : modelElement.eClass();
					findTranslatedElements.addTransformation(eClass, this);
				}
			}
		} else {
			try {
				final List<AbstractModelCodeTransformation<?, ?>> crossReferenceTransformations = createCrossReferencesTransformations();
				crossReferenceTransformations.forEach(t -> t.setToCode());
				invokeAll(crossReferenceTransformations);
			} catch (final Throwable e) {
				final String message = String.format(
						"Could not execute cross reference model-to-code transformation '%s' for element '%s'.",
						this.getClass().getName(), modelElement);
				addError(message, e);
			}
		}

	}

	private void computeTransformationToModel() {
		log.trace(TraceLogTopic.M2C_EXECUTED_TRANSFORMATIONS,
				String.format("Executing %s in direction Code-to-Model for the Java element %s",
						this.getClass().getSimpleName(),
						codeElement != null ? codeElement.getHandleIdentifier() : "(none)"));

		if (!onlyCrossReferences) {
			try {
				// Actually execute the transformation
				transformToModel();
				if (modelElement == null)
					throw new CodelingException("The transformation " + this.getClass().getName()
							+ " resulted in 'null' as model element.");
			} catch (final Throwable e) {
				final String elementName = codeElement == null ? "[codeElement is null]"
						: codeElement.getHandleIdentifier();
				final String message = String.format(
						"Errors during the execution of the code-to-model transformation '%s' for code element '%s'.",
						this.getClass().getName(), elementName);
				addError(message, e);
			} finally {
				// Execute child transformations.
				// Some Transformations, such as the ReferenceMechanismTransformations, already
				// do this themselves, because they use the results of the child
				// transformations. Therefore we only execute childTransformations that do not
				// already exist.
				if (childTransformations == null || childTransformations.isEmpty()) {
					childTransformations = createChildTransformationsToModel();
					invokeAll(childTransformations);
				}

				// Registers all class, reference, and attribute transformations for an Eclass.
				if (modelElement != null)
					findTranslatedElements.addTransformation(modelElement.eClass(), this);
			}
		} else {
			try {
				// Instantiate all cross reference transformations and invoke them.
				// These transformation instances will not have the flag "onlyCrossReferences"
				// set, because they shall actually be executed.
				invokeAll(createCrossReferencesTransformations());
			} catch (final Throwable e) {
				final String elementName = codeElement == null ? "[codeElement is null]"
						: codeElement.getHandleIdentifier();
				final String message = String.format(
						"Errors during the execution of the cross-reference code-to-model transformation '%s' for code element '%s'.",
						this.getClass().getName(), elementName);
				addError(message, e);
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * resolveCodeElement()
	 */
	@Override
	public JAVAELEMENTCLASS resolveCodeElement() throws CodelingException {
		if (priorIDRegistry != null && priorModelElement != null) {
			if (this instanceof IALTransformation)
				codeElement = (JAVAELEMENTCLASS) priorIDRegistry.getCodeElementFromTranslationModelElement(
						((IALTransformation<?, ?>) this).getIALHolder().getPriorFoundationalIALElement());
			else
				codeElement = (JAVAELEMENTCLASS) priorIDRegistry
						.getCodeElementFromImplementationModelElement(priorModelElement);
		} else {
			if (this instanceof IALTransformation)
				codeElement = (JAVAELEMENTCLASS) idRegistry.getCodeElementFromTranslationModelElement(
						((IALTransformation<?, ?>) this).getIALHolder().getFoundationalIALElement());
			else
				codeElement = (JAVAELEMENTCLASS) idRegistry.getCodeElementFromImplementationModelElement(modelElement);
		}
		return codeElement;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * createCodeFragments()
	 */
	@Override
	public abstract void createCodeFragments() throws CodelingException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * updateCodeFragments()
	 */
	@Override
	public abstract void updateCodeFragments() throws CodelingException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * deleteCodeFragments()
	 */
	@Override
	public abstract void deleteCodeFragments() throws CodelingException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * transformToModel()
	 */
	@Override
	public abstract ELEMENTECLASS transformToModel() throws CodelingException;

	public boolean validateToModel(List<String> validationMessages) {
		if (validationMessages == null) {
			validationMessages = new LinkedList<>();
		}
		if (toCode) {
			validationMessages.add("toCode must be set to false.");
			return false;
		}
		return true;
	}

	public boolean validateToCode(List<String> validationMessages) {
		if (validationMessages == null) {
			validationMessages = new LinkedList<>();
		}
		if (!toCode) {
			validationMessages.add("toCode must be set to true.");
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * canHandle(org.eclipse.emf.ecore.EModelElement)
	 */
	@Override
	public abstract boolean canHandle(EModelElement metaModelElement);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * getCodeRoot()
	 */
	@Override
	public List<IJavaProject> getCodeRoot() {
		return codeRoot;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * setCodeRoot(java.util.List)
	 */
	@Override
	public void setCodeRoot(List<IJavaProject> codeRoot) {
		this.codeRoot = codeRoot;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * getModelElement()
	 */
	@Override
	public ELEMENTECLASS getModelElement() {
		return modelElement;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * setModelElement(org.eclipse.emf.ecore.EObject)
	 */
	@Override
	public void setModelElement(EObject modelElement) {
		this.modelElement = (ELEMENTECLASS) modelElement;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * getCodeElement()
	 */
	@Override
	public JAVAELEMENTCLASS getCodeElement() {
		return codeElement;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * setCodeElement(JAVAELEMENTCLASS)
	 */
	@Override
	public void setCodeElement(JAVAELEMENTCLASS codeElement) {
		this.codeElement = codeElement;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * setParentCodeElement(org.eclipse.jdt.core.IJavaElement)
	 */
	@Override
	public void setParentCodeElement(IJavaElement parent) {
		this.parentCodeElement = parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * getParentTransformation()
	 */
	@Override
	public IModelCodeTransformation<? extends EObject, ? extends IJavaElement> getParentTransformation() {
		return parentTransformation;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * getPriorModelElement()
	 */
	@Override
	public ELEMENTECLASS getPriorModelElement() {
		return priorModelElement;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * setPriorModelElement(org.eclipse.emf.ecore.EObject)
	 */
	@Override
	public void setPriorModelElement(EObject priorModelElement) {
		this.priorModelElement = (ELEMENTECLASS) priorModelElement;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * setPriorIDRegistry(de.mkonersmann.advert.utils.IDRegistry)
	 */
	@Override
	public void setPriorIDRegistry(IDRegistry priorIDRegistry) {
		this.priorIDRegistry = priorIDRegistry;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * setIDRegistry(de.mkonersmann.advert.utils.IDRegistry)
	 */
	@Override
	public void setIDRegistry(IDRegistry idRegistry) {
		this.idRegistry = idRegistry;
	}

	protected abstract void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result);

	protected abstract void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * doCreateCrossReferencesTransformations(java.util.List)
	 */
	@Override
	public abstract void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * createChildTransformationsToCode()
	 */
	@Override
	public List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> createChildTransformationsToCode() {
		if (childTransformations != null)
			return childTransformations;
		childTransformations = new LinkedList<>();
		doCreateChildTransformationsToCode(childTransformations);

		for (final IModelCodeTransformation<? extends EObject, ? extends IJavaElement> childTransformation : childTransformations) {
			childTransformation.setToCode();

			// Set the prior model element for possibly changed transformation model
			// elements.
			// All other model elements or priorModelElements will already be set by the
			// doCreate... methods.
			EObject childModelElement = childTransformation.getModelElement();
			if (this instanceof IALTransformation) 
				childModelElement = ((IALTransformation<?, ?>) childTransformation).getIALHolder()
						.getFoundationalIALElement();
			
			if (childModelElement == null)
				continue;

			EObject priorChildElement = null;
			if (this instanceof IALTransformation) {
				if (idRegistry.knowsTranslationModelElement(childModelElement)) {
					final String id = idRegistry.getIDFromTranslationModelElement(childModelElement);
					priorChildElement = priorIDRegistry.resolveTranslationModelElement(id,
							EcoreUtil.getRootContainer(priorModelElement));
				}
			} else {
				if (idRegistry.knowsImplementationModelElement(childModelElement)) {
					final String id = idRegistry.getIDFromImplementationModelElement(childModelElement);
					priorChildElement = priorIDRegistry.resolveImplementationModelElement(id,
							EcoreUtil.getRootContainer(priorModelElement));
				}
			}
			childTransformation.setPriorModelElement(priorChildElement);
		}
		return childTransformations;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * createChildTransformationsToModel()
	 */
	@Override
	public List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> createChildTransformationsToModel() {
		if (childTransformations != null)
			return childTransformations;
		childTransformations = new LinkedList<>();
		doCreateChildTransformationsToModel(childTransformations);
		return childTransformations;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * createCrossReferencesTransformations()
	 */
	@Override
	public List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> createCrossReferencesTransformations() {
		if (crossReferencesTransformations != null)
			return crossReferencesTransformations;
		crossReferencesTransformations = new LinkedList<>();
		doCreateCrossReferencesTransformations(crossReferencesTransformations);
		return crossReferencesTransformations;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * setToCode()
	 */
	@Override
	public void setToCode() {
		toCode = true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * setToModel()
	 */
	@Override
	public void setToModel() {
		toCode = false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * setOnlyCrossReferences(boolean)
	 */
	@Override
	public void setOnlyCrossReferences(boolean b) {
		onlyCrossReferences = b;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * getParentModelElement()
	 */
	@Override
	public EObject getParentModelElement() {
		return parentModelElement;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * setParentModelElement(org.eclipse.emf.ecore.EObject)
	 */
	@Override
	public void setParentModelElement(EObject parentModelElement) {
		this.parentModelElement = parentModelElement;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * setWhereToFindTranslatedElements(org.codeling.lang.base.java.
	 * transformation.FindTranslatedElements)
	 */
	@Override
	public void setWhereToFindTranslatedElements(FindTranslatedElements findTranslatedElements) {
		this.findTranslatedElements = findTranslatedElements;
	}

	public FindTranslatedElements getWhereToFindTranslatedElements() {
		return findTranslatedElements;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * getIdRegistry()
	 */
	@Override
	public IDRegistry getIdRegistry() {
		return idRegistry;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.codeling.lang.base.java.transformation.IModelCodeTransformation#
	 * getPriorIdRegistry()
	 */
	@Override
	public IDRegistry getPriorIDRegistry() {
		return priorIDRegistry;
	}

	public String getLanguageName() {
		return getLanguageDefinition().getSymbolicName();
	}

	public LanguageDefinition getLanguageDefinition() {
		return languageDefinition;
	}

	public void setLanguageDefinition(LanguageDefinition languageDefinition) {
		this.languageDefinition = languageDefinition;
	}

	// Utility Methods
	protected IPackageFragment createDefaultPackage() throws CoreException {
		final IJavaProject project = codeRoot.get(0);
		List<IPackageFragmentRoot> roots = ASTUtils.getSourcePackageFragmentRoots(Arrays.asList(project));
		IPackageFragmentRoot packageFragmentRoot = null;
		if (!roots.isEmpty()) {
			packageFragmentRoot = roots.get(0);
		} else {
			IFolder folder = project.getProject().getFolder("src");
			if (!folder.exists())
				folder.create(true, true, null);
			folder = folder.getFolder("main");
			if (!folder.exists())
				folder.create(true, true, null);
			folder = folder.getFolder("java");
			if (!folder.exists())
				folder.create(true, true, null);
			packageFragmentRoot = project.getPackageFragmentRoot(folder);
		}

		String packageName = "new_architecture_elements";
		IPackageFragment packageFragment = packageFragmentRoot.getPackageFragment(packageName);
		if (!packageFragment.exists())
			packageFragment = packageFragmentRoot.createPackageFragment(packageName, true, null);
		return packageFragment;
	}

	protected EObject resolvePriorElement(EObject eObject) {
		String id = idRegistry.getIDFromImplementationModelElement(eObject);
		return priorIDRegistry.resolveImplementationModelElement(id, priorModelElement.eResource().getContents());
	}

	/**
	 * A convenience method for xtend subclasses for callung
	 * modelElement.nameAttributeValue
	 */
	protected String getNameAttributeValue(EObject modelElement) {
		return ModelUtils.getNameAttributeValue(modelElement);
	}

	/**
	 * A convenience method for xtend subclasses for callung
	 * modelElement.nameAttributeValue = ""
	 */
	protected void setNameAttributeValue(EObject modelElement, String value) {
		ModelUtils.setNameAttributeValue(modelElement, value);
	}

	/**
	 * Finds in the ID registry an eobject before the transformation by first
	 * finding the ID of currentElement in the ID registry, and then finding the
	 * corresponding eobject in the eobject tree that priorModelElement is in.
	 */
	protected EObject findPriorModelElementFor(EObject currentElement) {
		EObject priorTarget = null;
		if (idRegistry.knowsImplementationModelElement(currentElement)) {
			String oldID = idRegistry.getIDFromImplementationModelElement(currentElement);
			priorTarget = priorIDRegistry.resolveImplementationModelElement(oldID,
					EcoreUtil.getRootContainer(priorModelElement));
		}
		return priorTarget;
	}

	// Methods from IMayHaveIssues.
	MayHaveIssues issues = new MayHaveIssues(this.getClass());
	List<IMayHaveIssues> childIssues = new LinkedList<IMayHaveIssues>();

	protected void addChildIssue(IMayHaveIssues child) {
		childIssues.add(child);
	}

	@Override
	public boolean isOK() {
		return issues.isOK() || childIssues.stream().allMatch(x -> x.isOK());
	}

	@Override
	public boolean hasErrorsOrWarnings() {
		return issues.hasErrorsOrWarnings() || childIssues.stream().allMatch(x -> x.hasErrorsOrWarnings());
	}

	@Override
	public boolean hasErrors() {
		return issues.hasErrors() || childIssues.stream().allMatch(x -> x.hasErrors());
	}

	@Override
	public boolean hasWarnings() {
		return issues.hasWarnings() || childIssues.stream().allMatch(x -> x.hasWarnings());
	}

	@Override
	public boolean hasInfos() {
		return issues.hasInfos() || childIssues.stream().allMatch(x -> x.hasInfos());
	}

	@Override
	public List<Issue> issues() {
		final List<Issue> result = issues.issues();
		childIssues.forEach(x -> result.addAll(x.issues()));
		return result;

	}

	@Override
	public List<Issue> errorsAndWarnings() {
		final List<Issue> result = issues.errorsAndWarnings();
		childIssues.forEach(x -> result.addAll(x.errorsAndWarnings()));
		return result;

	}

	@Override
	public List<Issue> errors() {
		final List<Issue> result = issues.errors();
		childIssues.forEach(x -> result.addAll(x.errors()));
		return result;

	}

	@Override
	public List<Issue> warnings() {
		final List<Issue> result = issues.warnings();
		childIssues.forEach(x -> result.addAll(x.warnings()));
		return result;

	}

	@Override
	public List<Issue> infos() {
		final List<Issue> result = issues.infos();
		childIssues.forEach(x -> result.addAll(x.infos()));
		return result;

	}

	@Override
	public void addIssue(Level level, String message, Throwable e) {
		issues.addIssue(level, message, e);
	}

	@Override
	public void addIssue(Level level, String message) {
		issues.addIssue(level, message);
	}

	@Override
	public void addWarning(String message, Throwable e) {
		issues.addWarning(message, e);
	}

	@Override
	public void addWarning(String message) {
		issues.addWarning(message);
	}

	@Override
	public void addError(String message, Throwable e) {
		issues.addError(message, e);
	}

	@Override
	public void addError(String message) {
		issues.addError(message);
	}

	@Override
	public void addInfo(String message, Throwable e) {
		issues.addInfo(message, e);
	}

	@Override
	public void addInfo(String message) {
		issues.addInfo(message);
	}

	@Override
	public String getName() {
		return issues.getName();
	}

	@Override
	public Class<?> getOwningClass() {
		return issues.getOwningClass();
	}

}
