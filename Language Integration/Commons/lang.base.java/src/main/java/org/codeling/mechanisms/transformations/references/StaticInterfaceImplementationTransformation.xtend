package org.codeling.mechanisms.transformations.references

import org.codeling.lang.base.java.ASTUtils
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation
import java.util.LinkedHashSet
import java.util.List
import java.util.Set
import org.codeling.mechanisms.transformations.ClassMechanismTransformation
import org.codeling.mechanisms.transformations.ReferenceMechanismTransformation
import org.codeling.mechanisms.transformations.classes.StaticInterfaceTransformation
import org.codeling.utils.CodelingException
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.jdt.core.IField
import org.eclipse.jdt.core.IType

abstract class StaticInterfaceImplementationTransformation<OWNERECLASS extends EObject, TARGETECLASS extends EObject> extends ReferenceMechanismTransformation<OWNERECLASS, TARGETECLASS, IType> {

	protected val Set<IType> superInterfaces = new LinkedHashSet();
	protected val Set<IType> targetTypes = new LinkedHashSet();

	new(ClassMechanismTransformation<OWNERECLASS, IType> parentTransformation, EReference eReference) {
		super(parentTransformation, eReference)
	}

	override createCodeFragments() throws CodelingException {
	}

	override updateCodeFragments() throws CodelingException {
	}

	override deleteCodeFragments() {
	}

	override transformToModel() throws CodelingException {
		if (modelElement === null)
			throw new IllegalStateException("modelElement must not be null");

		resolveCodeElement();
		if (superInterfaces.size == 0) {
			// No target object. Do not change the model.
			return modelElement;
		}

		val List<String> typeNames = resolveTargetTypes().map(t|t.elementName).toList;

		val List<TARGETECLASS> targetElements = getTargetObjects(modelElement, eReference, typeNames);
		if (eReference.isMany) {
			(modelElement.eGet(eReference) as List<TARGETECLASS>).addAll(targetElements);
		} else {
			modelElement.eSet(eReference, targetElements.head);
		}

		return modelElement;
	}

	override IType resolveCodeElement() {
		val String[] superInterfacesSignatures = codeElement.superInterfaceTypeSignatures;
		for (String sig : superInterfacesSignatures) {
			val IType superInterface = ASTUtils.getType(ASTUtils.resolveTypeSignature(sig, codeElement), codeElement);
			val List<AbstractModelCodeTransformation<?, ?>> ts = whereToFindTranslatedElements.
				getTransformationsFor(superInterface);
			for (AbstractModelCodeTransformation<?,?> t : ts) {
				if (t instanceof StaticInterfaceTransformation<?>) {
					superInterfaces.add((t as StaticInterfaceTransformation<?>).codeElement);
				}
			}
		}

		return codeElement;
	}

	public def boolean hasExpectedAnnotation(IField field) {
		return field.getAnnotation(eReference.name.toFirstUpper).exists;
	}

	protected def String getNewAnnotationName() {
		return '''org.codeling.lang.«getLanguageName».mm.«eReference.name.toFirstUpper»''';
	}

	private def String getIdentifyingAnnotation() {
		if (newAnnotationName === null) {
			return "";
		}

		return '''@«newAnnotationName»''';
	}

	def Set<IType> resolveTargetTypes() {
		if (!targetTypes.isEmpty)
			return targetTypes;

		if (superInterfaces.isEmpty)
			resolveCodeElement();

		targetTypes.addAll(superInterfaces);

		return targetTypes;
	}

	def Set<IField> getFields() {
		return fields;
	}

}
