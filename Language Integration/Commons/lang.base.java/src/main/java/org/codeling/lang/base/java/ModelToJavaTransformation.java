package org.codeling.lang.base.java;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.base.mic.ModelIntegrationConceptTransformation;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;

public class ModelToJavaTransformation extends ModelIntegrationConceptTransformation {

	protected IProgressMonitor monitor = new NullProgressMonitor();
	final static private String taskName = "Model to Java Code";
	private final AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> rootTransformation;
	private List<EClass> crossReferenceOrder;

	public <E extends EObject, J extends IJavaElement> ModelToJavaTransformation(List<IJavaProject> projects,
			AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> rootTransformation,
			List<EClass> crossReferenceOrder) {
		super(projects.stream().map(p -> p.getProject().getLocation().toFile().toPath()).collect(Collectors.toList()),
				rootTransformation.getIdRegistry(), taskName, null);
		this.rootTransformation = rootTransformation;
		this.crossReferenceOrder = crossReferenceOrder;
	}

	@Override
	protected void doExecute(IProgressMonitor monitor) {
		rootTransformation.setToCode();

		// Translate all classes, their attributes, and the containment
		// references
		ForkJoinPool.commonPool().invoke(rootTransformation);

		// Translate non-containment references
		if (crossReferenceOrder == null)
			crossReferenceOrder = new LinkedList<>();
		for (final EClass eclass : crossReferenceOrder) {
			rootTransformation.getWhereToFindTranslatedElements().getTransformationsFor(eclass).forEach(t -> {
				t.setOnlyCrossReferences(true);
				t.setToCode();
				t.reinitialize();
				ForkJoinPool.commonPool().invoke(t);
			});
		}

		// Propagate new code elements to the IDRegistry
		rootTransformation.getWhereToFindTranslatedElements().getAllTransformations().forEach(t -> {
			try {
				// Some transformations do not have a code element (NinjaSingleton and related)
				if (t.getCodeElement() != null && t.getModelElement() != null) {
					String id = idRegistry.getIDFromImplementationModelElement(t.getModelElement());
					idRegistry.updateCodeElement(id, t.getCodeElement().getHandleIdentifier());
				}
			} catch (Throwable throwble) {
				addError("Error while propagating new code elements to the ID registry.", throwble);
			}
		});
	}

}
