package org.codeling.mechanisms.transformations.references

import org.codeling.lang.base.java.ASTUtils
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation
import java.util.LinkedList
import java.util.List
import org.codeling.mechanisms.transformations.ClassMechanismTransformation
import org.codeling.mechanisms.transformations.ReferenceMechanismTransformation
import org.codeling.utils.CodelingException
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.jdt.core.IJavaProject
import org.eclipse.jdt.core.IPackageFragment
import org.eclipse.jdt.core.IPackageFragmentRoot
import org.eclipse.jdt.core.IType

abstract class ProjectReferenceTransformation<OWNERECLASS extends EObject, TARGETECLASS extends EObject> extends ReferenceMechanismTransformation<OWNERECLASS, TARGETECLASS, IJavaProject> {

	new(ClassMechanismTransformation<OWNERECLASS, IJavaProject> parentTransformation, EReference eReference) {
		super(parentTransformation, eReference);
	}

	override createCodeFragments() throws CodelingException {
		// There is no code representation
	}

	override updateCodeFragments() throws CodelingException {
		(modelElement.eGet(eReference) as List<TARGETECLASS>).forEach [ component |
			val AbstractModelCodeTransformation<?, ?> trans = whereToFindTranslatedElements.
				getTransformationsFor(component).findFirst [ t |
					t.canHandle(eReference.getEReferenceType())
				];
				
			if (trans !== null && !trans.getCodeElement().getJavaProject().getHandleIdentifier().equals(codeElement.getHandleIdentifier())) {
				if (trans.codeElement instanceof IType) {
					ASTUtils.moveType(trans.codeElement as IType, codeElement);
				} else {
					addWarning('''Could not set project reference to «trans.modelElement.toString», because the mechanism can only handle IType as target, but it is a «trans.codeElement.class.name».''');
				}
			}
		];
	}

	override deleteCodeFragments() {
		// There is no code representation
	}

	override transformToModel() throws CodelingException {
		if (modelElement === null)
			throw new IllegalStateException(
				"modelElement must not be null. Please set the field value to the object that owns the reference.");

		// Find all types that represent a model object of the targeted type.
		val List<String> targetTypeNames = new LinkedList();
		codeElement.allPackageFragmentRoots.filter [ pfr |
			(pfr as IPackageFragmentRoot).javaProject === codeElement // Otherwise it will also search source folders of dependencies
			&& (pfr as IPackageFragmentRoot).kind == IPackageFragmentRoot.K_SOURCE // Otherwise it will also search jars
		].forEach [ r |
			r.children.forEach [ p |
				(p as IPackageFragment).compilationUnits.forEach [ cu |
					cu.types.forEach [ t |
						val transformations = whereToFindTranslatedElements.getTransformationsFor(t);
						if (!transformations.nullOrEmpty && transformations.exists [ transformation |
							transformation.canHandle(eReference.EReferenceType)
						]) {
							targetTypeNames.add(t.elementName);
						}
					]
				]
			]
		];

		val List<TARGETECLASS> targetElements = getTargetObjects(modelElement, eReference, targetTypeNames);
		if (eReference.isMany) {
			(modelElement.eGet(eReference) as List<TARGETECLASS>).addAll(targetElements);
		} else {
			modelElement.eSet(eReference, targetElements.head);
		}

		return modelElement;
	}
}
