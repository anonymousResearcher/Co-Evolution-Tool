package org.codeling.mechanisms.transformations;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;

public abstract class AttributeMechanismTransformation<OWNERECLASS extends EObject, JAVAELEMENT extends IJavaElement>
		extends AbstractModelCodeTransformation<OWNERECLASS, JAVAELEMENT> {

	protected EAttribute eAttribute = null;

	public AttributeMechanismTransformation(
			AbstractModelCodeTransformation<? extends EObject, JAVAELEMENT> parentTransformation,
			EAttribute eAttribute) {
		super(parentTransformation);
		this.eAttribute = eAttribute;
		if (parentTransformation != null) {
			setModelElement(parentTransformation.getModelElement());
			setPriorModelElement(parentTransformation.getPriorModelElement());
			setCodeElement(parentTransformation.getCodeElement());
		}
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// Does not have child transformations
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// Does not have child transformations
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// Does not have child transformations
	}

	@Override
	public boolean canHandle(EModelElement metaModelElement) {
		return metaModelElement == eAttribute;
	}
}