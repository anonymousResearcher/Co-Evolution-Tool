package org.codeling.lang.base.java;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.base.java.transformation.FindTranslatedElements;
import org.codeling.lang.base.java.transformation.references.IALTransformation;
import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.transformationmanager.TransformationResult;
import org.codeling.utils.CodelingConfiguration;
import org.codeling.utils.CodelingException;
import org.codeling.utils.IDRegistry;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.modelversioning.emfprofileapplication.EMFProfileApplicationPackage;
import org.modelversioning.emfprofileapplication.StereotypeApplication;

public abstract class JavaBasedImplementationLanguageDefinition extends ImplementationLanguageDefinition {

	protected HashMap<EClass, IALTransformationTuple> ialTransformations = new HashMap<>();
	protected LinkedList<EClass> crossReferenceOrder = new LinkedList<>();

	/**
	 * A resource set for "debug/profile-application.pa.xmi".
	 */
	private ResourceSetImpl rSet = new ResourceSetImpl();

	/**
	 * Stores all executed code-to-model and model-to-code transformations.
	 */
	protected FindTranslatedElements findTranslatedElements;

	/**
	 * Returns an instance of the root transformation to execute on the given
	 * projects.
	 */
	public abstract AbstractModelCodeTransformation<?, ?> createRootTransformationInstance();

	@Override
	public void transformIMToCode(List<EObject> newModelRoots, List<EObject> oldModelRoots,
			IDRegistry idRegistry_newModel, IDRegistry idregistry_oldModel, List<IJavaProject> projects)
			throws CodelingException {
		final AbstractModelCodeTransformation<?, ?> rootTransformation = createRootTransformationInstance();
		configureRootTransformation(rootTransformation, projects, newModelRoots, idRegistry_newModel, oldModelRoots,
				idregistry_oldModel);
		monitor.worked(10);

		final ModelToJavaTransformation executor = new ModelToJavaTransformation(projects, rootTransformation,
				getCrossReferenceOrder());
		executor.execute(monitor);
		monitor.worked(70);
	}

	private void configureRootTransformation(
			final AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> rootTransformation,
			List<IJavaProject> projects, List<EObject> newModelRoots, IDRegistry idRegistry_newModel,
			List<EObject> oldModelRoots, IDRegistry idregistry_oldModel) {
		// All attributes may already be set by createRootTransformationInstance
		if (rootTransformation.getLanguageDefinition() == null)
			rootTransformation.setLanguageDefinition(this);
		if (rootTransformation.getCodeRoot() == null)
			rootTransformation.setCodeRoot(projects);
		if (rootTransformation.getIdRegistry() == null)
			rootTransformation.setIDRegistry(idRegistry_newModel);
		if (rootTransformation.getPriorIDRegistry() == null)
			rootTransformation.setPriorIDRegistry(idregistry_oldModel);
		if (rootTransformation.getModelElement() == null && newModelRoots != null && !newModelRoots.isEmpty())
			rootTransformation.setModelElement(newModelRoots.get(0));
		if (rootTransformation.getPriorModelElement() == null && oldModelRoots != null && !oldModelRoots.isEmpty())
			rootTransformation.setPriorModelElement(oldModelRoots.get(0));
		if (rootTransformation.getWhereToFindTranslatedElements() == null) {
			findTranslatedElements = new FindTranslatedElements();
			rootTransformation.setWhereToFindTranslatedElements(findTranslatedElements);
		} else {
			findTranslatedElements = rootTransformation.getWhereToFindTranslatedElements();
		}
	}

	@Override
	public void transformTMToCode(List<EObject> newIMRoots, List<EObject> oldIMRoots, List<EObject> newTMRoots,
			List<EObject> oldTMRoots, IDRegistry idRegistry_newModel, IDRegistry idRegistry_oldModel,
			List<IJavaProject> projects) throws CodelingException {

		// Update or create elements
		idRegistry_newModel.getRegistryEntries().keySet().stream().forEach(id -> {
			final EObject imElement = idRegistry_newModel.resolveImplementationModelElement(id, newIMRoots);
			if (imElement == null)
				return;
			final List<? extends IALTransformation<?, ?>> ialTransformations = createIALTransformationInstance(
					imElement);
			if (ialTransformations == null || ialTransformations.isEmpty())
				return;

			final EObject foundationalIALElement = idRegistry_newModel.resolveTranslationModelElement(id, newTMRoots);
			final EObject priorFoundationalIALElement = idRegistry_oldModel.resolveTranslationModelElement(id,
					oldTMRoots);
			final IJavaElement codeElement = idRegistry_newModel.getCodeElement(id);

			if (ialTransformations != null) {
				ialTransformations.stream().forEach(t -> {
					try {
						t.setIDRegistry(idRegistry_newModel);
						t.setPriorIDRegistry(idRegistry_oldModel);
						t.getIALHolder().setFoundationalIALElement(foundationalIALElement);
						t.getIALHolder().setPriorFoundationalIALElement(priorFoundationalIALElement);
						t.getIALHolder().setIALCodeElement(codeElement);
						t.getIALHolder().setIALRoots(newTMRoots);
						if (findTranslatedElements == null)
							findTranslatedElements = new FindTranslatedElements();
						t.setWhereToFindTranslatedElements(findTranslatedElements);
						StereotypeApplication modelElement = t.resolveTranslatedIALElement(foundationalIALElement);
						StereotypeApplication priorModelElement = t
								.resolveTranslatedIALElement(priorFoundationalIALElement);
						if (modelElement == null && priorModelElement == null)
							return; // No new, existing, or deleted element

						t.setModelElement(modelElement);
						t.setPriorModelElement(priorModelElement);

						final ModelToJavaTransformation executor = new ModelToJavaTransformation(projects,
								(AbstractModelCodeTransformation<?, ?>) t, new LinkedList<>());
						executor.execute(monitor);
					} catch (Exception e) {
						addError(
								"Error while setting up or executing Model-to-Java transformation from Translation Model to Program Code",
								e);
					}
				});
			}
		});
		// TODO: Delete elements

		monitor.worked(10);
	}

	@Override
	public TransformationResult transformCodeToTM(List<IJavaProject> projects, List<EObject> imRoots,
			List<EObject> tmRoots, IDRegistry idRegistry) throws CodelingException {
		try {
			idRegistry.getRegistryEntries().keySet().stream().forEach(id -> {
				try {
					final EObject imElement = idRegistry.resolveImplementationModelElement(id, imRoots);
					if (imElement == null)
						return; // If this element has not implementation model representation, ignore it.

					final EObject tmElement = idRegistry.resolveTranslationModelElement(imElement, tmRoots);
					final List<? extends IALTransformation<?, ?>> ialTransformations = createIALTransformationInstance(
							imElement);
					if (ialTransformations != null)
						ialTransformations.stream().forEach(t -> {
							try {
								t.setIDRegistry(idRegistry);
								t.setModelElement(imElement);
								t.getIALHolder().setFoundationalIALElement(tmElement);
								t.getIALHolder().setIALRoots(tmRoots);
								if (findTranslatedElements == null)
									findTranslatedElements = new FindTranslatedElements();
								t.setWhereToFindTranslatedElements(findTranslatedElements);
								JavaToModelTransformation executor = new JavaToModelTransformation(projects, idRegistry,
										(AbstractModelCodeTransformation<?, ?>) t, null);
								executor.execute(monitor);
							} catch (final Exception ex) {
								addError("Could not extract IL information from the code.", ex);
							}
						});
				} catch (final Throwable t) {
					addWarning(String.format("Error while handling the element with the id '%s'.", id), t);
				}
			});

			// Create a temporary profile application file for using the emf
			// profiles facade.
			final Resource applicationResource = getResource(rSet,
					CodelingConfiguration.DEBUG_MODELDIR_PATH + "profile-application.pa.xmi");
			mergeStereotypeApplicationsWithTranslationModel(tmRoots, applicationResource);
			return new TransformationResult(tmRoots, idRegistry);
		} catch (final IOException e) {
			throw new CodelingException("Could not handle the temporary resource for the profile application.", e);
		}
	}

	@Override
	public TransformationResult transformCodeToIM(List<IJavaProject> projects, IDRegistry idregistry)
			throws CodelingException {

		// Initialize the root transformation
		final AbstractModelCodeTransformation<?, ?> rootTransformation = createRootTransformationInstance();
		configureRootTransformation(rootTransformation, projects, null, idregistry, null, null);
		monitor.worked(10);

		final JavaToModelTransformation executor = new JavaToModelTransformation(projects, idregistry,
				rootTransformation, getCrossReferenceOrder());
		executor.execute(monitor);
		monitor.worked(70);
		return executor.getTransformationResult();

	}

	private void mergeStereotypeApplicationsWithTranslationModel(final List<EObject> ilModelRoots,
			final Resource applicationResource) throws IOException {
		final TreeIterator<EObject> iterator = applicationResource.getAllContents();
		final LinkedList<EObject> stereotypeApplications = new LinkedList<>();
		while (iterator.hasNext()) {
			final EObject app = iterator.next();
			if (EMFProfileApplicationPackage.eINSTANCE.getStereotypeApplication().isInstance(app))
				stereotypeApplications.add(app);
		}
		ilModelRoots.addAll(stereotypeApplications);

		applicationResource.delete(null);
	}

	private Resource getResource(ResourceSet rSet, String path) throws IOException {
		final URI uri = URI.createPlatformResourceURI(path, true);
		Resource resource = null;
		try {
			resource = rSet.getResource(uri, true);
		} catch (final WrappedException e) {
			resource = rSet.createResource(uri);
			resource.save(null);
		}
		return resource;
	}

	@SuppressWarnings("unchecked")
	public List<? extends IALTransformation<?, ?>> createIALTransformationInstance(EObject cmElement) {

		final List<IALTransformationTuple> tuples = new LinkedList<>();
		final List<IALTransformation<?, ?>> result = new LinkedList<>();

		if (cmElement == null)
			return result;

		for (final Entry<EClass, IALTransformationTuple> entry : getIALTransformations().entrySet()) {
			if (entry.getKey() == cmElement.eClass() || entry.getKey().isSuperTypeOf(cmElement.eClass())) {
				tuples.add(entry.getValue());
			}
		}

		for (final IALTransformationTuple tuple : tuples) {
			for (Class<? extends IALTransformation<?, ?>> transformationClass : tuple.getTransformationClasses()) {
				IALTransformation<EObject, IJavaElement> referenceTrans = null;
				try {
					for (Constructor<?> constructor : transformationClass.getDeclaredConstructors()) {
						if (constructor.getParameterCount() == 0) {
							referenceTrans = (IALTransformation<EObject, IJavaElement>) constructor
									.newInstance(new Object[0]);
							break;
						} else if (constructor.getParameterCount() == 1 && AbstractModelCodeTransformation.class
								.isAssignableFrom(constructor.getParameterTypes()[0])) {
							referenceTrans = (IALTransformation<EObject, IJavaElement>) constructor
									.newInstance(new Object[] { null });
							break;
						} else if (constructor.getParameterCount() == 2
								&& AbstractModelCodeTransformation.class
										.isAssignableFrom(constructor.getParameterTypes()[0])
								&& ENamedElement.class.isAssignableFrom(constructor.getParameterTypes()[1])) {
							referenceTrans = (IALTransformation<EObject, IJavaElement>) constructor.newInstance(null,
									tuple.getIlElement());
							break;
						}
					}
					if (referenceTrans == null)
						throw new CodelingException(
								MessageFormat.format("Could not invoke any feasable constructor on class [{0}].",
										transformationClass.getName()));
					result.add(referenceTrans);
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException | SecurityException | CodelingException e) {
					addError("Could not instantiate reference transformation.", e);
				}
			}
		}
		return result;
	}

	public HashMap<EClass, IALTransformationTuple> getIALTransformations() {
		return ialTransformations;
	}

	public LinkedList<EClass> getCrossReferenceOrder() {
		return crossReferenceOrder;
	}

	public FindTranslatedElements getWhereToFindTranslatedElements() {
		return findTranslatedElements;
	}

	/**
	 * Returns the code element that corresponds to the given architecture
	 * implementation model element, based on the given idRegistry. This is a
	 * convenience method.
	 * 
	 * @throws {@link
	 *             IllegalArgumentException} when either an argument is null, or the
	 *             idRegistry does not know the architecture implementation element.
	 */
	protected IJavaElement resolveCodeElement(IDRegistry idRegistry, EObject cmElement) {
		if (idRegistry == null)
			throw new IllegalArgumentException("idRegistry must not be null");
		if (cmElement == null)
			throw new IllegalArgumentException("cmElement must not be null");
		if (!idRegistry.knowsImplementationModelElement(cmElement))
			throw new IllegalArgumentException("idRegistry must know the cmElement, but does not.");

		String id = idRegistry.getIDFromImplementationModelElement(cmElement);
		return idRegistry.getCodeElement(id);
	}

}
