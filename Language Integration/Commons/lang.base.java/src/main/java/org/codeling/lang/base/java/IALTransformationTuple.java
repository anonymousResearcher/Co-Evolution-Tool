package org.codeling.lang.base.java;

import java.util.List;

import org.codeling.lang.base.java.transformation.references.IALTransformation;
import org.eclipse.emf.ecore.ENamedElement;

public class IALTransformationTuple {
	ENamedElement ilElement;
	List<Class<? extends IALTransformation<?, ?>>> transformationClasses;

	public IALTransformationTuple(ENamedElement ilElement,
			List<Class<? extends IALTransformation<?, ?>>> transformationClasses) {
		this.ilElement = ilElement;
		this.transformationClasses = transformationClasses;
	}

	public ENamedElement getIlElement() {
		return ilElement;
	}

	public List<Class<? extends IALTransformation<?, ?>>> getTransformationClasses() {
		return transformationClasses;
	}

}
