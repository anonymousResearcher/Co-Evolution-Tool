package org.codeling.mechanisms.transformations.references

import java.util.List
import org.codeling.mechanisms.transformations.ClassMechanismTransformation
import org.codeling.mechanisms.transformations.ReferenceMechanismTransformation
import org.codeling.utils.CodelingException
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.jdt.core.IPackageFragment

abstract class NamespaceHierarchyChildTransformation<OWNERECLASS extends EObject> extends ReferenceMechanismTransformation<OWNERECLASS, OWNERECLASS, IPackageFragment> {

	new(ClassMechanismTransformation<OWNERECLASS, IPackageFragment> parentTransformation,
		EReference eReference) {
		super(parentTransformation, eReference);
	}

	override createCodeFragments() throws CodelingException {
		// There is no code representation
	}

	override updateCodeFragments() throws CodelingException {
		// There is no code representation
	}

	override deleteCodeFragments() {
		// There is no code representation
	}

	override transformToModel() throws CodelingException {
		val childNamespaceTransformations = createChildTransformationsToModel();
		val List<EObject> children = childNamespaceTransformations.map[t|t.modelElement].toList;
		modelElement.eSet(eReference, children);
		return modelElement;
	}
}
