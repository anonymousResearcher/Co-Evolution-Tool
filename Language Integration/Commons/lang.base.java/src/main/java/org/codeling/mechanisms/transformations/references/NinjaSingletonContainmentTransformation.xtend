package org.codeling.mechanisms.transformations.references

import java.util.LinkedList
import java.util.List
import org.codeling.mechanisms.Mechanism
import org.codeling.mechanisms.Mechanisms
import org.codeling.mechanisms.MechanismsMapping
import org.codeling.mechanisms.transformations.ClassMechanismTransformation
import org.codeling.mechanisms.transformations.ReferenceMechanismTransformation
import org.codeling.mechanisms.transformations.classes.ProjectTransformation
import org.codeling.utils.CodelingException
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.jdt.core.IJavaElement
import org.eclipse.jdt.core.IPackageFragment
import org.eclipse.jdt.core.IPackageFragmentRoot

abstract class NinjaSingletonContainmentTransformation<OWNERECLASS extends EObject, TARGETECLASS extends EObject> extends ReferenceMechanismTransformation<OWNERECLASS, TARGETECLASS, IJavaElement> {

	new(ClassMechanismTransformation<OWNERECLASS, IJavaElement> parentTransformation, EReference eReference) {
		super(parentTransformation, eReference)
	}

	override createCodeFragments() throws CodelingException {
		// There is no code representation
	}

	override updateCodeFragments() throws CodelingException {
		// There is no code representation
	}

	override deleteCodeFragments() {
		// There is no code representation
	}

	override transformToModel() throws CodelingException {
		if (modelElement === null)
			throw new IllegalStateException(
				"modelElement must not be null. Please set the field value to the object that owns the reference.");

		val EClass targetClass = eReference.EReferenceType;
		val Class<? extends Mechanism> targetMechanism = MechanismsMapping.instance.get(targetClass);
		val List<String> targetTypeNames = new LinkedList();
		// Find all projects that represent a model object of the targeted type
		if (targetMechanism == ProjectTransformation) {
			targetTypeNames.addAll(codeRoot.map[p|p.elementName]);
		} else {
			// Find all types that represent a model object of the targeted type.
			codeRoot.forEach [ project |
				project.allPackageFragmentRoots.filter [ pfr |
					(pfr as IPackageFragmentRoot).kind == IPackageFragmentRoot.K_SOURCE
				].forEach [ r |
					r.children.forEach [ p |
						(p as IPackageFragment).compilationUnits.forEach [ cu |
							cu.types.forEach [ t |
								if (targetMechanism !== null &&
									Mechanisms.create(targetMechanism).canHandle(t, targetClass)) {
									targetTypeNames.add(t.elementName);
								}
							]
						]
					]
				]
			];
		}

		val List<TARGETECLASS> targetElements = getTargetObjects(modelElement, eReference, targetTypeNames);
		if (eReference.isMany) {
			(modelElement.eGet(eReference) as List<TARGETECLASS>).addAll(targetElements);
		} else {
			modelElement.eSet(eReference, targetElements.head);
		}

		return modelElement;
	}
}
