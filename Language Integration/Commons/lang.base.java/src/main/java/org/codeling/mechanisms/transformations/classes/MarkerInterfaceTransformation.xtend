package org.codeling.mechanisms.transformations.classes

import org.codeling.lang.base.java.ASTUtils
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation
import org.codeling.mechanisms.transformations.ClassMechanismTransformation
import org.codeling.utils.CodelingException
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.jdt.core.ICompilationUnit
import org.eclipse.jdt.core.IJavaElement
import org.eclipse.jdt.core.IPackageFragment
import org.eclipse.jdt.core.IType

abstract class MarkerInterfaceTransformation<ELEMENTECLASS extends EObject> extends ClassMechanismTransformation<ELEMENTECLASS, IType> {

	new(AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation,
		EClass eClass) {
		super(parentTransformation, eClass)
	}

	override createCodeFragments() throws CodelingException {
		var IPackageFragment targetPackage = if (parentCodeElement !== null && parentCodeElement instanceof IType)
				(parentCodeElement as IType).packageFragment
			else
				createDefaultPackage();

		val String name = modelElement.nameAttributeValue;
		val String typeName = name.toFirstUpper
		val String interfaceName = modelElement.eClass.name.toFirstUpper;

		val String content = '''
		package «targetPackage.elementName»;
		
		import org.codeling.lang.«getLanguageName».mm.«interfaceName»;
		
		public class «typeName» implements «interfaceName» {}''';
		val ICompilationUnit cu = targetPackage.createCompilationUnit(typeName + ".java", content, true, null);
		codeElement = cu.getType(typeName);
	}

	override updateCodeFragments() throws CodelingException {
		// Update of name attribute
		val String oldName = priorModelElement.nameAttributeValue;
		val String newName = modelElement.nameAttributeValue;

		if (!newName.equals(oldName)) {
			// Execute rename refactoring
			ASTUtils.renameType(codeElement, newName);
			codeElement = codeElement.getPackageFragment().getCompilationUnit(newName + ".java").getType(newName);
		}
	}

	override deleteCodeFragments() {
		codeElement.resource.delete(true, null);
	}

	override transformToModel() throws CodelingException {
		// Create element
		modelElement = eClass.EPackage.EFactoryInstance.create(eClass);

		// Set name attribute value
		val String name = codeElement.elementName;
		setNameAttributeValue(modelElement, name);

		return modelElement;
	}

}
