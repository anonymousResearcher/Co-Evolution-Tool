package org.codeling.lang.base.java;

import java.text.MessageFormat;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

public class ModelUtils {
	public static String getNameAttributeValue(EObject modelElement) {
		final EStructuralFeature nameFeature = modelElement.eClass().getEStructuralFeature("name");
		if (nameFeature == null)
			throw new IllegalStateException(MessageFormat.format("The eclass {0} must have a 'name' attribute.",
					modelElement.eClass().getName()));
		if (!(nameFeature instanceof EAttribute))
			throw new IllegalStateException(
					MessageFormat.format("The 'name' feature of the eclass {0} is not an EAttribute, but must be.",
							modelElement.eClass().getName()));
		return (String) modelElement.eGet(nameFeature);
	}

	public static void setNameAttributeValue(EObject modelElement, String value) {
		final EStructuralFeature nameFeature = modelElement.eClass().getEStructuralFeature("name");
		if (nameFeature == null)
			throw new IllegalStateException(MessageFormat.format("The eclass {0} must have a 'name' attribute.",
					modelElement.eClass().getName()));
		if (!(nameFeature instanceof EAttribute))
			throw new IllegalStateException(
					MessageFormat.format("The 'name' feature of the eclass {0} is not an EAttribute, but must be.",
							modelElement.eClass().getName()));
		modelElement.eSet(nameFeature, value);
	}
}
