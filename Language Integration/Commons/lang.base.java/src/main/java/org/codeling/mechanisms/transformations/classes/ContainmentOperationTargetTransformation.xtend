package org.codeling.mechanisms.transformations.classes

import org.codeling.mechanisms.transformations.ClassMechanismTransformation
import org.codeling.mechanisms.transformations.references.ContainmentOperationTransformation
import org.codeling.utils.CodelingException
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.jdt.core.IMethod

abstract class ContainmentOperationTargetTransformation<ELEMENTECLASS extends EObject> extends ClassMechanismTransformation<ELEMENTECLASS, IMethod> {

	new(ContainmentOperationTransformation<? extends EObject, ELEMENTECLASS> parentTransformation, EClass eClass) {
		super(parentTransformation, eClass)
	}

	override createCodeFragments() throws CodelingException {
	}

	override updateCodeFragments() throws CodelingException {
	}

	override deleteCodeFragments() {
	}

	override transformToModel() throws CodelingException {
		// Create element
		modelElement = eClass.EPackage.EFactoryInstance.create(eClass);
		modelElement.nameAttributeValue = codeElement.elementName;
		return modelElement;
	}
}