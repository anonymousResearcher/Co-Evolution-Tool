package org.codeling.mechanisms.transformations.attributes

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation
import org.codeling.mechanisms.transformations.AttributeMechanismTransformation
import org.codeling.utils.CodelingException
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EObject
import org.eclipse.jdt.core.IField
import org.eclipse.jdt.core.IType

abstract class ConstantMemberAttributeTransformation<ELEMENTECLASS extends EObject> extends AttributeMechanismTransformation<ELEMENTECLASS, IType> {

	new(AbstractModelCodeTransformation<? extends EObject, IType> parentTransformation, EAttribute eAttribute) {
		super(parentTransformation, eAttribute)
	}

	override createCodeFragments() throws CodelingException {
		val String fieldName = eAttribute.name;
		val String fieldType = eAttribute.EAttributeType.instanceClassName;
		var String fieldValue = null;
		if (eAttribute.EAttributeType.instanceClass == String) {
			fieldValue = '''"«modelElement.eGet(eAttribute)»"''';
		} else {
			fieldValue = modelElement.eGet(eAttribute) as String;
		}

		val String content = '''
			@Attribute
			public static final «fieldType» «fieldName» = «fieldValue»;
		''';

		val IType type = parentCodeElement as IType;
		type.createField(content, null, true,
			null);
	}

	override updateCodeFragments() throws CodelingException {
		// Update of the value
		val IField field = codeElement.getField(eAttribute.name);
		val Object oldValue = field.constant;

		var String newValue = null;
		if (eAttribute.EAttributeType.instanceClass == String) {
			newValue = '''"«modelElement.eGet(eAttribute)»"''';
		} else {
			newValue = modelElement.eGet(eAttribute) as String;
		}

		if (!newValue.equals(oldValue)) {
			// Change constant field value TODO: it should be possible to replace the value instead of deleting the field.
			val String newSource = field.source.replaceFirst("=(.+);", "= " + newValue + ";");
			field.delete(true, null);
			codeElement.createField(newSource, null, true, null);
		}
	}

	override deleteCodeFragments() {
		codeElement.getField(eAttribute.name).delete(true, null);
	}

	override transformToModel() throws CodelingException {
		val IField field = codeElement.getField(eAttribute.name);
		modelElement.eSet(eAttribute, field.constant);
		return modelElement;
	}
}
