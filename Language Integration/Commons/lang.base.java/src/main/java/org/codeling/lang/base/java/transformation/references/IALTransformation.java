package org.codeling.lang.base.java.transformation.references;

import org.codeling.lang.base.java.transformation.IModelCodeTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.modelversioning.emfprofileapplication.StereotypeApplication;

public interface IALTransformation<ELEMENTECLASS extends EObject, JAVAELEMENTCLASS extends IJavaElement>
		extends IModelCodeTransformation<ELEMENTECLASS, JAVAELEMENTCLASS> {
	/**
	 * Returns an object that holds references to model and code elements specific
	 * to IAL to code transformations.
	 */
	public IALHolder getIALHolder();

	/**
	 * Returns the IAL element that is translated. E.g. a stereotype upon a
	 * component type that owns a reference.
	 */
	public StereotypeApplication resolveTranslatedIALElement(EObject foundationalElement);

	/**
	 * Returns whether the code fragment for the IAL element exists.
	 */
	public boolean codeFragmentExists();
}
