package org.codeling.lang.base.java.transformation;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.codeling.utils.CodelingLogger;
import org.codeling.utils.errorcontainers.MayHaveIssues;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;

/**
 * Registers finished model elements and requests for model elements with their
 * model class and their name.
 *
 */
public class FindTranslatedElements extends MayHaveIssues {

	CodelingLogger log = new CodelingLogger(getClass());

	/**
	 * A map of EClasses to a list of Transformations that handle objects of that
	 * class.
	 */
	private Map<EClassifier, List<AbstractModelCodeTransformation<?, ?>>> metaModelToTransformationClassMap = Collections
			.synchronizedMap(new LinkedHashMap<>());

	/**
	 * A map of IJavaElements to a list of Transformations that handle this eleent.
	 */
	private Map<IJavaElement, List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>>> codeToTransformationMap = Collections
			.synchronizedMap(new LinkedHashMap<>());

	/**
	 * Removes all translated elements from the registry.
	 */
	public void reset() {
		metaModelToTransformationClassMap = Collections.synchronizedMap(new LinkedHashMap<>());
	}

	/**
	 * Returns a copy of the list of transformations for objects of the given
	 * classifier.
	 */
	public List<AbstractModelCodeTransformation<?, ?>> getTransformationsFor(EClassifier classifier) {
		List<AbstractModelCodeTransformation<?, ?>> list = null;
		LinkedList<AbstractModelCodeTransformation<?, ?>> result = new LinkedList<>();
		synchronized (metaModelToTransformationClassMap) {
			list = metaModelToTransformationClassMap.get(classifier);
		}
		if (list == null)
			return result;

		synchronized (list) {
			result.addAll(list);
		}
		return result;
	}

	/**
	 * Returns a copy of the list of transformations for objects of the given
	 * classifier.
	 */
	public List<AbstractModelCodeTransformation<?, ?>> getTransformationsFor(IJavaElement codeElement) {
		List<AbstractModelCodeTransformation<?, ?>> list = null;
		LinkedList<AbstractModelCodeTransformation<?, ?>> result = new LinkedList<>();
		synchronized (codeToTransformationMap) {
			list = codeToTransformationMap.get(codeElement);
		}
		if (list == null)
			return result;

		synchronized (list) {
			result.addAll(list);
		}
		return result;
	}

	/**
	 * Add a transformation to the list of transformations for a classifier. These
	 * lists can be retrieved by the operation getTransformations(EClassifier). This
	 * is used to create cross references between model objects.
	 */
	public void addTransformation(EClassifier classifier,
			AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> transformation) {
		// Add transformation to map metaModelToTransformationsClassMap
		List<AbstractModelCodeTransformation<?, ?>> transformationsForEClassifier = null;
		synchronized (metaModelToTransformationClassMap) {
			transformationsForEClassifier = metaModelToTransformationClassMap.get(classifier);
		}
		if (transformationsForEClassifier == null)
			transformationsForEClassifier = Collections.synchronizedList(new LinkedList<>());
		synchronized (transformationsForEClassifier) {
			transformationsForEClassifier.add(transformation);
		}
		synchronized (metaModelToTransformationClassMap) {
			metaModelToTransformationClassMap.put(classifier, transformationsForEClassifier);
		}

		// Add transformation to map codeToTransformationMap, if any codeElement is defined in the transformation
		if (transformation.getCodeElement() != null) {
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> transformationsForCodeElements = null;
			synchronized (codeToTransformationMap) {
				transformationsForCodeElements = codeToTransformationMap.get(transformation.getCodeElement());
			}
			
			if(transformationsForCodeElements == null)
				transformationsForCodeElements = Collections.synchronizedList(new LinkedList<>());
			synchronized (transformationsForEClassifier) {
				transformationsForCodeElements.add(transformation);
			}
			synchronized (codeToTransformationMap) {
				codeToTransformationMap.put(transformation.getCodeElement(), transformationsForCodeElements);
			}
		}
	}

	/**
	 * Returns a list of all registered transformations. Should not be called while
	 * transformations are executed, because it heavily operates on the underlying
	 * synchronized map and lists.
	 */
	public List<AbstractModelCodeTransformation<?, ?>> getAllTransformations() {
		LinkedList<AbstractModelCodeTransformation<?, ?>> list = new LinkedList<>();
		synchronized (metaModelToTransformationClassMap) {
			for (Entry<EClassifier, List<AbstractModelCodeTransformation<?, ?>>> e : metaModelToTransformationClassMap
					.entrySet())
				synchronized (e.getValue()) {
					list.addAll(e.getValue());
				}
		}
		return list;
	}

	public List<AbstractModelCodeTransformation<?, ?>> getTransformationsFor(EObject modelElement) {
		List<AbstractModelCodeTransformation<?, ?>> list = getTransformationsFor(modelElement.eClass());
		return list.stream().filter(t -> t.getModelElement() == modelElement).collect(Collectors.toList());
	}

}