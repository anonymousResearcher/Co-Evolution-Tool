package org.codeling.mechanisms.transformations;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;

public abstract class ClassMechanismTransformation<ECLASS extends EObject, JAVAELEMENT extends IJavaElement>
		extends AbstractModelCodeTransformation<ECLASS, JAVAELEMENT> {

	protected EClass eClass = null;

	public ClassMechanismTransformation(
			AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation,
			EClass eClass) {
		super(parentTransformation);
		this.eClass = eClass;
	}

	@Override
	public boolean canHandle(EModelElement metaModelElement) {
		return metaModelElement instanceof EClass && ((EClass) metaModelElement).isSuperTypeOf(eClass);
	}

}