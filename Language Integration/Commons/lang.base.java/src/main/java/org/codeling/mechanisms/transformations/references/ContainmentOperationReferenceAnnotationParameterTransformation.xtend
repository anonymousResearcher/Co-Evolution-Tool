package org.codeling.mechanisms.transformations.references

import org.codeling.lang.base.java.ASTUtils
import java.text.MessageFormat
import java.util.Collection
import java.util.LinkedList
import java.util.List
import org.codeling.mechanisms.transformations.ReferenceMechanismTransformation
import org.codeling.mechanisms.transformations.classes.ContainmentOperationTargetTransformation
import org.codeling.utils.CodelingException
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.jdt.core.IJavaElement
import org.eclipse.jdt.core.IMethod

abstract class ContainmentOperationReferenceAnnotationParameterTransformation<OWNERECLASS extends EObject, TARGETECLASS extends EObject> extends ReferenceMechanismTransformation<OWNERECLASS, TARGETECLASS, IMethod> {

	new(ContainmentOperationTargetTransformation<OWNERECLASS> parentTransformation, EReference eReference) {
		super(parentTransformation, eReference)
	}

	override createCodeFragments() throws CodelingException {
		val String annotationName = (eReference.eContainer as EClass).name.toFirstUpper;
		val String memberKey = eReference.name;

		val List<EObject> newTargets = if (eReference.isMany)
				newLinkedList((modelElement).eGet(eReference) as Collection<EObject>)
			else
				newLinkedList(((modelElement).eGet(eReference)) as EObject);

		val List<IJavaElement> targetsToAdd = newLinkedList();
		newTargets.forEach [ newTarget |
			if (!idRegistry.knowsImplementationModelElement(newTarget)) {
				addWarning(
					MessageFormat.format(
						"Could not add the code representation of [{0}] with the name [{1}] as member value, because the id registry does not know the model element.",
						newTarget.eClass.name, newTarget.nameAttributeValue));
				return;
			}

			targetsToAdd.add(idRegistry.getCodeElementFromImplementationModelElement(newTarget));
		];

		if (!eReference.isMany && !targetsToAdd.empty) {
			for (Object o : (parentTransformation.parentTransformation as ContainmentOperationTransformation<?, ?>).
				methods) {
				val IMethod m = o as IMethod;
				if (m.getElementName().equals(modelElement.nameAttributeValue)) {
					ASTUtils.addMemberValueTypeLiteral(m, annotationName, memberKey, targetsToAdd.get(0).elementName);
				}
			}
		}
	}

	override updateCodeFragments() throws CodelingException {
		val String annotationName = (eReference.eContainer as EClass).name.toFirstUpper;
		val String memberKey = eReference.name;

		val List<EObject> newTargets = if (eReference.isMany)
				newLinkedList((modelElement).eGet(eReference) as Collection<EObject>)
			else
				newLinkedList(((modelElement).eGet(eReference)) as EObject);

		val List<IJavaElement> targetsToAdd = newLinkedList();
		newTargets.forEach [ newTarget |
			val targetTransformations = findTranslatedElements.getTransformationsFor(newTarget);
			if (targetTransformations.empty)
				addError("No transformation found for " + newTarget);
			val IJavaElement newCodeElement = targetTransformations.head.codeElement;
			if (targetTransformations.head.priorModelElement === null)
				targetsToAdd.add(newCodeElement);
		];

		if (!eReference.isMany && !targetsToAdd.empty) {
			ASTUtils.replaceMemberValueTypeLiteral(codeElement, annotationName, memberKey,
				targetsToAdd.get(0).elementName);
		}

		// Should handle deletions and references with isMany=true here
	}

	override deleteCodeFragments() {
		// remove annotation member value (only if 0..*)
	}

	override transformToModel() throws CodelingException {
		if (modelElement === null)
			throw new IllegalStateException("modelElement must not be null");
		if (codeElement === null)
			throw new IllegalStateException("codeElement must not be null");

		// Find the element that is referenced by the annotation value's type and set it as eReference value
		// Get the annotation
		// Get its annotation member value
		val String annotationName = (eReference.eContainer as EClass).name.toFirstUpper;
		val String annotationParameterName = eReference.name;

		val LinkedList<String> typeNames = new LinkedList();
		if (eReference.many) {
			val Object targetTypes = ASTUtils.getAnnotationMemberValue(codeElement.getAnnotation(annotationName),
				annotationParameterName, Object);
			val Object[] targetTypesArray = targetTypes as Object[];
			targetTypesArray.forEach[e|typeNames.add(e as String)];
		} else {
			val String typeName = ASTUtils.getAnnotationMemberValueClassType(codeElement.getAnnotation(annotationName),
				annotationParameterName);
			typeNames.add(typeName.substring(typeName.lastIndexOf('.') + 1));
		}

		val List<TARGETECLASS> targetElements = getTargetObjects(modelElement, eReference, typeNames);
		if (eReference.isMany) {
			(modelElement.eGet(eReference) as List<TARGETECLASS>).addAll(targetElements);
		} else {
			modelElement.eSet(eReference, targetElements.head);
		}

		return modelElement;
	}

}
