package org.codeling.lang.uml;

import java.util.Arrays;
import java.util.List;

import org.codeling.lang.base.modeltrans.henshintgg.HenshinTGGBasedLanguageDefinition;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.UMLFactory;
import org.osgi.framework.FrameworkUtil;

import de.mkonersmann.il.profiles.Profiles;

public class UMLLanguageDefinition extends HenshinTGGBasedLanguageDefinition {

	static final URI henshinTGGFileURI = URI.createPlatformPluginURI(
			"/" + FrameworkUtil.getBundle(UMLLanguageDefinition.class).getSymbolicName() + "/IAL2UML.henshin", true);

	public UMLLanguageDefinition() {
		super(henshinTGGFileURI);
	}

	/**
	 * Creates a comment in the UML model to save the element's id in Codeling
	 */
	@Override
	public void setID(EObject object, String id) {
		Comment comment = UMLFactory.eINSTANCE.createComment();
		comment.setBody("Codeling-ID: " + id);
		((Element) object).getOwnedComments().add(comment);
	}

	/**
	 * Retrieves the id from a comment in the UML model as it was stored by setID.
	 */
	@Override
	public String getID(EObject object) {
		for (Comment c : ((Element) object).getOwnedComments())
			if (c.getBody().matches("Codeling-ID: .+"))
				return c.getBody().substring("Codeling-ID: ".length());
		return null;
	}

	@Override
	public List<String> getSelectedProfiles() {
		return Arrays.asList(Profiles.COMPONENTS_HIERARCHY_SCOPED.getNsURI(), Profiles.CONNECTORS.getNsURI(),
				Profiles.CONNECTORS_PROCEDURE_CALLS_SYNCHRONOUS_1TO1.getNsURI(), Profiles.INTERFACES_SCOPED.getNsURI(),
				Profiles.INTERFACES_TYPE_OPERATIONS.getNsURI());
	}
}
