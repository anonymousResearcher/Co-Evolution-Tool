package org.codeling.test.ecore;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.EcoreUtil;

public class Ecore2Dot {
	private List<EObject> roots;
	private String dot;
	private static final String NL = System.getProperty("line.separator");

	public Ecore2Dot(EObject root) {
		this.roots = new LinkedList<>();
		this.roots.add(root);
	}

	public Ecore2Dot(List<EObject> roots) {
		this.roots = roots;
	}

	public String toString() {
		if (dot == null) {
			dot = createDotFromEcore();
		}
		return dot;
	}

	public Path storeToFile() throws IOException {
		Path tmpFile = Files.createTempFile("ecore2dot", ".dot");
		Files.write(tmpFile, toString().getBytes("UTF-8"));
		return tmpFile;
	}

	private String createDotFromEcore() {
		String res = "digraph ecore {" + NL;

		List<EObject> flatTree = new LinkedList<>();
		for (EObject r : roots) {
			flatTree.add(r);
			TreeIterator<EObject> it = r.eAllContents();
			while (it.hasNext()) {
				flatTree.add(it.next());
			}
		}

		for (EObject r : flatTree) {
			res += createDotNode(r);
		}

		for (EObject r : flatTree) {
			res += createDotReferences(r);
		}
		return res + NL + "}";
	}

	private String createDotReferences(EObject e) {
		String res = "";
		for (EReference ref : e.eClass().getEAllReferences()) {
			res += createDotReferences(e, ref);
		}
		return res;
	}

	private String createDotReferences(EObject e, EReference ref) {
		String res = "";
		if (ref.isMany()) {
			@SuppressWarnings("unchecked")
			List<EObject> targets = (List<EObject>) e.eGet(ref);
			for (EObject target : targets) {
				res += createDotReference(e, target, ref);
			}
		} else {
			EObject target = (EObject) e.eGet(ref);
			if (target != null)
				res += createDotReference(e, target, ref);
		}
		return res;
	}

	private String createDotReference(EObject source, EObject target, EReference ref) {
		String style;
		if (ref.isContainment())
			style = "style=\"bold\"";
		else {
			style = "color=\"grey\"";
		}
		return String.format("\t%s -> %s[label = \"%s\", %s];", getDotNodeID(source), getDotNodeID(target),
				ref.getName(), style) + NL;
	}

	private String createDotNode(EObject e) {
		String id = getDotNodeID(e);
		String res = "\t" + id + "[label = \"";
		res += e.eClass().getName() + ": ";

		List<EAttribute> attributes = e.eClass().getEAllAttributes();
		boolean commaNecessary = false;
		for (int i = 0; i < attributes.size(); i++) {
			if (commaNecessary)
				// Skip comma for first attribute and for the attribute after
				// the id.
				res += ", ";
			EAttribute attribute = attributes.get(i);
			if (attribute.isID()) {
				// Skip the id
				commaNecessary = false;
				continue;
			}
			res += attribute.getName() + " = \\\"" + e.eGet(attribute) + "\\\"";
			commaNecessary = true;
		}

		return res + "\"];" + NL;
	}

	private String getDotNodeID(EObject e) {
		String id = EcoreUtil.getID(e);
		if (id == null)
			id = "" + e.hashCode();
		return id;
	}
}
