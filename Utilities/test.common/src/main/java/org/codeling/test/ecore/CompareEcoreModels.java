package org.codeling.test.ecore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.javatuples.Pair;
import org.modelversioning.emfprofileapplication.StereotypeApplication;

public class CompareEcoreModels {
	boolean ignoreIDs = false;
	HashMap<EObject, LinkedList<EObject>> candidates = new HashMap<>();

	public void compare(List<EObject> expected, List<EObject> actual) {
		compare(expected, actual, true);
	}

	public boolean compare(List<EObject> expected, List<EObject> actual, boolean withAssertions) {
		if (withAssertions) {
			assertNotNull("Expected model is null, but must not be.", expected);
			assertNotNull("Actual model is null, but must not be.", actual);
			assertEquals("Actual model does not have the same root node count as the expected model.", expected.size(),
					actual.size());
		} else {
			if (expected == null || actual == null)
				return false;
			if (expected.size() != actual.size())
				return false;
		}

		final Set<EObject> allExpected = flatten(expected);
		final Set<EObject> allActual = flatten(actual);

		if (withAssertions) {
			assertEquals("Actual model does not have the same node count as the expected model.", allExpected.size(),
					allActual.size());
		} else {
			if (allExpected.size() != allActual.size())
				return false;
		}

		final boolean elementsMatchByAttributes = buildMatchesByAttributes(allExpected, allActual, withAssertions);
		if (!elementsMatchByAttributes)
			return false;

		reduceCandidatesByOutgoingRefSrcAndTrg(expected, actual, withAssertions);
		reduceCandidatesByIncomingRefSrcAndTrg(allExpected, allActual);

		for (final Entry<EObject, LinkedList<EObject>> entry : candidates.entrySet()) {
			final LinkedList<EObject> matches = entry.getValue();

			if (withAssertions) {
				assertNotEquals("Expected element " + entry.getKey()
						+ " has 0 matches in the actual model, but should have exactly one", 0, matches.size());
				final StringBuffer matchListString = new StringBuffer();
				matches.forEach(
						m -> matchListString.append(System.getProperty("line.separator")).append(m).append(", "));
				// matches.size() could be > 1 in "diamond" scenarios. This is
				// hard to detect, and not that crucial here, so we will not
				// assert that !(matches.size() > 1)
			} else if (matches.size() == 0) {
				// matches.size() could be > 1 in "diamond" scenarios. This is
				// hard to detect, and not that crucial here, so we will not
				// assert that !(matches.size() > 1)
				return false;
			}
		}

		return true;
	}

	private boolean buildMatchesByAttributes(Set<EObject> allExpected, Set<EObject> allActual, boolean withAssertions) {
		// Find candidates for element matching based on attributes and
		// containments
		for (final EObject expectedElement : allExpected) {
			int noOfCandidates = 0;
			for (final EObject actualElement : allActual) {
				if (expectedElement == null || actualElement == null)
					continue;
				if (!(expectedElement.eClass().getName().equals(actualElement.eClass().getName()) && expectedElement
						.eClass().getEPackage().getName().equals(actualElement.eClass().getEPackage().getName())))
					continue;

				final boolean attributesOk = compareAttributes(expectedElement, actualElement, false);
				if (!attributesOk)
					continue;

				addCandidate(expectedElement, actualElement);
				noOfCandidates++;
			}

			if (withAssertions)
				assertTrue(String.format(
						"An element '%s' of the expected model could not be matched in the actual model based on attributes and type.",
						expectedElement.toString()), noOfCandidates > 0);
			else if (noOfCandidates <= 0)
				return false;
		}
		return true;
	}

	private void reduceCandidatesByIncomingRefSrcAndTrg(Set<EObject> allExpected, Set<EObject> allActual) {
		for (final Entry<EObject, LinkedList<EObject>> entry : candidates.entrySet()) {
			final List<Pair<EObject, EReference>> incomingKeyReferences = getIncomingReferences(entry.getKey(), allExpected);
			final int keyIncomingRefsSize = incomingKeyReferences.size();

			final LinkedList<EObject> values = entry.getValue();
			final LinkedList<EObject> vToRemove = new LinkedList<>();
			for (final EObject v : values) {
				final List<Pair<EObject, EReference>> incomingValueReferences = getIncomingReferences(v, allActual);
				if (incomingValueReferences.size() != keyIncomingRefsSize)
					vToRemove.add(v);

				boolean allIncomingMatch = true;
				for (final Pair<EObject, EReference> keyReferencePair : incomingKeyReferences) {
					boolean incomingMatches = false;
					for (final Pair<EObject, EReference> valueReference : incomingValueReferences) {
						final EReference keyProfileRef = keyReferencePair.getValue1();
						final EReference candidateProfileRef = valueReference.getValue1();
						final EObject key = keyReferencePair.getValue0();
						final EObject candidate = valueReference.getValue0();

						if (key instanceof StereotypeApplication ^ candidate instanceof StereotypeApplication)
							continue;

						if (key instanceof StereotypeApplication && candidate instanceof StereotypeApplication
								&& !keyProfileRef.getName().equals(candidateProfileRef.getName()))
							continue;

						if (!matches(keyReferencePair.getValue0(), valueReference.getValue0()))
							continue;
						incomingMatches = true;
						break;
					}
					if (!incomingMatches) {
						allIncomingMatch = false;
						break;
					}
				}
				if (!allIncomingMatch)
					vToRemove.add(v);
			}
			values.removeAll(vToRemove);
		}
	}

	@SuppressWarnings("unchecked")
	private void reduceCandidatesByOutgoingRefSrcAndTrg(List<EObject> expected, List<EObject> actual,
			boolean withAssertions) {
		for (final Entry<EObject, LinkedList<EObject>> entry : candidates.entrySet()) {
			final EObject key = entry.getKey();
			final LinkedList<EObject> values = entry.getValue();
			final LinkedList<EObject> candidatesToRemove = new LinkedList<>();
			for (final EObject candidate : values) { // For all candidates
				boolean allReferencesMatch = true;
				// For all reference types
				for (final EReference keyProfileRef : key.eClass().getEAllReferences()) {
					EReference candidateProfileRef = keyProfileRef;
					if (key instanceof StereotypeApplication && candidate instanceof StereotypeApplication) {
						final StereotypeApplication candidatePA = (StereotypeApplication) candidate;
						for (final EReference candidateRef : candidatePA.eClass().getEAllReferences()) {
							if (keyProfileRef.getName().equals(candidateRef.getName())) {
								candidateProfileRef = candidateRef;
								break;
							}
						}
					}

					if (!keyProfileRef.isMany()) {
						if (!matches(key.eGet(keyProfileRef), candidate.eGet(candidateProfileRef))) {
							allReferencesMatch = false;
							break;
						}
					} else {
						final List<EObject> keysRefs = (List<EObject>) key.eGet(keyProfileRef);
						final List<EObject> candidatesRefs = (List<EObject>) candidate.eGet(candidateProfileRef);
						boolean allTargetsMatch = true;
						for (final EObject kTarget : keysRefs) {
							boolean thisTargetMatches = false;
							for (final EObject candidateTarget : candidatesRefs) {
								if (matches(kTarget, candidateTarget)) {
									thisTargetMatches = true;
									break;
								}
							}
							if (!thisTargetMatches) {
								allTargetsMatch = false;
								break;
							}
						}
						if (!allTargetsMatch) {
							allReferencesMatch = false;
							break;
						}
					}
				}
				if (!allReferencesMatch) {
					candidatesToRemove.add(candidate);
				}
			}
			for (final EObject candidate : candidatesToRemove)
				removeFromCandidates(key, candidate);
		}
	}

	private void removeFromCandidates(EObject key, EObject candidate) {
		LinkedList<EObject> value;
		if (candidates.containsKey(key))
			value = candidates.get(key);
		else
			value = new LinkedList<>();

		value.remove(candidate);

		candidates.put(key, value);
	}

	private boolean matches(Object k, Object v) {
		final LinkedList<EObject> entry = candidates.get(k);
		return k == null && v == null || entry != null && entry.contains(v);
	}

	@SuppressWarnings("unchecked")
	private List<Pair<EObject, EReference>> getIncomingReferences(EObject targetedElement, Set<EObject> allElements) {
		final EClass targetEClass = targetedElement.eClass();
		final List<Pair<EObject, EReference>> result = new LinkedList<>();

		for (final EObject o : allElements) {
			for (final EReference ref : o.eClass().getEAllReferences()) {
				if (ref.getEType() != targetEClass)
					continue;

				if (ref.isMany()) {
					final List<EObject> l = (List<EObject>) o.eGet(ref);
					for (final EObject li : l) {
						if (li == targetedElement) {
							result.add(new Pair<EObject, EReference>(o, ref));
							break;
						}
					}
				} else {
					if (o.eGet(ref) == targetedElement)
						result.add(new Pair<EObject, EReference>(o, ref));
				}
			}
		}

		return result;
	}

	private void addCandidate(EObject expectedElement, EObject actualElement) {
		LinkedList<EObject> value;
		if (candidates.containsKey(expectedElement))
			value = candidates.get(expectedElement);
		else
			value = new LinkedList<>();

		if (!value.contains(actualElement))
			value.add(actualElement);

		candidates.put(expectedElement, value);
	}

	private Set<EObject> flatten(List<EObject> tree) {
		final Set<EObject> flat = new HashSet<>();
		for (final EObject e : tree) {
			flat.add(e);
			flattenChildren(e, flat);
		}
		return flat;
	}

	private void flattenChildren(EObject object, Set<EObject> flat) {
		final List<EObject> it = object.eContents();
		for (final EObject e : it) {
			if (e == null)
				continue;
			flat.add(e);
			flattenChildren(e, flat);
		}
	}

	private boolean compareAttributes(EObject expectedElement, EObject actualElement, boolean withAssertions) {
		final EList<EAttribute> attributes = expectedElement.eClass().getEAllAttributes();
		boolean attributesOk = true;
		for (final EAttribute attribute : attributes) {
			if (ignoreIDs && attribute.isID())
				continue;
			if (withAssertions)
				assertEquals(expectedElement.eGet(attribute), actualElement.eGet(attribute));
			else {
				final Object of = expectedElement.eGet(attribute);
				final Object os = actualElement.eGet(attribute);
				if (of == null ^ os == null) {
					attributesOk = false;
					continue;
				} else if (!(of == null && os == null)
						&& !(expectedElement.eGet(attribute).equals(actualElement.eGet(attribute)))) {
					attributesOk = false;
					continue;
				}
			}
		}
		return attributesOk;
	}

	public CompareEcoreModels ignoringIds() {
		ignoreIDs = true;
		return this;
	}
}
