package org.codeling.utils;

public class IDRegistryEntry {
	String cmPath;
	String ilPath;
	String id;

	public IDRegistryEntry(String id, String cmPath, String ilPath) {
		this.cmPath = cmPath;
		this.ilPath = ilPath;
		this.id = id;
	}

	public String getCmPath() {
		return cmPath;
	}

	public void setCmPath(String cmPath) {
		this.cmPath = cmPath;
	}

	public String getIlPath() {
		return ilPath;
	}

	public void setIlPath(String ilPath) {
		this.ilPath = ilPath;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}