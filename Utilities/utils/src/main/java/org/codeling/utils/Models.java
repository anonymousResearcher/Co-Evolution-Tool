package org.codeling.utils;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

public class Models {
	final static CodelingLogger log = new CodelingLogger(Models.class);

	public static Resource store(List<EObject> modelRoot, String fileName) {
		try {
			// Create project if it does not exist.
			final IProject project = ResourcesPlugin.getWorkspace().getRoot()
					.getProject(CodelingConfiguration.TEMPORARY_MODEL_PROJECT_NAME);
			if (!project.exists())
				project.create(null);
			if (!project.isOpen())
				project.open(null);

			// Create debug directory if it does not exist
			final IFolder debugFolder = project.getFolder(CodelingConfiguration.TEMPORARY_MODEL_DEBUG_DIRECTORY_NAME);
			if (!debugFolder.exists())
				debugFolder.create(true, true, null);

			final Resource tempResource = new ResourceSetImpl()
					.createResource(URI.createPlatformResourceURI(fileName, true));
			tempResource.getContents().addAll(modelRoot);
			tempResource.save(Collections.EMPTY_MAP);
			log.info("Stored model into file: " + fileName);

			return tempResource;
		} catch (final Throwable t) {
			log.error(String.format("Could not store model to file '%s'", fileName), t);
			return null;
		}
	}

	public static List<EObject> loadFrom(String fileName) {
		final ResourceSet resSet = new ResourceSetImpl();
		final URI uri = URI.createPlatformResourceURI(fileName, true);
		final Resource resource = resSet.createResource(uri);
		try {
			resource.load(Collections.EMPTY_MAP);
			return resource.getContents();
		} catch (final IOException e) {
			// Could not find a method like isLoadable
			log.error("Could not load the file " + fileName, e);
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public static List<EObject> getTargetsAsList(EObject modelElement, EReference eReference) {
		LinkedList<EObject> targets = new LinkedList<>();
		if (modelElement == null)
			return targets;
		if (eReference.isMany())
			targets.addAll((Collection<? extends EObject>) modelElement.eGet(eReference));
		else {
			EObject target = (EObject) modelElement.eGet(eReference);
			if (target != null)
				targets.add(target);
		}
		return targets;
	}

}
