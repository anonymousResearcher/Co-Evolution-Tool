package org.codeling.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.JavaCore;

public class IDRegistry {
	CodelingLogger log = new CodelingLogger(getClass());

	HashMap<String, String[]> id2paths = new HashMap<String, String[]>();

	public HashMap<String, String[]> getRegistryEntries() {
		return id2paths;
	}

	private void updateEntry(String id, String codePath, String implementationModelPath, String translationModelPath) {
		final String[] paths = id2paths.get(id);
		if (paths == null) {
			id2paths.put(id, new String[] { codePath, implementationModelPath, translationModelPath });
		} else {
			paths[0] = codePath;
			paths[1] = implementationModelPath;
			paths[2] = translationModelPath;
		}
	}

	/**
	 * Registers an implementation model element in the registry and returns its new
	 * id.
	 */
	public String registerImplementationModelElement(IJavaElement codeElement, EObject implementationModelElement) {
		final String codePath = codeElement != null ? codeElement.getHandleIdentifier() : null;
		final String implementationModelPath = getPath(implementationModelElement);
		final String id = generateID();

		updateEntry(id, codePath, implementationModelPath, null);
		return id;
	}

	/**
	 * Registers a translation model element in the registry and returns its new id.
	 */
	public String registerTranslationModelElement(EObject translationModelElement) {
		final String translationModelPath = getPath(translationModelElement);
		final String id = generateID();

		updateEntry(id, null, null, translationModelPath);
		return id;
	}

	/**
	 * Adds a translation model language element to the registry for an existing
	 * implementation model element. If the path to the implementation model element
	 * does not exist in the registry, nothing is done.
	 */
	public void addTranslationModelElement(EObject translationModelElement, EObject implementationModelElement) {
		final String id = getIDFromImplementationModelElement(implementationModelElement);
		final String ilPath = getPath(translationModelElement);
		id2paths.get(id)[2] = ilPath;
	}

	/**
	 * Adds a code element for a given implementation model element. If the model
	 * element is not known in the registry yet, it is created.
	 */
	public void addCodeElement(EObject implementationModelElement, IJavaElement codeElement) {
		if (codeElement == null)
			throw new IllegalArgumentException("codeElement must not be null");
		if (implementationModelElement == null)
			throw new IllegalArgumentException("implementationModelElement must not be null");

		if (!knowsImplementationModelElement(implementationModelElement)) {
			registerImplementationModelElement(codeElement, implementationModelElement);
		} else {
			final String implementationModelPath = getPath(implementationModelElement);

			for (final Entry<String, String[]> e : id2paths.entrySet()) {
				if (e.getValue()[1] != null && e.getValue()[1].equals(implementationModelPath)) {
					e.getValue()[0] = codeElement.getHandleIdentifier();
					return;
				}
			}
		}

	}

	public void updateTranslationModelElement(String id, EObject translationModelElement) {
		String[] entries = id2paths.get(id);
		if (entries == null)
			throw new IllegalArgumentException(String.format("No registry entry for id %s exists.", id));
		final String ilPath = translationModelElement == null ? null : getPath(translationModelElement);
		if (ilPath == null && entries[2] != null
				|| ilPath != null && (entries[2] == null || !ilPath.equals(entries[2])))
			updateEntry(id, entries[0], entries[1], ilPath);
	}

	public void updateImplementationModelElement(String id, EObject implementationModelElement) {
		String[] entries = id2paths.get(id);
		if (entries == null)
			throw new IllegalArgumentException(String.format("No registry entry for id %s exists.", id));
		final String implementationModelPath = implementationModelElement == null ? null
				: getPath(implementationModelElement);
		if (implementationModelPath == null && entries[1] != null || implementationModelPath != null
				&& (entries[1] == null || !implementationModelPath.equals(entries[1])))
			updateEntry(id, entries[0], implementationModelPath, entries[2]);
	}

	public void updateCodeElement(String id, String codePath) {
		String[] entries = id2paths.get(id);
		if (entries == null)
			throw new IllegalArgumentException(String.format("No registry entry for id %s exists.", id));
		if (codePath == null && entries[0] != null
				|| codePath != null && (entries[0] == null || !codePath.equals(entries[0])))
			updateEntry(id, codePath, entries[1], entries[2]);
	}

	public String getIDFromImplementationModelElement(EObject implementationModelElement) {
		if (implementationModelElement == null)
			throw new IllegalArgumentException("Implementation Model Element must not be null.");

		final String implementationModelPath = getPath(implementationModelElement);

		for (final Entry<String, String[]> e : id2paths.entrySet()) {
			if (e.getValue()[1] != null && e.getValue()[1].equals(implementationModelPath)) {
				return e.getKey();
			}
		}

		return null;
	}

	public String getIDFromTranslationModelElement(EObject translationModelElement) {
		if (translationModelElement == null)
			throw new IllegalArgumentException("Translation Model must not be null.");

		final String translationModelPath = getPath(translationModelElement);

		for (final Entry<String, String[]> e : id2paths.entrySet()) {
			if (e.getValue()[2] != null && e.getValue()[2].equals(translationModelPath)) {
				return e.getKey();
			}
		}

		return null;
	}

	public String getIDFromCodeElement(IJavaElement codeElement) {
		if (codeElement == null)
			throw new IllegalArgumentException("Code Element must not be null.");

		final String codePath = codeElement.getHandleIdentifier();

		for (final Entry<String, String[]> e : id2paths.entrySet()) {
			if (e.getValue()[0] != null && e.getValue()[0].equals(codePath)) {
				return e.getKey();
			}
		}

		return null;
	}

	public EObject resolveImplementationModelElement(EObject translationModelElement,
			List<EObject> implementationModelRootElements) {
		final String translationModelPath = getPath(translationModelElement);

		for (final Entry<String, String[]> e : id2paths.entrySet()) {
			if (e.getValue()[2] != null && e.getValue()[2].equals(translationModelPath)) {
				for (EObject root : implementationModelRootElements) {
					EObject resolved = resolve(root, e.getValue()[1]);
					if (resolved != null)
						return resolved;
				}
			}
		}

		throw new IllegalArgumentException(String
				.format("Could not get implementation model element for il element '%s'.", translationModelElement));
	}

	private String generateID() {
		return EcoreUtil.generateUUID();
	}

	public void saveToFile(String fileName) {
		final IWorkspace workspace = ResourcesPlugin.getWorkspace();
		String sepPattern = File.separator;
		if (sepPattern.equals("\\"))
			sepPattern = "\\\\";

		final IFile file = workspace.getRoot().getProject(fileName.split(sepPattern)[0])
				.getFile(fileName.substring(fileName.indexOf(File.separator) + 1));

		try {
			if (file.exists())
				file.delete(true, null);
			file.create(new ByteArrayInputStream(this.toString().getBytes("UTF-8")), true, null);
		} catch (CoreException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static IDRegistry load(URL url) throws IOException {
		try (InputStream inputStream = url.openConnection().getInputStream()) {
			final BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
			return createIDRegistryFromReader(r);
		} catch (final IOException e) {
			throw e;
		}
	}

	public static IDRegistry load(String fileName) {
		final IWorkspace workspace = ResourcesPlugin.getWorkspace();
		String sepPattern = File.separator;
		if (sepPattern.equals("\\"))
			sepPattern = "\\\\";

		final IFile file = workspace.getRoot().getProject(fileName.split(sepPattern)[0])
				.getFile(fileName.substring(fileName.indexOf(File.separator) + 1));

		if (file.exists()) {
			try {
				final BufferedReader r = new BufferedReader(new InputStreamReader(file.getContents()));
				return createIDRegistryFromReader(r);
			} catch (CoreException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		} else
			return null;
	}

	private static IDRegistry createIDRegistryFromReader(BufferedReader r) throws IOException {
		final IDRegistry reg = new IDRegistry();
		String line;
		while ((line = r.readLine()) != null) {
			String[] cols = line.split("\\$");

			// Line might end with two $ when no implementation model element and no
			// translation model element is given.
			if (cols.length < 3)
				cols = new String[] { cols[0], cols[1], null, null };

			// Line might end with a $ when no translation model element is given.
			if (cols.length < 4)
				cols = new String[] { cols[0], cols[1], cols[2], null };

			reg.updateEntry(cols[0], cols[1], cols[2], cols[3]);
		}
		return reg;
	}

	private static String getPath(EObject o) {
		final EObject root = EcoreUtil.getRootContainer(o);
		String path = EcoreUtil.getRelativeURIFragmentPath(root, o);

		if (path.startsWith("//"))
			path = path.substring(1);

		int rootIndex = 0;
		// When this element is within a resource, the resource may contain
		// multiple roots.
		if (o.eResource() != null)
			rootIndex = o.eResource().getContents().indexOf(root);

		if (path.isEmpty())
			// This is a root object. Do not append a slash.
			return String.format("/.%d", rootIndex);

		return String.format("/.%d/%s", rootIndex, path);
	}

	private static EObject resolve(EObject root, String path) {
		try {
			if (path.matches("/"))
				return root;

			path = path.substring(1);

			return EcoreUtil.getEObject(root, path);
		} catch (final Exception e) {
			return null;
		}
	}

	/**
	 * Registers a code element with its corresponding implementation model element
	 * with a newly generated id.
	 */
	public IDRegistry generateID(IJavaElement codeElement, EObject implementationModelElement) {
		registerImplementationModelElement(codeElement, implementationModelElement);
		return this;
	}

	@Override
	public String toString() {
		final String NEWLINE = System.getProperty("line.separator");
		final StringBuffer sb = new StringBuffer();
		id2paths.entrySet().stream()
				.sorted((x, y) -> x.getValue()[0] == null ? -1
						: y.getValue()[0] == null ? 1 : x.getValue()[0].compareTo(y.getValue()[0]))
				.forEachOrdered(e -> {
					sb.append(String.format("%s$%s$%s$%s" + NEWLINE, e.getKey(),
							e.getValue()[0] == null ? "" : e.getValue()[0],
							e.getValue()[1] == null ? "" : e.getValue()[1],
							e.getValue()[2] == null ? "" : e.getValue()[2]));
				});
		return sb.toString();
	}

	public EObject resolveImplementationModelElement(String id, List<EObject> implementationModelRoots) {
		final String[] entry = id2paths.get(id);

		if (entry == null) {
			return null;
		} else {
			// The entry might be empty, when no component model element exists
			// for this id.
			if (entry[1] == null || "".equals(entry[1]))
				return null;

			final int rootIndex = getRootIndex(entry[1]);
			final String path = getPathWithoutRoot(entry[1]);

			return resolve(implementationModelRoots.get(rootIndex), path);
		}
	}

	public EObject resolveImplementationModelElement(String id, EObject rootContainer) {
		final LinkedList<EObject> roots = new LinkedList<>();
		roots.add(rootContainer);
		return resolveImplementationModelElement(id, roots);
	}

	public EObject resolveTranslationModelElement(String id, EObject rootContainer) {
		final LinkedList<EObject> roots = new LinkedList<>();
		roots.add(rootContainer);
		return resolveTranslationModelElement(id, roots);
	}

	public EObject resolveTranslationModelElement(String id, List<EObject> translationModelRoots) {
		final String[] entry = id2paths.get(id);

		if (entry == null || entry[2] == null) {
			return null;
		} else {
			final int rootIndex = getRootIndex(entry[2]);
			final String path = getPathWithoutRoot(entry[2]);

			return resolve(translationModelRoots.get(rootIndex), path);
		}
	}

	private String getPathWithoutRoot(String entryPath) {
		if (entryPath.matches("^/?\\.\\d+$")) {
			// is a root object
			return "/";
		} else {
			final int secondSlashIndex = entryPath.substring(1).indexOf("/") + 1;
			return entryPath.substring(secondSlashIndex);
		}
	}

	private int getRootIndex(String entryPath) {
		String rootIndexString = null;
		if (entryPath.matches("^/?\\.\\d+$")) {
			// is a root object
			rootIndexString = entryPath.substring("/.".length());
		} else {
			final int secondSlashIndex = entryPath.substring(1).indexOf("/") + 1;
			rootIndexString = entryPath.substring("/.".length(), secondSlashIndex);
		}
		return Integer.parseInt(rootIndexString);

	}

	public EObject resolveTranslationModelElement(EObject implementationModelElement,
			EObject translationModelRootElement) {
		final LinkedList<EObject> roots = new LinkedList<>();
		roots.add(translationModelRootElement);
		return resolveTranslationModelElement(implementationModelElement, roots);
	}

	public EObject resolveTranslationModelElement(EObject implementationModelElement,
			List<EObject> translationModelRoots) {
		final String implementationModelPath = getPath(implementationModelElement);

		boolean foundImplementationModelElement = false;
		final List<String> translationModelPaths = new LinkedList<>();
		for (final Entry<String, String[]> e : id2paths.entrySet()) {
			if (e.getValue()[1] != null && e.getValue()[1].equals(implementationModelPath)) {
				foundImplementationModelElement = true;
				if (e.getValue()[2] == null)
					return null;
				else {
					final String translationModelPath = e.getValue()[2];
					translationModelPaths.add(translationModelPath);
					final int rootIndex = getRootIndex(translationModelPath);
					final String path = getPathWithoutRoot(translationModelPath);
					return resolve(translationModelRoots.get(rootIndex), path);
				}
			}
		}

		if (!foundImplementationModelElement)
			throw new IllegalArgumentException(String.format(
					"Could not get an translation model element for implementation model element '%s'. The id registry does not contain an entry with a implementation model element path '%s'.",
					implementationModelElement, implementationModelPath));
		else
			throw new IllegalArgumentException(String.format(
					"Could not get an translation model element for implementation model element '%s'. The id registry contains an entry with a implementation model element path '%s', but it could not resolve any translation model element path of '%s'.",
					implementationModelElement, implementationModelPath,
					Arrays.toString(translationModelPaths.toArray())));
	}

	public IJavaElement getCodeElementFromImplementationModelElement(EObject modelElement) {
		String id = getIDFromImplementationModelElement(modelElement);
		return getCodeElement(id);
	}

	public IJavaElement getCodeElementFromTranslationModelElement(EObject modelElement) {
		String id = getIDFromTranslationModelElement(modelElement);
		if (id == null)
			return null;
		return getCodeElement(id);
	}

	public IJavaElement getCodeElement(String id) {
		final String[] entry = id2paths.get(id);

		if (entry == null) {
			return null;
		} else {
			// The entry might be empty, when no code model element exists
			// for this id.
			if (entry[0] == null || "".equals(entry[0]))
				return null;

			return JavaCore.create(entry[0]);
		}
	}

	public boolean knowsImplementationModelElement(EObject implementationModelElement) {
		final String implementationModelPath = getPath(implementationModelElement);

		for (final Entry<String, String[]> e : id2paths.entrySet())
			if (e.getValue()[1] != null && e.getValue()[1].equals(implementationModelPath))
				return true;
		return false;
	}

	public boolean knowsTranslationModelElement(EObject translationModelElement) {
		final String translationModelPath = getPath(translationModelElement);

		for (final Entry<String, String[]> e : id2paths.entrySet())
			if (e.getValue()[2] != null && e.getValue()[2].equals(translationModelPath))
				return true;
		return false;
	}

	public void deleteEntries(Collection<String> idsToDelete) {
		for (String id : idsToDelete)
			id2paths.remove(id);
	}

	public Set<String> getAllIds() {
		HashSet<String> result = new HashSet<>();
		result.addAll(id2paths.keySet());
		return result;
	}

}
