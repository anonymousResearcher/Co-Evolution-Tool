package org.codeling.utils.errorcontainers;

import java.util.LinkedList;
import java.util.List;

import org.codeling.utils.CodelingLogger;

public class MayHaveIssues implements IMayHaveIssues {

	protected CodelingLogger log = new CodelingLogger(getClass());

	Class<?> owningClass;
	String name;
	LinkedList<Issue> infos = new LinkedList<>();
	LinkedList<Issue> warnings = new LinkedList<>();
	LinkedList<Issue> errors = new LinkedList<>();

	public MayHaveIssues() {
	}

	public MayHaveIssues(Class<?> owningClass) {
		this.owningClass = owningClass;
	}

	@Override
	public boolean isOK() {
		return !hasErrorsOrWarnings();
	}

	@Override
	public boolean hasErrorsOrWarnings() {
		return hasErrors() || hasWarnings();
	}

	@Override
	public boolean hasErrors() {
		return !errors.isEmpty();
	}

	@Override
	public boolean hasWarnings() {
		return !warnings.isEmpty();
	}

	@Override
	public boolean hasInfos() {
		return !infos.isEmpty();
	}

	@Override
	public List<Issue> issues() {
		final LinkedList<Issue> result = new LinkedList<Issue>();
		result.addAll(errors);
		result.addAll(warnings);
		result.addAll(infos);
		return result;
	}

	@Override
	public List<Issue> errorsAndWarnings() {
		final LinkedList<Issue> result = new LinkedList<Issue>();
		result.addAll(errors);
		result.addAll(warnings);
		return result;
	}

	@Override
	public List<Issue> errors() {
		return errors;
	}

	@Override
	public List<Issue> warnings() {
		return warnings;
	}

	@Override
	public List<Issue> infos() {
		return infos;
	}

	@Override
	public void addIssue(Level level, String message, Throwable e) {
		switch (level) {
		case ERROR:
			addError(message, e);
			break;
		case WARNING:
			addWarning(message, e);
			break;
		case INFO:
			addInfo(message, e);
			break;
		}
	}

	@Override
	public void addIssue(Level level, String message) {
		addIssue(level, message, null);
	}

	@Override
	public void addWarning(String message, Throwable e) {
		warnings.add(new Issue(Level.WARNING, message, e));
		log.warning(message, e);
	}

	@Override
	public void addWarning(String message) {
		addWarning(message, null);
	}

	@Override
	public void addError(String message, Throwable e) {
		errors.add(new Issue(Level.ERROR, message, e));
		log.error(message, e);
	}

	@Override
	public void addError(String message) {
		addError(message, null);
	}

	@Override
	public void addInfo(String message, Throwable e) {
		infos.add(new Issue(Level.INFO, message, e));
		log.info(message, e);
	}

	@Override
	public void addInfo(String message) {
		addInfo(message, null);
	}

	@Override
	public Class<?> getOwningClass() {
		return owningClass;
	}

	public void setOwningClass(Class<?> owningClass) {
		this.owningClass = owningClass;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
