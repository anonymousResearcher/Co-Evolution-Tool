package org.codeling.utils.errorcontainers;

import java.util.List;

public interface IMayHaveIssues {
	public boolean isOK();

	public boolean hasErrorsOrWarnings();
	
	public boolean hasErrors();

	public boolean hasWarnings();
	
	public boolean hasInfos();

	public List<Issue> issues();

	public List<Issue> errorsAndWarnings();

	public List<Issue> errors();
	
	public List<Issue> warnings();

	public List<Issue> infos();

	public void addIssue(Level level, String message, Throwable e);

	public void addIssue(Level level, String message);

	public void addWarning(String message, Throwable e);

	public void addWarning(String message);

	public void addError(String message, Throwable e);

	public void addError(String message);

	public void addInfo(String message, Throwable e);

	public void addInfo(String message);

	public Class<?> getOwningClass();
	
	public String getName();

}
