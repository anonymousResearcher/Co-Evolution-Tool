package org.codeling.utils;

import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

/**
 * @author Anonymous Researcher <anonymous@example.org>
 */
public class CodelingLogger {

	String bundleId;
	ILog eclipseErrorLog;

	public CodelingLogger(Class<?> loggingClass) {
		if (loggingClass == null)
			throw new IllegalArgumentException("loggingClass must not be null");
		final Bundle loggingBundle = FrameworkUtil.getBundle(loggingClass);
		eclipseErrorLog = Platform.getLog(loggingBundle);
		bundleId = loggingBundle.getSymbolicName();
	}

	private void addLogEntry(int level, String msg, Throwable t) {
		eclipseErrorLog.log(new Status(level, bundleId, msg, t));
	}

	public void warning(String message, Throwable t) {
		addLogEntry(IStatus.WARNING, message, t);
	}

	public void error(String message, Throwable t) {
		addLogEntry(IStatus.ERROR, message, t);
	}

	public void info(String message) {
		addLogEntry(IStatus.INFO, message, null);
	}

	public void info(String message, Throwable t) {
		addLogEntry(IStatus.INFO, message, t);
	}

	public void trace(TraceLogTopic traceLogTopic, String message) {
		if (Platform.inDebugMode()) {
			final String debugOption = Platform.getDebugOption(traceLogTopic.getValue());
			if ("true".equalsIgnoreCase(debugOption))
				addLogEntry(IStatus.INFO, message, null);
		}
	}

}
