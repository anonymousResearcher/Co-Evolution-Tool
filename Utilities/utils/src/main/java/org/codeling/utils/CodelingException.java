package org.codeling.utils;

/**
 * The codeling exception is the high level exception of the codeling tool. It
 * wraps underlying exceptions that cannot be handled properly, and should be
 * taken to the user. This should only be thrown on a relatively high level.
 */
public class CodelingException extends Exception {
	private static final long serialVersionUID = -2107754728672217667L;

	public CodelingException(String msg) {
		super(msg);
	}

	public CodelingException(String msg, Throwable t) {
		super(msg, t);
	}
}
