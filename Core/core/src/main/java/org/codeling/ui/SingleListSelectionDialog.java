package org.codeling.ui;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.stream.Collectors;

import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.languageregistry.LanguageDefinition;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class SingleListSelectionDialog extends Dialog {
	private Text txtSearchTwo;
	LanguageDefinition[] dataTwo;
	ILabelProvider provider;
	List listDataTwo;
	LinkedList<LanguageDefinition> filteredDataTwo = new LinkedList<>();
	LanguageDefinition selectionTwo;
	boolean isImplementationMigration = false;
	boolean hasImplementationMigrationButton = false;

	public SingleListSelectionDialog(Shell parentShell, ILabelProvider p) {
		super(parentShell);
		provider = p;
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(null);

		int baseY = hasImplementationMigrationButton ? 62 : 37;
		txtSearchTwo = new Text(container, SWT.BORDER);
		txtSearchTwo.setBounds(6, baseY, 285, 20);
		txtSearchTwo.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				refreshListDataTwo();
			}
		});

		listDataTwo = new List(container, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		listDataTwo.setBounds(6, baseY + 25, 285, 214);
		refreshListDataTwo();
		listDataTwo.setSelection(0);
		if (listDataTwo.getSelectionIndex() != -1)
			selectionTwo = dataTwo[listDataTwo.getSelectionIndex()];
		listDataTwo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if ((listDataTwo.getSelectionIndex() >= 0))
					selectionTwo = dataTwo[listDataTwo.getSelectionIndex()];
			}
		});

		Label lblCaptionTwo = new Label(container, SWT.NONE);
		lblCaptionTwo.setAlignment(SWT.CENTER);
		lblCaptionTwo.setBounds(6, 14, 285, 15);
		lblCaptionTwo.setText("Target Language");

		if (hasImplementationMigrationButton) {
			Button implementationMigrationBox = new Button(container, SWT.CHECK | SWT.BORDER);
			implementationMigrationBox.setAlignment(SWT.CENTER);
			implementationMigrationBox.setBounds(6, 35, 285, 20);
			implementationMigrationBox.setText("Target Implementation Language");
			implementationMigrationBox.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					isImplementationMigration = ((Button) e.widget).getSelection();
					refreshListDataTwo();
				}
			});
		}

		return container;
	}

	private void refreshListDataTwo() {
		filteredDataTwo.clear();
		filteredDataTwo.addAll(Arrays.asList(dataTwo));

		for (LanguageDefinition x : dataTwo) {
			LanguageDefinition ld = (LanguageDefinition) x;
			// Always add empty language
			if (ld instanceof EmptySpecificationLanguage)
				continue;

			// Filter by checkbox
			if (hasImplementationMigrationButton
					&& (isImplementationMigration ^ ld instanceof ImplementationLanguageDefinition))
				filteredDataTwo.remove(ld);

			// Filter by search field
			if (txtSearchTwo.getText() != null && !txtSearchTwo.getText().isEmpty()
					&& !ld.getName().toLowerCase().contains(txtSearchTwo.getText().toLowerCase()))
				filteredDataTwo.remove(ld);
		}

		listDataTwo.setItems(filteredDataTwo.stream().map(ld -> String.format("%s (%s)", ld.getName(), ld.getVersion())).collect(Collectors.toList())
				.toArray(new String[filteredDataTwo.size()]));
		listDataTwo.redraw();
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected Point getInitialSize() {
		return new Point(300, 400);
	}

	public void setDataTwo(LanguageDefinition[] o) {
		dataTwo = o;
	}

	public void setData(LanguageDefinition[] o1) {
		setDataTwo(o1);
	}

	public Object getSecondResult() {
		return selectionTwo;
	}

	public void setHasImplementationMigrationButton(boolean hasImplementationMigrationButton) {
		this.hasImplementationMigrationButton = hasImplementationMigrationButton;
	}

}
