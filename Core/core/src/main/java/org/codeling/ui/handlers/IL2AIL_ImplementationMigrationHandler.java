package org.codeling.ui.handlers;

import java.util.List;

import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.transformationmanager.internal.steps.InterProfileTransformationToImplementationModelTask;
import org.codeling.transformationmanager.internal.steps.TranslationModelToImplementationModelTask;
import org.codeling.utils.IDRegistry;
import org.codeling.utils.Models;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.EObject;

public class IL2AIL_ImplementationMigrationHandler extends AbstractSteppedHandler {

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		executedAsSingleStep = true;

		final ImplementationLanguageDefinition implementationLanguage = selectImplementationLanguage(
				LABEL_SELECT_TARGET_LANG);
		if (implementationLanguage == null)
			return Status.CANCEL_STATUS;

		final IDRegistry priorIdRegistry = IDRegistry.load(InterProfileTransformationToImplementationModelTask.ID_FILEPATH);
		final List<EObject> priorModelRoots = Models.loadFrom(InterProfileTransformationToImplementationModelTask.MODEL_FILEPATH);
		
		final Job job = new TranslationModelToImplementationModelTask(implementationLanguage, priorIdRegistry, priorModelRoots, true, true);

		job.setUser(true);
		job.schedule();
		return Status.OK_STATUS;
	}

}
