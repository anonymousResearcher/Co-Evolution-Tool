package org.codeling.ui;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.stream.Collectors;

import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.languageregistry.LanguageDefinition;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class MultiListSelectionDialog extends Dialog {
	private Text txtSearchOne;
	private Text txtSearchTwo;
	LanguageDefinition[] dataOne;
	LanguageDefinition[] dataTwo;
	LinkedList<LanguageDefinition> filteredDataOne = new LinkedList<>();
	LinkedList<LanguageDefinition> filteredDataTwo = new LinkedList<>();
	ILabelProvider provider;
	List listDataOne;
	List listDataTwo;
	Object selectionOne;
	Object selectionTwo;
	boolean isImplementationMigration = false;

	public MultiListSelectionDialog(Shell parentShell, ILabelProvider p) {
		super(parentShell);
		provider = p;
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(null);

		txtSearchOne = new Text(container, SWT.BORDER);
		txtSearchOne.setBounds(10, 35, 435, 20);
		txtSearchOne.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				refreshListDataOne();
			}
		});

		txtSearchTwo = new Text(container, SWT.BORDER);
		txtSearchTwo.setBounds(451, 62, 435, 20);
		txtSearchTwo.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				refreshListDataTwo();
			}
		});

		listDataOne = new List(container, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		listDataOne.setBounds(10, 62, 435, 240);
		refreshListDataOne();
		listDataOne.setSelection(0);
		if (listDataOne.getSelectionIndex() != -1)
			selectionOne = dataOne[listDataOne.getSelectionIndex()];
		listDataOne.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (listDataOne.getSelectionIndex() >= 0)
					selectionOne = filteredDataOne.get(listDataOne.getSelectionIndex());
			}
		});

		listDataTwo = new List(container, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		listDataTwo.setBounds(451, 87, 435, 214);
		refreshListDataTwo();
		listDataTwo.setSelection(0);
		if (listDataTwo.getSelectionIndex() != -1)
			selectionTwo = dataTwo[listDataTwo.getSelectionIndex()];
		listDataTwo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if ((listDataTwo.getSelectionIndex() >= 0))
					selectionTwo = filteredDataTwo.get(listDataTwo.getSelectionIndex());
			}
		});

		Label lblCaptionOne = new Label(container, SWT.NONE);
		lblCaptionOne.setAlignment(SWT.CENTER);
		lblCaptionOne.setBounds(10, 14, 435, 15);
		lblCaptionOne.setText("Source Language");

		Label lblCaptionTwo = new Label(container, SWT.NONE);
		lblCaptionTwo.setAlignment(SWT.CENTER);
		lblCaptionTwo.setBounds(451, 14, 435, 15);
		lblCaptionTwo.setText("Target Language");

		Button implementationMigrationBox = new Button(container, SWT.CHECK | SWT.BORDER);
		implementationMigrationBox.setAlignment(SWT.CENTER);
		implementationMigrationBox.setBounds(451, 35, 435, 20);
		implementationMigrationBox.setText("Target Implementation Language");
		implementationMigrationBox.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				isImplementationMigration = ((Button) e.widget).getSelection();
				refreshListDataTwo();
			}
		});

		return container;
	}

	private void refreshListDataOne() {
		filteredDataOne.clear();
		filteredDataOne.addAll(Arrays.asList(dataOne));

		for (LanguageDefinition x : dataOne) {
			LanguageDefinition ld = (LanguageDefinition) x;
			// Always add empty language
			if (ld instanceof EmptySpecificationLanguage)
				continue;

			// Filter by search field
			if (txtSearchOne.getText() != null && !txtSearchOne.getText().isEmpty()
					&& !ld.getName().toLowerCase().contains(txtSearchOne.getText().toLowerCase()))
				filteredDataOne.remove(ld);
		}

		listDataOne.setItems(filteredDataOne.stream().map(ld -> String.format("%s (%s)", ld.getName(), ld.getVersion())).collect(Collectors.toList())
				.toArray(new String[filteredDataOne.size()]));
		listDataOne.redraw();
	}

	private void refreshListDataTwo() {
		filteredDataTwo.clear();
		filteredDataTwo.addAll(Arrays.asList(dataTwo));

		for (LanguageDefinition x : dataTwo) {
			LanguageDefinition ld = (LanguageDefinition) x;
			// Always add empty language
			if (ld instanceof EmptySpecificationLanguage)
				continue;

			// Filter by checkbox
			if (isImplementationMigration ^ ld instanceof ImplementationLanguageDefinition)
				filteredDataTwo.remove(ld);

			// Filter by search field
			if (txtSearchTwo.getText() != null && !txtSearchTwo.getText().isEmpty()
					&& !ld.getName().toLowerCase().contains(txtSearchTwo.getText().toLowerCase()))
				filteredDataTwo.remove(ld);
		}

		listDataTwo.setItems(filteredDataTwo.stream().map(ld -> String.format("%s (%s)", ld.getName(), ld.getVersion())).collect(Collectors.toList())
				.toArray(new String[filteredDataTwo.size()]));
		listDataTwo.redraw();
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected Point getInitialSize() {
		return new Point(900, 403);
	}

	public void setDataOne(LanguageDefinition[] o) {
		dataOne = o;
	}

	public void setDataTwo(LanguageDefinition[] o) {
		dataTwo = o;
	}

	public void setData(LanguageDefinition[] o1, LanguageDefinition[] o2) {
		setDataOne(o1);
		setDataTwo(o2);
	}

	public Object getFirstResult() {
		return selectionOne;
	}

	public Object getSecondResult() {
		return selectionTwo;
	}

}
