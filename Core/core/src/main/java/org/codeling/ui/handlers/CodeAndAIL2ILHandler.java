package org.codeling.ui.handlers;

import java.util.LinkedList;
import java.util.List;

import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.transformationmanager.internal.steps.ImplementationModelToTranslationModelTask;
import org.codeling.transformationmanager.internal.steps.ProgramCodeToImplementationModelTask;
import org.codeling.transformationmanager.internal.steps.ProgramCodeToTranslationModelTask;
import org.codeling.utils.IDRegistry;
import org.codeling.utils.Models;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaProject;

public class CodeAndAIL2ILHandler extends AbstractSteppedHandler {

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		executedAsSingleStep = true;

		final ImplementationLanguageDefinition implementationLanguage = selectImplementationLanguage(LABEL_SELECT_SOURCE_LANG);
		if (implementationLanguage == null)
			return Status.CANCEL_STATUS;

		// Load source code and translation to IL model
		final LinkedList<IJavaProject> projects = getSelectedProjects(event);

		final IDRegistry priorIdRegistry = IDRegistry.load(ImplementationModelToTranslationModelTask.ID_FILEPATH);
		final List<EObject> componentModelRoots = Models.loadFrom(ProgramCodeToImplementationModelTask.MODEL_FILEPATH);
		final List<EObject> ilModelRoots = Models.loadFrom(ImplementationModelToTranslationModelTask.MODEL_FILEPATH);

		final Job job = new ProgramCodeToTranslationModelTask(implementationLanguage, projects, priorIdRegistry, componentModelRoots,
				ilModelRoots, true);
		job.setUser(true);
		job.schedule();

		return Status.OK_STATUS;
	}

}
