package org.codeling.ui.handlers;

import java.util.List;

import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.transformationmanager.internal.steps.ImplementationModelToTranslationModelTask;
import org.codeling.transformationmanager.internal.steps.ProgramCodeToImplementationModelTask;
import org.codeling.utils.IDRegistry;
import org.codeling.utils.Models;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.EObject;

public class AIL2ILHandler extends AbstractSteppedHandler {

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {

		final ImplementationLanguageDefinition implementationLanguage = selectImplementationLanguage(LABEL_SELECT_SOURCE_LANG);
		if (implementationLanguage == null)
			return Status.CANCEL_STATUS;

		final IDRegistry priorIdRegistry = IDRegistry.load(ProgramCodeToImplementationModelTask.ID_FILEPATH);
		final List<EObject> currentModelRoots = Models.loadFrom(ProgramCodeToImplementationModelTask.MODEL_FILEPATH);

		final Job job = new ImplementationModelToTranslationModelTask(implementationLanguage, priorIdRegistry, currentModelRoots, true, true);
		job.setUser(true);
		job.schedule();

		return Status.OK_STATUS;
	}

}
