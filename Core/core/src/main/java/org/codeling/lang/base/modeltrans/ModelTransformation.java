/**
 *
 */
package org.codeling.lang.base.modeltrans;

import java.text.MessageFormat;
import java.util.List;

import org.codeling.transformationmanager.TransformationResult;
import org.codeling.utils.CodelingLogger;
import org.codeling.utils.IDRegistry;
import org.codeling.utils.TraceLogTopic;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.modelversioning.emfprofile.Profile;
import org.modelversioning.emfprofile.registry.IProfileRegistry;

import de.mkonersmann.il.core.CorePackage;

/**
 * @author Anonymous Researcher <anonymous@example.org>
 *
 */
public abstract class ModelTransformation {

	protected String taskName = null;
	protected URI transformationFileURI = null;
	protected List<EObject> modelRoots = null;
	protected TransformationResult transformationResult = null;
	protected IDRegistry idRegistry;

	protected CodelingLogger log = new CodelingLogger(getClass());

	public ModelTransformation(URI transformationFileURI, String taskName, IDRegistry idRegistry,
			List<EObject> modelRoots) {
		this.taskName = taskName;
		this.transformationFileURI = transformationFileURI;
		this.idRegistry = idRegistry;
		this.modelRoots = modelRoots;
		
		// Register the Intermediate Language
		CorePackage.eINSTANCE.getName();
		// Initialize all registered profiles for the Intermediate Language
		for (final Profile e : IProfileRegistry.eINSTANCE.getRegisteredProfiles())
			if (EPackage.Registry.INSTANCE.containsKey(e.getNsURI()))
				EPackage.Registry.INSTANCE.put(e.getNsURI(), e);
	}

	public TransformationResult execute(IProgressMonitor monitor) {
		log.trace(TraceLogTopic.TASK_EXECUTION, String.format("Starting task: %s", getTaskName()));
		try {
			final TransformationResult res = doExecute(monitor);
			log.trace(TraceLogTopic.TASK_EXECUTION, String.format("Finished task: %s", getTaskName()));
			return res;
		} catch (Exception e) {
			log.warning(MessageFormat.format(
					"Could not execute henshin transformation [{0}]. Returning an unchanged model.",
					transformationFileURI), e);
			return new TransformationResult(modelRoots, idRegistry);
		}
	}

	protected abstract TransformationResult doExecute(IProgressMonitor monitor);

	public TransformationResult getTransformationResult() {
		return transformationResult;
	}

	public List<EObject> getModelRoots() {
		return modelRoots;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

}
