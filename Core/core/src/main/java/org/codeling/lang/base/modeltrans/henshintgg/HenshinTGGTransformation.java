package org.codeling.lang.base.modeltrans.henshintgg;

import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.codeling.lang.base.modeltrans.ModelTransformation;
import org.codeling.languageregistry.LanguageDefinition;
import org.codeling.languageregistry.SpecificationLanguageDefinition;
import org.codeling.transformationmanager.TransformationResult;
import org.codeling.utils.CodelingConfiguration;
import org.codeling.utils.IDRegistry;
import org.codeling.utils.Models;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.henshin.trace.TracePackage;

import de.mkonersmann.henshin.tgg.api.TGGExecutor;
import de.mkonersmann.henshin.tgg.api.TGGExecutorFactory;
import de.mkonersmann.henshin.tgg.api.TGGModel;
import de.mkonersmann.henshin.tgg.api.TGGTripleComponent;

/**
 * @author Anonymous Researcher <anonymous@example.org>
 *
 */
public class HenshinTGGTransformation extends ModelTransformation {

	/**
	 * A reference to the language to be translated to or from the IAL
	 */
	final LanguageDefinition language;

	TGGExecutor executor;
	TGGDirection direction = TGGDirection.BACKWARD;
	TransformationResult result;

	public HenshinTGGTransformation(LanguageDefinition language, URI tggFileURI, String taskName,
			TGGDirection direction, IDRegistry idRegistry, List<EObject> currentModelRoots) {
		super(tggFileURI, taskName, idRegistry, currentModelRoots);
		this.language = language;
		setDirection(direction);

		// The propagation uses the traces. Therefore the TracePackage should be
		// instantiated
		TracePackage.eINSTANCE.getClass();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.mkonersmann.advert.transformationmanager.AbstractTransformationTask
	 * #execute()
	 */
	@Override
	public TransformationResult doExecute(IProgressMonitor monitor) {
		SubMonitor m = SubMonitor.convert(monitor, 5);
		transformModel(modelRoots, transformationFileURI, m.newChild(4));

		result = exportModel(idRegistry, m.newChild(1));
		return result;
	}

	public void propagateIDsFromTM2IMInRegistry() {
		Map<List<EObject>, List<EObject>> initial2resultElements = getInitial2ResultElements();
		if (initial2resultElements == null)
			throw new IllegalStateException(
					"initial2resultElements must not be null. Please execute a transformation first.");
		if (idRegistry == null)
			throw new IllegalStateException("idRegistry must not be null");

		for (final Entry<List<EObject>, List<EObject>> entry : initial2resultElements.entrySet()) {
			EObject initialObject = null;
			EObject targetObject = null;
			if (entry.getKey().size() == 0 && entry.getValue().size() == 0)
				continue;
			else if (entry.getKey().size() == 0) {
				targetObject = entry.getValue().get(0);
				log.warning(MessageFormat.format("The result element {0} has 0 initial elements.", targetObject), null);
			} else if (entry.getValue().size() == 0) {
				initialObject = entry.getKey().get(0);
				log.warning(MessageFormat.format("The initial element {0} has 0 result elements.", initialObject),
						null);
			} else {
				initialObject = entry.getKey().get(0);
				targetObject = entry.getValue().get(0);

				if (entry.getKey().size() > 1)
					log.warning(MessageFormat.format(
							"The result element {0} has {1} initial elements. Only propagating the id of the first found initial element. Translations should not create correspondence objects that have multiple sources or multiple targets.",
							targetObject, entry.getKey().size()), null);
				if (entry.getValue().size() > 1)
					log.warning(MessageFormat.format(
							"The initial element {0} has {1} result elements. Only propagating the id of the first found result element. Translations should not create correspondence objects that have multiple sources or multiple targets.",
							initialObject, entry.getValue().size()), null);
			}

			try {
				if (initialObject == null)
					continue; // This is a new element in the ADL. Therefore we did not find any id. Ignore

				String id = idRegistry.getIDFromTranslationModelElement(initialObject);
				if (id == null) {
					log.warning(MessageFormat.format("Could not find an ID for the initialObject [{0}]",
							initialObject.toString()), null);
					continue;
				}

				idRegistry.updateImplementationModelElement(id, targetObject);
			} catch (final Throwable t) {
				log.warning(
						MessageFormat.format("Could not propagate the id of the specification language element [{0}]",
								initialObject.toString()),
						t);
			}
		}
	}

	/**
	 * Updates the path in the translation model for all id registry entries, which
	 * are parts of traces.
	 * 
	 * @return The ids of all updated elements
	 */
	public List<String> propagateIDsFromModelToRegistry() {
		Map<List<EObject>, List<EObject>> initial2resultElements = getInitial2ResultElements();
		if (initial2resultElements == null)
			throw new IllegalStateException(
					"initial2resultElements must not be null. Please execute a transformation first.");
		if (idRegistry == null)
			throw new IllegalStateException("idRegistry must not be null");
		if (language == null)
			throw new IllegalStateException("language must not be null");
		if (!(language instanceof SpecificationLanguageDefinition))
			throw new IllegalStateException(
					"language must be an instane of SpecificationLanguageDefinition. ID propagation is not possible for implementation languages.");

		// The model elements need a resource for
		// propagateIDsFromSourceModelToRegistry to work in a multi root
		// element environment.
		Models.store(result.getModelRoots(),
				CodelingConfiguration.DEBUG_MODELDIR_PATH + "tmp-propagateIDsFromModelToRegistry.xmi");

		LinkedList<String> updatedIDs = new LinkedList<String>();
		for (final Entry<List<EObject>, List<EObject>> entry : initial2resultElements.entrySet()) {
			EObject initialObject = null;
			EObject targetObject = null;
			if (entry.getKey().size() == 0 && entry.getValue().size() == 0)
				continue;
			else if (entry.getKey().size() == 0) {
				targetObject = entry.getValue().get(0);
				log.warning(MessageFormat.format("The result element {0} has 0 initial elements.", targetObject), null);
				continue;
			} else if (entry.getValue().size() == 0) {
				initialObject = entry.getKey().get(0);
				log.warning(MessageFormat.format("The initial element {0} has 0 result elements.", initialObject),
						null);
				continue;
			} else {
				initialObject = entry.getKey().get(0);
				targetObject = entry.getValue().get(0);

				if (entry.getKey().size() > 1)
					log.warning(MessageFormat.format(
							"The result element {0} has {1} initial elements. Only propagating the id of the first found initial element. Translations should not create correspondence objects that have multiple sources or multiple targets.",
							targetObject, entry.getKey().size()), null);
				if (entry.getValue().size() > 1)
					log.warning(MessageFormat.format(
							"The initial element {0} has {1} result elements. Only propagating the id of the first found result element. Translations should not create correspondence objects that have multiple sources or multiple targets.",
							initialObject, entry.getValue().size()), null);
			}

			try {
				final String id = ((SpecificationLanguageDefinition) language).getID(initialObject);
				if (id == null)
					;// This was a new element in the ADL. Therefore we did not find any id. Ignore
				else {
					idRegistry.updateTranslationModelElement(id, targetObject);
					updatedIDs.add(id);
				}
			} catch (IllegalArgumentException e) {
				// Should not happen
				log.warning("An element in the ID registry could not be updated", e);
			}
		}
		return updatedIDs;
	}

	public void addTranslationModelElementsToRegistry() {
		Map<List<EObject>, List<EObject>> initial2resultElements = getInitial2ResultElements();
		if (initial2resultElements == null)
			throw new IllegalStateException(
					"initial2resultElements must not be null. Please execute a transformation first.");
		if (result == null)
			throw new IllegalStateException("result must not be null. Please execute a transformation first.");
		if (idRegistry == null)
			throw new IllegalStateException("idRegistry must not be null");

		// The model elements need a resource for
		// addIntermediateLanguageElementsToRegistry to work in a multi root
		// element environment.
		Models.store(result.getModelRoots(),
				CodelingConfiguration.DEBUG_MODELDIR_PATH + "tmp-addIntermediateLanguageElementsToRegistry.xmi");

		for (final Entry<List<EObject>, List<EObject>> entry : initial2resultElements.entrySet()) {
			EObject initialObject = null;
			EObject targetObject = null;
			if (entry.getKey().size() == 0 && entry.getValue().size() == 0)
				continue;
			else if (entry.getKey().size() == 0) {
				targetObject = entry.getValue().get(0);
				log.warning(MessageFormat.format("The result element {0} has 0 initial elements.", targetObject), null);
			} else if (entry.getValue().size() == 0) {
				initialObject = entry.getKey().get(0);
				log.warning(MessageFormat.format("The initial element {0} has 0 result elements.", initialObject),
						null);
			} else {
				initialObject = entry.getKey().get(0);
				targetObject = entry.getValue().get(0);

				if (entry.getKey().size() > 1)
					log.warning(MessageFormat.format(
							"The result element {0} has {1} initial elements. Will only add the first initial element. Translations should not create correspondence objects that have multiple sources or multiple targets.",
							targetObject, entry.getKey().size()), null);
				if (entry.getValue().size() > 1)
					log.warning(MessageFormat.format(
							"The initial element {0} has {1} result elements. Will only add the first initial element. Translations should not create correspondence objects that have multiple sources or multiple targets.",
							initialObject, entry.getValue().size()), null);

				idRegistry.addTranslationModelElement(targetObject, initialObject);
			}
		}
	}

	/**
	 * Synchronizes the IDs of the nodes in the {@link TripleGraph}. for each result
	 * node that is connected to an initial node via a trace node, the id of the
	 * result node is set to the id of that initial node.
	 */
	public void propagateIDsFromRegistryToModel() {
		Map<List<EObject>, List<EObject>> initial2resultElements = getInitial2ResultElements();
		if (initial2resultElements == null)
			throw new IllegalStateException(
					"initial2resultElements must not be null. Please execute a transformation first.");
		if (idRegistry == null)
			throw new IllegalStateException("idRegistry must not be null");
		if (language == null)
			throw new IllegalStateException("language must not be null");
		if (!(language instanceof SpecificationLanguageDefinition))
			throw new IllegalStateException(
					"language must be an instane of SpecificationLanguageDefinition. ID propagation is not possible for implementation languages.");

		for (final Entry<List<EObject>, List<EObject>> entry : initial2resultElements.entrySet()) {
			EObject initialObject = null;
			EObject targetObject = null;
			if (entry.getKey().size() == 0 && entry.getValue().size() == 0)
				continue;
			else if (entry.getKey().size() == 0) {
				targetObject = entry.getValue().get(0);
				log.warning(MessageFormat.format("The result element {0} has 0 initial elements.", targetObject), null);
			} else if (entry.getValue().size() == 0) {
				initialObject = entry.getKey().get(0);
				log.warning(MessageFormat.format("The initial element {0} has 0 result elements.", initialObject),
						null);
			} else {
				initialObject = entry.getKey().get(0);
				targetObject = entry.getValue().get(0);

				if (entry.getKey().size() > 1)
					log.warning(MessageFormat.format(
							"The result element {0} has {1} initial elements. Only propagating the id of the first found initial element. Translations should not create correspondence objects that have multiple sources or multiple targets.",
							targetObject, entry.getKey().size()), null);
				if (entry.getValue().size() > 1)
					log.warning(MessageFormat.format(
							"The initial element {0} has {1} result elements. Only propagating the id of the first found result element. Translations should not create correspondence objects that have multiple sources or multiple targets.",
							initialObject, entry.getValue().size()), null);
			}

			try {
				final String id = idRegistry.getIDFromTranslationModelElement(initialObject);
				if (id != null)
					((SpecificationLanguageDefinition) language).setID(targetObject, id);
			} catch (final Throwable t) {
				log.warning(String.format("Could not set id for '%s', based on '%s'", targetObject, initialObject), t);
			}
		}
	}

	public void transformModel(List<EObject> modelRoots, URI henshinTggFileUri, IProgressMonitor monitor) {
		SubMonitor m = SubMonitor.convert(monitor, "Loading model into TGG (" + henshinTggFileUri + ")", 2);
		// Import root elements into Henshin TGG
		executor = importModelToTGG(modelRoots, henshinTggFileUri);
		m.worked(1);
		m.setTaskName("Executing TGG (" + henshinTggFileUri + ")");
		// Execute the model-to-model transformation
		executeTransformation(executor, direction);
		m.worked(1);
	}

	/**
	 * Execute a transformation of the given {@link TGGDirection} defined in the
	 * Henshin TGG file URI on the given {@link TripleGraph}.
	 *
	 * @param tripleGraph
	 * @param henshinTggFileUri
	 * @param translationType
	 */
	private void executeTransformation(TGGExecutor executor, TGGDirection translationType) {
		switch (translationType) {
		case BACKWARD:
			executor.executeBackwardTransformation();
			break;
		case BACKWARD_PROPAGATION:
			executor.executeBackwardPropagation();
			break;
		case FORWARD:
			executor.executeForwardTransformation();
			break;
		case FORWARD_PROPAGATION:
			executor.executeForwardPropagation();
			break;
		case INTEGRATION:
			executor.executeIntegration();
		}
	}

	/**
	 * Imports model roots into henshin TGG runtime.
	 *
	 * @param modelRoot
	 *            The model root elements
	 * @param henshinTggFileUri
	 *            The URI to the Henshin TGG transformation file.
	 * @return An {@link ImportResult} containing the {@link TripleGraph}, a mapping
	 *         between Henshin Nodes and eobjects, and a reference to a
	 *         {@link TGGExecutor}.
	 */
	private TGGExecutor importModelToTGG(List<EObject> modelRoots, URI henshinTggFileUri) {
		final TGGModel tggModel = TGGExecutorFactory.INSTANCE.createTGGModel(henshinTggFileUri);
		final TGGExecutor executor = tggModel.createTGGExecutor(modelRoots);

		return executor;
	}

	public Map<List<EObject>, List<EObject>> getInitial2ResultElements() {
		return executor.getInitial2ResultElements();
	}

	public TransformationResult exportModel(IDRegistry idRegistry, IProgressMonitor monitor) {
		SubMonitor m = SubMonitor.convert(monitor, 1);
		TGGTripleComponent exportComponent = null;

		switch (direction) {
		case BACKWARD:
		case BACKWARD_PROPAGATION:
			m.setTaskName("Removing target and correspondence graph.");
			exportComponent = TGGTripleComponent.SOURCE;
			break;
		case FORWARD:
		case FORWARD_PROPAGATION:
			m.setTaskName("Removing source and correspondence graph.");
			exportComponent = TGGTripleComponent.TARGET;
			break;
		default:
			break;
		}

		final List<EObject> resultRoots = executor.createModelInstanceFromTripleGraph(exportComponent);
		m.worked(1);

		// Return the collected results
		return new TransformationResult(resultRoots, idRegistry);
	}

	public void setDirection(TGGDirection direction) {
		this.direction = direction;
	}

}
