package org.codeling.lang.base.modeltrans.henshintgg;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.codeling.languageregistry.LanguageDefinition;
import org.codeling.transformationmanager.TransformationResult;
import org.codeling.utils.CodelingConfiguration;
import org.codeling.utils.CodelingException;
import org.codeling.utils.IDRegistry;
import org.codeling.utils.Models;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.henshin.model.Module;

/**
 * Implements the functionality for translating between architecture
 * implementation models and translation models, as well as between translation
 * models and architecture specification models using HenshinTGG.
 */
public class HenshinTGGBasedLanguageDefinitionHelper {

	public TransformationResult transformIMToTM(LanguageDefinition languageDefinition, URI henshinTGGFileURI,
			List<EObject> cmRoots, IDRegistry idregistry, IProgressMonitor monitor) throws CodelingException {
		final HenshinTGGTransformation task = new HenshinTGGTransformation(languageDefinition, henshinTGGFileURI,
				"Transforming Implementation Model to Implementation Model", TGGDirection.FORWARD, idregistry, cmRoots);
		TransformationResult result = task.execute(monitor);
		task.addTranslationModelElementsToRegistry();
		return result;
	}

	public TransformationResult transformTMToIM(LanguageDefinition languageDefinition, URI henshinTGGFileURI,
			List<EObject> ilRoots, IDRegistry idRegistry, IProgressMonitor monitor) throws CodelingException {
		final HenshinTGGTransformation task = new HenshinTGGTransformation(languageDefinition, henshinTGGFileURI,
				"Transforming Translation Model to Implementation Model", TGGDirection.BACKWARD, idRegistry, ilRoots);
		TransformationResult result = task.execute(monitor);
		task.propagateIDsFromTM2IMInRegistry();
		return result;
	}

	public TransformationResult transformTMToSM(HenshinTGGBasedLanguageDefinition henshinTGGBasedLanguageDefinition,
			URI henshinTGGFileURI, List<EObject> tmRoots, IDRegistry idRegistry, IProgressMonitor monitor) {
		final HenshinTGGTransformation task = new HenshinTGGTransformation(henshinTGGBasedLanguageDefinition,
				henshinTGGFileURI, "Transforming Translation Model to Specification Model", TGGDirection.FORWARD,
				idRegistry, tmRoots);
		TransformationResult result = task.execute(monitor);
		task.propagateIDsFromRegistryToModel();
		return result;
	}

	public TransformationResult transformSMToTM(HenshinTGGBasedLanguageDefinition henshinTGGBasedLanguageDefinition,
			URI henshinTGGFileURI, List<EObject> tmRoots, List<EObject> preChangeTMRoots, IDRegistry idRegistry,
			IProgressMonitor monitor) {

		// Execute =Backwards Propagation=
		final HenshinTGGTransformation bwppgTask = new HenshinTGGTransformation(henshinTGGBasedLanguageDefinition,
				henshinTGGFileURI, "Executing Backwards Propagation Rules for Specification Model to Translation Model",
				TGGDirection.BACKWARD_PROPAGATION, idRegistry, tmRoots);
		final TransformationResult result = bwppgTask.execute(monitor);

		// Recover elements lost during the translation
		Models.store(result.getModelRoots(), CodelingConfiguration.DEBUG_MODELDIR_PATH + "tmp-recover-TM.xmi");
		new RecoverPriorModelElements().recoverAll(preChangeTMRoots, result.getModelRoots(),
				henshinTGGBasedLanguageDefinition, result.getIdRegistry(), idRegistry);

		// Update the id registry
		List<String> updatedIDs = bwppgTask.propagateIDsFromModelToRegistry();
		Set<String> deletedIDs = idRegistry.getAllIds();
		deletedIDs.removeAll(updatedIDs);
		idRegistry.deleteEntries(deletedIDs);
		
		return result;
	}

	public List<String> getSelectedProfiles(URI transformationFileURI) {
		final ResourceSet rset = new ResourceSetImpl();
		final Resource resource = rset.getResource(transformationFileURI, true);
		final Module module = (Module) resource.getContents().get(0);
		final EList<EPackage> importedPkgs = module.getImports();
		final List<String> result = new LinkedList<String>();
		for (final EPackage p : importedPkgs) {
			if (p.eIsProxy()) {
				// The package might be an unresolved proxy. Then the eProxyURI
				// contains the uri we are interested in.
				result.add(((InternalEObject) p).eProxyURI().trimFragment().toString());
			} else {
				result.add(p.getNsURI());
			}
		}
		return result;
	}
}
