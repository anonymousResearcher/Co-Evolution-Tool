/**
 *
 */
package org.codeling.lang.base.modeltrans.henshin;

import java.util.List;

import org.codeling.lang.base.modeltrans.ModelTransformation;
import org.codeling.transformationmanager.TransformationResult;
import org.codeling.utils.IDRegistry;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.henshin.interpreter.ApplicationMonitor;
import org.eclipse.emf.henshin.interpreter.EGraph;
import org.eclipse.emf.henshin.interpreter.Engine;
import org.eclipse.emf.henshin.interpreter.UnitApplication;
import org.eclipse.emf.henshin.interpreter.impl.EGraphImpl;
import org.eclipse.emf.henshin.interpreter.impl.EngineImpl;
import org.eclipse.emf.henshin.interpreter.impl.LoggingApplicationMonitor;
import org.eclipse.emf.henshin.interpreter.impl.UnitApplicationImpl;
import org.eclipse.emf.henshin.model.HenshinPackage;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Unit;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;

/**
 * @author Anonymous Researcher <anonymous@example.org>
 *
 */
public class HenshinTransformation extends ModelTransformation {

	// Prepare the engine. Only one single instance is recommended
	// (http://wiki.eclipse.org/Henshin_Interpreter)
	static Engine engine = null;

	public HenshinTransformation(URI transformationURI, String taskName, IDRegistry idRegistry,
			List<EObject> currentModelRoots, IProgressMonitor progressMonitor) {
		super(transformationURI, taskName, idRegistry, currentModelRoots);

		// Initialize necessary packages
		HenshinPackage.eINSTANCE.getClass();
	}

	/*
	 * (non-Javadoc) c1
	 *
	 * @see de.mkonersmann.advert.transformationmanager.AbstractTransformationTask
	 * #doExecute()
	 */
	@Override
	public TransformationResult doExecute(IProgressMonitor monitor) {

		if (engine == null)
			engine = new EngineImpl();

		// Create a resource set for the working directory
		// "my/working/directory"
		final HenshinResourceSet resourceSet = new HenshinResourceSet();

		// Load the Henshin module:
		final Module module = resourceSet.getModule(transformationFileURI, false);

		// Initialize the graph:
		final EGraph graph = new EGraphImpl(modelRoots);

		// Find the unit to be applied:
		final Unit unit = module.getUnit("main");

		// Prepare unit application
		final UnitApplication application = new UnitApplicationImpl(engine, graph, unit, null);

		// Log only in debug mode
		ApplicationMonitor henshinMonitor = Platform.inDebugMode() ? new LoggingApplicationMonitor() : null;

		// Actually execute the unit
		application.execute(henshinMonitor);

		return new TransformationResult(graph.getRoots(), idRegistry);
	}

}
