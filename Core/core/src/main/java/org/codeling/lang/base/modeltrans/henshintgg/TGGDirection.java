/**
 *
 */
package org.codeling.lang.base.modeltrans.henshintgg;

/**
 * An enumeration of transformation types.
 *
 * @author Anonymous Researcher <anonymous@example.org>
 *
 */
public enum TGGDirection {
	/**
	 * A forward transformation. Creates new correspondence and target models.
	 */
	FORWARD,

	/**
	 * A forward propagation transformation. Considers the existing
	 * corresponding model and target model, and transforms only the rest of the
	 * source model.
	 */
	FORWARD_PROPAGATION,

	/**
	 * A backward transformation. Creates new correspondence and source models.
	 */
	BACKWARD,

	/**
	 * A backward propagation transformation. Considers the existing
	 * corresponding model and source model, and transforms only the rest of the
	 * target model.
	 */
	BACKWARD_PROPAGATION,

	/**
	 * An integration rule. Find and create Traces between a source and a target model.
	 */
	INTEGRATION
}
