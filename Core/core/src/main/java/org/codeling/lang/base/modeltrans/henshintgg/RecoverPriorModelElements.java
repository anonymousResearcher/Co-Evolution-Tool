package org.codeling.lang.base.modeltrans.henshintgg;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.codeling.languageregistry.LanguageDefinition;
import org.codeling.utils.CodelingLogger;
import org.codeling.utils.IDRegistry;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;
import org.modelversioning.emfprofile.Stereotype;

/**
 * Recovers model elements that an ADL does not work with. When an ADL does not
 * use a specific profile, the information from the application of that profile
 * is lost during the IL2ADL step. This class can be used to recover this
 * information.
 *
 * Elements within profiles which the ADL does not work with are copied and the
 * references of the copied elements are fixed to target their expected target
 * elements within the new IL model.
 */
public class RecoverPriorModelElements {
	CodelingLogger log = new CodelingLogger(getClass());
	PriorIlToNewIlCopier copier;

	public void recoverAll(List<EObject> priorIlModel, List<EObject> newIlModel, LanguageDefinition adl,
			IDRegistry idRegistry, IDRegistry priorIdRegistry) {
		copier = new PriorIlToNewIlCopier(idRegistry, priorIdRegistry, newIlModel);
		for (final EObject priorIlElement : priorIlModel) {
			final boolean isInProfile = priorIlElement.eClass() instanceof Stereotype;

			if (isInProfile && !isProfileActive(adl, priorIlElement.eClass().getEPackage().getNsURI())) {
				try {
					final EObject recoveredElement = recoverElement(priorIlElement, newIlModel);
					newIlModel.add(recoveredElement);
				} catch (final IllegalArgumentException e) {
					log.warning("Could not recover an element", e);
				}
			}
		}
	}

	private boolean isProfileActive(LanguageDefinition adl, String nsURI) {
		final List<String> selectedModules = adl.getSelectedProfiles();
		return selectedModules.contains(nsURI);
	}

	private EObject recoverElement(EObject priorIlElement, List<EObject> newIlModel) {
		final EObject copy = copier.copy(priorIlElement);
		copier.copyReferences();
		return copy;
	}

}

/**
 * Copies an IL element from before a change in an ADL and fixes the outgoing
 * edges: It replaces the original targets with the targets in the newly
 * composed IL model from after the change in the ADL.
 */
class PriorIlToNewIlCopier extends Copier {
	private static final long serialVersionUID = -1359443960498580586L;

	IDRegistry idRegistry;
	IDRegistry priorIdRegistry;
	List<EObject> ilModelRoots;

	public PriorIlToNewIlCopier(IDRegistry idRegistry, IDRegistry priorIdRegistry, List<EObject> ilModelRoots) {
		this.idRegistry = idRegistry;
		this.priorIdRegistry = priorIdRegistry;
		this.ilModelRoots = ilModelRoots;
	}

	@Override
	protected void copyReference(EReference ref, EObject original, EObject clone) {
		final Object originalTarget = original.eGet(ref);
		if (originalTarget == null)
			// The reference might be not set.
			return;

		if (ref.isMany()) {
			@SuppressWarnings("unchecked")
			final Collection<EObject> originalTargetList = (Collection<EObject>) originalTarget;
			final Collection<EObject> cloneTargetList = new LinkedList<EObject>();
			for (final EObject originalTargetElement : originalTargetList) {
				final String targetId = priorIdRegistry.getIDFromTranslationModelElement(originalTargetElement);
				if (targetId == null)
					continue; // this happens, when a prior il object has been deleted. Ignore.
				final EObject cloneTarget = idRegistry.resolveTranslationModelElement(targetId, ilModelRoots);
				cloneTargetList.add(cloneTarget);
			}
			clone.eSet(ref, cloneTargetList);
		} else {
			final String targetId = priorIdRegistry.getIDFromTranslationModelElement((EObject) originalTarget);
			final EObject cloneTarget = idRegistry.resolveTranslationModelElement(targetId, ilModelRoots);
			clone.eSet(ref, cloneTarget);
		}
	}
}