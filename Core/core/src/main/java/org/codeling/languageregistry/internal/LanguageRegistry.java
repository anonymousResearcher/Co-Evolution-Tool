/**
 *
 */
package org.codeling.languageregistry.internal;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import org.codeling.languageregistry.ILanguageRegistry;
import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.languageregistry.LanguageDefinition;
import org.codeling.languageregistry.SpecificationLanguageDefinition;
import org.codeling.utils.CodelingException;
import org.codeling.utils.CodelingLogger;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;

/**
 * A registry for language definitions. Modeling language and programming
 * language plugins are registered at this registry at run time.
 *
 * To register a language at this registry, use the extension point id stated in
 * {@link ILanguageRegistry}.
 *
 * @author Anonymous Researcher <anonymous@example.org>
 */
public class LanguageRegistry implements ILanguageRegistry {

	CodelingLogger log = new CodelingLogger(getClass());

	private static ILanguageRegistry INSTANCE = null;

	/**
	 * @return the application wide instance of the registry.
	 */
	public static ILanguageRegistry getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new LanguageRegistry();
		}
		return INSTANCE;
	}

	private LanguageRegistry() {
	}

	private final LinkedHashSet<LanguageDefinition> languages = new LinkedHashSet<>();

	/*
	 * (non-Javadoc)
	 *
	 * @see de.mkonersmann.advert.transformationmanager.ILanguageRegistry#
	 * addLanguageDefinition(de.mkonersmann.advert.transformationmanager.
	 * LanguageDefinition)
	 */
	@Override
	public void addLanguageDefinition(LanguageDefinition languageDefinition) {
		languages.add(languageDefinition);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.mkonersmann.advert.transformationmanager.ILanguageRegistry#
	 * getLanguages()
	 */
	@Override
	public Set<LanguageDefinition> getLanguages() throws CodelingException {
		resolveLanguages();
		return Collections.unmodifiableSet(languages);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.mkonersmann.advert.transformationmanager.ILanguageRegistry#
	 * getModelingLanguages()
	 */
	@Override
	public Set<SpecificationLanguageDefinition> getSpecificationLanguages() throws CodelingException {
		resolveLanguages();
		final LinkedHashSet<SpecificationLanguageDefinition> result = new LinkedHashSet<>();
		for (final LanguageDefinition l : languages)
			if (l instanceof SpecificationLanguageDefinition)
				result.add((SpecificationLanguageDefinition) l);
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.mkonersmann.advert.transformationmanager.ILanguageRegistry#
	 * getProgrammingLanguages()
	 */
	@Override
	public Set<ImplementationLanguageDefinition> getImplementationLanguages() throws CodelingException {
		resolveLanguages();
		final LinkedHashSet<ImplementationLanguageDefinition> result = new LinkedHashSet<>();
		for (final LanguageDefinition l : languages)
			if (l instanceof ImplementationLanguageDefinition)
				result.add((ImplementationLanguageDefinition) l);
		return result;
	}

	/**
	 * Resolves all languages in this registry based on their definitions. Searches
	 * all extensions of the language registry extension point id for languages to
	 * register, and then registers them.
	 *
	 * @return
	 * @throws CodelingException
	 *             when a language definition could not be resolved correctly
	 */
	private Set<LanguageDefinition> resolveLanguages() throws CodelingException {
		final IExtensionRegistry registry = Platform.getExtensionRegistry();
		final IConfigurationElement[] config = registry.getConfigurationElementsFor(EXTENSIONPOINT_ID);

		for (final IConfigurationElement e : config) {
			try {
				LanguageDefinition o = (LanguageDefinition) e.createExecutableExtension("languageDefinitionClass");
				o.configure(e);
				addLanguageDefinition(o);
			} catch (final Exception ex) {
				log.warning(MessageFormat.format(
						"Could not read the attribute languageDefinitionClass from the extension [{0}] of the plugin [{1}].",
						EXTENSIONPOINT_ID, e.getContributor().getName()), ex);
			}
		}
		return languages;
	}

}
