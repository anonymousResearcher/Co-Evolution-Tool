package org.codeling.interprofile.internal;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.osgi.framework.FrameworkUtil;

import de.mkonersmann.il.profiles.Profiles;

public class LanguageMatcher {
	private final List<String> sourceModules, targetModules;

	public LanguageMatcher(List<String> sourceModules, List<String> targetModules) {
		this.sourceModules = sourceModules;
		this.targetModules = targetModules;
	}

	/**
	 * For the set of selected profiles of the source model S and the set of
	 * selected profiles of the target model T, this method returns T union S.
	 */
	public List<String> getCommonProfiles() {
		final List<String> result = new LinkedList<>();
		for (final String sourceURI : sourceModules) {
			if (targetModules.contains(sourceURI))
				result.add(sourceURI);
		}
		return result;
	}

	/**
	 * For the set of selected profiles of the source model S and the set of
	 * selected profiles of the target model T, this method returns T \ S.
	 */
	public List<String> getTargetModulesWithoutSourceModules() {
		final List<String> result = new LinkedList<>();
		for (final String targetURI : targetModules) {
			if (!sourceModules.contains(targetURI))
				result.add(targetURI);
		}
		return result;
	}

	/**
	 * For the set of selected profiles of the source model S and the set of
	 * selected profiles of the target model T, this method returns S \ T.
	 */
	public List<String> getSourceModulesWithoutTargetModules() {
		final List<String> result = new LinkedList<>();
		for (final String sourceURI : sourceModules) {
			if (!targetModules.contains(sourceURI))
				result.add(sourceURI);
		}
		return result;
	}

	private final String TRANSFORMATION_URI_BASE = "/" + FrameworkUtil.getBundle(getClass()).getSymbolicName()
			+ "/src/main/resources/";

	public URI getHenshinTransformationURI(Profiles sourceProfile, Profiles targetProfile) {
		final String sourcePath = sourceProfile.getTransformationSourcePath();
		final String targetPath = targetProfile.getTransformationTargetPath();

		final String transformationFileURI = MessageFormat.format(TRANSFORMATION_URI_BASE + "{0}_to_{1}.henshin",
				sourcePath, targetPath);
		return URI.createPlatformPluginURI(transformationFileURI, true);
	}

	private URI getHenshinTransformationURI(Profiles missingProfile) {
		final String transformationPath = missingProfile.getTransformationSourcePath();
		final String transformationFileURI = MessageFormat
				.format(TRANSFORMATION_URI_BASE + "profileactivation/{0}.henshin", transformationPath);
		return URI.createPlatformPluginURI(transformationFileURI, true);
	}

	public List<URI> getSourceToTargetTransformationURIs() {
		final List<URI> result = new LinkedList<>();

		// Store which profiles have been added for transformation, so that the "profile
		// activation" translations will not be added additionally
		final List<Profiles> possiblyMissingProfiles = new LinkedList<>(Arrays.asList(Profiles.values()));

		// Component Hierarchy
		Profiles sourceProfile = Profiles.COMPONENTS_HIERARCHY_FLAT;
		Profiles[] possibleTargets = new Profiles[] { Profiles.COMPONENTS_HIERARCHY_SCOPED,
				Profiles.COMPONENTS_HIERARCHY_SHAREDCONTEXT };
		if (addTransformationIfApplicable(sourceProfile, possibleTargets, result))
			possiblyMissingProfiles.removeAll(Arrays.asList(possibleTargets));

		sourceProfile = Profiles.COMPONENTS_HIERARCHY_SCOPED;
		possibleTargets = new Profiles[] { Profiles.COMPONENTS_HIERARCHY_FLAT,
				Profiles.COMPONENTS_HIERARCHY_SHAREDCONTEXT };
		if (addTransformationIfApplicable(sourceProfile, possibleTargets, result))
			possiblyMissingProfiles.removeAll(Arrays.asList(possibleTargets));

		sourceProfile = Profiles.COMPONENTS_HIERARCHY_SHAREDCONTEXT;
		possibleTargets = new Profiles[] { Profiles.COMPONENTS_HIERARCHY_SCOPED, Profiles.COMPONENTS_HIERARCHY_FLAT };
		if (addTransformationIfApplicable(sourceProfile, possibleTargets, result))
			possiblyMissingProfiles.removeAll(Arrays.asList(possibleTargets));

		// Interface Hierarchy
		sourceProfile = Profiles.INTERFACES_SCOPED;
		possibleTargets = new Profiles[] { Profiles.INTERFACES_SHARED };
		if (addTransformationIfApplicable(sourceProfile, possibleTargets, result))
			possiblyMissingProfiles.removeAll(Arrays.asList(possibleTargets));

		sourceProfile = Profiles.INTERFACES_SHARED;
		possibleTargets = new Profiles[] { Profiles.INTERFACES_SCOPED };
		if (addTransformationIfApplicable(sourceProfile, possibleTargets, result))
			possiblyMissingProfiles.removeAll(Arrays.asList(possibleTargets));

		// Add transformations for "activating" missing profiles, that e.g. just add
		// "ArchitectureWithDeployments" etc.
		addTransformationsIfSourcesMisses(possiblyMissingProfiles.toArray(new Profiles[0]), result);

		return result;
	}

	private void addTransformationsIfSourcesMisses(Profiles[] profiles, List<URI> result) {
		for (Profiles possiblyMissingProfile : profiles)
			if (targetModules.contains(possiblyMissingProfile.getNsURI())
					&& !sourceModules.contains(possiblyMissingProfile.getNsURI())) {
				result.add(getHenshinTransformationURI(possiblyMissingProfile));
			}
	}

	private boolean addTransformationIfApplicable(Profiles sourceProfile, Profiles[] possibleTargets,
			List<URI> result) {
		boolean changed = false;
		if (sourceModules.contains(sourceProfile.getNsURI()) && !targetModules.contains(sourceProfile.getNsURI())) {
			for (final Profiles possibleTarget : possibleTargets) {
				if (targetModules.contains(possibleTarget.getNsURI())) {
					result.add(getHenshinTransformationURI(sourceProfile, possibleTarget));
					changed = true;
				}
			}
		}
		return changed;
	}
}
