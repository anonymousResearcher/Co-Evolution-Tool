package org.codeling.interprofile.internal;

import java.util.List;

import org.codeling.interprofile.IInterprofileTransformation;
import org.codeling.lang.base.modeltrans.henshin.HenshinTransformation;
import org.codeling.languageregistry.LanguageDefinition;
import org.codeling.transformationmanager.TransformationResult;
import org.codeling.utils.IDRegistry;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;

public class InterProfileTransformation implements IInterprofileTransformation {

	@Override
	public TransformationResult transform(LanguageDefinition sourceLanguage, LanguageDefinition targetLanguage,
			List<EObject> priorModelRoots, IDRegistry priorIdRegistry, String taskName,
			IProgressMonitor progressMonitor) {
		return execute(sourceLanguage, targetLanguage, priorModelRoots, priorIdRegistry, taskName, progressMonitor);
	}

	private TransformationResult execute(LanguageDefinition sourceLanguage, LanguageDefinition targetLanguage,
			List<EObject> priorModelRoots, IDRegistry priorIdRegistry, String taskName,
			IProgressMonitor progressMonitor) {
		SubMonitor monitor = SubMonitor.convert(progressMonitor, 10);

		// 1. Get all transformation URIs for the two involved languages
		monitor.setTaskName("Identifying necessary Inter-Profile Transformations");
		List<String> sourceProfiles, targetProfiles;
		sourceProfiles = loadProfilesOf(sourceLanguage, monitor);
		targetProfiles = loadProfilesOf(targetLanguage, monitor);

		final LanguageMatcher matcher = new LanguageMatcher(sourceProfiles, targetProfiles);
		final List<URI> sourceToTargetTransformationURIs = matcher.getSourceToTargetTransformationURIs();

		// 2. Execute all transformations as HenshinTransformationTasks
		List<EObject> intermediateModelRoots = priorModelRoots;
		IDRegistry intermediateIDRegistry = priorIdRegistry;

		monitor.setWorkRemaining(sourceToTargetTransformationURIs.size());
		for (final URI transformationURI : sourceToTargetTransformationURIs) {
			monitor.setTaskName("Executing " + transformationURI);
			final HenshinTransformation task = new HenshinTransformation(transformationURI, taskName,
					intermediateIDRegistry, intermediateModelRoots, monitor);
			// Execute task
			final TransformationResult intermediateResult = task.execute(monitor);

			intermediateModelRoots = intermediateResult.getModelRoots();
			intermediateIDRegistry = intermediateResult.getIdRegistry();
			monitor.worked(1);
		}

		return new TransformationResult(intermediateModelRoots, intermediateIDRegistry);
	}

	public List<String> loadProfilesOf(LanguageDefinition language, SubMonitor monitor) {
		monitor.setTaskName("Loading profiles used by language [" + language.getName() + "]");
		List<String> profiles = language.getSelectedProfiles();
		monitor.worked(2);
		return profiles;
	}
}
