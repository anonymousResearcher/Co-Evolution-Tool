package org.codeling.transformationmanager.internal;

import java.util.LinkedList;

import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.transformationmanager.internal.steps.ImplementationModelToProgramCodeTask;
import org.codeling.transformationmanager.internal.steps.ImplementationModelToTranslationModelTask;
import org.codeling.transformationmanager.internal.steps.InterProfileTransformationToSpecificationModelTask;
import org.codeling.transformationmanager.internal.steps.ProgramCodeToImplementationModelTask;
import org.codeling.transformationmanager.internal.steps.ProgramCodeToTranslationModelTask;
import org.codeling.transformationmanager.internal.steps.TranslationModelToImplementationModelTask;
import org.codeling.transformationmanager.internal.steps.TranslationModelToProgramCodeTask;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.launching.JavaRuntime;

public class ImplementationMigrationTask extends AbstractTask {
	final LinkedList<IJavaProject> projects;
	final ImplementationLanguageDefinition sourceLanguage;
	final ImplementationLanguageDefinition targetLanguage;

	public ImplementationMigrationTask(ImplementationLanguageDefinition sourceLanguage,
			ImplementationLanguageDefinition targetLanguage, LinkedList<IJavaProject> projects) {
		super("Migrating Code to new Architecture Implementation Language");
		this.sourceLanguage = sourceLanguage;
		this.targetLanguage = targetLanguage;
		this.projects = projects;
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		try {
			monitor.beginTask("Migrating Code to new Architecture Implementation Language", 7);

			final ProgramCodeToImplementationModelTask code2ail = new ProgramCodeToImplementationModelTask(
					sourceLanguage, projects, true, false);
			code2ail.setProgressGroup(monitor, 1);
			code2ail.schedule();
			code2ail.join();

			final ImplementationModelToTranslationModelTask ail2il = new ImplementationModelToTranslationModelTask(
					sourceLanguage, code2ail.getIdRegistry(), code2ail.getResultModelRoots(), true, false);
			ail2il.setProgressGroup(monitor, 1);
			ail2il.schedule();
			ail2il.join();

			final ProgramCodeToTranslationModelTask codeAndAil2il = new ProgramCodeToTranslationModelTask(
					sourceLanguage, projects, ail2il.getIdRegistry(), code2ail.getResultModelRoots(),
					ail2il.getResultModelRoots(), true);
			codeAndAil2il.setProgressGroup(monitor, 1);
			codeAndAil2il.schedule();
			codeAndAil2il.join();

			final InterProfileTransformationToSpecificationModelTask profileTransformation = new InterProfileTransformationToSpecificationModelTask(
					sourceLanguage, targetLanguage, codeAndAil2il.getIdRegistry(), codeAndAil2il.getResultModelRoots(),
					true);
			profileTransformation.setProgressGroup(monitor, 1);
			profileTransformation.schedule();
			profileTransformation.join();

			final TranslationModelToImplementationModelTask il2ail = new TranslationModelToImplementationModelTask(
					targetLanguage, profileTransformation.getIdRegistry(), profileTransformation.getResultModelRoots(),
					true, true);
			il2ail.setProgressGroup(monitor, 1);
			il2ail.schedule();
			il2ail.join();

			IJavaProject targetProject = createJavaProject(
					projects.getFirst().getElementName() + "-in-" + targetLanguage.getSymbolicName());
			LinkedList<IJavaProject> targetProjects = new LinkedList<>();
			targetProjects.add(targetProject);
			ImplementationModelToProgramCodeTask ail2code = new ImplementationModelToProgramCodeTask(targetLanguage, targetProjects,
					true);
			ail2code.setProgressGroup(monitor, 1);
			ail2code.schedule();
			ail2code.join();

			TranslationModelToProgramCodeTask ilAndAil2code = new TranslationModelToProgramCodeTask(targetLanguage,
					projects, profileTransformation.getResultModelRoots());
			ilAndAil2code.setImplementationMigration(true);
			ilAndAil2code.setProgressGroup(monitor, 1);
			ilAndAil2code.schedule();
			ilAndAil2code.join();
			return Status.OK_STATUS;
		} catch (final InterruptedException e) {
			addError("Exception while migrating code to a new architecture implementation language.", e);
			return Status.CANCEL_STATUS;
		} finally {
			monitor.done();
		}
	}

	private IJavaProject createJavaProject(String name) {
		try {
			// create a project
			final IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
			final IProject project = root.getProject(name);
			if (!project.exists())
				project.create(null);
			if (!project.isOpen())
				project.open(null);

			// set the Java nature
			IProjectDescription description = project.getDescription();
			String[] natures = new String[] { JavaCore.NATURE_ID };
			description.setNatureIds(natures);

			// create the project
			project.setDescription(description, null);
			IJavaProject codeElement = JavaCore.create(project);

			// set the build path
			String sourceFolder = "src/main/java";
			IClasspathEntry[] buildPath = new IClasspathEntry[] {
					JavaCore.newSourceEntry(project.getFullPath().append(sourceFolder)),
					JavaRuntime.getDefaultJREContainerEntry() };

			codeElement.setRawClasspath(buildPath, project.getFullPath().append("target/classes"), null);

			// create folder by using the resources package
			String path = "";
			for (String segment : sourceFolder.split("/")) {
				path += segment + "/";
				IFolder folder = project.getFolder(path);
				if (!folder.exists())
					folder.create(true, true, null);
			}
			return codeElement;
		} catch (Exception e) {
			addError("Could not create code fragments.", e);
			return null;
		}
	}

};