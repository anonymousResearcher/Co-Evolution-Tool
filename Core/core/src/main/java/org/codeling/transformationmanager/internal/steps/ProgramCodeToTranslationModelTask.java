package org.codeling.transformationmanager.internal.steps;

import java.util.List;

import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.transformationmanager.TransformationResult;
import org.codeling.transformationmanager.internal.AbstractTask;
import org.codeling.utils.CodelingConfiguration;
import org.codeling.utils.CodelingException;
import org.codeling.utils.IDRegistry;
import org.codeling.utils.Models;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jdt.core.IJavaProject;

public class ProgramCodeToTranslationModelTask extends AbstractTask {
	public static final String TASK_NAME = "Code and Architecture Implementation Model To Intermediate Language Model Transformation";
	public static final String ID_FILEPATH = CodelingConfiguration.DEBUG_MODELDIR_PATH + "3_code&cm2il.ids";
	public static final String MODEL_FILEPATH = CodelingConfiguration.DEBUG_MODELDIR_PATH + "3_code&cm2il.xmi";

	final List<IJavaProject> projects;
	final ImplementationLanguageDefinition implementationLanguage;
	IDRegistry priorIdRegistry;
	List<EObject> componentModelRoots;
	List<EObject> ilModelRoots;
	boolean storeResult = false;
	Resource applicationResource = null;

	public ProgramCodeToTranslationModelTask(ImplementationLanguageDefinition implementationLanguage, List<IJavaProject> projects,
			IDRegistry priorIdRegistry, List<EObject> componentModelRoots, List<EObject> ilModelRoots,
			boolean storeResult) {
		super(TASK_NAME);
		this.projects = projects;
		this.implementationLanguage = implementationLanguage;
		this.priorIdRegistry = priorIdRegistry;
		this.componentModelRoots = componentModelRoots;
		this.ilModelRoots = ilModelRoots;
		this.storeResult = storeResult;
	}

	@Override
	protected IStatus run(IProgressMonitor progress) {
		if (progress == null)
			progress = new NullProgressMonitor();
		progress.beginTask("Executing " + TASK_NAME, 100);

		TransformationResult result = null;
		try {
			result = implementationLanguage.transformCodeToTM(projects, componentModelRoots, ilModelRoots,
					priorIdRegistry);
		} catch (final CodelingException e) {
			addError("Could not transform Code and AIL to IL.", e);
			progress.done();
			return Status.CANCEL_STATUS;
		}

		resultModelRoots = result.getModelRoots();
		idRegistry = result.getIdRegistry();

		if (storeResult) {
			Models.store(resultModelRoots, MODEL_FILEPATH);
			idRegistry.saveToFile(ID_FILEPATH);
		}

		if (executedAsSingleStep)
			progress.done();
		return Status.OK_STATUS;
	}

}