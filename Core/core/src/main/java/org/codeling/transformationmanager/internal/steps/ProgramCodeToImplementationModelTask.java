package org.codeling.transformationmanager.internal.steps;

import java.util.List;

import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.transformationmanager.TransformationResult;
import org.codeling.transformationmanager.internal.AbstractTask;
import org.codeling.utils.CodelingConfiguration;
import org.codeling.utils.IDRegistry;
import org.codeling.utils.Models;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

public class ProgramCodeToImplementationModelTask extends AbstractTask {

	public static final String TASK_NAME = "Extracting Architecture Implementation Model from Code";
	public static final String ID_FILEPATH = CodelingConfiguration.DEBUG_MODELDIR_PATH + "1_code2cm.ids";
	public static final String MODEL_FILEPATH = CodelingConfiguration.DEBUG_MODELDIR_PATH + "1_code2cm.xmi";
	public static final String MODEL_FILEPATH_RESULT = CodelingConfiguration.TEMPORARY_MODELDIR_PATH
			+ "implementation-model.xmi";

	final List<IJavaProject> projects;
	final ImplementationLanguageDefinition implementationLanguage;
	boolean storeResult;
	private boolean translateToImplementationModelOnly;

	public ProgramCodeToImplementationModelTask(ImplementationLanguageDefinition implementationLanguage, List<IJavaProject> projects,
			boolean storeResult, boolean translateToImplementationModelOnly) {
		super(TASK_NAME);
		this.projects = projects;
		this.implementationLanguage = implementationLanguage;
		this.storeResult = storeResult;
		this.translateToImplementationModelOnly = translateToImplementationModelOnly;
	}

	@Override
	public IStatus run(IProgressMonitor progressMonitor) {
		progressMonitor.beginTask(TASK_NAME, 100);
		idRegistry = new IDRegistry();

		try {
			final TransformationResult result = implementationLanguage.transformCodeToIM(projects, idRegistry);
			progressMonitor.worked(80);

			resultModelRoots = result.getModelRoots();
			idRegistry = result.getIdRegistry();

			if (storeResult) {
				Models.store(resultModelRoots,
						translateToImplementationModelOnly ? MODEL_FILEPATH_RESULT : MODEL_FILEPATH);
				idRegistry.saveToFile(ID_FILEPATH);
			}
			progressMonitor.worked(20);
		} catch (final Throwable e) {
			final String message = "Error while executing " + TASK_NAME;
			addError(message, e);
			if (executedAsSingleStep)
				progressMonitor.done();
		} finally {
			if (!isOK()) {
				Display.getDefault().asyncExec(() -> {
					int maxErrorKind = IStatus.OK;
					if (hasErrors())
						maxErrorKind = IStatus.ERROR;
					else if (hasWarnings())
						maxErrorKind = IStatus.WARNING;
					else if (hasInfos())
						maxErrorKind = IStatus.INFO;

					final Shell activeShell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
					MessageDialog.open(maxErrorKind, activeShell, "Issues raised during translation",
							"There were issues during the translation. Please take a look at the Eclipse Error View.",
							SWT.NONE);

				});
			}
		}
		if (executedAsSingleStep)
			progressMonitor.done();
		return Status.OK_STATUS;
	}
};