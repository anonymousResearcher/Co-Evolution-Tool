package org.codeling.transformationmanager.internal.steps;

import java.util.HashMap;
import java.util.List;

import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.transformationmanager.internal.AbstractTask;
import org.codeling.utils.CodelingConfiguration;
import org.codeling.utils.IDRegistry;
import org.codeling.utils.Models;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaProject;

public class ImplementationModelToProgramCodeTask extends AbstractTask {

	public static final String TASK_NAME = "Integrating changed Architecture Implementation Model with Code";
	public static final String CHANGED_ID_FILEPATH = CodelingConfiguration.DEBUG_MODELDIR_PATH
			+ "1_code2cm_changed.ids";
	public static final String CHANGED_MODEL_FILEPATH = CodelingConfiguration.DEBUG_MODELDIR_PATH
			+ "1_code2cm_changed.xmi";
	public static final String ID_FILEPATH = CodelingConfiguration.DEBUG_MODELDIR_PATH + "9_cm2code.ids";

	final List<IJavaProject> projects;
	final ImplementationLanguageDefinition implementationLanguage;
	private boolean isImplementationMigration;

	public ImplementationModelToProgramCodeTask(ImplementationLanguageDefinition implementationLanguage,
			List<IJavaProject> projects, boolean isImplementationMigration) {
		super(TASK_NAME);
		this.projects = projects;
		this.implementationLanguage = implementationLanguage;
		this.isImplementationMigration = isImplementationMigration;
	}

	@Override
	protected IStatus run(IProgressMonitor progressMonitor) {
		progressMonitor.beginTask("Executing " + TASK_NAME, 100);
		try {
			// Load old IDs
			final IDRegistry idregistry_oldModel = IDRegistry.load(ProgramCodeToImplementationModelTask.ID_FILEPATH);
			progressMonitor.worked(5);

			// Load old model (must be null during implementation migration)
			List<EObject> oldModelRoots = null;
			if (!isImplementationMigration)
				oldModelRoots = Models.loadFrom(ProgramCodeToImplementationModelTask.MODEL_FILEPATH);
			progressMonitor.worked(5);

			// Load new IDs
			IDRegistry idregistry_newModel = IDRegistry.load(TranslationModelToImplementationModelTask.ID_FILEPATH);
			if (idregistry_newModel == null)
				idregistry_newModel = IDRegistry.load(ProgramCodeToImplementationModelTask.ID_FILEPATH);
			progressMonitor.worked(5);

			// Load new model
			List<EObject> newModelRoots = Models.loadFrom(TranslationModelToImplementationModelTask.MODEL_FILEPATH);
			if (newModelRoots == null)
				newModelRoots = Models.loadFrom(ProgramCodeToImplementationModelTask.MODEL_FILEPATH);
			progressMonitor.worked(5);

			// When an implementation migration is executed, the code element and the cm
			// element should not be found by the translations
			if (isImplementationMigration) {
				removeCodeAndCMElementEntries(idregistry_oldModel);
				removeCodeElementEntries(idregistry_newModel);
			}
			implementationLanguage.transformIMToCode(newModelRoots, oldModelRoots, idregistry_newModel,
					idregistry_oldModel, projects);
			progressMonitor.worked(80);

			// New code elements should be propagated to the ID registry.
			idregistry_newModel.saveToFile(ID_FILEPATH);
		} catch (final Throwable e) {
			e.printStackTrace();
			progressMonitor.done();
			return Status.CANCEL_STATUS;
		}
		issues.addInfo("Updated code elements.");

		if (executedAsSingleStep)
			progressMonitor.done();
		return Status.OK_STATUS;
	}

	private void removeCodeAndCMElementEntries(final IDRegistry idregistry) {
		HashMap<String, String[]> registryEntries = idregistry.getRegistryEntries();
		for (String e : registryEntries.keySet()) {
			idregistry.updateCodeElement(e, null);
			idregistry.updateImplementationModelElement(e, null);
		}
	}

	private void removeCodeElementEntries(final IDRegistry idregistry) {
		HashMap<String, String[]> registryEntries = idregistry.getRegistryEntries();
		for (String e : registryEntries.keySet()) {
			idregistry.updateCodeElement(e, null);
		}
	}

};