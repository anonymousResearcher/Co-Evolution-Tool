package org.codeling.transformationmanager;

import java.util.LinkedList;
import java.util.List;

import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.languageregistry.SpecificationLanguageDefinition;
import org.codeling.transformationmanager.internal.TransformationManager;
import org.codeling.utils.CodelingException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaProject;

/**
 * The {@link ITransformationManager} as a component abstractly handles the
 * transformation between a model and the corresponding code with possibly a set
 * of in-between transformations. An example root transformation is the class
 * EnterpriseArchive2JavaProjectsTransformation in the project ejb.embed.
 *
 * @author Anonymous Researcher <anonymous@example.org>
 */
public interface ITransformationManager {

	/**
	 * Parses the given java projects and stores the resulting model in a list of
	 * model elements. This operation can be long-running.
	 *
	 * @throws CodelingException
	 */
	public List<EObject> extractModelFromCode(ImplementationLanguageDefinition implementationLanguage,
			SpecificationLanguageDefinition modelingLanguage, IProgressMonitor progressMonitor,
			List<IJavaProject> projects) throws CodelingException;

	/**
	 * Migrates program code to a new architecture implementation language.
	 * 
	 * @param sourceLanguage
	 * @param targetLanguage
	 * @param progressMonitor
	 * @param projects
	 */
	public void migrateImplementation(ImplementationLanguageDefinition sourceLanguage,
			ImplementationLanguageDefinition targetLanguage, IProgressMonitor progressMonitor,
			LinkedList<IJavaProject> projects) throws CodelingException;

	/**
	 * Updates the underlying code base with the information of an updated model.
	 * This operation can be long-running.
	 *
	 * @throws CodelingException
	 */
	public void storeModelToCode(List<EObject> updatedModel, ImplementationLanguageDefinition implementationLanguage,
			SpecificationLanguageDefinition modelingLanguage, IProgressMonitor progressMonitor,
			List<IJavaProject> projects) throws CodelingException;

	public static ITransformationManager createInstance() {
		return new TransformationManager();
	}

}
