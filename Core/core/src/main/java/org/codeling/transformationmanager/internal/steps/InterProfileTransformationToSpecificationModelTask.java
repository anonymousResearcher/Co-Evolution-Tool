package org.codeling.transformationmanager.internal.steps;

import java.util.List;

import org.codeling.interprofile.IInterprofileTransformation;
import org.codeling.languageregistry.LanguageDefinition;
import org.codeling.transformationmanager.TransformationResult;
import org.codeling.transformationmanager.internal.AbstractTask;
import org.codeling.utils.CodelingConfiguration;
import org.codeling.utils.IDRegistry;
import org.codeling.utils.Models;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EObject;

public class InterProfileTransformationToSpecificationModelTask extends AbstractTask {
	public static final String TASK_NAME = "Intermediate Language Module Transformations";
	public static final String ID_FILEPATH = CodelingConfiguration.DEBUG_MODELDIR_PATH
			+ "4_ilModuleTransformations.ids";
	public static final String MODEL_FILEPATH = CodelingConfiguration.DEBUG_MODELDIR_PATH
			+ "4_ilModuleTransformations.xmi";

	final LanguageDefinition sourceLanguage;
	final LanguageDefinition targetLanguage;
	final IDRegistry priorIdRegistry;
	final List<EObject> priorModelRoots;
	final boolean storeResult;

	public InterProfileTransformationToSpecificationModelTask(LanguageDefinition sourceLanguage,
			LanguageDefinition targetLanguage, IDRegistry priorIdRegistry, List<EObject> priorModelRoots,
			boolean storeResult) {
		super(TASK_NAME);
		this.sourceLanguage = sourceLanguage;
		this.targetLanguage = targetLanguage;
		this.priorIdRegistry = priorIdRegistry;
		this.priorModelRoots = priorModelRoots;
		this.storeResult = storeResult;
	}

	@Override
	protected IStatus run(IProgressMonitor progressMonitor) {
		progressMonitor.subTask("Executing " + TASK_NAME);

		final IInterprofileTransformation t = IInterprofileTransformation.createInstance();
		final TransformationResult result = t.transform(sourceLanguage, targetLanguage, priorModelRoots,
				priorIdRegistry, TASK_NAME, progressMonitor);

		resultModelRoots = result.getModelRoots();
		idRegistry = result.getIdRegistry();

		if (storeResult) {
			Models.store(resultModelRoots, MODEL_FILEPATH);
			idRegistry.saveToFile(ID_FILEPATH);
		}

		if (executedAsSingleStep)
			progressMonitor.done();
		return Status.OK_STATUS;
	}

};