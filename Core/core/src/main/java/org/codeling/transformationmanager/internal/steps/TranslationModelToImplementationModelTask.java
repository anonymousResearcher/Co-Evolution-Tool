
package org.codeling.transformationmanager.internal.steps;

import java.util.List;

import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.transformationmanager.TransformationResult;
import org.codeling.transformationmanager.internal.AbstractTask;
import org.codeling.utils.CodelingConfiguration;
import org.codeling.utils.CodelingException;
import org.codeling.utils.IDRegistry;
import org.codeling.utils.Models;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EObject;

public class TranslationModelToImplementationModelTask extends AbstractTask {

	public static final String TASK_NAME = "Intermediate Language Model to Architecture Implementation Model Transformation";
	public static final String ID_FILEPATH = CodelingConfiguration.DEBUG_MODELDIR_PATH + "8_il2cm.ids";
	public static final String MODEL_FILEPATH = CodelingConfiguration.DEBUG_MODELDIR_PATH + "8_il2cm.xmi";

	final ImplementationLanguageDefinition implementationLanguage;
	boolean storeResult = false;
	boolean isImplementationMigration;
	private IDRegistry priorIdRegistry;
	private List<EObject> priorModelRoots;

	public TranslationModelToImplementationModelTask(ImplementationLanguageDefinition implementationLanguage,
			IDRegistry priorIdRegistry, List<EObject> priorModelRoots, boolean storeResult,
			boolean isImplementationMigration) {
		super(TASK_NAME);
		this.implementationLanguage = implementationLanguage;
		this.priorIdRegistry = priorIdRegistry;
		this.priorModelRoots = priorModelRoots;
		this.storeResult = storeResult;
		this.isImplementationMigration = isImplementationMigration;
	}

	@Override
	protected IStatus run(IProgressMonitor progressMonitor) {
		progressMonitor.subTask("Executing " + TASK_NAME);

		// Execute task
		TransformationResult result;
		try {
			result = implementationLanguage.transformTMToIM(priorModelRoots, priorIdRegistry);
		} catch (final CodelingException e) {
			addError("Could not transform IL model to AIL model", e);
			progressMonitor.done();
			return Status.CANCEL_STATUS;
		}

		resultModelRoots = result.getModelRoots();
		idRegistry = result.getIdRegistry();

		if (storeResult) {
			Models.store(resultModelRoots, MODEL_FILEPATH);
			idRegistry.saveToFile(ID_FILEPATH);
		}

		progressMonitor.worked(10);
		if (executedAsSingleStep)
			progressMonitor.done();
		return Status.OK_STATUS;
	}

};