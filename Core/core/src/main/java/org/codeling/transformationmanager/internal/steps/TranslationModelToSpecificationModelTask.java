package org.codeling.transformationmanager.internal.steps;

import java.util.List;

import org.codeling.languageregistry.SpecificationLanguageDefinition;
import org.codeling.transformationmanager.TransformationResult;
import org.codeling.transformationmanager.internal.AbstractTask;
import org.codeling.utils.CodelingConfiguration;
import org.codeling.utils.IDRegistry;
import org.codeling.utils.Models;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.ecore.EObject;

public class TranslationModelToSpecificationModelTask extends AbstractTask {

	public static final String TASK_NAME = "Intermediate Language Model to Architecture Description Language Model Transformation";
	public static final String ID_FILEPATH = CodelingConfiguration.DEBUG_MODELDIR_PATH + "5_il2adl.ids";
	public static final String MODEL_FILEPATH = CodelingConfiguration.TEMPORARY_MODELDIR_PATH
			+ "specification-model.xmi";

	final SpecificationLanguageDefinition modelingLanguage;
	IDRegistry priorIdRegistry;
	List<EObject> priorModelRoots;
	boolean storeResult = false;

	public TranslationModelToSpecificationModelTask(SpecificationLanguageDefinition modelingLanguage, IDRegistry priorIdRegistry,
			List<EObject> priorModelRoots, boolean storeResult) {
		super(TASK_NAME);
		this.modelingLanguage = modelingLanguage;
		this.priorIdRegistry = priorIdRegistry;
		this.priorModelRoots = priorModelRoots;
		this.storeResult = storeResult;
	}

	@Override
	protected IStatus run(IProgressMonitor progress) {
		final SubMonitor progressMonitor = SubMonitor.convert(progress, 100);
		progressMonitor.subTask("Executing " + TASK_NAME);

		progress.subTask(String.format("Creating TGG Task"));
		final TransformationResult result = modelingLanguage.transformToSM(priorModelRoots, priorIdRegistry,
				progressMonitor);

		resultModelRoots = result.getModelRoots();
		idRegistry = result.getIdRegistry();

		if (storeResult) {
			Models.store(resultModelRoots, MODEL_FILEPATH);
			idRegistry.saveToFile(ID_FILEPATH);
		}
		// Call the transformation completed hook for the modeling
		// language
		progress.subTask("Executing post processing");
		modelingLanguage.transformationCompleteHook(result.getModelRoots(), progressMonitor);

		if (executedAsSingleStep)
			progressMonitor.done();
		return Status.OK_STATUS;
	}

};