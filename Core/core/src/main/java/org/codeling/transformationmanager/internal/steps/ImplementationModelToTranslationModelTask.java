package org.codeling.transformationmanager.internal.steps;

import java.util.List;

import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.transformationmanager.TransformationResult;
import org.codeling.transformationmanager.internal.AbstractTask;
import org.codeling.utils.CodelingConfiguration;
import org.codeling.utils.CodelingException;
import org.codeling.utils.IDRegistry;
import org.codeling.utils.Models;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.ecore.EObject;

public class ImplementationModelToTranslationModelTask extends AbstractTask {

	public static final String TASK_NAME = "Architecture Implementation Model To Intermediate Language Model Transformation";
	public static final String ID_FILEPATH = CodelingConfiguration.DEBUG_MODELDIR_PATH + "2_cm2il.ids";
	public static final String MODEL_FILEPATH = CodelingConfiguration.DEBUG_MODELDIR_PATH + "2_cm2il.xmi";

	final ImplementationLanguageDefinition implementationLanguage;
	IDRegistry priorIdRegistry;
	List<EObject> priorModelRoots;
	boolean storeResult = false;

	public ImplementationModelToTranslationModelTask(ImplementationLanguageDefinition implementationLanguage, IDRegistry priorIdRegistry,
			List<EObject> priorModelRoots, boolean storeResult, boolean executedAsSingleStep) {
		super(TASK_NAME);
		this.implementationLanguage = implementationLanguage;
		this.priorIdRegistry = priorIdRegistry;
		this.priorModelRoots = priorModelRoots;
		this.storeResult = storeResult;
		this.executedAsSingleStep = executedAsSingleStep;
	}

	@Override
	protected IStatus run(IProgressMonitor progressMonitor) {
		final SubMonitor progress = SubMonitor.convert(progressMonitor, 100);
		progress.beginTask("Executing " + TASK_NAME, 100);

		TransformationResult result = null;
		try {
			result = implementationLanguage.transformIMToTM(priorModelRoots, priorIdRegistry);
		} catch (final CodelingException e) {
			addError("Could not transform AIL to IL.", e);
			progressMonitor.done();
			return Status.CANCEL_STATUS;
		}

		resultModelRoots = result.getModelRoots();
		idRegistry = result.getIdRegistry();

		if (storeResult) {
			Models.store(resultModelRoots, MODEL_FILEPATH);
			idRegistry.saveToFile(ID_FILEPATH);
		}

		if (executedAsSingleStep)
			progressMonitor.done();
		return Status.OK_STATUS;
	}

}