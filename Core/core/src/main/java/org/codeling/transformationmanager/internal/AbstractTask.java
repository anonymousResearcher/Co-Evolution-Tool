package org.codeling.transformationmanager.internal;

import java.util.LinkedList;
import java.util.List;

import org.codeling.utils.CodelingLogger;
import org.codeling.utils.IDRegistry;
import org.codeling.utils.errorcontainers.IMayHaveIssues;
import org.codeling.utils.errorcontainers.Issue;
import org.codeling.utils.errorcontainers.Level;
import org.codeling.utils.errorcontainers.MayHaveIssues;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.EObject;

public abstract class AbstractTask extends Job implements IMayHaveIssues {

	protected CodelingLogger log = new CodelingLogger(getClass());
	protected IDRegistry idRegistry;
	protected List<EObject> resultModelRoots;
	protected boolean executedAsSingleStep = false;

	public AbstractTask(String name) {
		super(name);
	}

	public IDRegistry getIdRegistry() {
		return idRegistry;
	}

	public List<EObject> getResultModelRoots() {
		return resultModelRoots;
	}

	// Methods from IMayHaveIssues.
		protected MayHaveIssues issues = new MayHaveIssues(this.getClass());
		private final List<IMayHaveIssues> childIssues = new LinkedList<IMayHaveIssues>();

		protected void addChildIssue(IMayHaveIssues child) {
			childIssues.add(child);
		}

		@Override
		public boolean isOK() {
			return issues.isOK() || childIssues.stream().allMatch(

					x -> x.isOK());

		}

		@Override
		public boolean hasErrorsOrWarnings() {
			return issues.hasErrorsOrWarnings() || childIssues.stream().allMatch(

					x -> x.hasErrorsOrWarnings());

		}

		@Override
		public boolean hasErrors() {
			return issues.hasErrors() || childIssues.stream().allMatch(

					x -> x.hasErrors());

		}

		@Override
		public boolean hasWarnings() {
			return issues.hasWarnings() || childIssues.stream().allMatch(

					x -> x.hasWarnings());

		}

		@Override
		public boolean hasInfos() {
			return issues.hasInfos() || childIssues.stream().allMatch(

					x -> x.hasInfos());

		}

		@Override
		public List<Issue> issues() {
			final List<Issue> result = issues.issues();
			childIssues.forEach(

					x -> result.addAll(x.issues()));
			return result;

		}

		@Override
		public List<Issue> errorsAndWarnings() {
			final List<Issue> result = issues.errorsAndWarnings();
			childIssues.forEach(

					x -> result.addAll(x.errorsAndWarnings()));
			return result;

		}

		@Override
		public List<Issue> errors() {
			final List<Issue> result = issues.errors();
			childIssues.forEach(

					x -> result.addAll(x.errors()));
			return result;

		}

		@Override
		public List<Issue> warnings() {
			final List<Issue> result = issues.warnings();
			childIssues.forEach(

					x -> result.addAll(x.warnings()));
			return result;

		}

		@Override
		public List<Issue> infos() {
			final List<Issue> result = issues.infos();
			childIssues.forEach(

					x -> result.addAll(x.infos()));
			return result;

		}

		@Override
		public void addIssue(Level level, String message, Throwable e) {
			issues.addIssue(level, message, e);
		}

		@Override
		public void addIssue(Level level, String message) {
			issues.addIssue(level, message);
		}

		@Override
		public void addWarning(String message, Throwable e) {
			issues.addWarning(message, e);
		}

		@Override
		public void addWarning(String message) {
			issues.addWarning(message);
		}

		@Override
		public void addError(String message, Throwable e) {
			issues.addError(message, e);
		}

		@Override
		public void addError(String message) {
			issues.addError(message);
		}

		@Override
		public void addInfo(String message, Throwable e) {
			issues.addInfo(message, e);
		}

		@Override
		public void addInfo(String message) {
			issues.addInfo(message);
		}

		@Override
		public Class<?> getOwningClass() {
			return issues.getOwningClass();
		}
}
