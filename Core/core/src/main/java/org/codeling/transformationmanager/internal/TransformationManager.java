package org.codeling.transformationmanager.internal;

import java.util.LinkedList;
import java.util.List;

import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.languageregistry.SpecificationLanguageDefinition;
import org.codeling.transformationmanager.ITransformationManager;
import org.codeling.utils.CodelingException;
import org.codeling.utils.errorcontainers.MayHaveIssues;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaProject;

public class TransformationManager extends MayHaveIssues implements ITransformationManager {

	@Override
	public List<EObject> extractModelFromCode(ImplementationLanguageDefinition implementationLanguage,
			SpecificationLanguageDefinition modelingLanguage, IProgressMonitor progressMonitor,
			List<IJavaProject> projects) throws CodelingException {
		ModelExtractionTask task = new ModelExtractionTask(implementationLanguage, modelingLanguage, projects);
		task.setProgressGroup(progressMonitor == null ? new NullProgressMonitor() : progressMonitor, 1);
		task.schedule();
		try {
			task.join();
		} catch (InterruptedException e) {
			throw new CodelingException("Error while extracting a model from program code", e);
		}
		return task.getResultModelRoots();
	}

	@Override
	public void storeModelToCode(List<EObject> updatedModel, ImplementationLanguageDefinition implementationLanguage,
			SpecificationLanguageDefinition specificationLanguage, IProgressMonitor progressMonitor,
			List<IJavaProject> projects) throws CodelingException {
		ModelIntegrationTask task = new ModelIntegrationTask(updatedModel, implementationLanguage,
				specificationLanguage, projects);
		task.setProgressGroup(progressMonitor == null ? new NullProgressMonitor() : progressMonitor, 1);
		task.schedule();
		try {
			task.join();
		} catch (InterruptedException e) {
			throw new CodelingException("Error while propagating changes in the model to the program code", e);
		}
	}

	@Override
	public void migrateImplementation(ImplementationLanguageDefinition sourceLanguage,
			ImplementationLanguageDefinition targetLanguage, IProgressMonitor progressMonitor,
			LinkedList<IJavaProject> projects) throws CodelingException {
		ImplementationMigrationTask task = new ImplementationMigrationTask(sourceLanguage, targetLanguage, projects);
		task.setProgressGroup(progressMonitor == null ? new NullProgressMonitor() : progressMonitor, 1);
		task.schedule();
		try {
			task.join();
		} catch (InterruptedException e) {
			throw new CodelingException(
					"Error while migrating program code to another architecture implementation language", e);
		}
	}

}
