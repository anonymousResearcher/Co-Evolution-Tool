/**
 */
package de.mkonersmann.il.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Provision</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.mkonersmann.il.core.Provision#getInterface <em>Interface</em>}</li>
 * </ul>
 *
 * @see de.mkonersmann.il.core.CorePackage#getProvision()
 * @model
 * @generated
 */
public interface Provision extends IdentifiedElement {
	/**
	 * Returns the value of the '<em><b>Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface</em>' reference.
	 * @see #setInterface(Interface)
	 * @see de.mkonersmann.il.core.CorePackage#getProvision_Interface()
	 * @model required="true"
	 * @generated
	 */
	Interface getInterface();

	/**
	 * Sets the value of the '{@link de.mkonersmann.il.core.Provision#getInterface <em>Interface</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interface</em>' reference.
	 * @see #getInterface()
	 * @generated
	 */
	void setInterface(Interface value);

} // Provision
