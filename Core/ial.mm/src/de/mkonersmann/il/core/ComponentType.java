/**
 */
package de.mkonersmann.il.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.mkonersmann.il.core.ComponentType#getRequiredInterfaces <em>Required Interfaces</em>}</li>
 *   <li>{@link de.mkonersmann.il.core.ComponentType#getProvidedInterfaces <em>Provided Interfaces</em>}</li>
 *   <li>{@link de.mkonersmann.il.core.ComponentType#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see de.mkonersmann.il.core.CorePackage#getComponentType()
 * @model
 * @generated
 */
public interface ComponentType extends IdentifiedElement {
	/**
	 * Returns the value of the '<em><b>Required Interfaces</b></em>' containment reference list.
	 * The list contents are of type {@link de.mkonersmann.il.core.Requirement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Interfaces</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Interfaces</em>' containment reference list.
	 * @see de.mkonersmann.il.core.CorePackage#getComponentType_RequiredInterfaces()
	 * @model containment="true"
	 * @generated
	 */
	EList<Requirement> getRequiredInterfaces();

	/**
	 * Returns the value of the '<em><b>Provided Interfaces</b></em>' containment reference list.
	 * The list contents are of type {@link de.mkonersmann.il.core.Provision}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provided Interfaces</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provided Interfaces</em>' containment reference list.
	 * @see de.mkonersmann.il.core.CorePackage#getComponentType_ProvidedInterfaces()
	 * @model containment="true"
	 * @generated
	 */
	EList<Provision> getProvidedInterfaces();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see de.mkonersmann.il.core.CorePackage#getComponentType_Name()
	 * @model dataType="de.mkonersmann.il.core.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link de.mkonersmann.il.core.ComponentType#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // ComponentType
