/**
 */
package de.mkonersmann.il.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.mkonersmann.il.core.ComponentInstance#getRequirements <em>Requirements</em>}</li>
 *   <li>{@link de.mkonersmann.il.core.ComponentInstance#getProvisions <em>Provisions</em>}</li>
 *   <li>{@link de.mkonersmann.il.core.ComponentInstance#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see de.mkonersmann.il.core.CorePackage#getComponentInstance()
 * @model
 * @generated
 */
public interface ComponentInstance extends IdentifiedElement {
	/**
	 * Returns the value of the '<em><b>Requirements</b></em>' containment reference list.
	 * The list contents are of type {@link de.mkonersmann.il.core.RequirementInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requirements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requirements</em>' containment reference list.
	 * @see de.mkonersmann.il.core.CorePackage#getComponentInstance_Requirements()
	 * @model containment="true"
	 * @generated
	 */
	EList<RequirementInstance> getRequirements();

	/**
	 * Returns the value of the '<em><b>Provisions</b></em>' containment reference list.
	 * The list contents are of type {@link de.mkonersmann.il.core.ProvisionInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provisions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provisions</em>' containment reference list.
	 * @see de.mkonersmann.il.core.CorePackage#getComponentInstance_Provisions()
	 * @model containment="true"
	 * @generated
	 */
	EList<ProvisionInstance> getProvisions();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(ComponentType)
	 * @see de.mkonersmann.il.core.CorePackage#getComponentInstance_Type()
	 * @model required="true"
	 * @generated
	 */
	ComponentType getType();

	/**
	 * Sets the value of the '{@link de.mkonersmann.il.core.ComponentInstance#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(ComponentType value);

} // ComponentInstance
