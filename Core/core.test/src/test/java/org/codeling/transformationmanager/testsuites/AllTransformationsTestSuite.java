package org.codeling.transformationmanager.testsuites;

import org.codeling.transformationmanager.internal.ModelExtractionHandlerTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ModelExtractionHandlerTest.class, TransformationStepsTestSuite.class })
public class AllTransformationsTestSuite {

}
