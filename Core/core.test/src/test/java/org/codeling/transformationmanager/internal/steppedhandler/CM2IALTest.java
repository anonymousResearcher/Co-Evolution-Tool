package org.codeling.transformationmanager.internal.steppedhandler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Set;

import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.languageregistry.internal.LanguageRegistry;
import org.codeling.test.ecore.CompareEcoreModels;
import org.codeling.transformationmanager.internal.AbstractTransformationTaskTest;
import org.codeling.transformationmanager.internal.steps.ImplementationModelToTranslationModelTask;
import org.codeling.utils.CodelingException;
import org.codeling.utils.IDRegistry;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.junit.Test;

public class CM2IALTest extends AbstractTransformationTaskTest {

	@Test
	public void codeAndAIL2ILTest() throws Exception {
		final List<EObject> componentModel = loadComponentModel();

		final List<EObject> actualRoots = executeCode2ModelTransformations(componentModel);

		assertNotNull(actualRoots);

		assertEquals(16, actualRoots.size());
		// Evaluate the size of the object tree
		int treeSize = 0;
		for (final EObject root : actualRoots) {
			treeSize++; // Include the actual object
			final TreeIterator<EObject> it = root.eAllContents();
			while (it.hasNext()) {
				treeSize++;
				it.next();
			}
		}
		assertEquals(32, treeSize);

		final List<EObject> expectedRoots = loadExpectedModel();
		new CompareEcoreModels().ignoringIds().compare(expectedRoots, actualRoots);

	}

	private List<EObject> executeCode2ModelTransformations(List<EObject> componentModel)
			throws InterruptedException, CodelingException {
		ImplementationLanguageDefinition implementationLanguage = null;
		final Set<ImplementationLanguageDefinition> progLang = LanguageRegistry.getInstance().getImplementationLanguages();
		for (final ImplementationLanguageDefinition ld : progLang) {
			if (ld.getName().equals("Enterprise JavaBeans")) {
				implementationLanguage = ld;
				break;
			}
		}

		final ImplementationModelToTranslationModelTask task = new ImplementationModelToTranslationModelTask(implementationLanguage, new IDRegistry(), componentModel,
				false, true);
		task.schedule();
		task.join();

		assertTrue("Errors during translation. See error log / console output for details.", task.isOK());
		return task.getResultModelRoots();
	}

	private List<EObject> loadExpectedModel() {
		return loadModel(URI.createPlatformPluginURI(BUNDLE_NAME + "/src/test/resources/expected/2_cm2il.xmi", true));
	}

	private List<EObject> loadComponentModel() {
		return loadModel(URI.createPlatformPluginURI(BUNDLE_NAME + "/src/test/resources/expected/1_code2cm.xmi", true));
	}

}
