package org.codeling.transformationmanager.internal.steppedhandler;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.languageregistry.internal.LanguageRegistry;
import org.codeling.test.ecore.CompareEcoreModels;
import org.codeling.transformationmanager.internal.AbstractTransformationTaskTest;
import org.codeling.transformationmanager.internal.steps.ProgramCodeToTranslationModelTask;
import org.codeling.utils.CodelingException;
import org.codeling.utils.IDRegistry;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaProject;
import org.junit.Test;

public class CodeAndAIL2ILTest extends AbstractTransformationTaskTest {

	@Test
	public void codeAndAIL2ILTest() throws Exception {
		final IJavaProject project = setupJavaProject();
		final List<EObject> componentModel = loadComponentModel();
		final List<EObject> ilModel = loadILModel();
		final IDRegistry idRegistry = loadIDRegisty();

		final List<EObject> actualRoots = executeCode2ModelTransformations(project, componentModel, ilModel,
				idRegistry);

		final List<EObject> expectedRoots = loadExpectedModel();
		new CompareEcoreModels().ignoringIds().compare(expectedRoots, actualRoots);
	}

	private List<EObject> executeCode2ModelTransformations(IJavaProject project, List<EObject> componentModel,
			List<EObject> ilModel, IDRegistry idRegistry) throws InterruptedException, IOException, CodelingException {
		ImplementationLanguageDefinition implementationLanguage = null;
		final Set<ImplementationLanguageDefinition> progLang = LanguageRegistry.getInstance().getImplementationLanguages();
		for (final ImplementationLanguageDefinition ld : progLang) {
			if (ld.getName().equals("Enterprise JavaBeans")) {
				implementationLanguage = ld;
				break;
			}
		}

		final List<IJavaProject> projects = new LinkedList<>();
		projects.add(project);

		final ProgramCodeToTranslationModelTask task = new ProgramCodeToTranslationModelTask(implementationLanguage, projects, idRegistry,
				componentModel, ilModel, true);
		task.schedule();
		task.join();

		assertTrue("Errors during translation. See error log / console output for details.", task.isOK());
		return task.getResultModelRoots();
	}

	private List<EObject> loadExpectedModel() {
		return loadModel(URI.createPlatformPluginURI(
				BUNDLE_NAME+"/src/test/resources/expected/3_code&cm2il.xmi", true));
	}

	private List<EObject> loadILModel() {
		return loadModel(URI.createPlatformPluginURI(
				BUNDLE_NAME+"/src/test/resources/expected/2_cm2il.xmi", true));
	}

	private List<EObject> loadComponentModel() {
		return loadModel(URI.createPlatformPluginURI(
				BUNDLE_NAME + "/src/test/resources/expected/1_code2cm.xmi", true));
	}

	private IDRegistry loadIDRegisty() throws IOException {
		return IDRegistry.load(
				new URL("platform:/plugin/org.codeling.transformationmanager.test/src/test/resources/expected/2_cm2il.ids"));

	}

}
