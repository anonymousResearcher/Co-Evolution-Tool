package org.codeling.transformationmanager.internal.steppedhandler;

import static org.junit.Assert.assertTrue;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.languageregistry.internal.LanguageRegistry;
import org.codeling.test.ecore.CompareEcoreModels;
import org.codeling.transformationmanager.internal.AbstractTransformationTaskTest;
import org.codeling.transformationmanager.internal.steps.ProgramCodeToImplementationModelTask;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaProject;
import org.junit.Test;

public class Code2AILTest extends AbstractTransformationTaskTest {

	@Test
	public void code2ailTest() throws Exception {
		final IJavaProject project = setupJavaProject();
		final List<EObject> actualRoots = executeCode2ModelTransformations(project);

		final List<EObject> expectedRoots = loadExpectedModel();
		new CompareEcoreModels().ignoringIds().compare(expectedRoots, actualRoots);
	}

	private List<EObject> executeCode2ModelTransformations(IJavaProject project)
			throws InterruptedException, CodelingException {
		ImplementationLanguageDefinition implementationLanguage = null;
		final Set<ImplementationLanguageDefinition> progLang = LanguageRegistry.getInstance().getImplementationLanguages();
		for (final ImplementationLanguageDefinition ld : progLang) {
			if (ld.getName().equals("Enterprise JavaBeans")) {
				implementationLanguage = ld;
				break;
			}
		}

		final List<IJavaProject> projects = new LinkedList<>();
		projects.add(project);
		final ProgramCodeToImplementationModelTask task = new ProgramCodeToImplementationModelTask(implementationLanguage, projects, true, true);
		task.schedule();
		task.join();

		assertTrue("Errors during translation. See error log / console output for details.", task.isOK());
		return task.getResultModelRoots();
	}

	private List<EObject> loadExpectedModel() {
		return loadModel(URI.createPlatformPluginURI(BUNDLE_NAME + "/src/test/resources/expected/1_code2cm.xmi", true));
	}

}
