/**
 */
package correspondece.impl;

import correspondece.CorrespondeceFactory;
import correspondece.CorrespondecePackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import source.SourcePackage;

import source.impl.SourcePackageImpl;

import targetLanguage.TargetLanguagePackage;

import targetLanguage.impl.TargetLanguagePackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CorrespondecePackageImpl extends EPackageImpl implements CorrespondecePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ctEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass afkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cd2DBEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass a2TEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see correspondece.CorrespondecePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CorrespondecePackageImpl() {
		super(eNS_URI, CorrespondeceFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link CorrespondecePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CorrespondecePackage init() {
		if (isInited) return (CorrespondecePackage)EPackage.Registry.INSTANCE.getEPackage(CorrespondecePackage.eNS_URI);

		// Obtain or create and register package
		CorrespondecePackageImpl theCorrespondecePackage = (CorrespondecePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CorrespondecePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CorrespondecePackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		SourcePackageImpl theSourcePackage = (SourcePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SourcePackage.eNS_URI) instanceof SourcePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SourcePackage.eNS_URI) : SourcePackage.eINSTANCE);
		TargetLanguagePackageImpl theTargetLanguagePackage = (TargetLanguagePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TargetLanguagePackage.eNS_URI) instanceof TargetLanguagePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TargetLanguagePackage.eNS_URI) : TargetLanguagePackage.eINSTANCE);

		// Create package meta-data objects
		theCorrespondecePackage.createPackageContents();
		theSourcePackage.createPackageContents();
		theTargetLanguagePackage.createPackageContents();

		// Initialize created meta-data
		theCorrespondecePackage.initializePackageContents();
		theSourcePackage.initializePackageContents();
		theTargetLanguagePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCorrespondecePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CorrespondecePackage.eNS_URI, theCorrespondecePackage);
		return theCorrespondecePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCT() {
		return ctEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCT_Class() {
		return (EReference)ctEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCT_Table() {
		return (EReference)ctEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAFK() {
		return afkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAFK_Ass() {
		return (EReference)afkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAFK_Fkey() {
		return (EReference)afkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAC() {
		return acEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAC_Atr() {
		return (EReference)acEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAC_Col() {
		return (EReference)acEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCD2DB() {
		return cd2DBEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCD2DB_Db2cd() {
		return (EReference)cd2DBEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCD2DB_Cd2db() {
		return (EReference)cd2DBEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getA2T() {
		return a2TEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getA2T_Ass() {
		return (EReference)a2TEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getA2T_Table() {
		return (EReference)a2TEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrespondeceFactory getCorrespondeceFactory() {
		return (CorrespondeceFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		ctEClass = createEClass(CT);
		createEReference(ctEClass, CT__CLASS);
		createEReference(ctEClass, CT__TABLE);

		afkEClass = createEClass(AFK);
		createEReference(afkEClass, AFK__ASS);
		createEReference(afkEClass, AFK__FKEY);

		acEClass = createEClass(AC);
		createEReference(acEClass, AC__ATR);
		createEReference(acEClass, AC__COL);

		cd2DBEClass = createEClass(CD2DB);
		createEReference(cd2DBEClass, CD2DB__DB2CD);
		createEReference(cd2DBEClass, CD2DB__CD2DB);

		a2TEClass = createEClass(A2T);
		createEReference(a2TEClass, A2T__ASS);
		createEReference(a2TEClass, A2T__TABLE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		SourcePackage theSourcePackage = (SourcePackage)EPackage.Registry.INSTANCE.getEPackage(SourcePackage.eNS_URI);
		TargetLanguagePackage theTargetLanguagePackage = (TargetLanguagePackage)EPackage.Registry.INSTANCE.getEPackage(TargetLanguagePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(ctEClass, correspondece.CT.class, "CT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCT_Class(), theSourcePackage.getClass_(), null, "class", null, 0, 1, correspondece.CT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCT_Table(), theTargetLanguagePackage.getTable(), null, "table", null, 0, 1, correspondece.CT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(afkEClass, correspondece.AFK.class, "AFK", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAFK_Ass(), theSourcePackage.getAssociation(), null, "ass", null, 0, 1, correspondece.AFK.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAFK_Fkey(), theTargetLanguagePackage.getFKey(), null, "fkey", null, 0, 1, correspondece.AFK.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(acEClass, correspondece.AC.class, "AC", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAC_Atr(), theSourcePackage.getAttribute(), null, "atr", null, 0, 1, correspondece.AC.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAC_Col(), theTargetLanguagePackage.getColumn(), null, "col", null, 0, 1, correspondece.AC.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cd2DBEClass, correspondece.CD2DB.class, "CD2DB", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCD2DB_Db2cd(), theSourcePackage.getClassDiagram(), null, "db2cd", null, 0, 1, correspondece.CD2DB.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCD2DB_Cd2db(), theTargetLanguagePackage.getDatabase(), null, "cd2db", null, 0, 1, correspondece.CD2DB.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(a2TEClass, correspondece.A2T.class, "A2T", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getA2T_Ass(), theSourcePackage.getAssociation(), null, "ass", null, 0, 1, correspondece.A2T.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getA2T_Table(), theTargetLanguagePackage.getTable(), null, "table", null, 0, 1, correspondece.A2T.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //CorrespondecePackageImpl
