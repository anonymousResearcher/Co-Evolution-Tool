package org.codeling.examples.resource_demand;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.LinkedList;

import org.apache.commons.io.FileUtils;
import org.codeling.examples.case_studies.CaseStudyCommons;
import org.codeling.examples.case_studies.Repeat;
import org.codeling.lang.jee7.JEE7LanguageDefinition;
import org.codeling.lang.uml.UMLLanguageDefinition;
import org.codeling.transformationmanager.ITransformationManager;
import org.codeling.utils.CodelingConfiguration;
import org.codeling.utils.CodelingException;
import org.codeling.utils.CodelingLogger;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.junit.After;
import org.junit.Test;

public abstract class AbstractResourceDemandTest {

	protected static int size;

	private final static Path BASE_PATH = Paths.get("/Users/mmueller/git/codeling/Examples/Resource_Demand/");
	private Path csvFile = BASE_PATH.resolve("resource_demand_test_results.csv");
	private Path resultModelsPath = BASE_PATH.resolve("results").resolve("result-models");

	private long start; // Nano seconds
	private long end; // Nano seconds
	private long uptimeAtStart; // Milli seconds
	private long uptimeAtEnd; // Milli seconds
	private long[] timeAtStart = new long[5];
	private static int currentRun = 0;
	private static LinkedList<IJavaProject> projects = new LinkedList<>();
	private static CodelingLogger log = new CodelingLogger(AbstractResourceDemandTest.class);

	private static Path targetDir;

	private static void replaceWithinFiles(String original, String replacement, Path... files) throws IOException {
		for (Path p : files) {
			Charset charset = StandardCharsets.UTF_8;
			String content = new String(Files.readAllBytes(p), charset);
			content = content.replaceAll(original, replacement);
			Files.write(p, content.getBytes(charset));
		}
	}

	@Test
	@Repeat(5)
	public void translate() throws CodelingException {
		uptimeAtStart = ManagementFactory.getRuntimeMXBean().getUptime();
		start = System.nanoTime();
		ITransformationManager.createInstance().extractModelFromCode(new JEE7LanguageDefinition(),
				new UMLLanguageDefinition(), null, projects);
		end = System.nanoTime();
		uptimeAtEnd = ManagementFactory.getRuntimeMXBean().getUptime();
	}

	public static void validatePreloadAndCreateTargetProject() throws IOException, CoreException, CodelingException {
		assert (false) : "Checking java assertion must not be switched on during these tests, because HenshinTGG will not work in this mode. Please do not use -ea as VM parameters";
		Path workspace = Paths.get(ResourcesPlugin.getWorkspace().getRoot().getLocation().toString());
		targetDir = workspace.resolve("resource-demand-project_" + size);
		// copy complete base project directory
		Path baseDir = BASE_PATH.resolve("resource-demand-project_2");

		log.info(MessageFormat.format("Creating {1} based on {0}", baseDir.toString(), targetDir.toString()));
		FileUtils.copyDirectory(baseDir.toFile(), targetDir.toFile());

		// Import and translate 2-sized project for pre-loading meta models and
		// translation files
		log.info(
				"Executing 2-sized translation before to ensure that the meta models and transformation files are cached.");
		IJavaProject twoSizedProject = null;
		if (size == 2)
			twoSizedProject = JavaCore.create(
					new CaseStudyCommons().importProject(baseDir.resolve(".project").toFile().getAbsolutePath()));
		else
			twoSizedProject = JavaCore.create(
					new CaseStudyCommons().copyImportProject(baseDir.resolve(".project").toFile().getAbsolutePath()));
		projects.addAll(Arrays.asList(twoSizedProject));
		ITransformationManager.createInstance().extractModelFromCode(new JEE7LanguageDefinition(),
				new UMLLanguageDefinition(), null, projects);
		projects.clear();

		// correct POM and .project files
		replaceWithinFiles("resource-demand-project_2", "resource-demand-project_" + size, targetDir.resolve("pom.xml"),
				targetDir.resolve(".project"));

		for (int i = 2; i < size + 1; i++) {
			// copy package with classes
			Path baseSourceFolder = baseDir.resolve("src").resolve("main").resolve("java");
			Path targetSourceFolder = targetDir.resolve("src").resolve("main").resolve("java");
			FileUtils.copyDirectory(baseSourceFolder.resolve("package1").toFile(),
					targetSourceFolder.resolve("package" + i).toFile());

			// rename class files
			Path originalProvider = targetSourceFolder.resolve("package" + i).resolve("Provider1.java");
			Path targetProvider = targetSourceFolder.resolve("package" + i).resolve("Provider" + i + ".java");
			if (targetProvider.toFile().exists())
				targetProvider.toFile().delete();
			FileUtils.moveFile(originalProvider.toFile(), targetProvider.toFile());

			Path originalRequirer = targetSourceFolder.resolve("package" + i).resolve("Requirer1.java");
			Path targetRequirer = targetSourceFolder.resolve("package" + i).resolve("Requirer" + i + ".java");
			if (targetRequirer.toFile().exists())
				targetRequirer.toFile().delete();
			FileUtils.moveFile(originalRequirer.toFile(), targetRequirer.toFile());

			// Change the files' content accordingly
			replaceWithinFiles("1", "" + i, targetProvider, targetRequirer);
		}

		IJavaProject mainProject = JavaCore
				.create(new CaseStudyCommons().importProject(targetDir.resolve(".project").toFile().getAbsolutePath()));
		projects.addAll(Arrays.asList(mainProject));
	}

	/**
	 * Backup former result files
	 */
	protected void backupResults() throws IOException {
		timeAtStart[currentRun] = System.currentTimeMillis();
		String prefix = "" + timeAtStart[currentRun] + "-" + size + "-" + (currentRun + 1) + "-";
		if (csvFile.toFile().exists())
			Files.move(csvFile, BASE_PATH.resolve("results").resolve(prefix + csvFile.getFileName()));
		if (resultModelsPath.toFile().exists())
			Files.move(resultModelsPath, BASE_PATH.resolve("results").resolve(prefix + resultModelsPath.getFileName()));
	}

	@After
	public void tearDown() throws IOException {
		// Write the results
		// Rename debug directory for keeping the results
		if (resultModelsPath.toFile().exists())
			FileUtils.deleteDirectory(resultModelsPath.toFile());
		Files.move(CodelingConfiguration.getTemporaryModelProjectPath(), resultModelsPath);

		// Write to results file
		LinkedList<String> lines = new LinkedList<>();
		if (!csvFile.toFile().exists()) {
			csvFile.toFile().createNewFile();
			lines.add("Size,Start,End,Duration,Uptime At Start,Uptime At End");
		}
		lines.add(String.format("%d,%d,%d,%d,%d,%d", size, start, end, end - start, uptimeAtStart, uptimeAtEnd));
		Files.write(csvFile, lines, Charset.forName("UTF-8"), StandardOpenOption.APPEND);

		backupResults();

		currentRun++; // Increase run counter
	}
}