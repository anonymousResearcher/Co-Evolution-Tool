package org.codeling.examples.case_studies;

import org.codeling.utils.CodelingLogger;
import org.eclipse.core.runtime.IProgressMonitor;

public class ConsoleProgressMonitor implements IProgressMonitor {

	CodelingLogger log;
	private String taskName;
	private int totalWork;

	public ConsoleProgressMonitor(Class<?> callingClass) {
		log = new CodelingLogger(callingClass);
	}

	@Override
	public void beginTask(String name, int totalWork) {
		this.totalWork = totalWork;
		log.info("Starting Task \"" + name + "\" (total work: " + totalWork + ")");
	}

	@Override
	public void done() {
		log.info("Finished Task \"" + taskName + "\"");
	}

	@Override
	public void internalWorked(double work) {
	}

	@Override
	public boolean isCanceled() {
		return false;
	}

	@Override
	public void setCanceled(boolean value) {
	}

	@Override
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	@Override
	public void subTask(String name) {
		log.info("Starting Sub Task \"" + name + "\"");
	}

	@Override
	public void worked(int work) {
		log.info("Some work finished in \"" + taskName + "\": " + work + " / " + totalWork);
	}

}
