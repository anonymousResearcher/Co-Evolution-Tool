package org.codeling.examples.case_studies;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.JDTUtils;
import org.codeling.lang.jee7.JEE7LanguageDefinition;
import org.codeling.lang.jee7.JEE7UMLLanguageDefinition;
import org.codeling.transformationmanager.ITransformationManager;
import org.codeling.utils.CodelingException;
import org.codeling.utils.Models;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IAnnotation;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Realization;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UMLPackage;
import org.junit.BeforeClass;
import org.junit.Test;

public class JACK3_UML_CaseStudy {

	private static IJavaProject coreProject;
	private static IJavaProject businessProject;
	private static IJavaProject webProject;
	private static LinkedList<IJavaProject> projects = new LinkedList<>();

	public static void importProjects() throws CoreException {
		CaseStudyCommons c = new CaseStudyCommons();
		IProject productProject = c.copyImportProject(c.getJACKPath());

		String jackCorePath = productProject.getLocation().append("jack3-core").append(".project").toString();
		coreProject = JavaCore.create(c.importProject(jackCorePath));
		String jackBusinessPath = productProject.getLocation().append("jack3-business").append(".project").toString();
		businessProject = JavaCore.create(c.importProject(jackBusinessPath));
		String jackWebPath = productProject.getLocation().append("jack3-webclient").append(".project").toString();
		webProject = JavaCore.create(c.importProject(jackWebPath));
		projects.addAll(Arrays.asList(coreProject, businessProject, webProject));
	}

	@BeforeClass
	public static void addNewComponentWithinCore() throws CoreException, CodelingException, IOException {
		importProjects();

		// Translate code to UML model
		JEE7LanguageDefinition implementationLanguage = new JEE7LanguageDefinition();
		JEE7UMLLanguageDefinition specificationLanguage = new JEE7UMLLanguageDefinition();
		long startToModel = System.nanoTime();
		List<EObject> modelElements = ITransformationManager.createInstance()
				.extractModelFromCode(implementationLanguage, specificationLanguage, null, projects);
		long endToModel = System.nanoTime();
		System.out.println("Required time for translation to model: " + (endToModel - startToModel) + " nano seconds.");

		// Verify UML model
		Model model = (Model) modelElements.get(0);
		assertNotNull(model);
		// assertEquals(3, model.getPackagedElements().size());
		Component jack3core = (Component) model.getPackagedElement("jack3-core");
		assertNotNull(jack3core);
		// assertEquals(30, jack3core.getPackagedElements().size());
		Component jack3webclient = (Component) model.getPackagedElement("jack3-webclient");
		assertNotNull(jack3webclient);

		Component loginViewComponent = null;
		Interface loginViewInterface = null;
		Realization loginViewRealization = null;
		Component myAccountViewComponent = null;
		Interface myAccountViewInterface = null;
		Realization myAccountViewRealization = null;
		for (PackageableElement e : jack3webclient.getPackagedElements()) {
			if ("LoginView".equals(e.getName())) {
				if (e instanceof Component) {
					loginViewComponent = (Component) e;
				} else if (e instanceof Interface) {
					loginViewInterface = (Interface) e;
				}
			}
			if ("MyAccountView".equals(e.getName())) {
				if (e instanceof Component) {
					myAccountViewComponent = (Component) e;
				} else if (e instanceof Interface) {
					myAccountViewInterface = (Interface) e;
				}
			}
			if (e instanceof Realization) {
				if (((Realization) e).getSupplier("LoginView") != null) {
					loginViewRealization = (Realization) e;
					continue;
				} else if (((Realization) e).getSupplier("MyAccountView") != null) {
					myAccountViewRealization = (Realization) e;
					continue;
				}
			}
			if (loginViewComponent != null && loginViewInterface != null && loginViewRealization != null
					&& myAccountViewComponent != null && myAccountViewInterface != null
					&& myAccountViewRealization != null)
				break;
		}

		Operation op = null;
		for (PackageableElement e : jack3core.getPackagedElements()) {
			if (e instanceof Interface && "CourseService".equals(e.getName())) {
				for (Operation o : ((Interface) e).getOperations()) {
					if ("getAllCoursesForUser".equals(o.getName())) {
						op = o;
						break;
					}
				}
			}
		}
		// assertNotNull(loginViewComponent);
		// assertNotNull(loginViewInterface);
		// assertNotNull(loginViewRealization);
		// assertNotNull(myAccountViewComponent);
		// assertNotNull(myAccountViewInterface);
		// assertNotNull(myAccountViewRealization);
		// assertNotNull(op);

		// Update UML model
		// New AdministrationService
		Component component = (Component) jack3core.createPackagedElement("AdministrationService",
				UMLPackage.eINSTANCE.getComponent());
		Interface iface = (Interface) jack3core.createPackagedElement("AdministrationService",
				UMLPackage.eINSTANCE.getInterface());
		Realization r = (Realization) jack3core.createPackagedElement(null, UMLPackage.eINSTANCE.getRealization());
		r.getSuppliers().add(iface);
		r.getClients().add(component);

		// Delete LoginView
		if (loginViewComponent != null && loginViewInterface != null && loginViewRealization != null) {
			loginViewRealization.destroy();
			loginViewInterface.destroy();
			loginViewComponent.destroy();
		}

		// Rename MyAccountView
		if (myAccountViewComponent != null && myAccountViewInterface != null && myAccountViewRealization != null) {
			myAccountViewComponent.setName("AccountDetailsView");
			myAccountViewInterface.setName("AccountDetailsView");
			myAccountViewRealization.setName("AccountDetailsView");
		}

		// Add Time Resoruce Demand
		if (op != null) {
			Comment comment = UMLFactory.eINSTANCE.createComment();
			comment.setBody("Time Resource Demand: [50ms]");
			op.getOwnedComments().add(comment);
		}

		Models.store(modelElements, "/architecture-carrying-code-temp/specification-model-changed.xmi");

		// Translate UML to code
		long startToCode = System.nanoTime();
		ITransformationManager.createInstance().storeModelToCode(modelElements, implementationLanguage,
				specificationLanguage, null, projects);
		long endToCode = System.nanoTime();
		System.out.println("Required time for translation to code: " + (endToCode - startToCode) + " nano seconds.");
	}

	@Test
	public void verifyNewElement() throws JavaModelException {
		// Verify changed code
		String packageName = "new_architecture_elements";
		String compilationUnitName = "AdministrationService.java";
		assertFalse("Addition created compilation unit in wrong project",
				compilationUnitExists(businessProject, packageName, compilationUnitName));
		assertFalse("Addition created compilation unit in wrong project",
				compilationUnitExists(webProject, packageName, compilationUnitName));
		assertTrue("Addition did not create new compilation unit",
				compilationUnitExists(coreProject, packageName, compilationUnitName));
	}

	@Test
	public void verifyDeletedElement() throws JavaModelException {
		assertFalse("Deletion did not work",
				compilationUnitExists(webProject, "de.uni_due.s3.jack3.beans", "LoginView.java"));
	}

	@Test
	public void verifyRenamedElement() throws JavaModelException {
		assertTrue("Change (renaming) did not work",
				compilationUnitExists(webProject, "de.uni_due.s3.jack3.beans", "AccountDetailsView.java"));
		assertFalse("Change (renaming) did not change the old file, but only created a new one",
				compilationUnitExists(webProject, "de.uni_due.s3.jack3.beans", "MyAccountView.java"));
	}

	@Test
	public void verifyAddedIALElement() throws NoSuchElementException, JavaModelException {
		IMethod method = getMethod(coreProject, "de.uni_due.s3.jack3.services", "CourseService.java",
				"getAllCoursesForUser", new String[] { "QUser;" });
		IAnnotation annotation = method.getAnnotation("TimeResourceDemand");
		assertTrue("Did not add time resource demand to the code.", annotation.exists());
		assertTrue("The time resource demand duration was not added to the code.",
				annotation.getSource().contains("50ms"));
	}

	private boolean compilationUnitExists(IJavaProject project, String packageName, String compilationUnitName)
			throws JavaModelException {
		IPackageFragment packageFragment = ASTUtils.getPackageFragment(packageName, Arrays.asList(project));
		if (packageFragment == null)
			return false;
		return packageFragment.getCompilationUnit(compilationUnitName).exists();
	}

	private IMethod getMethod(IJavaProject project, String packageName, String compilationUnitName, String methodName,
			String[] argumentTypes) throws NoSuchElementException, JavaModelException {
		IPackageFragment packageFragment = ASTUtils.getPackageFragment(packageName, Arrays.asList(project));
		ICompilationUnit cu = packageFragment.getCompilationUnit(compilationUnitName);
		return cu.getType(compilationUnitName.substring(0, compilationUnitName.length() - ".java".length()))
				.getMethod(methodName, argumentTypes);
	}

}
