package org.codeling.examples.resource_demand;

import java.io.IOException;

import org.codeling.examples.case_studies.ExtendedRunner;
import org.codeling.utils.CodelingException;
import org.eclipse.core.runtime.CoreException;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@RunWith(ExtendedRunner.class)
public class ResourceDemand_70_Test extends AbstractResourceDemandTest {
	@BeforeClass
	public static void setup() throws CoreException, IOException, CodelingException {
		AbstractResourceDemandTest.size = 70;
		validatePreloadAndCreateTargetProject();
	}
}
